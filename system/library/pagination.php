<?php
class Pagination {
	public $total = 0;
	public $page = 1;
	public $limit = 20;
	public $num_links = 8;
	public $url = '';
	public $text_first = '|&lt;';
	public $text_last = '&gt;|';
	public $text_next = '&gt;';
	public $text_prev = '&lt;';

	public function render() {
		$total = $this->total;

		if ($this->page < 1) {
			$page = 1;
		} else {
			$page = $this->page;
		}

		if (!(int)$this->limit) {
			$limit = 10;
		} else {
			$limit = $this->limit;
		}

		$num_links = $this->num_links;
		$num_pages = ceil($total / $limit);

		$this->url = str_replace('%7Bpage%7D', '{page}', $this->url);

		$output = '<div class="pagination prodcucts__pagination" aria-label="Page navigation">';

		if ($page > 1) {
			
			if ($page - 1 === 1) {
                $output .= '<a class="pagination__item  pagination__item--arrow" href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $this->url) . '"><svg class="icon icon-arrow " width="9px" height="15px"><use xlink:href="img/sprite.svg#arrow"></use></svg></a>
';
			} else {
				$output .= '<a class="pagination__item  pagination__item--arrow" href="' . str_replace('{page}', $page - 1, $this->url) . '"><svg class="icon icon-arrow " width="9px" height="15px"><use xlink:href="img/sprite.svg#arrow"></use></svg></a>
';
			}
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);

				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}

				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= '<a class="pagination__item  pagination__item--active">' . $i . '</a>';
				} else {
					if ($i === 1) {
                        $output .= '<a class="pagination__item" href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $this->url) . '">' . $i . '</a>';
					} else {
						$output .= '<a class="pagination__item" href="' . str_replace('{page}', $i, $this->url) . '">' . $i . '</a>';
					}
				}
			}
		}

		if ($page < $num_pages) {
			$output .= '<a class="pagination__item  pagination__item--arrow" href="' . str_replace('{page}', $page + 1, $this->url) . '"><svg class="icon icon-arrow " width="9px" height="15px"><use xlink:href="img/sprite.svg#arrow"></use></svg></a>';
		}

		$output .= '</div>';

		if ($num_pages > 1) {
			return $output;
		} else {
			return '';
		}
	}
}