<?php

$_['mod_affiliate_dopfun']                 = 'Оплата бонусами</br>Работает в авто режиме с модулем:
<br><a href="http://partnerkaprog.ru/partnerskie-programmy/dop-fun" target="_blank"><b>Доп. функции Партнерской программы</b></a></span></br>Для этого необходимо</br><b>Количество дней на обработку заказа</b></br>сделать больше 0 и включить </br><b>Перевод выплаты на баланс магазина</b>';
$_['mod_affiliate_trackingproduct']                 = '<br>Работает с модулем:
<br><a href="http://partnerkaprog.ru/new-fun/affiliate-tracking-product" target="_blank"><b>Товары с реф. ссылками в кабинете партнера</b></a></span>';
$_['text_bonus'] = 'Оплата бонусами</br>Для авторежима необходимо</br><b>Количество дней на обработку заказа</b></br>сделать больше 0 и включить </br><b>Перевод выплаты на баланс магазина</b>';
$_['text_qiwi']                 = 'QIWI Кошелек';
$_['text_card']                 = 'VISA/MasterCard';
$_['text_yandex']               = 'Яндекс.Деньги';
$_['text_webmoney_wmr']             = 'Webmoney WMR';
$_['text_webmoney_wmz']             = 'Webmoney WMZ';
$_['text_webmoney_wmu']             = 'Webmoney WMU';
$_['text_webmoney_wme']             = 'Webmoney WME';
$_['text_webmoney_wmy']             = 'Webmoney WMY';
$_['text_webmoney_wmb']             = 'Webmoney WMB';
$_['text_webmoney_wmg']             = 'Webmoney WMG';
$_['text_cheque']                 = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']             = 'Банковский перевод';
$_['text_alert_pay']             = 'AlertPay';
$_['text_moneybookers']         = 'Moneybookers';
$_['text_liqpay']               = 'LIQPAY';
$_['text_sage_pay']              = 'SagePay';
$_['text_two_checkout']          = '2Checkout';
$_['text_google_wallet']         = 'GoogleWallet';
$_['entry_alert_pay']            = 'AlertPay:';
$_['entry_moneybookers']        = 'Moneybookers:';
$_['entry_liqpay']              = 'Номер LIQPAY:';
$_['entry_sage_pay']             = 'SagePay:';
$_['entry_two_checkout']         = '2Checkout:';
$_['entry_google_wallet']        = 'GoogleWallet:';
$_['entry_order_status'] = 'Автоматическое зачисление комиссии при статусе: ';
$_['entry_total'] = 'Минимальная сумма для вывода';
$_['entry_payment'] = 'Выберите систему оплаты';
$_['entry_days']          = 'Количество дней на обработку заказа';
$_['entry_add']    = 'Авторизация партнера без подтверждения';
$_['entry_category_visible']    = 'Отображение категорий и товаров в Кабинете партнера "Реферальные ссылки"';
$_['entry_number_tracking']    = '"Реферальные код" как число <span class="help">1000+id_affiliate</span>';
$_['entry_affiliate_sumbol']    = '"Реферальные код" содержит знак <span class="help">Если по рефю ссылке страница не найдена</br> то меняем знак "?" или "&" </span>';

			
// Heading
$_['heading_title']    = 'Партнерская программа';

// Text
$_['text_extension']   = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
