<?php

$_['text_bonus'] = 'Payment of bonuses';
$_['text_qiwi']                 = 'QIWI Wallet';
$_['text_card']                 = 'VISA/MasterCard';
$_['text_yandex']               = 'Yandex.money';
$_['text_webmoney_wmr']             = 'Webmoney WMR';
$_['text_webmoney_wmz']             = 'Webmoney WMZ';
$_['text_webmoney_wmu']             = 'Webmoney WMU';
$_['text_webmoney_wme']             = 'Webmoney WME';
$_['text_webmoney_wmy']             = 'Webmoney WMY';
$_['text_webmoney_wmb']             = 'Webmoney WMB';
$_['text_webmoney_wmg']             = 'Webmoney WMG';
$_['entry_qiwi']                = 'Number QIWI Wallet:';
$_['entry_card']                = 'Number card VISA/MasterCard:';
$_['entry_yandex']              = 'Number Yandex.money:';
$_['entry_webmoney_wmr']            = 'Number Webmoney WMR:';
$_['entry_webmoney_wmz']            = 'Number Webmoney WMZ:';
$_['entry_webmoney_wmu']            = 'Number Webmoney WMU:';
$_['entry_webmoney_wme']            = 'Number Webmoney WME:';
$_['entry_webmoney_wmy']            = 'Number Webmoney WMY:';
$_['entry_webmoney_wmb']            = 'Number Webmoney WMB:';
$_['entry_webmoney_wmg']            = 'Number Webmoney WMG:';
$_['text_alert_pay']             = 'AlertPay';
$_['text_moneybookers']         = 'Moneybookers';
$_['text_liqpay']               = 'LIQPAY';
$_['text_sage_pay']              = 'SagePay';
$_['text_two_checkout']          = '2Checkout';
$_['text_google_wallet']         = 'GoogleWallet';
$_['entry_alert_pay']            = 'AlertPay:';
$_['entry_moneybookers']        = 'Moneybookers:';
$_['entry_liqpay']              = 'Number LIQPAY:';
$_['entry_sage_pay']             = 'SagePay:';
$_['entry_two_checkout']         = '2Checkout:';
$_['entry_google_wallet']        = 'GoogleWallet:';
$_['column_request_payment']    = 'Request payment';
$_['entry_payment_comment']     = 'Paid, the method of payment: ';
			
// Heading
$_['heading_title']             = 'Affiliates';

// Text
$_['text_success']              = 'Success: You have modified affiliates!';
$_['text_approved']             = 'You have approved %s accounts!';
$_['text_list']                 = 'Affiliate List';
$_['text_add']                  = 'Add Affiliate';
$_['text_edit']                 = 'Edit Affiliate';
$_['text_affiliate_detail']     = 'Affiliate Details';
$_['text_affiliate_address']    = 'Affiliate Address';
$_['text_balance']              = 'Balance';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';

// Column
$_['column_name']               = 'Affiliate Name';
$_['column_email']              = 'E-Mail';
$_['column_code']               = 'Tracking Code';
$_['column_balance']            = 'Balance';
$_['column_status']             = 'Status';
$_['column_approved']           = 'Approved';
$_['column_date_added']         = 'Date Added';
$_['column_description']        = 'Description';
$_['column_amount']             = 'Amount';
$_['column_action']             = 'Action';

// Entry
$_['entry_firstname']           = 'First Name';
$_['entry_lastname']            = 'Last Name';
$_['entry_email']               = 'E-Mail';
$_['entry_telephone']           = 'Telephone';
$_['entry_fax']                 = 'Fax';
$_['entry_status']              = 'Status';
$_['entry_password']            = 'Password';
$_['entry_confirm']             = 'Confirm';
$_['entry_company']             = 'Company';
$_['entry_website']             = 'Web Site';
$_['entry_address_1']           = 'Address 1';
$_['entry_address_2']           = 'Address 2';
$_['entry_city']                = 'City';
$_['entry_postcode']            = 'Postcode';
$_['entry_country']             = 'Country';
$_['entry_zone']                = 'Region / State';
$_['entry_code']                = 'Tracking Code';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Tax ID';
$_['entry_payment']             = 'Payment Method';
$_['entry_cheque']              = 'Cheque Payee Name';
$_['entry_paypal']              = 'PayPal Email Account';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';
$_['entry_amount']              = 'Amount';
$_['entry_description']         = 'Description';
$_['entry_name']                = 'Affiliate Name';
$_['entry_approved']            = 'Approved';
$_['entry_date_added']          = 'Date Added';

// Help
$_['help_code']                 = 'The tracking code that will be used to track referrals.';
$_['help_commission']           = 'Percentage the affiliate receives on each order.';

// Error
$_['error_warning']             = 'Warning: Please check the form carefully for errors!';
$_['error_permission']          = 'Warning: You do not have permission to modify affiliates!';
$_['error_exists']              = 'Warning: E-Mail Address is already registered!';
$_['error_firstname']           = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']            = 'Last Name must be between 1 and 32 characters!';
$_['error_email']               = 'E-Mail Address does not appear to be valid!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';
$_['error_telephone']           = 'Telephone must be between 3 and 32 characters!';
$_['error_password']            = 'Password must be between 4 and 20 characters!';
$_['error_confirm']             = 'Password and password confirmation do not match!';
$_['error_address_1']           = 'Address 1 must be between 3 and 128 characters!';
$_['error_city']                = 'City must be between 2 and 128 characters!';
$_['error_postcode']            = 'Postcode must be between 2 and 10 characters for this country!';
$_['error_country']             = 'Please select a country!';
$_['error_zone']                = 'Please select a region / state!';
$_['error_code']                = 'Tracking Code required!';
$_['error_code_exists']         = 'Tracking code is being used by another affiliate!';
