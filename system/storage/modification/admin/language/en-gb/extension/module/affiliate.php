<?php

$_['mod_affiliate_dopfun']                 = 'Оплата бонусами</br>Работает в авто режиме с модулем:
<br><a href="http://partnerkaprog.ru/partnerskie-programmy/dop-fun" target="_blank"><b>Доп. функции Партнерской программы</b></a></span></br>Для этого необходимо</br><b>Количество дней на обработку заказа</b></br>сделать больше 0 и включить </br><b>Перевод выплаты на баланс магазина</b>';
$_['mod_affiliate_trackingproduct']                 = '<br>Работает с модулем:
<br><a href="http://partnerkaprog.ru/new-fun/affiliate-tracking-product" target="_blank"><b>Товары с реф. ссылками в кабинете партнера</b></a></span>';
$_['text_bonus'] = 'Оплата бонусами</br>Для авторежима необходимо</br><b>Количество дней на обработку заказа</b></br>сделать больше 0 и включить </br><b>Перевод выплаты на баланс магазина</b>';
$_['text_qiwi']                 = 'QIWI Wallet';
$_['text_card']                 = 'VISA/MasterCard';
$_['text_yandex']               = 'Yandex.money';
$_['text_webmoney_wmr']             = 'Webmoney WMR';
$_['text_webmoney_wmz']             = 'Webmoney WMZ';
$_['text_webmoney_wmu']             = 'Webmoney WMU';
$_['text_webmoney_wme']             = 'Webmoney WME';
$_['text_webmoney_wmy']             = 'Webmoney WMY';
$_['text_webmoney_wmb']             = 'Webmoney WMB';
$_['text_webmoney_wmg']             = 'Webmoney WMG';
$_['text_alert_pay']             = 'AlertPay';
$_['text_moneybookers']         = 'Moneybookers';
$_['text_liqpay']               = 'LIQPAY';
$_['text_sage_pay']              = 'SagePay';
$_['text_two_checkout']          = '2Checkout';
$_['text_google_wallet']         = 'GoogleWallet';
$_['entry_alert_pay']            = 'AlertPay:';
$_['entry_moneybookers']        = 'Moneybookers:';
$_['entry_liqpay']              = 'Number LIQPAY:';
$_['entry_sage_pay']             = 'SagePay:';
$_['entry_two_checkout']         = '2Checkout:';
$_['entry_google_wallet']        = 'GoogleWallet:';
$_['text_cheque']                 = 'Check';
$_['text_paypal']               = 'PayPal';
$_['text_bank']             = 'Bank transfer';
$_['entry_order_status'] = 'Automatic enrollment status at the Commission: ';
$_['entry_total'] = 'Minimum Payout';
$_['entry_payment'] = 'Select a payment system';
$_['entry_days']          = 'Number of days to process orders';
$_['entry_add']    = 'Partner without your authorization';
$_['entry_category_visible']    = 'Showing categories and products in the Cabinet partner "Referral links"';
$_['entry_number_tracking']    = '"Referral code" to nomber<span class="help">1000+id_affiliate</span>';
$_['entry_affiliate_sumbol']    = '"Referral code" contains a sign <span class="help">If the ref. link page not found </br> then change the sign "?" or "&"</span>';
			
// Heading
$_['heading_title']    = 'Affiliate';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified affiliate module!';
$_['text_edit']        = 'Edit Affiliate Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify affiliate module!';