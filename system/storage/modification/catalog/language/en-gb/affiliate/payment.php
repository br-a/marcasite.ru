<?php

$_['text_bonus'] = 'Payment of bonuses';
$_['text_qiwi']                 = 'QIWI Wallet';
$_['text_card']                 = 'VISA/MasterCard';
$_['text_yandex']               = 'Yandex.money';
$_['text_webmoney_wmr']             = 'Webmoney WMR';
$_['text_webmoney_wmz']             = 'Webmoney WMZ';
$_['text_webmoney_wmu']             = 'Webmoney WMU';
$_['text_webmoney_wme']             = 'Webmoney WME';
$_['text_webmoney_wmy']             = 'Webmoney WMY';
$_['text_webmoney_wmb']             = 'Webmoney WMB';
$_['text_webmoney_wmg']             = 'Webmoney WMG';
$_['entry_qiwi']                = 'Number QIWI Wallet:';
$_['entry_card']                = 'Number card VISA/MasterCard:';
$_['entry_yandex']              = 'Number Yandex.money:';
$_['entry_webmoney_wmr']            = 'Number Webmoney WMR:';
$_['entry_webmoney_wmz']            = 'Number Webmoney WMZ:';
$_['entry_webmoney_wmu']            = 'Number Webmoney WMU:';
$_['entry_webmoney_wme']            = 'Number Webmoney WME:';
$_['entry_webmoney_wmy']            = 'Number Webmoney WMY:';
$_['entry_webmoney_wmb']            = 'Number Webmoney WMB:';
$_['entry_webmoney_wmg']            = 'Number Webmoney WMG:';
$_['text_alert_pay']             = 'AlertPay';
$_['text_moneybookers']         = 'Moneybookers';
$_['text_liqpay']               = 'LIQPAY';
$_['text_sage_pay']              = 'SagePay';
$_['text_two_checkout']          = '2Checkout';
$_['text_google_wallet']         = 'GoogleWallet';
$_['entry_alert_pay']            = 'AlertPay:';
$_['entry_moneybookers']        = 'Moneybookers:';
$_['entry_liqpay']              = 'Number LIQPAY:';
$_['entry_sage_pay']             = 'SagePay:';
$_['entry_two_checkout']         = '2Checkout:';
$_['entry_google_wallet']        = 'GoogleWallet:';
$_['title_qiwi']                = 'for example: 9179386644';
$_['title_card']                = 'for example: <br>0000-0000-0000-0000<br>0000-0000-0000-0<br>0000-0000-0000-0000-000';
$_['title_yandex']              = 'for example: 410011687527103';
$_['title_webmoney_wmr']            = 'for example: R853627838893';
$_['title_webmoney_wmz']            = 'for example: Z853627838893';
$_['title_webmoney_wmu']            = 'for example: U853627838893';
$_['title_webmoney_wme']            = 'for example: E853627838893';
$_['title_webmoney_wmy']            = 'for example: Y853627838893';
$_['title_webmoney_wmb']            = 'for example: B853627838893';
$_['title_webmoney_wmg']            = 'for example: G853627838893';
$_['error_qiwi']    = 'Please enter a 10 digit number';
$_['error_card']    = 'Please follow the format shown in the example';
$_['error_yandex']    = 'Please enter correct';
$_['error_webmoney_wmr']    = 'Please enter correct';
$_['error_webmoney_wmz']    = 'Please enter correct';
$_['error_webmoney_wmu']    = 'Please enter correct';
$_['error_webmoney_wme']    = 'Please enter correct';
$_['error_webmoney_wmy']    = 'Please enter correct';
$_['error_webmoney_wmb']    = 'Please enter correct';
$_['error_webmoney_wmg']    = 'Please enter correct';
			
// Heading
$_['heading_title']             = 'Payment Method';

// Text
$_['text_account']              = 'Account';
$_['text_payment']              = 'Payment';
$_['text_your_payment']         = 'Payment Information';
$_['text_your_password']        = 'Your Password';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';
$_['text_success']              = 'Success: Your account has been successfully updated.';

// Entry
$_['entry_tax']                 = 'Tax ID';
$_['entry_payment']             = 'Payment Method';
$_['entry_cheque']              = 'Cheque Payee Name';
$_['entry_paypal']              = 'PayPal Email Account';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';