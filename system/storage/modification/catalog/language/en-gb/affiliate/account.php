<?php

$_['text_orderpayment']    = 'Order payment';
$_['text_my_orderpayment'] = 'Order payment';
$_['text_statistics'] = 'View Statistics';
$_['text_my_statistics'] = 'My Statistics';
$_['text_balance'] = '<b>Get Balance: %s</b>';
$_['text_percentage'] = '<b>Get Commission: %s %s</b>';
$_['text_name_affiliate'] = '<b>%s %s</b>';
			
// Heading
$_['heading_title']        = 'My Affiliate Account';

// Text
$_['text_account']         = 'Account';
$_['text_my_account']      = 'My Affiliate Account';
$_['text_my_tracking']     = 'My Tracking Information';
$_['text_my_transactions'] = 'My Transactions';
$_['text_edit']            = 'Edit your account information';
$_['text_password']        = 'Change your password';
$_['text_payment']         = 'Change your payment preferences';
$_['text_tracking']        = 'Custom Affiliate Tracking Code';
$_['text_transaction']     = 'View your transaction history';