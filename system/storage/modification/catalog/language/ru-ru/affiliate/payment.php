<?php

$_['text_bonus'] = 'Оплата бонусами';
$_['text_qiwi']                 = 'QIWI Кошелек';
$_['text_card']                 = 'VISA/MasterCard';
$_['text_yandex']               = 'Яндекс.Деньги';
$_['text_webmoney_wmr']             = 'Webmoney WMR';
$_['text_webmoney_wmz']             = 'Webmoney WMZ';
$_['text_webmoney_wmu']             = 'Webmoney WMU';
$_['text_webmoney_wme']             = 'Webmoney WME';
$_['text_webmoney_wmy']             = 'Webmoney WMY';
$_['text_webmoney_wmb']             = 'Webmoney WMB';
$_['text_webmoney_wmg']             = 'Webmoney WMG';
$_['text_alert_pay']             = 'AlertPay';
$_['text_moneybookers']         = 'Moneybookers';
$_['text_liqpay']               = 'LIQPAY';
$_['text_sage_pay']              = 'SagePay';
$_['text_two_checkout']          = '2Checkout';
$_['text_google_wallet']         = 'GoogleWallet';
$_['entry_alert_pay']            = 'AlertPay:';
$_['entry_moneybookers']        = 'Moneybookers:';
$_['entry_liqpay']              = 'Номер LIQPAY:';
$_['entry_sage_pay']             = 'SagePay:';
$_['entry_two_checkout']         = '2Checkout:';
$_['entry_google_wallet']        = 'GoogleWallet:';
$_['entry_qiwi']                = 'Номер QIWI Кошелька:';
$_['entry_card']                = 'Номер карты VISA или MasterCard:';
$_['entry_yandex']              = 'Номер кошелька Яндекс.Деньги:';
$_['entry_webmoney_wmr']            = 'Номер кошелька WMR:';
$_['entry_webmoney_wmz']            = 'Номер кошелька WMZ:';
$_['entry_webmoney_wmu']            = 'Номер кошелька WMU:';
$_['entry_webmoney_wme']            = 'Номер кошелька WME:';
$_['entry_webmoney_wmy']            = 'Номер кошелька WMY:';
$_['entry_webmoney_wmb']            = 'Номер кошелька WMB:';
$_['entry_webmoney_wmg']            = 'Номер кошелька WMG:';
$_['title_qiwi']                = 'Например: 9179386644';
$_['title_card']                = 'Например: <br>0000-0000-0000-0000<br>0000-0000-0000-0<br>0000-0000-0000-0000-000';
$_['title_yandex']              = 'Например: 410011687527103';
$_['title_webmoney_wmr']            = 'Например: R853627838893';
$_['title_webmoney_wmz']            = 'Например: Z853627838893';
$_['title_webmoney_wmu']            = 'Например: U853627838893';
$_['title_webmoney_wme']            = 'Например: E853627838893';
$_['title_webmoney_wmy']            = 'Например: Y853627838893';
$_['title_webmoney_wmb']            = 'Например: B853627838893';
$_['title_webmoney_wmg']            = 'Например: G853627838893';
$_['error_qiwi']    = 'Пожалуйста введите 10 значный номер';
$_['error_card']    = 'Пожалуйста соблюдайте формат как показано в примере';
$_['error_yandex']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmr']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmz']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmu']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wme']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmy']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmb']    = 'Пожалуйста введите корректные значения';
$_['error_webmoney_wmg']    = 'Пожалуйста введите корректные значения';
			
// Heading
$_['heading_title']             = 'Способ оплаты';

// Text
$_['text_account']              = 'Кабинет партнера';
$_['text_payment']              = 'Оплата';
$_['text_your_payment']         = 'Ваши перечисления';
$_['text_your_password']        = 'Пароль';
$_['text_cheque']               = 'Чек';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Банковский перевод';
$_['text_success']              = 'Учетные данные партнера успешно обновлены.';

// Entry
$_['entry_tax']                 = 'Налоговый код:';
$_['entry_payment']             = 'Способ оплаты:';
$_['entry_cheque']              = 'Чек, Имя получателя платежа:';
$_['entry_paypal']              = 'PayPal Email аккаунт:';
$_['entry_bank_name']           = 'Название банка:';
$_['entry_bank_branch_number']  = 'ABA/BSB номер (номер отделения):';
$_['entry_bank_swift_code']     = 'SWIFT код:';
$_['entry_bank_account_name']   = 'Название счёта:';
$_['entry_bank_account_number'] = 'Номер счёта:';
