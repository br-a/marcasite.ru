<footer>
  <div class="container">
    <div class="row">
      <?php if ($informations) { ?>
      <div class="col-sm-3">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <hr>
    <p><?php echo $powered; ?></p>
  </div>
</footer>

        <?php if (isset($smca_status) || isset($smac_status)) { ?>
          <!-- start: OCdevWizard Setting -->
          <script type="text/javascript">     
            var ocdev_modules = [];
                  
            <?php if (isset($smca_status) && $smca_status == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_cart',
                type:'ajax'
              });
            <?php } ?>
            <?php if (isset($smac_status) && $smac_status == 1 && $smart_abandoned_cart == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_abandoned_cart',
                type:'ajax'
              });
            <?php } ?>
          </script>
          <!-- end: OCdevWizard Setting -->
        <?php } ?>
      

<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->

<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->


            <div id='updown' class="hidden-xs">
                <button id="up" class='updown'>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                </button>
                <button id="down" class='updown'>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
            </div>
<style type="text/css">
#updown {
	height: 48px;
    position: fixed;
    left: 50px;
    bottom: 25px;
    width: 4.2%;
    z-index: 1;
    background: #ddd;
    border-radius: 50%;
}
#updown > button {
	background-color: rgba(200, 200, 200, 0.05);
	color: rgb(150, 150, 150);
	opacity: 0.7;
	transition: all 0.3s ease-out 0s;
	border-radius: 0 !important;
}
#updown > button:hover {
	background-color: rgba(150, 150, 150, 0.2);
	opacity: 1;
}
.updown {
	border: 0 none;
	cursor: pointer;
	display: none;
	height: 100%;
	padding: 0;
	width: 100%;
}
.fa.fa-angle-up, .fa.fa-angle-down {
	font-size: 32px;
	font-weight: bold;
	transition: all 0.9s ease-out 0s;
	color: black;
}
</style>
<script type="text/javascript">
$(function(){
var pageYLabel = 0;

 $(window)
 .load(function() {
  var pageY = $(window).scrollTop();
 if (pageY > 200) {
  $("#up").show();
 }
 })
 .scroll(function(e){
  var pageY = $(window).scrollTop();
  var innerHeight = $(window).innerHeight();
  var docHeight = $(document).height();
 if (pageY > innerHeight) {
		 $("#up").show();
 }else{$("#up").hide();}

 if (pageY < innerHeight && pageYLabel >= innerHeight) {
		 $("#down").show();
 }else{$("#down").hide();}
 });

 $('#up').click(
 function() {
  var pageY = $(window).scrollTop();
  pageYLabel = pageY;
  $("html,body").animate({scrollTop:0},"slow");
 });
 $('#down').click(
 function(){
  $("html,body").animate({scrollTop:pageYLabel},"slow");
 });
});
</script>

</body></html>