<?php
$theme_options = $registry->get('theme_options');
$config = $registry->get('config');
$page_direction = $theme_options->get( 'page_direction' );

require_once( DIR_TEMPLATE.$config->get($config->get('config_theme') . '_directory')."/lib/module.php" );
$modules_old_opencart = new Modules($registry);
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" class="page  no-js" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detection" content="date=no">
  <meta name="format-detection" content="address=no">
  <meta name="format-detection" content="email=no">
  <meta content="notranslate" name="google">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="js/fix-script.js?6"></script>
<script src="js/bundle.js?6"></script>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/style.css?6">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>

<link rel="stylesheet" href="/blocks/details/style_discont.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js";, "ym");

   ym(84075019, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/84075019"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PWCL8WS');</script>
<!-- End Google Tag Manager -->

<script src="https://yastatic.net/share2/share.js"></script>
	<script src="/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<link href="/catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet">
	<link href="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen">
	<link href="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
	<link href="/catalog/view/theme/default/stylesheet/quickview.css" rel="stylesheet">
	<script type="text/javascript">
	var color_schem = '1';
	var loading_masked_img = '<img src="catalog/view/theme/default/image/ring-alt-'+ color_schem +'.svg" />';
	function loading_masked(action) {
		if (action) {
			$('.loading_masked').html(loading_masked_img);
			$('.loading_masked').show();
		} else {
			$('.loading_masked').html('');
			$('.loading_masked').hide();
		}
	}
	function creatOverlayLoadPage(action) {
		if (action) {
			$('#messageLoadPage').html(loading_masked_img);
			$('#messageLoadPage').show();
		} else {
			$('#messageLoadPage').html('');
			$('#messageLoadPage').hide();
		}
	}
	function quickview_open(id) {
	$('body').prepend('<div id="messageLoadPage"></div><div class="mfp-bg-quickview"></div>');
		$.ajax({
			type:'post',
			data:'quickviewpost=1',
			url:'index.php?route=product/product&product_id='+id,	
			beforeSend: function() {
				creatOverlayLoadPage(true); 
			},
			complete: function() {
				$('.mfp-bg-quickview').hide();
				$('#messageLoadPage').hide();
				creatOverlayLoadPage(false); 
			},	
			success:function (data) {
				$('.mfp-bg-quickview').hide();
				$data = $(data);
				var new_data = $data.find('#quickview-container').html();							
				$.magnificPopup.open({
					tLoading: loading_masked_img,
					items: {
						src: new_data,
					},
					type: 'inline'
				});
			}
	});							
	}



	</script>

				<?php if ($class == 'common-home') { ?>
				<script type="application/ld+json">
                {
                 "@context": "http://schema.org",
                 "@type": "WebSite",
                 "url": "<?php echo $base; ?>",
				 "name" : "<?php echo $store_name; ?>",
                 "potentialAction": {
                   "@type": "SearchAction",
                   "target": "<?php echo $base; ?>index.php?route=product/search&search={q}",
                   "query-input": "required name=q"
                 }
                }
                </script>
				<script type="application/ld+json">
                { "@context" : "http://schema.org",
                  "@type" : "Organization",
                  <?php if ($logo) { ?>
                  "logo" : "<?php echo $logo; ?>",
                  <?php } ?>
				  <?php if (!empty($support)) { ?>
                  "contactPoint" : [
                  { "@type" : "ContactPoint",
                    "telephone" : "<?php echo $support; ?>",
                    "contactType" : "customer service"
                  } ],
				  <?php } ?>
				  <?php if (!empty($social)) { ?>
				  "sameAs" : [<?php echo $social; ?>],
				  <?php } ?>
				  "url" : "<?php echo $base; ?>"
				}
				</script>
				<?php } ?>
                

				<script type="text/javascript">
$(document).ready(function() {
    colorListRollover();

	$('.color_items').hide();
	$('#color_options .color_option .preview-block').hide();
	$('.color_list').hover(function(){
		//on hover
		$(this).find('.color_items').fadeIn();
		$(this).find('.color_heading i').removeClass('fa-chevron-circle-down');
		$(this).find('.color_heading i').addClass('fa-chevron-circle-up');
	}, function() {
		//on unhover
		$(this).find('.color_items').fadeOut();
		$(this).find('.color_heading i').removeClass('fa-chevron-circle-up');
		$(this).find('.color_heading i').addClass('fa-chevron-circle-down');
	});
	$('#color_options div.color_option a').hover(function(){
			$this = $(this);
			$this.find('.preview-block').fadeIn();
		}, function(){
			//on unhover
			$this = $(this);
			$this.find('.preview-block').fadeOut();
		});
});

function colorListRollover() {
		$('.color_list .color-item a').hover(function(){
			//on hover
			$this = $(this);
			var hoverImage = $this.attr('rel');
			$this.parents('.newproduct').find('.image a img').attr('src', hoverImage);
		}, function(){
			//on unhover
			$this = $(this);
			var defaultImage = $this.attr('default-image');
			$this.parents('.newproduct').find('.image a img').attr('src', defaultImage);
		});
		
}
</script>

<!--BOF Product Series-->
			<style>	
				.pds a, .pds a:hover, .pds a:visited
				{
					text-decoration: none;
				}
			
				.pds a.preview
				{
					display: inline-block;
				}
				
				.pds a.preview.pds-current
				{
					border-bottom: 3px solid orange;
				}
				
				#preview{
					position: absolute;
					border: 1px solid #DBDEE1;
					background: #F8F8F8;
					padding: 5px;
					display: none;
					color: #333;
					z-index: 1000000;
				}
				.categories-nav__item--selected .categories-nav__link:first-child {
				    padding: 7px 12px;
                    border: 1px solid #00a3cf;
                    color: #00a3cf;
                    font-weight: 700;
                    display: flex;
                    align-items: center;
                    justify-content: space-around;
                    font-weight: 600;
                    /* font-size: 20px; */
                    text-transform: capitalize;
                    width: 137px;
                    margin-right: 45px;
				}
				.categories-nav__item--selected .categories-nav__link:first-child:hover {
				    color: #fff;
				}
				.categories-nav__item--selected .categories-nav__link:first-child:hover svg path {
				    fill: #fff;
				}
				@media (max-width: 767px) {
                    .categories-nav__item--selected .categories-nav__link:first-child {
                    display: none}
                }
			</style>
			<script type="text/javascript" src="catalog/view/javascript/imagepreview/imagepreview.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					pdsListRollover();
				});
				
				function pdsListRollover()
				{
					$('.pds a.pds-thumb-rollover').hover(function(){
						//on hover
						$this = $(this);
						var hoverImage = $this.attr('rel');
						$this.parents('.product-thumb').find('.image a img').attr('src', hoverImage);
					}, function(){
						//on unhover
						$this = $(this);
						var masterImage = $this.attr('master-image');
						$this.parents('.product-thumb').find('.image a img').attr('src', masterImage);
					});
				}
			</script>
			<!--EOF Product Series-->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWCL8WS";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="page__wrapper">
    <div class="page__content">
      <header class="header" role="banner">
        <div class="header__top">
          <div class="container header__container">
            <div class="header__wrapper">
              <nav class="main-nav header__main-nav">
                <ul class="main-nav__list">
                  <li class="main-nav__item"><a class="main-nav__link" href="/onas">О нас</a></li>
                  <li class="main-nav__item"><a class="main-nav__link" href="/dostavka">Доставка и оплата</a></li>
                  <li class="main-nav__item"><a class="main-nav__link" href="/contact-us/">Контакты</a></li>
                  <li class="main-nav__item main-nav__item--selected"><a class="main-nav__link" href="https://marcasite.su">Стать партнером</a></li>
                </ul>
              </nav>
              <div class="header__links"><a class="header__phone" href="tel:<?php echo $telephone; ?>"> <svg class="icon icon-phone " width="20px" height="20px">
                    <use xlink:href="img/sprite.svg#phone"></use>
                  </svg>7 (495) 730-51-14</a><a class="header__user" href="<?php echo $account; ?>"><svg class="icon icon-user " width="15px" height="17px">
                    <use xlink:href="img/sprite.svg#user"></use>
                  </svg>Кабинет</a></div>
            </div>
          </div>
        </div>
        <div class="header__main">
          <div class="container header__container">
            <div class="header__wrapper">
              <nav class="mobile-nav header__mobile-nav"><button class="burger mobile-nav__burger" aria-label="Toggle block undefined"><span>Toggle block undefined</span></button>
                <div class="mobile-nav__hidden">
                  <div class="mobile-nav__header"><a class="logo mobile-nav__logo" href="/"><img src="img/logo-white.svg" width="74px" height="40px" alt="Марказит" /></a><button class="close mobile-nav__close" type="button"><span></span></button></div>
                  <div class="mobile-nav__body">
                    <ul class="mobile-nav__list">
                      <li class="mobile-nav__item mobile-nav__item--has-child"><a class="mobile-nav__link" href="javascript:void(0);">Каталог товаров</a>
                        <ul class="mobile-nav__list mobile-nav__list--lvl-2">
                          <?php foreach ($categories as $category) { ?>
						  <li class="mobile-nav__item mobile-nav__item--lvl-2"><a class="mobile-nav__link mobile-nav__link--lvl-2" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                          <?php } ?>
                          <li class="mobile-nav__item mobile-nav__item--lvl-2 mobile-nav__item--selected"><a class="mobile-nav__link mobile-nav__link--lvl-2" href="/jewelry/gold/">Золото</a></li>
                        </ul>
                      </li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/onas">О нас</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/dostavka">Доставка и оплата</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/contact-us/">Контакты</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="https://marcasite.su">Стать партнером</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="<?php echo $account; ?>">Кабинет</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="<?php echo $wishlist; ?>">Избранное</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="tel:<?php echo $telephone; ?>">Тел. <?php echo $telephone; ?></a></li>
                    </ul>
                  </div>
                </div>
              </nav><a class="logo header__logo" href="/"><img src="img/logo.svg" width="130px" height="69px" alt="Марказит" /></a>
			  
			  <?php echo $search; ?>
			<div class="categories-nav__item categories-nav__item--selected"><a class="categories-nav__link" href="/jewelry/gold/">
			<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M15.0831 4C15.3055 4.00005 15.5227 4.06698 15.7065 4.1921C15.8903 4.31723 16.0323 4.49475 16.1139 4.7016L18.3705 10.423C18.5988 11.0027 18.6509 11.1135 18.7119 11.1989C18.7827 11.2973 18.8692 11.3834 18.9679 11.4538C19.0533 11.5159 19.1641 11.568 19.7438 11.7974L25.4652 14.0529C25.6719 14.1346 25.8493 14.2766 25.9743 14.4604C26.0993 14.6442 26.1662 14.8614 26.1662 15.0837C26.1662 15.306 26.0993 15.5232 25.9743 15.707C25.8493 15.8908 25.6719 16.0328 25.4652 16.1145L19.7438 18.3711C19.1641 18.5995 19.0533 18.6515 18.9679 18.7125C18.8695 18.7833 18.7834 18.8698 18.713 18.9685C18.6509 19.0539 18.5988 19.1647 18.3694 19.7444L16.1139 25.4658C16.0322 25.6725 15.8902 25.8499 15.7064 25.9749C15.5225 26.0999 15.3054 26.1668 15.0831 26.1668C14.8608 26.1668 14.6436 26.0999 14.4598 25.9749C14.276 25.8499 14.134 25.6725 14.0523 25.4658L11.7957 19.7444C11.5673 19.1647 11.5152 19.0539 11.4543 18.9685C11.3835 18.8701 11.297 18.784 11.1982 18.7136C11.1129 18.6515 11.0021 18.5995 10.4224 18.37L4.70098 16.1145C4.49424 16.0328 4.31685 15.8908 4.19185 15.707C4.06684 15.5232 4 15.306 4 15.0837C4 14.8614 4.06684 14.6442 4.19185 14.4604C4.31685 14.2766 4.49424 14.1346 4.70098 14.0529L10.4224 11.7963C11.0021 11.568 11.1129 11.5159 11.1982 11.4549C11.2967 11.3841 11.3828 11.2976 11.4532 11.1989C11.5152 11.1135 11.5673 11.0027 11.7968 10.423L14.0523 4.7016C14.1339 4.49475 14.2758 4.31723 14.4597 4.1921C14.6435 4.06698 14.8607 4.00005 15.0831 4ZM15.0831 8.12979L13.8583 11.2354L13.8229 11.3263C13.65 11.7652 13.497 12.1521 13.2565 12.4901C13.0442 12.7861 12.7847 13.0452 12.4884 13.2571C12.1514 13.4976 11.7646 13.6506 11.3246 13.8224L11.2359 13.859L8.12806 15.0837L11.2337 16.3085L11.3246 16.3439C11.7635 16.5168 12.1503 16.6698 12.4884 16.9103C12.7832 17.1231 13.0426 17.3825 13.2554 17.6784C13.4959 18.0153 13.6489 18.4022 13.8206 18.8422L13.8572 18.9309L15.0831 22.0387L16.3078 18.9331L16.3433 18.8422C16.5162 18.4033 16.6692 18.0165 16.9097 17.6784C17.1225 17.3836 17.3818 17.1242 17.6778 16.9114C18.0147 16.6709 18.4015 16.5179 18.8416 16.3461L18.9302 16.3096L22.0381 15.0837L18.9325 13.859L18.8416 13.8235C18.4027 13.6506 18.0158 13.4976 17.6778 13.2571C17.3818 13.0448 17.1227 12.7853 16.9108 12.489C16.6703 12.1521 16.5173 11.7652 16.3455 11.3252L16.3089 11.2365L15.0831 8.12979Z" fill="#00A3CF"/>
</svg>

			Золото
			</a></div>
              <div class="header__links"><a class="header__favorite" href="<?php echo $wishlist; ?>">

                  <div class="header__favorite-icon"><svg class="icon icon-heart " width="25px" height="20px">
                      <use xlink:href="img/sprite.svg#heart"></use>
                    </svg><span class="header__favorite-number" id="wishlist-total"><span><?php echo $text_wishlist; ?></span></span></div>Избранное
                </a>

			
				<a class="header__cart" href="/cart">
                  <div class="header__cart-icon"><svg class="icon icon-cart " width="23px" height="19px">
                      <use xlink:href="img/sprite.svg#cart"></use>
                    </svg><?php echo $cart; ?></div>Корзина
                </a> 
			</div>
              <nav class="categories-nav header__categories"><a class="categories-nav__title" href="javascript:void(0);"><svg class="icon icon-burger " width="20px" height="14px">
                    <use xlink:href="img/sprite.svg#burger"></use>
                  </svg>Каталог</a>
                <div class="categories-nav__dropdown">
                  <ul class="categories-nav__list">
                    <?php foreach ($categories as $category) { ?>
					<li class="categories-nav__item"><a class="categories-nav__link" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
					<?php } ?>
					<li class="categories-nav__item"><a class="categories-nav__link" href="/jewelry/mfp/6f-komplekty,комплекты">Комплекты</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
			<?php $slideshow = $modules_old_opencart->getModules('slideshow'); ?>
	<?php  if(count($slideshow)) { ?>
	<!-- Slider -->
	<div id="slider" style="margin-bottom:50px;" class="<?php if($theme_options->get( 'slideshow_layout' ) == 1) { echo 'full-width'; } elseif($theme_options->get( 'slideshow_layout' ) == 4) { echo 'fixed3 fixed2'; } elseif($theme_options->get( 'slideshow_layout' ) == 3) { echo 'fixed2'; } else { echo 'fixed'; } ?>">
		<div class="background-slider"></div>
		<div class="background">
			<div class="shadow"></div>
			<div class="pattern">
				<?php foreach($slideshow as $module) { ?>
				<?php echo $module; ?>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>

      </header>