<?php echo $header; 
$theme_options = $registry->get('theme_options');
$config = $registry->get('config'); 
$page_direction = $theme_options->get( 'page_direction' ); ?>


     <div class="container page__container">
          <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <div class="page__top">
          <h1 class="page-title page-title--line"><?php echo $heading_title; ?></h1>
		  <?php include('catalog/view/theme/' . $config->get($config->get('config_theme') . '_directory') . '/template/new_elements/wrapper_top.tpl'); ?>
        </div>
        <div class="page__cols">
          <main class="page__main">
            <section class="blog">


              <div class="categories-tabs__content categories-tabs__content--active" id="collection">
                <div class="blog__row">
                  
				  <?php
    include('catalog/view/theme/' . $config->get($config->get('config_theme') . '_directory') . '/template/blog/article_list/'. $settings['article_list_template']); 
    ?>


                </div>
		
              </div>

			  
            </section>
          </main>
          <sidebar class="sidebar page__sidebar">
		  
		  <?php include('catalog/view/theme/' . $config->get($config->get('config_theme') . '_directory') . '/template/new_elements/wrapper_bottom.tpl'); ?>



          </sidebar>
        </div>
      </div>
	  
	  
<?php echo $hyper_positions_bottom_content; ?>
<?php echo $footer; ?>