﻿<?php echo $header; ?>

<div class="container page__container">
  <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
    <?php foreach ($breadcrumbs as $i=>
    $breadcrumb) { ?>
    <?php if($i+1<count($breadcrumbs)) { ?>
    <li class="breadcrumbs__item">
      <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    </li>
    <?php } else { ?>
    <li class="breadcrumbs__item">
      <span><?php echo $breadcrumb['text']; ?></span>
    </li>
    <?php } ?>
    <?php } ?>
  </ul>

  <h1 class="page-title"><?php echo $heading_title; ?></h1>

  <main class="page__main">
    <div class="catalog">
      <div class="catalog__wrapper">
        <?php echo $column_left; ?>

        <section class="products catalog__products" id="filtermega">
          <?php if ($products) { ?>
          <div class="products__options">
            <a class="products__filters-link" href="javascript:void(0);"
              ><svg class="icon icon-filters" width="22px" height="22px">
                <use xlink:href="img/sprite.svg#filters"></use></svg
              >Фильтр</a
            >
            <div class="products__sort">
              <div class="field-select">
                <div class="field-select__name">Сортировать:</div>
                <div class="field-select__select-wrap">
                  <select
                    id="input-sort"
                    class="field-select__select"
                    onchange="location = this.value;"
                  >
                    <?php foreach ($sorts as $sorts) { ?>
                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                    <option value="<?php echo $sorts['href']; ?>" selected="selected">
                      <?php echo $sorts['text']; ?>
                    </option>
                    <?php } else { ?>
                    <option value="<?php echo $sorts['href']; ?>">
                      <?php echo $sorts['text']; ?>
                    </option>
                    <?php } ?>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="products__row products__row--4">
            <?php foreach ($products as $product) { ?>
            <div class="item-product">
              <a class="item-product__body" href="<?php echo $product['href']; ?>">
                <div class="item-product__img">
                  <img
                    src="<?php echo $product['thumb']; ?>"
                    alt="<?php echo $product['name']; ?>"
                  />
                </div>
                <div class="item-product__info">
                  <? if($product['special']) {
                    echo "<span class='label item-product__label inner'> -". ceil((($product['price'] - $product['special']) * 100) / $product['price'])  . "%</span> ";
                  }
                  ?>
                  <div class="item-product__name"><?php echo $product['name']; ?></div>
                  <div class="item-product__prices">
                    <?php if (!$product['special']) { ?>

                    <div class="item-product__price">
                      <span><?php echo $product['price']; ?> </span>руб
                    </div>

                    <? } else { ?>
                    <div class="card-main__prices" style="margin-bottom: 0">
                      <div class="item-product__price">
                        <span><?php echo $product['special']; ?> </span>руб
                      </div>
                      <div class="item-product__price">
                        <span class="mobile_price_sale"> <?php echo $product['price'] ?>руб </span>
                      </div>
                    </div>
                    <? } ?>
                  </div>
                </div>
              </a>
              <?php if($config_on_off_category_page_quickview =='1'){?>
              <div class="quickview">
                <a
                  class="btn btn-quickview"
                  onclick="quickview_open(<?php echo $product['product_id']?>);"
                  ><i class="fa fa-external-link fa-fw"></i
                  ><?php echo $config_quickview_btn_name[$lang_id]['config_quickview_btn_name']; ?></a
                >
              </div>
              <?php } ?>
            </div>
            <?php } ?>
          </div>

          <div class="products__footer">
            <div class="1234" style="display: none"><?php var_dump($pagination) ?></div>
            <?php echo $pagination; ?>

            <span class="products__pagination-text"><?php echo $results; ?></span>
          </div>

          <?php } ?>
          <?php if (!$categories && !$products) { ?>
          <p><?php echo $text_empty; ?></p>
          <?php } ?>
        </section>
      </div>
    </div>
  </main>

  <section class="seo">
    <div class="seo__text">
      <?php if ($description) { ?>
      <?php echo $description; ?>
      <?php } else { ?>
      <p>Описание категории не указано</p>
      <?php } ?>
    </div>
  </section>
</div>

<script type="application/ld+json">
  {
  "@context": "http://schema.org",
              "@type": "BreadcrumbList",
              "itemListElement":
              [
  <?php $home = array_shift($breadcrumbs); ?>
  {
              "@type": "ListItem",
              "position": 1,
              "item":
              {
                "@id": "<?php echo $base; ?>",
                "name": "<?php echo $store_name; ?>"
              }
  },
  <?php for($i = 0; $i < count($breadcrumbs); ++$i) {
  if ( strpos($breadcrumbs[$i]['href'], '?route=') == false ) {
     $breadcrumb_url = explode("?", $breadcrumbs[$i]['href']);
  } else { $breadcrumb_url = explode("&", $breadcrumbs[$i]['href']); }
  ?>
              {
              "@type": "ListItem",
              "position": <?php echo $i+2; ?>,
              "item":
              {
                "@id": "<?php echo $breadcrumb_url[0]; ?>",
                "name": "<?php echo $breadcrumbs[$i]['text']; ?>"
              }
              }<?php echo($i !== (count($breadcrumbs)-1) ? ',' : ''); ?>
              <?php } ?>
  ]
  }
</script>

<?php echo $footer; ?>
