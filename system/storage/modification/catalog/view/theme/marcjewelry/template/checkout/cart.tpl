<?php echo $header; ?>
<?php echo $hyper_positions_top_content; ?>
  <div class="container page__container">
   
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <h1 class="page-title">Корзина</h1>
       
	   <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
		<main class="page__main">
          <div class="cart">
            <div class="cart__items">
              <?php foreach ($products as $product) { ?>
			  <div class="item-cart cart__item"> 
			  
			  <a class="item-cart__remove" style="cursor: pointer;" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><svg class="icon icon-trash " width="30px" height="30px">
                    <use xlink:href="img/sprite.svg#trash"></use>
                  </svg></a>
				  
				  <a class="item-cart__img" href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                <div class="item-cart__info">
                  <div class="item-cart__col">
                    <div class="item-cart__name"><?php echo $product['name']; ?></div>
                  </div>
                  <?php if ($product['option']) { ?>
				  
				  <?php foreach ($product['option'] as $option) { ?>
				  <div class="item-cart__col"><span class="item-cart__info-title"><?php echo $option['name']; ?>: </span>
				  <?php } ?>
				  
				  <?php foreach ($product['option'] as $option) { ?>
				  <span class="item-cart__info-text"><?php echo $option['value']; ?></span></div>
				  <?php } ?>
				  
				  <?php } ?>
				  
                  <div class="item-cart__col"><span class="item-cart__info-title">Вес: </span><span class="item-cart__info-text"><?php echo $product['weight']; ?></span></div>

									<div class="item-cart__col"><span class="item-cart__price"> <span><?php echo $product['quantity']; ?> </span>шт</span></div>
				  
                  <div class="item-cart__col"><span class="item-cart__price"> <span><?php echo $product['total']; ?> </span>руб</span></div>
                </div>
              </div>
			  <?php } ?>

              
            </div>
            <div class="footer-cart cart__footer">
              <?php foreach ($totals as $total) { ?>
			  <div class="footer-cart__item">
                <div class="footer-cart__title"><?php echo $total['title']; ?>:</div>
                <div class="footer-cart__sale"> <span><?php echo $total['text']; ?> </span>руб</div>
              </div>
			  <?php } ?>
  
              <div class="footer-cart__btns"> 
			  <a class="btn  btn--white footer-cart__shopping" href="<?php echo $continue; ?>">Продолжить покупки</a>
			  <a class="btn footer-cart__order" href="/simplecheckout/">Оформить заказ</a></div>
            </div>
          </div>
        </main>
		</form>
		
       <?php echo $content_bottom; ?>
	
      </div>
<?php echo $hyper_positions_bottom_content; ?>
<?php echo $footer; ?>