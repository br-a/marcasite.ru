<?php echo $header; ?>
      <div class="container page__container">
           <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <h1 class="page-title page-title--line">Контакты</h1>
        <main class="page__main">
          <section class="contacts">
            <div class="contacts__row">
              <div class="feedback contacts__feedback">
                <div class="feedback__body">
                  <h3 class="feedback__subtitle">Форма обратной связи</h3>
				  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form feedback__form" data-check-form="true">
                    <div class="form__row">

                        <label class="field-text">
                            <span class="field-text__input-wrap">
                                <input type="text" name="name" class="field-text__input" value="<?php echo $name; ?>"  data-check-pattern="^[a-zа-яё]+.+" id="input-name"  />
                                <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                                <span class="field-text__help-text">Имя</span>
                            </span>
                        </label>

                        <label class="field-text">
                            <span class="field-text__input-wrap">
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="field-text__input" data-check-pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"/>
                                <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                                <span class="field-text__help-text">E-Mail</span>
                            </span>
                        </label>

                    </div>

                      <label class="field-text label-number">
                          <span class="field-text__input-wrap">
                              <input type="text" name="number" id="input-number" class="field-text__input"/>
                              <?php if ($error_number) { ?>
                              <div class="text-danger"><?php echo $error_number; ?></div>
                              <?php } ?>
                              <span class="field-text__help-text">Телефон</span>
                          </span>
                      </label>

                      <label class="field-text textarea-enquiry">
                          <span class="field-text__input-wrap">
                              <textarea name="enquiry" id="input-enquiry" class="field-text__input"></textarea>
                              <?php if ($error_enquiry) { ?>
                              <div class="text-danger"><?php echo $error_enquiry; ?></div>
                              <?php } ?>
                              <span class="field-text__help-text">Текст сообщения</span>
                          </span>
                      </label>
                      <button type="submit" class="btn form__btn">Отправить</button>


                  </form>
                </div>
              </div>
			  
              <div class="contacts__info">
                <div class="contacts__info-body">
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Наш адрес</h3>
                    <div class="contacts__text">
                      <p><?php echo $address; ?></p>
                    </div>
                  </div>
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Время работы</h3>
                    <div class="contacts__text">
                      <p><?php echo $open; ?></p>
                    </div>
                  </div>
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Телефон</h3>
                    <div class="contacts__text"><?php echo $telephone; ?></div>
                  </div>
                </div>
              </div>

                <div class="contacts__map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2239.536226262758!2d37.56628757799874!3d55.853362373121705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b522f6e34ec7dd%3A0x74857e2db701d807!2z0JvQuNGF0L7QsdC-0YDRgdC60LjQuQ!5e0!3m2!1sru!2sru!4v1689694318390!5m2!1sru!2sru" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></div>
            </div>
          </section>
        </main>
      </div>

				<script type="application/ld+json">
				{
				"@context": "http://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement":
                [
				<?php $home = array_shift($breadcrumbs); ?>
				{
                "@type": "ListItem",
                "position": 1,
                "item":
                {
                  "@id": "<?php echo $base; ?>",
                  "name": "<?php echo $store_name; ?>"
                }
				},
				<?php for($i = 0; $i < count($breadcrumbs); ++$i) { 
				if ( strpos($breadcrumbs[$i]['href'], '?route=') == false ) {
				   $breadcrumb_url = explode("?", $breadcrumbs[$i]['href']);
				} else { $breadcrumb_url = explode("&", $breadcrumbs[$i]['href']); }
				?>
                {
                "@type": "ListItem",
                "position": <?php echo $i+2; ?>,
                "item":
                {
                  "@id": "<?php echo $breadcrumb_url[0]; ?>",
                  "name": "<?php echo $breadcrumbs[$i]['text']; ?>"
                }
                }<?php echo($i !== (count($breadcrumbs)-1) ? ',' : ''); ?>
                <?php } ?>
				]
				}
<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/7.1.3/imask.min.js" integrity="sha512-54QZ6V8x2x5v5+8JNy98/rP4Ui3CSNlv8KLVS5tTQJCx/Ag4jgUlVUqe54TJ3QLJYSVwt5xPbINvxX7A9XHdHQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<?php echo $footer; ?>

