<?php echo $header; ?>

   <div class="container page__container">
      <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
		<span id="notify"></span>
        <main class="page__main">
          <div class="card">
            <section class="card-main card__main-info">
              <div class="card-main__cols">
                <div class="card-main__left">
				
                  <div class="card-gallery card-main__gallery">
					
                    <div class="card-gallery__thumbs js-card-thumbs">
                     <div class="card-gallery__img"><a class="thumbnail" data-fancybox="gallery" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $popup; ?>" alt="<?php echo $heading_title; ?>"></a></div>
                     <?php foreach ($images as $image) { ?>
					 <div class="card-gallery__img"><a class="thumbnail" data-fancybox="gallery video" href="<?php echo $image['video'] ? $image['video'] : $image['popup'] ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $image['popup']; ?>" alt="<?php echo $heading_title; ?>"></a></div>
					 <?php } ?>
                    </div>
					
                    <div class="card-gallery__main js-card-slider">
                      <div class="card-gallery__img"><a class="thumbnail" data-fancybox="gallery" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $popup; ?>" alt="<?php echo $heading_title; ?>"></a></div>
                     <?php foreach ($images as $image) { ?>
					 <div class="card-gallery__img"><a class="thumbnail" data-fancybox="gallery" 
            href="<?php echo $image['video'] ? $image['video'] : $image['popup'] ?> 
             ">
            <img src="<?php echo $image['popup']; ?>" alt="<?php echo $heading_title; ?>">
          </a></div>
					 <?php } ?>
                    </div>
					
                  </div>

	<?php if(sizeof($pds) > 0) { ?>
          <div class="card-main__subtitle"><?php echo $text_in_the_same_series; ?></div>
                  <div class="card-another card-main__another">
                    <div class="card-another__items">
                      <?php foreach ($pds as $p) { ?>
					  <div class="card-another__item"><a class="card-another__img" href="<?php echo $p['product_link']; ?>"><img src="<?php echo $p['product_pds_image']; ?>" alt="image"></a></div>
                      <?php } ?>
                    </div>
                  </div>
	<?php } ?>
				  

			
			
                </div>
                <div class="card-main__right" id="product">
                  <div class="card-main__info">
                    <h1 class="page-title card-main__title"><?php echo $heading_title; ?></h1>

<?php if(!empty($colors)){ ?>
<div class="card-set card-main__set">
                      <div class="card-set__items">
					  
					  <?php foreach($colors as $color){ ?>
					  <?php if($color['product_id'] == $product_id) { ?>
                        <div class="card-set__item card-set__item--active">
                          <div class="card-set__body">
                            <div class="card-set__img"><img src="<?php echo $color['preview_photo']?>" alt="image"></div>
                          </div>
                        </div>
					<?php } else {  ?>
                        <div class="card-set__item">
                          <div class="card-set__body">
                            <div class="card-set__img"><img src="<?php echo $color['preview_photo']?>" alt="image"></div><a class="card-set__info" href="<?php echo $color['href'] ?>">
                              <div class="card-set__img"><img src="<?php echo $color['preview_photo']?>" alt="image"></div>
                              <div class="card-set__name"><?php echo $color['color_name'] ?></div>
                              <div class="card-set__price"> <span><?php echo "от " . $color['price'] . " ₽"?> </span>руб</div>
                            </a>
                          </div>
                        </div>
					<?php } ?>	
					<?php } ?>	

                      </div>
					  
				  <?php if($complect_price_special){ ?>
					<div class="card-set__total-price">Цена за комплект <span><?php echo $complect_price_special; ?> </span>руб</div>
				 <?php }else{ ?>
					<div class="card-set__total-price">Цена за комплект <span><?php echo $complect_price; ?> </span>руб</div>
 					<?php } ?>
					  
</div>
<?php } ?>
				
                  
					
					
                    <div class="card-main__info-group">
                      <div class="card-main__weight">Вес изделия <span><?php echo $weight; ?> </span>грамм</div><a class="card-main__choice-size"  target="_blank" href="/razmer">Подобрать размер</a>
                    </div>
					
					<?php if ($options) { ?>
                    <?php foreach ($options as $option) { ?>
					<div class="card-main__sizes" id="input-option<?php echo $option['product_option_id']; ?>">
                      <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <?php
                             if ($option_value['special'] == false){
                                $special = false;
                             }
                            ?>
					  <div class="field-radio  field-radio--size">
					  <label class="field-radio__name">
					  <input type="radio" class="field-radio__input" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" data-points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" data-prefix="<?php echo $option_value['price_prefix']; ?>" data-price="<?php echo $option_value['price_value']; ?>" />
					  <span class="field-radio__name-text"><?php echo $option_value['name']; ?></span>
					  </label>
					  </div>
                      <?php } ?>
                    </div> 
					<?php } ?>
					<?php } ?>
					
                    <div class="card-main__descr">
                      <p><?php echo $heading_title; ?></p>
                    </div>
                    
					<?php if (!$special) { ?>
					<div class="card-main__prices">
                      <div class="card-main__price"> <span><?php echo $price; ?>руб</div>
                    </div>
					<?php } else { ?>
					<div class="card-main__prices">
                      <div class="card-main__price"> <span><?php echo $special; ?> </span>руб</div>
                      <span class="123" style="display: none;"><?php echo $price_value; ?></span>
                      <div class="card-main__old-price"><span><?php echo $price; ?> </span>руб</div>
                      <span class="123" style="display: none;"><?php echo $special_price_start; ?></span>
                    </div>
					<?php } ?>

					
<div class="card-main__btns">
    <?if ($price_value > 0 && $quantity > 0){ ?>

					<input type="text" name="quantity" style="display: none;" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
          <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
					<a class="btn card-main__btn" id="button-cart">В корзину</a>
          <div class="card-main__btns-group">
						<a class="card-main__favorite" style="cursor: pointer;" onclick="wishlist.add('<?php echo $product_id; ?>');"><svg class="icon icon-heart " width="22px" height="22px"><use xlink:href="img/sprite.svg#heart"></use></svg></a>
						<span class="card-main__share"><svg class="icon icon-share " width="22px" height="22px"><use xlink:href="img/sprite.svg#share"></use></svg></span>						
						<div class="ya-share2" data-curtain data-color-scheme="whiteblack" data-services="vkontakte,facebook,odnoklassniki,telegram,twitter,viber,whatsapp,moimir"></div>
					</div>
    <?php } ?>
</div>
					
					
				<?php if($quickviewmodule) { ?>
					<div id="quickview-my"><?php echo $quickviewmodule;?></div>
				<?php } ?>
				
                  </div>
                </div>
              </div>
            </section>
            <section class="card-descr card__detailed-descr">
              <div class="card-descr__cols">
                <div class="card-descr__left">
                  <div class="tabs">
                    <ul class="tabs__links" role="tablist">
                      <li class="tabs__link-wrap   tabs__link-wrap--active" role="presentation"><a class="tabs__link" href="#characteristics" data-toggle="tab" role="tab">Характеристики</a></li>
                      <li class="tabs__link-wrap" role="presentation"><a class="tabs__link" href="#guarantee" data-toggle="tab" role="tab">Гарантии</a></li>
                      <li class="tabs__link-wrap" role="presentation"><a class="tabs__link" href="#gift" data-toggle="tab" role="tab">Подарок</a></li>
                    </ul>
                    <div class="tabs__content-wrapper">
                      <div class="tabs__content-item   tabs__content-item--active" id="characteristics" role="tabpanel">
                        <div class="card-descr__info">
                          <div class="card-descr__list card-descr__list--col-2">
                            <?php foreach ($attribute_groups as $attribute_group) { ?>
							<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
							<div class="card-descr__item"><span class="card-descr__title"><?php echo $attribute['name']; ?>: </span><span class="card-descr__text"><?php echo $attribute['text']; ?></span></div>
							<?php } ?>
							<?php } ?>


                          </div>
                        </div>
                      </div>
                      <div class="tabs__content-item" id="guarantee" role="tabpanel">
                        <div class="card-descr__info">
						<iframe frameborder="0" src="//www.youtube.com/embed/pN-gZjXb3TQ" width="100%" height="360" class="note-video-clip"></iframe>
                          <p><b>Возврат денег</b></p>
<p>Все ювелирные украшения, продающиеся в России, имеют специальную пробу, которая наносится на пробирной инспекции РФ.На каждом украшении есть бирка, которая является паспортом украшения. На ней указаны все характеристики украшения – проба,
вес, вставка и т.д. Бирка может состоять из нескольких частей. Если серебряное украшение не
соответствует информации на бирке, мы вернём покупателю его деньги. За всё время нашей работы таких
прецедентов не было.</p>

<p><b>Гарантия высокого качества (6  месяцев)</b></p>
<p>Если вы приобрели ювелирную продукцию в интернет-магазине «Марказит» и  если вы обнаружили заводской брак такой как (выпавшая вставка, сломанный замок, трещина в изделии) мы производим ремонт. При невозможности починить изделие, мы возвращаем покупателю деньги</p>
или производим обмен (по желанию). Такой механизм мы предлагаем для удобства наших
покупателей.</p>
<p>Обязательно!!! Бирка должна быть прикреплена на изделии, либо товар не подлежит возврату
Рекомендуем Вам сохранять чек, как доказательство покупки товара в нашем интернет-
магазине.</p>

<p><b>Обмен товара</b></p>
<p>В течение 7 дней после покупки, мы обменяем украшение, если оно не понравилось
(не подошло). Для этого должны быть соблюдены условия – сохранность бирки и
отсутствие следов носки. Изделия с полудрагоценными камнями подлежат экспертизе.
Только после этого будет обмен. Доставка товара в обе стороны за Ваш счет.</p>

<p><b>Забота о покупателях</b></p>
<p>Мы ценим каждого нашего клиента и заботимся о его интересах. Мы понимаем, что
покупателям нужен исключительно лишь качественный товар и приятное обслуживание.
Интернет-магазин ювелирных украшений «Марказит» гарантирует надежность
продукции и профессионализм сервиса обслуживания.</p>
                        </div>
                      </div>
                      <div class="tabs__content-item" id="gift" role="tabpanel">
                        <div class="card-descr__info">
<p>При покупке на любую сумму дарим подарок - салфетку, пропитанную специальным составом, которая позволит Вам легко и эффективно очистить грязь и налет, а также придать изделию бриллиантовый блеск!</p>
<p>В комплект к каждому изделию прилагается подарочная упаковка!</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-descr__right">
                  <div class="card-advantages card-descr__advantages">
                    <div class="card-advantages__row">
                      <div class="card-advantages__item">
                        <div class="card-advantages__icon"><svg class="icon icon-free-shipping " width="46px" height="46px">
                            <use xlink:href="img/sprite.svg#free-shipping"></use>
                          </svg></div>
                        <div class="card-advantages__text">Бесплатная доставка<br>по России от 10000 руб.</div>
                      </div>
                      <div class="card-advantages__item">
                        <div class="card-advantages__icon"><svg class="icon icon-card-security " width="46px" height="46px">
                            <use xlink:href="img/sprite.svg#card-security"></use>
                          </svg></div>
                        <div class="card-advantages__text">Безопасная оплата (Visa, <br>Mastercard, Наличными)</div>
                      </div>
                      <div class="card-advantages__item">
                        <div class="card-advantages__icon"><svg class="icon icon-try-and-buy " width="46px" height="46px">
                            <use xlink:href="img/sprite.svg#try-and-buy"></use>
                          </svg></div>
                        <div class="card-advantages__text">Примерка на дому<br>в любом городе РФ</div>
                      </div>
                      <div class="card-advantages__item">
                        <div class="card-advantages__icon"><svg class="icon icon-gift " width="46px" height="46px">
                            <use xlink:href="img/sprite.svg#gift"></use>
                          </svg></div>
                        <div class="card-advantages__text">Подарки при покупке<br>на любую сумму!</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
		  <?php echo $content_top; ?>
          
        </main>
		
		
       <?php echo $content_bottom; ?>
	   
      </div>
	  
	  
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('#notify').after('<div class="alert alert-success">' + json['success'] + '</div>');
        dataLayer.push({'event': 'add_to_cart'});
        console.log('add to cart');
        // добавление в корзину
				$('#cart').html('<span id="cart-total">' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});

});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);
						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
    grecaptcha.reset();
});

$(document).ready(function() {

		$('.card-main__share').on('click', function () {
		$(this).toggleClass('active');
		$('.ya-share2_inited').toggleClass('active');		
	});

	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});

$(document).ready(function() {
	var hash = window.location.hash;
	if (hash) {
		var hashpart = hash.split('#');
		var  vals = hashpart[1].split('-');
		for (i=0; i<vals.length; i++) {
			$('#product').find('select option[value="'+vals[i]+'"]').attr('selected', true).trigger('select');
			$('#product').find('input[type="radio"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
			$('#product').find('input[type="checkbox"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
		}
	}
})
//--></script>



<script type="text/javascript"><!--
function price_format(price)
{ 
    c = <?php echo (empty($autocalc_currency['decimals']) ? "0" : $autocalc_currency['decimals'] ); ?>;
    d = '<?php echo $autocalc_currency['decimal_point']; ?>'; // decimal separator
    t = '<?php echo $autocalc_currency['thousand_point']; ?>'; // thousands separator
    s_left = '<?php echo str_replace("'", "\'", $autocalc_currency['symbol_left']); ?>';
    s_right = '<?php echo str_replace("'", "\'", $autocalc_currency['symbol_right']); ?>';
    n = price * <?php echo $autocalc_currency['value']; ?>;
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 
    j = ((j = i.length) > 3) ? j % 3 : 0; 
    price_text = s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right; 
    
    <?php if (!empty($autocalc_currency2)) { ?>
    c = <?php echo (empty($autocalc_currency2['decimals']) ? "0" : $autocalc_currency2['decimals'] ); ?>;
    d = '<?php echo $autocalc_currency2['decimal_point']; ?>'; // decimal separator
    t = '<?php echo $autocalc_currency2['thousand_point']; ?>'; // thousands separator
    s_left = '<?php echo str_replace("'", "\'", $autocalc_currency2['symbol_left']); ?>';
    s_right = '<?php echo str_replace("'", "\'", $autocalc_currency2['symbol_right']); ?>';
    n = price * <?php echo $autocalc_currency2['value']; ?>;
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 
    j = ((j = i.length) > 3) ? j % 3 : 0; 
    price_text += '  <span class="currency2">(' + s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right + ')</span>'; 
    <?php } ?>
    
    return price_text;
}

function calculate_tax(price)
{
    <?php // Process Tax Rates
      if (isset($tax_rates) && $tax) {
         foreach ($tax_rates as $tax_rate) {
           if ($tax_rate['type'] == 'F') {
             echo 'price += '.$tax_rate['rate'].';';
           } elseif ($tax_rate['type'] == 'P') {
             echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
           }
         }
      }
    ?>
    return price;
}

function process_discounts(price, quantity)
{
    <?php
      foreach ($dicounts_unf as $discount) {
        echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
      }
    ?>
    return price;
}


animate_delay = 20;

main_price_final = calculate_tax(<?php echo $price_value; ?>);
main_price_start = calculate_tax(<?php echo $price_value; ?>);
main_step = 0;
main_timeout_id = 0;

function animateMainPrice_callback() {
    // main_price_start += main_step;
    
    // if ((main_step > 0) && (main_price_start > main_price_final)){
    //     main_price_start = main_price_final;
    // } else if ((main_step < 0) && (main_price_start < main_price_final)) {
    //     main_price_start = main_price_final;
    // } else if (main_step == 0) {
    //     main_price_start = main_price_final;
    // }
    
    // $('.autocalc-product-price').html( price_format(main_price_start) );
    
    // if (main_price_start != main_price_final) {
    //     main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
    // }
}

function animateMainPrice(price) {
    main_price_start = main_price_final;
    main_price_final = price;
    main_step = (main_price_final - main_price_start) / 10;
    
    clearTimeout(main_timeout_id);
    main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
}


<?php if ($special) { ?>
special_price_final = calculate_tax(<?php echo $special_value; ?>);
special_price_start = calculate_tax(<?php echo $special_value; ?>);
special_step = 0;
special_timeout_id = 0;

function animateSpecialPrice_callback() {
    special_price_start += special_step;
    
    if ((special_step > 0) && (special_price_start > special_price_final)){
        special_price_start = special_price_final;
    } else if ((special_step < 0) && (special_price_start < special_price_final)) {
        special_price_start = special_price_final;
    } else if (special_step == 0) {
        special_price_start = special_price_final;
    }
    
    // $('.autocalc-product-special').html( price_format(special_price_start) );
    
    if (special_price_start != special_price_final) {
        special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
    }
}

function animateSpecialPrice(price) {
    special_price_start = special_price_final;
    special_price_final = price;
    special_step = (special_price_final - special_price_start) / 10;
    
    clearTimeout(special_timeout_id);
    special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
}
<?php } ?>


function recalculateprice()
{
    var main_price = <?php echo (float)$price_value; ?>;
    var input_quantity = Number($('input[name="quantity"]').val());
    var special = <?php echo (float)$special_value; ?>;
    var tax = 0;
    var selected = [];
    var discount_coefficient = 1;
    
    if (isNaN(input_quantity)) input_quantity = 0;
    
    <?php if ($special) { ?>
        special_coefficient = <?php echo ((float)$price_value/(float)$special_value); ?>;
    <?php } else { ?>
        <?php if (empty($autocalc_option_discount)) { ?>
            main_price = process_discounts(main_price, input_quantity);
            tax = process_discounts(tax, input_quantity);
        <?php } else { ?>
            if (main_price) discount_coefficient = process_discounts(main_price, input_quantity) / main_price;
        <?php } ?>
    <?php } ?>
    
    
    var option_price = 0;
    
    <?php if ($points) { ?>
      var points = <?php echo (float)$points_value; ?>;
      $('input:checked,option:selected').each(function() {
          if ($(this).data('points')) points += Number($(this).data('points'));
      });
      $('.autocalc-product-points').html(points);
    <?php } ?>
    
    $('input:checked,option:selected').each(function() {
      if ($(this).data('prefix') == '=') {
        option_price += Number($(this).data('price'));
        main_price = 0;
        special = 0;
      }
      if ($(this).val()) selected.push($(this).val());
    });

    <?php if ($autocalc_hash_url) { ?>
    if (selected.length) window.location.hash = selected.join('-'); else window.history.pushState(null,null,(window.location.href).split('#')[0]);
    <?php } ?>
    
    $('input:checked,option:selected').each(function() {
      if ($(this).data('prefix') == '+') {
        option_price += Number($(this).data('price'));
      }
      if ($(this).data('prefix') == '-') {
        option_price -= Number($(this).data('price'));
      }
      if ($(this).data('prefix') == 'u') {
        pcnt = 1.0 + (Number($(this).data('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).data('prefix') == 'd') {
        pcnt = 1.0 - (Number($(this).data('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).data('prefix') == '*') {
        option_price *= Number($(this).data('price'));
        main_price *= Number($(this).data('price'));
        special *= Number($(this).data('price'));
      }
      if ($(this).data('prefix') == '/') {
        option_price /= Number($(this).data('price'));
        main_price /= Number($(this).data('price'));
        special /= Number($(this).data('price'));
      }
    });
    
    special += option_price;
    main_price += option_price;

    <?php if ($special) { ?>
      <?php if (empty($autocalc_option_special))  { ?>
        main_price = special * special_coefficient;
      <?php } else { ?>
        special = main_price / special_coefficient;
      <?php } ?>
      tax = special;
    <?php } else { ?>
      <?php if (!empty($autocalc_option_discount)) { ?>
          main_price *= discount_coefficient;
      <?php } ?>
      tax = main_price;
    <?php } ?>
    
    // Process TAX.
    main_price = calculate_tax(main_price);
    special = calculate_tax(special);
    
    <?php if (!$autocalc_not_mul_qty) { ?>
    if (input_quantity > 0) {
      main_price *= input_quantity;
      special *= input_quantity;
      tax *= input_quantity;
    }
    <?php } ?>

    // Display Main Price
    animateMainPrice(main_price);
      
    <?php if ($special) { ?>
      animateSpecialPrice(special);
    <?php } ?>
}

$(document).ready(function() {
    $('input[type="checkbox"]').bind('change', function() { recalculateprice(); });
    $('input[type="radio"]').bind('change', function() { recalculateprice(); });
    $('select').bind('change', function() { recalculateprice(); });
    
    $quantity = $('input[name="quantity"]');
    $quantity.data('val', $quantity.val());
    (function() {
        if ($quantity.val() != $quantity.data('val')){
            $quantity.data('val',$quantity.val());
            recalculateprice();
        }
        setTimeout(arguments.callee, 250);
    })();

    <?php if ($autocalc_select_first) { ?>
    $('select[name^="option"] option[value=""]').remove();
    last_name = '';
    $('input[type="radio"][name^="option"]').each(function(){
        if ($(this).attr('name') != last_name) $(this).prop('checked', true);
        last_name = $(this).attr('name');
    });
    <?php } ?>
    
    <?php if ($autocalc_hash_url) { ?>
    var hash = (window.location.hash);
    if (hash) {
        hash = hash.split('#')[1].split('-');
        hash.forEach(function(item){
            $('option[value="'+item+'"]').prop('selected', true);
            $('input[type="radio"][value="'+item+'"],input[type="checkbox"][value="'+item+'"]').prop('checked', true);
        });
    }
    <?php } ?>
    
    recalculateprice();
});

//--></script>	


				<script type="application/ld+json">
				{
				"@context": "http://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement":
                [
				<?php $home = array_shift($breadcrumbs); ?>
				{
                "@type": "ListItem",
                "position": 1,
                "item":
                {
                  "@id": "<?php echo $base; ?>",
                  "name": "<?php echo $store_name; ?>"
                }
				},
				<?php for($i = 0; $i < count($breadcrumbs); ++$i) { 
				if ( strpos($breadcrumbs[$i]['href'], '?route=') == false ) {
				   $breadcrumb_url = explode("?", $breadcrumbs[$i]['href']);
				} else { $breadcrumb_url = explode("&", $breadcrumbs[$i]['href']); }
				?>
                {
                "@type": "ListItem",
                "position": <?php echo $i+2; ?>,
                "item":
                {
                  "@id": "<?php echo $breadcrumb_url[0]; ?>",
                  "name": "<?php echo $breadcrumbs[$i]['text']; ?>"
                }
                }<?php echo($i !== (count($breadcrumbs)-1) ? ',' : ''); ?>
                <?php } ?>
				]
				}
				</script>
                

                <?php if($video_status){  ?>
                    <script type="text/javascript">
                       jQuery('a[data-video]:not([data-video=""])').each(function(index,element) {
                            jQuery(this).attr('href', $(this).attr('data-video'));
                            jQuery(this).attr('target','_blank');
                            jQuery(this).css({'display':'block','background-repeat':'no-repeat','background-position':'center center', 'background-size': '100%', 'background-image': 'url("/image/play.png")'}).find('img').css({ opacity: 0.6 });
                        });
                    
                        jQuery('a[data-video][data-video_role="video_main"]:not([data-video=""])').css({ 'background-image': 'none' });

                        jQuery('a[data-video]').magnificPopup({
                                    type: 'iframe',
                                    mainClass: 'mfp-fade',
                                    removalDelay: 160,
                                    preloader: false,
                                    fixedContentPos: false,
                                    iframe: {
                                      patterns: {
                                          youtube: {
                                              index: 'youtube.com/', 
                                              id: function(url) {        
                                                  var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                                                  if ( !m || !m[1] ) return null;
                                                  return m[1];
                                              },
                                              src: '//www.youtube.com/embed/%id%?autoplay=1'
                                          },
                                          vimeo: {
                                              index: 'vimeo.com/', 
                                              id: function(url) {        
                                                  var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
                                                  if ( !m || !m[5] ) return null;
                                                  return m[5];
                                              },
                                              src: '//player.vimeo.com/video/%id%?autoplay=1'
                                          }
                                      }
                                  }
                        });
                        jQuery('a[data-video]:not([data-video=""]').click(function () {
                            return false;
                        });
                    </script>
                <?php } ?>
            
<?php echo $footer; ?>