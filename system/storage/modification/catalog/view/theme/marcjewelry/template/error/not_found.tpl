<?php echo $header; ?>
<?php echo $hyper_positions_top_content; ?>
      <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
        <h1 class="page-title page-title--line"><?php echo $heading_title; ?></h1>
        <main class="page__main">
		
          <section class="privacy-policy">
           <?php echo $text_error; ?>
          </section>
        </main>
      </div>
<?php echo $hyper_positions_bottom_content; ?>
<?php echo $footer; ?>