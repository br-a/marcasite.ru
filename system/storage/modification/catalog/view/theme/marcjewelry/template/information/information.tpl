<?php echo $header; ?>
      <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
        <h1 class="page-title page-title--line"><?php echo $heading_title; ?></h1>
        <main class="page__main">
		
          <section <?php if ($_SERVER['REQUEST_URI']=="/dostavka.html") { ?>class="delivery"<?php } else { ?><?php if ($_SERVER['REQUEST_URI']=="/rekvizity.html") { ?>class="requisites"<?php } else { ?>class="privacy-policy"<?php } ?><?php } ?>>
            <?php echo $description; ?>
          </section>
        </main>
      </div>

				<script type="application/ld+json">
				{
				"@context": "http://schema.org",
                "@type": "BreadcrumbList",
                "itemListElement":
                [
				<?php $home = array_shift($breadcrumbs); ?>
				{
                "@type": "ListItem",
                "position": 1,
                "item":
                {
                  "@id": "<?php echo $base; ?>",
                  "name": "<?php echo $store_name; ?>"
                }
				},
				<?php for($i = 0; $i < count($breadcrumbs); ++$i) { 
				if ( strpos($breadcrumbs[$i]['href'], '?route=') == false ) {
				   $breadcrumb_url = explode("?", $breadcrumbs[$i]['href']);
				} else { $breadcrumb_url = explode("&", $breadcrumbs[$i]['href']); }
				?>
                {
                "@type": "ListItem",
                "position": <?php echo $i+2; ?>,
                "item":
                {
                  "@id": "<?php echo $breadcrumb_url[0]; ?>",
                  "name": "<?php echo $breadcrumbs[$i]['text']; ?>"
                }
                }<?php echo($i !== (count($breadcrumbs)-1) ? ',' : ''); ?>
                <?php } ?>
				]
				}
				</script>
                
<?php echo $footer; ?>