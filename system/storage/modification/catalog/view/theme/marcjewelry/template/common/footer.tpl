</div>
<div class="page__footer-wrapper">
      <footer class="footer" role="contentinfo">
        <div class="container footer__container">
          <div class="footer__wrapper">
            <nav class="footer-nav footer-nav--wide footer__group">
              <div class="footer-title footer__nav-title">Меню</div>
              <ul class="footer-nav__list footer-nav__list--col-3">
                <li class="footer-nav__item"><a class="footer-nav__link" href="/onas">О нас</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/blog">Блог</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/vystavki">Выставки</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/rekvizity">Реквизиты</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/contact-us">Наши контакты</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/dostavka">Доставка и оплата</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/publichnaya-oferta">Публичная оферта</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/sitemap">Карта сайта</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/vakansiy">Вакансии</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/specials">Акции</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="https://marcasite.su/">Партнерская программа</a></li>
              </ul>
            </nav>
            <nav class="footer-nav footer-nav--narrow footer__group">
              <div class="footer-title footer__nav-title">Каталог</div>
              <ul class="footer-nav__list footer-nav__list--col-2">
			    <?php foreach ($categories as $category) { ?>
                <li class="footer-nav__item"><a class="footer-nav__link" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>

              </ul>
            </nav>
            <div class="footer__info"><a class="logo footer__logo" href="javascript:void(0);"><img src="img/logo-white.svg" width="161px" height="86px" alt="Марказит" /></a>
              <div class="footer__info-group">
                <div class="footer-title footer__info-title">Всегда с вами</div>
                <div class="footer__social">
                  <div class="footer__social-text">Добро пожаловать в наши<br/>группы в соцсетях:</div>
                  <ul class="social footer__social-list">
                    <!-- <li class="social__item" title="instagram"><a class="social__link" href="https://www.instagram.com"><svg class="icon icon-instagram " width="31px" height="32px">
                          <use xlink:href="img/sprite.svg#instagram"></use>
                        </svg></a></li> -->
                    <!-- <li class="social__item" title="facebook"><a class="social__link" href="https://www.facebook.com"><svg class="icon icon-facebook " width="34px" height="35px">
                          <use xlink:href="img/sprite.svg#facebook"></use>
                        </svg></a></li> -->
                        <li class="social__item" title="vk"><a class="social__link" >
                              <img src="../img/vk.svg" alt="">
                            </a>
                          </li>
                            <li class="social__item" title="tg"><a class="social__link" >
                              <img src="../img/telega (1).svg" alt="">
                            </a>
                              </li>
                    <li class="social__item" title="youtube"><a class="social__link" href="https://www.youtube.com/c/%D0%9C%D0%B0%D1%80%D0%BA%D0%B0%D0%B7%D0%B8%D1%82%D0%BE%D1%84%D0%B8%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D0%B9%D0%BA%D0%B0%D0%BD%D0%B0%D0%BB"><svg class="icon icon-youtube " width="36px" height="27px">
                          <use xlink:href="img/sprite.svg#youtube"></use>
                        </svg></a></li>
                  </ul>
                </div><a class="footer__write" href="/contact-us/">Написать руководству</a>
              </div>
            </div>
            <div class="footer__bottom">
              <div class="footer__copyright">2010-2023 Markazit. <br/>Все права защищены.</div>
              <div class="footer__bottom-group"><a class="footer__privacy-policy" href="/politika-konfidentsialnosti">Политика конфиденциальности</a><a class="footer__develop"
                </a></div>
                <div class="footer__copyright">Техническая поддержка сайта - <a href="https://blackriver.agency/" target="_blank">Black River</a></div>
            </div>
          </div>
        </div>
      </footer>

        <?php if (isset($smca_status) || isset($smac_status)) { ?>
          <!-- start: OCdevWizard Setting -->
          <script type="text/javascript">     
            var ocdev_modules = [];
                  
            <?php if (isset($smca_status) && $smca_status == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_cart',
                type:'ajax'
              });
            <?php } ?>
            <?php if (isset($smac_status) && $smac_status == 1 && $smart_abandoned_cart == 1) { ?>
              ocdev_modules.push({
                src:  'index.php?route=ocdevwizard/smart_abandoned_cart',
                type:'ajax'
              });
            <?php } ?>
          </script>
          <!-- end: OCdevWizard Setting -->
        <?php } ?>
      
    </div>
  </div>
<!-- Yandex.Metrika counter -->

<script src='/js/jquery.zoom.min.js'></script>
<script  type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

   ym(47357799, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        ecommerce:"dataLayer"
   });
   $(function () { $(".card-gallery__main .card-gallery__img a.thumbnail").zoom(); });
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/47357799" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
 


            <div id='updown' class="hidden-xs">
                <button id="up" class='updown'>
                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                    <img  src="/img/button-up.png">
                </button>
                <button id="down" class='updown'>
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                    <img  src="/img/button-down.png">
                </button>
            </div>

<script type="text/javascript">
$(function(){
var pageYLabel = 0;

 $(window)
 .load(function() {
  var pageY = $(window).scrollTop();
 if (pageY > 200) {
  $("#up").show();
 }
 })
 .scroll(function(e){
  var pageY = $(window).scrollTop();
  var innerHeight = $(window).innerHeight();
  var docHeight = $(document).height();
 if (pageY > innerHeight) {
		 $("#up").show();
 }else{$("#up").hide();}

 if (pageY < innerHeight && pageYLabel >= innerHeight) {
		 $("#down").show();
 }else{$("#down").hide();}
 });

 $('#up').click(
 function() {
  var pageY = $(window).scrollTop();
  pageYLabel = pageY;
  $("html,body").animate({scrollTop:0},"slow");
 });
 $('#down').click(
 function(){
  $("html,body").animate({scrollTop:pageYLabel},"slow");
 });
});
</script>
        
</body>

</html>