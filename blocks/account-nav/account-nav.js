
$(function() {

    // account-nav
    $('.account-nav__select').on('click', function(){
        if($(window).width() < 991) {
            $(this).toggleClass('account-nav__select--active');
            $(this).next('.account-nav__list').slideToggle(0);
        }
    })

    $(document).on('click', function(event) {
        if($(window).width() < 991) {
            if( $(event.target).closest('.account-nav__select').length) 
                return;
                $('.account-nav__select').removeClass('account-nav__select--active');
                $('.account-nav__list').slideUp(0);
        }
    })
});
