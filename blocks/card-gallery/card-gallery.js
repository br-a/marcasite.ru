
const slick = require('slick-carousel');

$(function() {

    // card-gallery
    $('.js-card-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.js-card-thumbs',
        arrows: false,
    });
    $('.js-card-thumbs').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.js-card-slider',
        dots: false,
        arrows: false,
        centerMode: false,
        focusOnSelect: true,
        vertical: true,
        verticalSwiping: true,
        responsive: [
            {
                breakpoint: 1440,
                settings: {
                    vertical: false,
                    verticalSwiping: false,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    variableWidth: false,
                    vertical: true,
                    verticalSwiping: true,
                }
            },
            {
                breakpoint: 560,
                settings: {
                    slidesToShow: 5,
                    vertical: false,
                    verticalSwiping: false,
                }
            },
        ]
    });
});
