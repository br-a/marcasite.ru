
const slick = require('slick-carousel');

$(function() {

    // products slider
    $('.js-products-slider').each(function(){
        var slider = $(this)
        slider.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            responsive: [
                {
                    breakpoint: 1366,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 560,
                    settings: {
                        slidesToShow: 2,
                    }
                },
            ]
        });
    });

    // products show more
    $('.products__btn-more').on('click', function(){
        $('.item-product:nth-child(n + 11').fadeIn();
    })
});
