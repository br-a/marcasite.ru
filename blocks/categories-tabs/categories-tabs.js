
$(function() {
    // categories-tabs
    $('.categories-tabs__link').on('click', function(event){
        event.preventDefault();
        var el = $(this).attr('href');
        $(this).closest('.categories-tabs').find('.categories-tabs__item').removeClass('categories-tabs__item--active');
        $(this).closest('.categories-tabs__item').addClass('categories-tabs__item--active');
        if ($('.categories-tabs').length > 1) {
            $(this).closest('.products').find('.categories-tabs__content').removeClass('categories-tabs__content--active');
        } else {
            $('.categories-tabs__content').removeClass('categories-tabs__content--active');
        }
        $(el).addClass('categories-tabs__content--active');

        if($(window).width() < 991) {
            $(this).closest('.categories-tabs').find('.categories-tabs__select').html($(this).html())
            $(this).closest('.categories-tabs').find('.categories-tabs__select').removeClass('categories-tabs__select--active');
            $(this).closest('.categories-tabs').find('.categories-tabs__list').slideUp(0);
        }
        
        // products slider
        var slider = $(el).find('.js-products-slider')
        slider.slick('unslick');
        slider.slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            responsive: [
                {
                    breakpoint: 1366,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 3,
                    }
                },
                {
                    breakpoint: 560,
                    settings: {
                        slidesToShow: 2,
                    }
                },
            ]
        });

        return false; 
    });

    if($(window).width() < 991) {
        $('.categories-tabs__select').each(function() {
            $(this).html($(this).next('.categories-tabs__list').find('.categories-tabs__item--active .categories-tabs__link').html())
        })
    }

    $('.categories-tabs__select').on('click', function(){
        if($(window).width() < 991) {
            $(this).toggleClass('categories-tabs__select--active');
            $(this).next('.categories-tabs__list').slideToggle(0);
        }
    })

    $(document).on('click', function(event) {
        if($(window).width() < 991) {
            if( $(event.target).closest('.categories-tabs').length) 
                return;
                $('.categories-tabs__select').removeClass('categories-tabs__select--active');
                $('.categories-tabs__list').slideUp(0);
            }
    })

});
