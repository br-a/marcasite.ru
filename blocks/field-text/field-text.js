/* global document */

const autosize = require('autosize');
const ready = require('../../js/utils/documentReady.js');
const $ = require('jquery');

ready(function(){

    autosize(document.querySelectorAll('textarea'));

});

$(function() {
    $('[type="tel"]').inputmask({"mask": "+7 (999) 999-99-99"});
    $('input, textarea').on('keyup', function(event){
        if ($(this).val().length > 0) {
            $(this).closest('.field-text').addClass('field-text--fix')
        } else {
            $(this).closest('.field-text').removeClass('field-text--fix')
        }
    })

    $('.js-promo input').on('keyup change', function() {
        var length = $(this).val().length
        if (length >= 2) {
            $('.js-promo-btn').removeClass('btn--disabled');
        } else {
            $('.js-promo-btn').addClass('btn--disabled');
        }
    })
});

