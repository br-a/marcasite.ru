
const ionRangeSlider = require('ion-rangeslider');
const {hideScroll, showScroll} = require('../../js/utils/scrollHideShow');

$(function() {
    // filters

    $('.filters__title--dropdown').on('click', function(){
        $(this).toggleClass('filters__title--open').closest('.filters__group').find('.filters__dropdown').slideToggle();
    })

    $('.filters__show-more').on('click', function(){
        if($(this).hasClass('filters__show-more--open')) {
            var checkbox = parseInt($(this).closest('.filters__dropdown').find('.field-checkbox').length) - 6
            $(this).removeClass('filters__show-more--open').text('Ещё(+'+checkbox+')').closest('.filters__group').find('.field-checkbox:nth-child(n + 7)').fadeOut();
        } else {
            $(this).addClass('filters__show-more--open').text('Свернуть').closest('.filters__group').find('.field-checkbox:nth-child(n + 7)').fadeIn();
        }
    })

    // filters mobile

    $('.products__filters-link').on('click', function(){
        $('.filters').addClass('filters--show');
        hideScroll();
        $('body').addClass('shadow');
    })

    $('.filters__close').on('click', function(){
        $('.filters').removeClass('filters--show');
        showScroll();
        $('body').removeClass('shadow');
    })

    // cost

    function prettify(num) {
        var n = num.toString();
        return n.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + " ");
    }

    var $range = $(".js-range-slider"),
        $inputFrom = $(".js-input-from"),
        $inputTo = $(".js-input-to"),
        instance,
        min = 0,
        max = 50000,
        from = 0,
        to = 0;

    $range.ionRangeSlider({
        skin: "round",
        type: "double",
        min: min,
        max: max,
        from: 1000,
        to: 35000,
        step: 500,
        onStart: updateInputs,
        onChange: updateInputs
    });
    instance = $range.data("ionRangeSlider");

    function updateInputs (data) {
        from = data.from;
        to = data.to;

        $inputFrom.prop("value", prettify(from));
        $inputTo.prop("value", prettify(to));
        
    }

    $inputFrom.on("input", function () {
        var val = $(this).prop("value");
        
        // validate
        if (val < min) {
            val = min;
        } else if (val > to) {
            val = to;
        }
        if(instance)
        instance.update({
            from: val
        });
    });

    $inputTo.on("input", function () {
        var val = $(this).prop("value");
        
        // validate
        if (val < from) {
            val = from;
        } else if (val > max) {
            val = max;
        }
        if(instance)
        instance.update({
            to: val
        });
    });

    $('.js-input-from, .js-input-to').on('keydown', function(e){
        let key = Number(e.key)
        if (e.key === 'Backspace' || e.key === 'Tab' || e.key === 'Enter' || e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
            return
        }
        if (isNaN(key) || e.key === null || e.key === ' ') {
            return false;
        }
    })

    $('.js-input-from, .js-input-to').on('change', function() {
        var res = $(this).val().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + " ");
        $(this).val(res);
    });

    $('.js-reset').on("click", function (e) {
        e.preventDefault()
        $(this).closest('form').find('input:text, input:password, input:file, select, textarea').val('');
        $(this).closest('form').find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
        $(this).closest('form').find('input:radio, input:checkbox').prop('checked', false).prop('selected', false);
        if(instance)instance.update({
            from: 1000,
            to: 35000,
        });
        if(instance)updateInputs(instance.result)
    });
});


