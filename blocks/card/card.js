
$(function() {
    // card

    if($(window).width() < 768){
        $('.card-main__title').insertBefore('.card-main__gallery');
    }

    $(window).resize(function(){
        if($(window).width() < 768){
            $('.card-main__title').insertBefore('.card-main__gallery');
        } else {
            $('.card-main__title').prependTo('.card-main__info');
        }
    })
});
