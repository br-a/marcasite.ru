const {hideScroll, showScroll} = require('../../js/utils/scrollHideShow');


$(function() {
    // mobile-nav

    $('.mobile-nav__burger').on('click', function(){
        $('.mobile-nav__hidden').addClass('mobile-nav__hidden--show');
        hideScroll();
        $('body').addClass('shadow');
    })

    $('.mobile-nav__close').on('click', function(){
        $('.mobile-nav__hidden').removeClass('mobile-nav__hidden--show');
        showScroll()
        $('body').removeClass('shadow');
    })

    $(document).on('click', function(event) {
        if ( $('body').hasClass('no-scroll') ) {
            if( $(event.target).closest('.mobile-nav__hidden').length || 
                $(event.target).closest('.mobile-nav__burger').length || 
                $(event.target).closest('.filters').length || 
                $(event.target).closest('.products__filters-link').length
            ) 
                return;
                $('.mobile-nav__hidden').removeClass('mobile-nav__hidden--show');
                $('.filters').removeClass('filters--show');
                showScroll()
                $('body').removeClass('shadow');
        }
    })

    $('.mobile-nav__item--has-child .mobile-nav__link').on('click', function(){
        $(this).closest('.mobile-nav__item--has-child').toggleClass('mobile-nav__item--open').find('.mobile-nav__list--lvl-2').slideToggle();
    })
});
