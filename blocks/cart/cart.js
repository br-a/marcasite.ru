
$(function() {

    // cart remove
    $('.item-cart__remove').on('click', function(){
        $(this).closest('.item-cart').fadeOut(300, function(){
            $(this).closest('.item-cart').remove();
        });
    });
});
