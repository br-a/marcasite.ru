
const slick = require('slick-carousel');

$(function() {
    // main-slider
    $('.js-main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 5000,
        fade: true,
        dots: true,
        appendArrows: '.js-main-slider-nav',
        prevArrow: '.js-main-slider-prev',
        nextArrow: '.js-main-slider-next',
    });
});
