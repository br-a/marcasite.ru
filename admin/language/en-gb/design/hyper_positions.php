<?php
//
$_['hp_name_titles']			= 'SUPPORT AND UPGRADES';
$_['hp_name']					= 'HYPER POSITIONS';
$_['width_name']				= '100%';
$_['width_title']				= 'Width 100%';
$_['text_pos_left_center']		= 'Left center';
$_['text_pos_center']			= 'Central column';
$_['text_pos_right_center']		= 'Right center';
// HEADER
$_['text_header_pos']			= 'TOP HEADER';
//
$_['text_top_menus']			= 'TOP MENU';
$_['text_headers']				= 'HEADER';
$_['text_menus']				= 'MENU';
// TOP
$_['text_pos_sliders']			= 'SLIDESHOW';
$_['text_top_pos']				= 'TOP';
$_['text_top1_pos']				= 'TOP 1';
$_['text_top2_pos']				= 'TOP 2';
$_['text_banner_pos']			= 'BANNER 1';
$_['text_top3_pos']				= 'TOP 3';
// CONTENT
$_['text_content_pos']			= 'MAIN CONTENT';
$_['text_banner2_pos']			= 'BANNER 2';
$_['text_content2_pos']			= 'CONTENT 2';
// BOTTOM
$_['text_bottom_pos']			= 'BOTTOM UNIT';
$_['text_banner3_pos']			= 'BANNER 3';
$_['text_bottom1_pos']			= 'BOTTOM UNIT 1';
$_['text_bottom2_pos']			= 'BOTTOM UNIT 2';
$_['text_bottom3_pos']			= 'BOTTOM UNIT 3';
$_['text_banner4_pos']			= 'BANNER 4';
$_['text_bottom4_pos']			= 'BOTTOM UNIT 4';
// FOOTER
$_['text_footer_pos']			= 'FOOTER 1';
$_['text_map_pos']				= 'MAP';
//
$_['text_footer2_pos']			= 'FOOTER 2';
$_['text_footer3_pos']			= 'FOOTER 3';
//
$_['text_footers']				= 'FOOTER';
//
$_['text_footer4_pos']			= 'FOOTER 4';
$_['text_footer5_pos']			= 'FOOTER 5';
//
$_['text_copyright_pos']		= 'COPYRIGHT';
$_['text_copyright2_pos']		= 'COPYRIGHT 2';