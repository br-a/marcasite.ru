<?php

$_['heading_title']    = 'Shipping DDelivery';

$_['text_extensions']  = 'Extensions';
$_['text_success']     = 'Settings successfully saved!';
$_['text_edit']        = 'Edit';

$_['entry_geo_zone']   = 'Geographical zone';
$_['entry_api_key']    = 'API-key';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sorting order';

$_['error_permission'] = 'Warning: you are not allowed to change the settings of the module DDelivery!';