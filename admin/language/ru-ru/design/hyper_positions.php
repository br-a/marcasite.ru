<?php
//
$_['hp_name_titles']			= 'Поддержка и обновление модуля';
$_['hp_name']					= 'HYPER POSITIONS';
$_['width_name']				= '100%';
$_['width_title']				= 'Блок по ширине монитора';
$_['text_pos_left_center']		= 'Левый центр';
$_['text_pos_center']			= 'Центральная колонка';
$_['text_pos_right_center']		= 'Правый центр';
// HEADER
$_['text_header_pos']			= 'ТОП ХЕДЕР';
// TOP
$_['text_pos_sliders']			= 'СЛАЙДЕР';
$_['text_top_pos']				= 'ТОП';
$_['text_top1_pos']				= 'ТОП 1';
$_['text_top2_pos']				= 'ТОП 2';
$_['text_banner_pos']			= 'БАННЕР 1';
$_['text_top3_pos']				= 'ТОП 3';
// CONTENT
$_['text_content_pos']			= 'ОСНОВНОЙ КОНТЕНТ';
$_['text_banner2_pos']			= 'БАННЕР 2';
$_['text_content2_pos']			= 'КОНТЕНТ 2';
// BOTTOM
$_['text_bottom_pos']			= 'НИЖНИЙ БЛОК';
$_['text_banner3_pos']			= 'БАННЕР 3';
$_['text_bottom1_pos']			= 'НИЖНИЙ БЛОК 1';
$_['text_bottom2_pos']			= 'НИЖНИЙ БЛОК 2';
$_['text_bottom3_pos']			= 'НИЖНИЙ БЛОК 3';
$_['text_banner4_pos']			= 'БАННЕР 4';
$_['text_bottom4_pos']			= 'НИЖНИЙ БЛОК 4';
// FOOTER
$_['text_footer_pos']			= 'ФУТЕР 1';
$_['text_map_pos']				= 'MAP';
//
$_['text_footer2_pos']			= 'ФУТЕР 2';
$_['text_footer3_pos']			= 'ФУТЕР 3';
//
$_['text_footer4_pos']			= 'ФУТЕР 4';
$_['text_footer5_pos']			= 'ФУТЕР 5';
//
$_['text_copyright_pos']		= 'КОПИРАЙТ';
$_['text_copyright2_pos']		= 'КОПИРАЙТ 2';