<?php
/*
@author  nikifalex
@skype   logoffice1
@email    nikifalex@yandex.ru
@link https://opencartforum.com/files/file/4617-pohozhie-tovary/
*/

// Heading
$_['heading_title']    = 'Похожие товары';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки модуля успешно обновлены!!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_name']       = 'Название';
$_['entry_limit']      = 'Лимит';
$_['entry_use_category'] = 'Учитывать категорию';
$_['entry_use_manufacturer'] = 'Учитывать производителя';
$_['entry_add_diff_attributes'] = 'Добавить к описанию список отличий в атрибутах';
$_['entry_cnt_diff'] = 'Макс. количество отличий чтобы товар появился в списке';
$_['entry_excluded_attributes'] = 'Исключить атрибуты';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления модулем!';
$_['error_name']       = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Необходимо указать Ширину!';
$_['error_height']     = 'Необходимо указать Высоту!';