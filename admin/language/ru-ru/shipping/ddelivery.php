<?php

$_['heading_title']    = 'Доставка DDelivery';

$_['text_extensions']  = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

$_['entry_geo_zone']   = 'Географическая зона';
$_['entry_api_key']    = 'API-ключ';
$_['entry_status']     = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

$_['error_permission'] = 'Внимание: у вас недостаточно прав для изменения настроек модуля DDelivery!';