<?php
// HTTP
define('HTTP_SERVER', 'https://marcasite.ru/admin/');
define('HTTP_CATALOG', 'https://marcasite.ru/');

// HTTPS
define('HTTPS_SERVER', 'https://marcasite.ru/admin/');
define('HTTPS_CATALOG', 'https://marcasite.ru/');

// DIR
define('DIR_APPLICATION', '/var/www/marcroz/data/www/marcasite.ru/admin/');
define('DIR_EXCEL', '/var/www/marcroz/data/www/marcasite.ru/excel/');
define('DIR_SYSTEM', '/var/www/marcroz/data/www/marcasite.ru/system/');
define('DIR_IMAGE', '/var/www/marcroz/data/www/marcasite.ru/image/');
define('DIR_LANGUAGE', '/var/www/marcroz/data/www/marcasite.ru/admin/language/');
define('DIR_TEMPLATE', '/var/www/marcroz/data/www/marcasite.ru/admin/view/template/');
define('DIR_CONFIG', '/var/www/marcroz/data/www/marcasite.ru/system/config/');
define('DIR_CACHE', '/var/www/marcroz/data/www/marcasite.ru/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/marcroz/data/www/marcasite.ru/system/storage/download/');
define('DIR_LOGS', '/var/www/marcroz/data/www/marcasite.ru/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/marcroz/data/www/marcasite.ru/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/marcroz/data/www/marcasite.ru/system/storage/upload/');
define('DIR_CATALOG', '/var/www/marcroz/data/www/marcasite.ru/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'marcrozbd');
define('DB_PASSWORD', '2D8b7S6c');
define('DB_DATABASE', 'testnew');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
