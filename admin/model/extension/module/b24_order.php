<?php
class ModelExtensionModuleB24Order extends Model{

	//public $UF_DEAL_PAYMENT_METHOD = 'UF_CRM_1527844108';
	//public $UF_LEAD_PAYMENT_METHOD = 'UF_CRM_1527844072';
	
	//public $UF_DEAL_DISCOUNT_TYPE = 'UF_CRM_1527844360';
	//public $UF_LEAD_DISCOUNT_TYPE = 'UF_CRM_1524036962';
	
	
	
	public function __construct( $registry ){ 
		parent::__construct($registry);
		
        $this->load->model('setting/setting');

        $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
		$this->b24->setFields($b24_setting);
	}

	protected function initEditOrder($order_id){	
        $this->load->model('module/b24_customer');
        $this->load->model('checkout/order');
		
        $order = $this->model_checkout_order->getOrder($order_id);
        $customerName = $order['firstname'];

        $b24Contact = $this->getContactFromDB($order['customer_id']);

        if (empty($b24Contact)) {
            $b24Contact = $this->getContactFromB24($order['email']);
        }

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->config->get('b24_manager')['manager'];
        $b24ContactId = !empty($b24Contact['ID']) ? $b24Contact['ID'] : 0;
		
        return [
            'fields' => [
                'CONTACT_ID' => $b24ContactId,
                'ASSIGNED_BY_ID' => $managerId,
                'NAME' => $customerName
            ]
        ];
	}
	
	/*
	public function SendOrderToBitrix()	{
		$this->load->model('setting/setting');
        $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
		$this->b24->setFields($b24_setting);
		
		$sql = 'SELECT '. DB_PREFIX .'order.order_id
				FROM '. DB_PREFIX .'order 
				LEFT JOIN b24_order ON '. DB_PREFIX .'order.order_id=b24_order.oc_order_id
				WHERE b24_order.oc_order_id IS NULL AND '. DB_PREFIX .'order.customer_id!=0 AND '. DB_PREFIX .'order.order_status_id!=0 LIMIT 100';
        $rows = $this->db->query($sql);

        if (1 > $rows->num_rows) {
            return;
        }

        $count_order = $rows->num_rows;
		
        $orders = $rows->rows;
		
		foreach ($orders as $order) {
			$order_id = $order['order_id'];
			$this->addOrder($order_id);
		}
    }
*/
	

	
	public function SendOrderToBitrix($orderForSend){
        $this->load->model('setting/setting');
        $b24_setting = $this->model_setting_setting->getSetting('b24_hook_key');
		$this->b24->setFields($b24_setting);
		
		$this->log->write(print_r('Количество заказов для добавления в Битрикс 24'.count($orderForSend),true)); 
		
		if (1 > $orderForSend) {
            return;
        }
		
		foreach ($orderForSend as $key => $IDs) {
			foreach ($IDs as $id) {
				$orderID[$key] = $id;
			}	
		}
		$orderChunk = array_chunk($orderID, 25);
        $build = []; 
		
		foreach($orderChunk as $orderId) {
			
			foreach($orderId as $value) {
            $dataToAdd = $this->prepareDataToB24($value);
			$productToAdd = $this->prepareProductToB24($value);
			$typeOrder = $dataToAdd['fields']['CONTACT_ID'] == 0 ? 'lead' : 'deal';
				foreach ($dataToAdd as $fields) {
					$build['cmd']['order_add'.$value] = 'crm.'.$typeOrder.'.add?'. http_build_query(['fields' => $fields]);				
					$build['cmd']['product_add'.$value] = 'crm.' . $typeOrder . '.productrows.set?id=$result[order_add'.$value.']&' . http_build_query($productToAdd);
				}		
			}
			$params = [
                    'type' => 'batch',
                    'params' => $build 
            ];
			$result = $this->b24->callHook($params);
			$this->log->write(print_r($result['result']['result'],true));
			$b24Id = $result['result']['result'];
			
			foreach($orderId as $value) { 
					$adddb = $this->addToDB($value, $b24Id['order_add'.$value]);
			}
			
			/*
	if (!empty($result['result']['result_error'])) {
                trigger_error('Ошибка при запросе SendOrderToBitrix ' . print_r($result['result']['result_error'], 1));
            } else {
              foreach($orderId as $value) { 
					$adddb = $this->addToDB($value, $b24Id['order_add'.$value]);
					$this->log->write(print_r($adddb,true));
			  }
            }
*/
            $build['cmd'] = [];
        }
		
    }

	public function addOrder($order_id) {
        
		$this->load->model('sale/order');
        $this->load->model('extension/module/b24_customer');
		
		$siteName = html_entity_decode($this->config->get('config_name'),ENT_QUOTES, 'UTF-8');
		
		$order = $this->model_sale_order->getOrder($order_id);		
		
		if ($order['order_status_id'] <= 0) {
		    return;
		}

		$dataToAdd = $this->prepareDataToB24($order_id);


		// Оповещение менеджера о новом клиенте
		$dataToAdd = array_merge($dataToAdd, ['params' => ['REGISTER_SONET_EVENT' => 'Y']]);

		//Product
		$productToAdd = $this->prepareProductToB24($order_id);
		// Product

		// Not Registered user b24ContactId. LEAD
		$extraField = [];
		if ($dataToAdd['fields']['CONTACT_ID'] == 0) {
			$typeApi = 'lead';
            $typeApiRu = 'лид';
            $typeApiRu2 = 'лида';
            $typeApiUrl = '/crm/lead/details/';
            $managerId = isset($this->config->get('b24_manager')['manager']) ? $this->config->get('b24_manager')['manager'] : '';

            //$text = 'На сайте ' . $siteName . ' получен новый <a href="{typeApiUrl}{b24Id}/">{typeApiRu}</a> от {dataToAdd[fields][NAME]}'
            //   . '. Перейдите к просмотру нового {typeApiRu2} <a href="{typeApiUrl}{b24Id}/">{b24Id}</a>';
			//$customerGroupId = $this->config->get('config_customer_group_id');
			//customerGroupId = $this->model_module_b24_customer->getGroupId();
			//$groupName = $this->getCustomerGroupName($customerGroupId);
			//$discountType = $groupName . ' ' . $this->getDiscountCoef($customerGroupId) . '%';
			//$extraField = [
				//$this->UF_LEAD_PAYMENT_METHOD => $order['payment_method'],
				//$this->UF_LEAD_DISCOUNT_TYPE => $discountType,
			//];
		} else {
			$typeApi = 'deal';
            $typeApiRu = 'сделка';
            $typeApiRu2 = 'сделки';
            $typeApiUrl = '/crm/deal/details/';
            $managerId = $dataToAdd['fields']['ASSIGNED_BY_ID'];

            //$text = 'На сайте ' . $siteName . ' полученa новая <a href="{typeApiUrl}{b24Id}/">{typeApiRu}</a> от {dataToAdd[fields][NAME]}'
            //   . '. Перейдите к просмотру новой {typeApiRu2} <a href="{typeApiUrl}{b24Id}/">{b24Id}</a>';

            //$customerGroupId = $this->config->get('config_customer_group_id');
            //$customerGroupId = $this->model_module_b24_customer->getGroupId();
			//$groupName = $this->getCustomerGroupName($customerGroupId);
			//$discountType = $groupName . ' ' . $this->getDiscountCoef($customerGroupId) . '%';
			//$extraField = [
				//$this->UF_DEAL_PAYMENT_METHOD => $order['payment_method'],
				//$this->UF_DEAL_DISCOUNT_TYPE => $discountType,
			//];
		}


		//$dataToAdd['fields'] = array_merge($dataToAdd['fields'], $extraField);
		//$typeApi = 'lead';
		$params = [
			'type' => 'batch',
			'params' => [
			    'cmd' => [
                    'order_add' => 'crm.' . $typeApi . '.add?' . http_build_query($dataToAdd),
                    'product_add' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
			]
		];

		$result = $this->b24->callHook($params);
		$this->log->write(print_r($result,true));
		$b24Id = $result['result']['result']['order_add'];
		//$b24Fields = $result['result']['result']['product_add'];

		if (!empty($result['result']['result_error'])) {
			trigger_error('Ошибка при добавлении клиента в Б24 ' . print_r($result['result_error'], 1), E_USER_WARNING);
		}

		$this->addToDB($order_id, $b24Id);
		return $b24Id;

        //$findSearch = ['{typeApiUrl}', '{b24Id}', '{typeApiRu}', '{dataToAdd[fields][NAME]}', '{typeApiRu2}'];
        //$findReplace = [$typeApiUrl, $b24Id, $typeApiRu, $dataToAdd['fields']['NAME'], $typeApiRu2];

        //$message = str_replace($findSearch, $findReplace, $text);

        /*
	$params2 = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'im_notify' => 'im.notify?' . http_build_query([
                        'to' => $managerId,
                        'message' => $message
                    ]),
                ],
            ],
        ];

        $this->b24->callHook($params2);
*/
	}
	
	public function editOrder($order_id) {
        //$this->load->model('module/b24_customer');
        $this->load->model('sale/order');  
		
        $b24OrderById = $this->getById($order_id);
        $b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;
		
		$order = $this->model_sale_order->getOrder($order_id); 

		$managerId = $order['customer_group_id'] != 0 ?  $this->config->get('b24_manager')['manager'][$order['customer_group_id']] : 1;
		$status_id = $this->getStatusById($order['order_status_id'],$order['customer_id']);
        $typeApi = 'deal';


        $dataToB24 = [
            'fields' => [
                'ASSIGNED_BY_ID' => $managerId,
				'STAGE_ID' => $status_id,
            ]
        ];
		
		 

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id='. $b24OrderId . http_build_query($dataToB24),
                    //'product_update' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
		$this->log->write(print_r($result,true));


        if (!empty($result['result']['result_error'])) {
            trigger_error('Ошибка при обновлении заказа в Б24 ' . print_r($result['result_error'], 1), E_USER_WARNING);
        }
	}
	
	public function prepareProductToB24($order_id)
	{
		$this->load->model('sale/order');

		$productToAdd = [];
		$productRows = $this->model_sale_order->getOrderProducts($order_id);
		foreach ( $productRows as $product )
		{
			$productId = $product['product_id'];

            // region Get B24 Product ID
			$orderOption = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);
			$productOptions = '';
			foreach ($orderOption as $option) {
				$productOptions .= ' | ' .  $option['name'] . ': ' . $option['value'];
			}
            //	$size = $orderOption[0]['value'] ? $orderOption[0]['value'] : self::WITHOUT_SIZE;

			//$filter = ['oc_product_id' => $productId, 'size' => $size];
			//$b24Product = $this->model_module_b24_product->getList($filter)[0];
            //endregion Get B24 Product ID

			//$newProduct = $this->recalculatePriceAndDiscount($product['product_id'], $this->customer->getGroupId());
			$taxRate = ($product['tax']/$product['price']) * 100;
			$price = $product['price'] + $product['tax'];
			$productName = html_entity_decode(trim($product['name'] . $productOptions));
			$productToAdd['rows'][] = [
				//'PRODUCT_ID' => $b24Product['b24_product_id'],
				'PRODUCT_NAME' => $productName,
				'PRICE' => $price,
				//'DISCOUNT_RATE' => $newProduct['discount_rate'],
				//'DISCOUNT_SUM' => $newProduct['discount_sum'],
				//'DISCOUNT_TYPE_ID' => 2,
				'TAX_RATE' => $taxRate,
				'TAX_INCLUDED' => 'N',
				'QUANTITY' => $product['quantity'],
				'MEASURE_CODE' => '', // piece
			];
		}

		$productToAdd = $this->addDeliveryCost($order_id, $productToAdd);

		return $productToAdd;
	}

	public function addDeliveryCost($order_id, array $productToAdd)
	{
		$this->load->model('sale/order');
		
		$orderTotalList = $this->model_sale_order->getOrderTotals($order_id);

		foreach ($orderTotalList as $orderTotal) {
			if ($orderTotal['code'] == 'shipping') {
				$productToAdd['rows'][] = [
					'PRODUCT_ID' => 0,
					'PRICE' => $orderTotal['value'],
					'PRODUCT_NAME' => $orderTotal['title'],
					'QUANTITY' => 1,
					'MEASURE_CODE' => '', // piece
				];
			}
		}

		return $productToAdd;
	}
	
	//Подготовка данных
	public function addToDB( $order_id, $b24Id, array $fields = [])
	{
		if( empty($b24Id) || (int) $order_id <= 0 ){
			trigger_error('Empty $b24Id or $order_id '
				.". Order ID : ". print_r($order_id, 1) .". ID B24: ".  print_r($b24Id, 1),
				E_USER_WARNING);
		}
		//$fields = json_encode($fields);
		$fieldsToAdd = ['oc_order_id' => $order_id, 'b24_order_id' => $b24Id];
		$add = $this->insertToDB($fieldsToAdd);
		return $add;
	}
	
	//Вставка в DB 
	public function insertToDB(array $fields)
	{
		$db = $this->db;
		$sql = 'REPLACE INTO b24_order SET ' . $this->prepareFields($fields) . ';';
		$db->query($sql);
		
		$lastId = $this->db->getLastId();
		return $lastId;
	}

	/*
	public function editOrderStatus($order_id, $order_status_id) {
		$dataToB24 = $this->initEditOrder($order_id);	 
		$typeApi = ($dataToB24['fields']['CONTACT_ID'] == 0) ? 'lead' : 'deal';
		
		$b24OrderStatusById = $this->getStatusById($order_status_id);
			
		if ($typeApi == 'lead') {	
			$b24OrderStatusId = !empty($b24OrderStatusById['b24_status_id']) ? $b24OrderStatusById['b24_status_id'] : 'NEW';
			$dataToB24['fields']['STATUS_ID'] =  $b24OrderStatusId;
		} elseif ($typeApi == 'deal') {	
			$b24OrderStatusId = !empty($b24OrderStatusById['b24_stage_id']) ? $b24OrderStatusById['b24_stage_id'] : 'NEW';
			$dataToB24['fields']['STAGE_ID'] =  $b24OrderStatusId;
		}
	 
		$b24OrderById = $this->getById($order_id);
		$b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;		

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id='. $b24OrderId . http_build_query($dataToB24),
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
	}
*/
	

	public function prepareDataToB24($order_id){
		
		$this->load->model('sale/order');
		$this->load->model('extension/module/b24_customer');
		$this->load->model('setting/setting');
		//$this->load->model('module/b24_product');

		// DATA
		$order = $this->model_sale_order->getOrder($order_id);
        $orderName = html_entity_decode($order['store_name'],ENT_QUOTES, 'UTF-8') . '. Заказ № ' . $order['order_id'];
		$orderComment = $order['comment'];
		$customerLastname = $order['lastname'];
		$customerName = $order['firstname'];
		$customerEmail = $order['email'];
		$customerPhone = preg_replace("/[^0-9]/", '', $order['telephone']);
		$customerId = $order['customer_id'];
		
		//Получаем статус и присваеваем его заказу в Битрикс 24
		$order_status_id = $order['order_status_id'];
		$order_statuses = $this->getStatusById($order_status_id);


		if ($order['customer_id'] != 0) {
			$b24_status_id = empty($order_statuses['b24_stage_id']) ? 'NEW' : $order_statuses['b24_stage_id'];
		} else {
			$b24_status_id = empty($order_statuses['b24_status_id']) ? 'NEW' : $order_statuses['b24_status_id'];
		}		
		
		$roistat = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : 0;
		
		if ($order['customer_id'] != 0) {
			$b24Contact = $this->getContactFromDB($order['customer_id']);
            $b24ContactId = $b24Contact['ID'];
		} else {
			$b24ContactByPhone = $this->getContactFromDBByPhone($customerPhone);
			$b24ContactId = isset($b24ContactByPhone['ID']) ? $b24ContactByPhone['ID'] : 0;
			
			if (!$b24ContactId){
				$b24Contact = $this->getContactFromB24($order['email']);
				$b24ContactId = 0;				
			}
		}

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->config->get('b24_manager')['manager'];

		//Способ оплаты
		if($order['payment_code'] == 'cod'){
			$UF_CRM_payment_method = 226;
		} elseif ($order['payment_code'] == 'prompay'){
			$UF_CRM_payment_method = 220;
		} else {
			$UF_CRM_payment_method = 0;
		}
		
		
		//Оплачено или нет
		$UF_CRM_payment_status = $order['order_status_id'] == 17 ? 1 : 0;
		
		//Сумма по доставке и купоны
		$orderTotalList = $this->model_sale_order->getOrderTotals($order_id);
		foreach ($orderTotalList as $orderTotal) {
			if ($orderTotal['code'] == 'shipping') {
				$deliveryCost = $orderTotal['value'];
			}
			
			if ($orderTotal['code'] == 'sub_total') {
				$sub_total = $orderTotal['value'];
			}
			
			if ($orderTotal['code'] == 'coupon'){
				if( preg_match( '!\(([^\)]+)\)!', $orderTotal['title'], $match ) ){
					$coupontitle = $match[1];
				}
				$couponDiscount = abs($orderTotal['value']);
			}
			
			if ($orderTotal['code'] == 'personaldisc'){
				$coupontitle = $orderTotal['title'];
				$couponDiscount = abs($orderTotal['value']);
			}
		}
		$deliverySumm = isset($deliveryCost) ? $deliveryCost : '';
		$couponName = isset($coupontitle) ? $coupontitle : '';
		$cDiscount = isset($couponDiscount) ? $couponDiscount / $sub_total * 100 : '';
		
		
		//Способ доставки
		if($order['shipping_method'] == 'Shop Logistic'){
			$UF_CRM_shipping_method = 228;
		} elseif ($order['shipping_method'] == 'Почта России'){
			$UF_CRM_shipping_method = 240;
		} elseif ($order['shipping_method'] == 'EMS Почта России'){
			$UF_CRM_shipping_method = 244;
		} elseif ($order['shipping_method'] == 'PONY EXPRESS'){
			$UF_CRM_shipping_method = 232;
		} elseif ($order['shipping_method'] == 'Самовывоз'){
			$UF_CRM_shipping_method = 408;
		} elseif ($order['shipping_method'] == 'Курьер'){
			$UF_CRM_shipping_method = 406;
		} else {
			$UF_CRM_shipping_method = 236;
		}

		//$managerId = $this->config->get('b24_manager')['manager'];
		$dataToB24 = [];
		$dataToB24 = [
		    'fields' => [
                'TITLE' => $orderName,
				'STAGE_ID'=> $b24_status_id,
                'CURRENCY_ID' => $this->config->get('config_currency'),
                'SOURCE_ID' => 'STORE',
                'OPENED' => 'N',
                'ASSIGNED_BY_ID' => $managerId,
                'CONTACT_ID' => $b24ContactId,
                'COMMENTS' => $orderComment,
                //'CREATED_BY_ID' => self::CREATED_BY,

                // LEAD Fields
				'STATUS_ID' => $b24_status_id,
                'NAME' => $customerName,
                'LAST_NAME' => $customerLastname,
                'ADDRESS' => $order['payment_address_1'],
                'ADDRESS_COUNTRY' => $order['payment_country'],
                'ADDRESS_PROVINCE' => $order['payment_zone'],
                'ADDRESS_CITY' => $order['payment_city'],
                'ADDRESS_POSTAL_CODE' => $order['payment_postcode'],
                'PHONE' => [['VALUE' => $customerPhone, "VALUE_TYPE" => "WORK"]],
                'EMAIL' => [['VALUE' => $customerEmail, "VALUE_TYPE" => "WORK"]],
                //'UF_CRM_1518879521' => $roistat,
                'UF_CRM_1541665172' => 1,
                'UF_CRM_1541665237' => 218,
				'UF_CRM_1541665278' => $deliverySumm,
				'UF_CRM_1541665439' => $cDiscount,
				'UF_CRM_1543990905664' => $couponName,
				'UF_CRM_1541665372' => $UF_CRM_payment_status,
                'UF_CRM_1541665522' => $UF_CRM_shipping_method,
                'UF_CRM_1541665353' => $UF_CRM_payment_method,
				'UF_CRM_1543304647279' => $order['payment_country'].', '.$order['payment_city'].', '.$order['payment_address_1'].', '.$order['payment_address_2'],
            ]
        ];
		

		return $dataToB24;
	}
	// Получаем контакт 
	public function getContactFromDB($customerId)
	{
		$this->load->model('extension/module/b24_customer');
		if (abs($customerId) <= 0) {
		    return [];
		}

		$b24Row = $this->model_extension_module_b24_customer->getById($customerId);
		$b24Contact = json_decode(isset($b24Row['b24_contact_field']) ? $b24Row['b24_contact_field'] : 0, 1);

		return $b24Contact;
	}
	
	public function getContactFromDBByPhone($phone){
		$result = array();
		
		$query = $this->db->query("SELECT * FROM b24_customer WHERE phone = '" . $phone . "'");
		
		if ($query->num_rows){
			$result = json_decode($query->row['b24_contact_field'], 1);
		}

		return $result;
	}

	public function getContactFromB24($contactEmail)
	{
        $B24ContactList = $this->getB24ContactList(['EMAIL' => $contactEmail]);
        $b24Contact = isset($B24ContactList[0]) ? $B24ContactList[0] : [];

		return $b24Contact;
	}

	public function getB24ContactList($filter) {
		if (empty($filter)) {
		    trigger_error('Empty filter', E_USER_WARNING);
		}

		foreach ($filter as $value) {
			if (empty($value)) {
			    return false;
			}
		}

		$params = [
			'type' => 'crm.contact.list',
			'params' => [
				'filter' => $filter
			]
		];

		$result = $this->b24->callHook($params);

		return $result['result'];
	}

	

	public function prepareFields(array $fields)
	{
		$sql = '';
		$index = 0;
		foreach ( $fields as $columnName => $value )
		{
			$glue = $index === 0 ? ' ' : ', ';
			$sql .= $glue . "`$columnName`" . ' = "' . $this->db->escape($value) . '"';
			$index++;
		}

		return $sql;
	}

	
	public function getStatusById($order_status_id, $customer_id) {		
		if($customer_id == 0){
			$status_map = $this->config->get('b24_status')['lead'];
		} else {
			$status_map = $this->config->get('b24_status')['deal'];
		}
		return array_search($order_status_id, $status_map); 
	}

	public function getById($order_id) {
		if (abs($order_id ) <= 0) {
		    trigger_error('ID must be integer', E_USER_WARNING);
		}

		$db = $this->db;
		$sql = 'SELECT * FROM b24_order WHERE oc_order_id = "' . $db->escape($order_id ) . '"';
		$query = $db->query($sql);

		return $query->row;
	}
	// Получаем заказы которых нет в Битрикс
	public function getOrderForSend(){
        $orderId = $this->db->query("SELECT o.order_id FROM ". DB_PREFIX ."order o
										LEFT JOIN b24_order b24 ON (o.order_id = b24.oc_order_id) 
										WHERE b24.oc_order_id IS NULL AND o.order_status_id!=0 LIMIT 500;");
		
		if (1 > $orderId->num_rows) {
            return;
        }
		
		return $orderId->rows;
	}

	/*
	public function getList(array $filter)
	{
		$db = $this->db;
		$where = ' WHERE ';
		$index = 0;
		foreach ($filter as $columnName => $value) {
			$glue = $index === 0 ? ' ' : ' AND ';
			$where .= $glue . $columnName. ' = "' . $db->escape($value) . '"';
			$index++;
		}

		$sql = 'SELECT * FROM ' . self::TABLE_NAME . $where . ';';
		$query = $db->query($sql);

		return $query->rows;
	}
*/
}