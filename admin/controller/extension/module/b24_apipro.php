<?php
class ControllerExtensionModuleB24Apipro extends Controller {
	private $error = array();
    private $minMaxParentIds = [];

	public function install(){		
		$delimiter = '$registry = new Registry();';
		$class = '$b24 = new B24();$registry->set(\'b24\', $b24);';
			
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/system/framework.php';
		$indexArr = explode($delimiter, file_get_contents($filename, true));
		$index = $indexArr[0] . $delimiter . $class . $indexArr[1];
		file_put_contents($filename, $index);	
		
		$this->load->model('extension/event');

		$this->model_extension_event->addEvent('b24_status_edit', 'catalog/controller/api/order/history/after', 'module/b24_apipro/editOrderStatus');
		$this->model_extension_event->addEvent('b24_order_add', 'catalog/model/checkout/order/addOrderHistory/after', 'module/b24_apipro/addOrder');
		$this->model_extension_event->addEvent('b24_customer_add', 'catalog/model/account/customer/addCustomer/after', 'module/b24_apipro/addCustomer');
		$this->model_extension_event->addEvent('b24_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'module/b24_apipro/editCustomer');
		$this->model_extension_event->addEvent('b24_customer_add_address', 'catalog/model/account/address/addAddress/after', 'module/b24_apipro/addAddress');
		$this->model_extension_event->addEvent('b24_customer_edit_address', 'catalog/model/account/address/editAddress/after', 'module/b24_apipro/editAddress');
		$this->model_extension_event->addEvent('b24_customer_admin_customer_edit', 'admin/model/customer/customer/editCustomer/after', 'extension/module/b24_apipro/editCustomer');
        //$this->model_extension_event->addEvent('b24_category_edit', 'admin/model/catalog/category/editCategory/after', 'extension/module/b24_apipro/edit');
        //$this->model_extension_event->addEvent('b24_category_add',  'admin/model/catalog/category/addCategory/after', 'extension/module/b24_apipro/add');
        //$this->model_extension_event->addEvent('b24_category_delete', 'admin/model/catalog/category/deleteCategory/after', 'extension/module/b24_apipro/delete');
        //$this->model_extension_event->addEvent('b24_product_add', 'admin/model/catalog/product/addProduct/after', 'extension/module/b24_apipro/addproduct');
        //$this->model_extension_event->addEvent('b24_product_edit', 'admin/model/catalog/product/editProduct/after', 'extension/module/b24_apipro/editproduct');
        //$this->model_extension_event->addEvent('b24_product_delete', 'admin/model/catalog/product/deleteProduct/after', 'extension/module/b24_apipro/deleteproduct');

		$createTableSql = "CREATE TABLE IF NOT EXISTS `b24_order` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`oc_order_id` INT(11) NULL DEFAULT NULL,
			`b24_order_id` INT(11) NULL DEFAULT NULL,
			`type` INT(1) NOT NULL DEFAULT '1',
			`date_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`))
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;";

		$createConfigTableSql = "CREATE TABLE IF NOT EXISTS `b24_order_config` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`name` VARCHAR(255) NULL DEFAULT NULL,
			`value` TEXT NULL,
			PRIMARY KEY (`id`),
			UNIQUE INDEX `name` (`name`))
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;";

		$createCustomer = "CREATE TABLE IF NOT EXISTS `b24_customer` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`oc_customer_id` INT(11) NOT NULL,
			`b24_contact_id` INT(11) NOT NULL,
			`phone` VARCHAR(16) NOT NULL,
			`b24_contact_field` TEXT NULL,
			`date_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
			PRIMARY KEY (`id`),
			UNIQUE INDEX `b24_contact_id` (`b24_contact_id`))
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;";

        $creatCategory = "Create table if not exists b24_category(
			`id` INT(11) AUTO_INCREMENT,
            `oc_category_id` INT(11)  NOT NULL,
            `b24_category_id` INT(11) NOT NULL,
            PRIMARY KEY(id),
            UNIQUE b24_category_id (b24_category_id))
            COLLATE='utf8_general_ci'
            ENGINE=InnoDB;";

        $sqlProduct = "CREATE TABLE if not exists `b24_product` (
            `id` INT(11) NOT NULL AUTO_INCREMENT,
            `oc_product_id` INT(11) NOT NULL,
            `b24_product_id` INT(11) NOT NULL,
            `option` INT(11) NULL DEFAULT NULL,
            `date_update` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`id`),
            UNIQUE INDEX `option_product_id` (`option`, `oc_product_id`))
			COLLATE='utf8_general_ci'
			ENGINE=InnoDB;";

		$this->db->query($createTableSql);
		$this->db->query($createConfigTableSql);
		$this->db->query($createCustomer);
        $this->db->query($creatCategory);
        $this->db->query($sqlProduct);
	}

	public function uninstall(){	
		$delimiter = '$b24 = new B24();$registry->set(\'b24\', $b24);';
			
		$filename = $_SERVER['DOCUMENT_ROOT'] . '/system/framework.php';
		$indexArr = explode($delimiter, file_get_contents($filename, true));
		$index = $indexArr[0] . $indexArr[1];
		file_put_contents($filename, $index);
		
		$this->load->model('extension/event');
		
		$this->model_extension_event->deleteEvent('b24_status_edit');
		$this->model_extension_event->deleteEvent('b24_order_add');
		$this->model_extension_event->deleteEvent('b24_order_edit');
		$this->model_extension_event->deleteEvent('b24_customer_edit');
		$this->model_extension_event->deleteEvent('b24_customer_add');
		$this->model_extension_event->deleteEvent('b24_customer_add_address');
		$this->model_extension_event->deleteEvent('b24_customer_edit_address');
		$this->model_extension_event->deleteEvent('b24_customer_admin_customer_edit');
        //$this->model_extension_event->deleteEvent('b24_category_edit');
        //$this->model_extension_event->deleteEvent('b24_category_add');
        //$this->model_extension_event->deleteEvent('b24_category_delete');
        //$this->model_extension_event->deleteEvent('b24_product_edit');
        //$this->model_extension_event->deleteEvent('b24_product_add');
        //$this->model_extension_event->deleteEvent('b24_product_delete');
	}

	public function __construct( $registry ){
		parent::__construct($registry);
		
        $this->load->model('setting/setting');
        $b24_setting = $this->model_setting_setting->getSetting('b24');
		
		if ($registry->has('b24')){
			$this->b24->setFields($b24_setting);
		} 
	}

	public function index() {
        set_time_limit(9999);
		$this->load->language('extension/module/b24_apipro');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('extension/module');
        $this->load->model('extension/module/b24_category');
        $this->load->model('extension/module/b24_product');
        $this->load->model('extension/module/b24_order');
        $this->load->model('extension/module/b24_customer');
		$this->load->model('localisation/order_status');
		$this->load->model('customer/customer_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$authData = $this->parseHookURL(isset($this->request->post['b24_in_hook']) ? $this->request->post['b24_in_hook'] : '');
			if(!empty($authData)){
				$this->model_setting_setting->editSetting('b24', array_merge($this->request->post, $authData));
			} else {
				$this->model_setting_setting->editSetting('b24', $this->request->post);
			}
			
			$this->session->data['success'] = 'Настройки сохранены';
			$this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));	
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['token'] = $this->session->data['token'];
		
		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'token=' . $data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/b24_apipro', 'token=' . $data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/b24_apipro', 'token=' . $data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/b24_apipro', 'token=' . $data['token'], 'SSL');
            $data['action_web_hook'] = $this->url->link('module/b24_apipro/setwebhookkey', 'token=' . $data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/module/b24_apipro', 'token=' . $data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
            $data['action_web_hook'] = $this->url->link('module/b24_apipro/setwebhookkey', 'token=' . $data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info)) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['module_description'])) {
			$data['module_description'] = $this->request->post['module_description'];
		} elseif (!empty($module_info)) {
			$data['module_description'] = $module_info['module_description'];
		} else {
			$data['module_description'] = array();
		}

		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['lang'] = $this->language->get('lang');

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info)) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$order_sett = json_decode(json_encode($this->config->get('b24_order')), true);

		// Получение входящих хуков
		
		$data['b24_key_api'] = $this->config->get('b24_key_api');
		$data['b24_key_domain'] = $this->config->get('b24_key_domain');
		$data['b24_key_id'] = $this->config->get('b24_key_id');
		$data['b24_in_hook'] = isset($data['b24_key_domain']) ? 'https://'.$data['b24_key_domain'].'/rest/'.$data['b24_key_id'].'/'.$data['b24_key_api'].'/profile/' : 'Вставьте ссылку входящего вебхука';
		
		// Получение исходящих хуков 
		$data['out_hooks'] = [
			//'ONCRMINVOICEADD' => 	'Создание счета', 
			//'ONCRMINVOICEUPDATE' => 'Обновление счета', 
			//'ONCRMINVOICEDELETE' => 'Удаление счета', 
			//'ONCRMINVOICESETSTATUS' => 'Обновление статуса счета', 
			'ONCRMLEADADD' => 'Создание лида', 
			'ONCRMLEADUPDATE' => 'Обновление лида', 
			'ONCRMLEADDELETE' => 'Удаление лида', 
			'ONCRMDEALADD' => 'Создание сделки', 
			'ONCRMDEALUPDATE' => 'Обновление сделки', 
			'ONCRMDEALDELETE' => 'Удаление сделки', 
			//'ONCRMCOMPANYADD' => 'Создание компании', 
			//'ONCRMCOMPANYUPDATE' => 'Обновление компании', 
			//'ONCRMCOMPANYDELETE' => 'Удаление компании', 
			'ONCRMCONTACTADD' => 'Создание контакта', 
			'ONCRMCONTACTUPDATE' => 'Обновление контакта', 
			'ONCRMCONTACTDELETE' => 'Удаление контакта', 
			//'ONCRMCURRENCYADD' => 'Создание валюты', 
			//'ONCRMCURRENCYUPDATE' => 'Обновление валюты', 
			//'ONCRMCURRENCYDELETE' => 'Удаление валюты', 
			'ONCRMPRODUCTADD' => 'Создание товара', 
			'ONCRMPRODUCTUPDATE' => 'Обновление товара', 
			'ONCRMPRODUCTDELETE' => 'Удаление товара', 
			//'ONCRMACTIVITYADD' => 'Создание дела', 
			//'ONCRMACTIVITYUPDATE' => 'Обновление дела', 
			//'ONCRMACTIVITYDELETE' => 'Удаление дела', 
		];
		$data['b24_out_hooks'] = $this->config->get('b24_out_hooks');
		
		// Получение статусов заказов
		$data['oc_statuses'] = $this->model_localisation_order_status->getOrderStatuses(array());
		$data['statuses'] = $this->config->get('b24_status');
		$data['stages'] = $this->config->get('b24_stage');
		
		// Получение списка статусов из Битрикс24
		$data['b24_statuses'] = $this->getStatusList('STATUS');
		$data['b24_stages'] = $this->getStatusList('DEAL_STAGE');
		$data['b24_source'] = $this->getStatusList('SOURCE');
		$data['b24_contact_type'] = $this->getStatusList('CONTACT_TYPE');
		$data['b24_deal_type'] = $this->getStatusList('DEAL_TYPE');
		$data['source_id'] = $this->config->get('b24_customer')['settings']['SOURCE'];
		$data['retail_id'] = $this->config->get('b24_customer')['settings']['RETAIL'];
		
		// Получаем направления сделок	
		$data['b24_dealcategory'] = $this->GetDealCategoryList();
		$data['b24_deal_stage_lists'] = $this->GetDealCategoryStageList();
		
		$data['b24_order_data'] = [
			'opened' => isset($order_sett['opened']) ? $order_sett['opened'] : 0,
			'lead_text' => !empty($order_sett['text']['lead']) ? $order_sett['text']['lead'] : 'Поступил новый лид #{orderId} от {name}',
			'deal_text' => !empty($order_sett['text']['deal']) ? $order_sett['text']['deal'] : 'Поступила новая сделка #{orderId} от {name}',
			'abandoned' => isset($order_sett['abandoned']) ? $order_sett['abandoned'] : 0,
			'status_abandoned_lead' => isset($order_sett['abandoned_lead']) ? $order_sett['abandoned_lead'] : 0,
			'status_abandoned_deal' => isset($order_sett['abandoned_deal']) ? $order_sett['abandoned_deal'] : 0,
			'dealcategory' => isset($order_sett['dealcategory']) ? $order_sett['dealcategory'] : 0,
			'dealtype' => isset($order_sett['dealtype']) ? $order_sett['dealtype'] : 0,
			'status_for_upload' => isset($order_sett['status_for_upload']) ? $order_sett['status_for_upload'] : 0,
		];
		
		//Получение свойств товаров
		$data['b24_propertys'] = $this->getPropertyList();
		$data['oc_propertys'] = [
			'PROPERTY_SKU' => 'Артикул',
			'PROPERTY_MODEL' => 'Модель',
			'PROPERTY_QUANTITY' => 'Количество',
			'PROPERTY_WAREHOUSE_STATUS' => 'Статус на складе',
			'PROPERTY_MANUFACTURER' => 'Производитель',
			'PROPERTY_OPTION' => 'Опция',
		];
		
		$data['b24_productprops'] = $this->config->get('b24_productprops');
		
		//Проверка валидности хуков
		$data['connectB24'] = $this->getScopeList();
		
		//Статусы заказов при которых происходит отправка 
		if (isset($this->request->post['b24_order'])) {
			$data['b24_order_status'] = $this->request->post['b24_order'];
		} elseif ($this->config->get('b24_order')['status']) {
			$data['b24_order_status'] = $this->config->get('b24_order')['status'];
		} else {
			$data['b24_order_status'] = array();
		}
		
		// Получение user из Битрикс24
		$data['user_list'] = $this->GetUser();
		
		// Менеджер  
		$data['manager_id'] = isset($this->config->get('b24_manager')['d_manager']) ? $this->config->get('b24_manager')['d_manager'] : 1;
		$data['created_id'] = isset($this->config->get('b24_manager')['created']) ? $this->config->get('b24_manager')['created'] : 1;
		$data['order_open'] = isset($this->config->get('b24_manager')['order_open']) ? $this->config->get('b24_manager')['order_open'] : 'Y';
		$data['manager_group_id'] = isset($this->config->get('b24_manager')['manager']) ? $this->config->get('b24_manager')['manager'] : 1;
		
		$grous = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_group cg LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (cg.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		
		foreach ($grous->rows as $result) {
			$data['opencart_manager'][] = array(
				'customer_group_id' => $result['customer_group_id'],
				'name'              => $result['name'] . (($result['customer_group_id'] == $this->config->get('config_customer_group_id')) ? $this->language->get('text_default') : null),
			);
		}
		
		//Заказы
		$data['b24_order'] = $this->config->get('b24_order');
		
		// Клиенты 
		$data['b24_customer'] = $this->config->get('b24_customer')['settings'];
		$data['customer_open'] = isset($this->config->get('b24_customer')['settings']['customer_open']) ? $this->config->get('b24_customer')['settings']['customer_open'] : 'Y';
		
		//Кнопки синхронизации товаров
        $data['button_synchronizationproducts'] = $this->url->link('extension/module/b24_apipro/SyncProductToB24', 'token=' . $data['token'], 'SSL');
		$data['getProductForSync'] = count($this->model_extension_module_b24_product->getProductForSync());
		$data['button_updateproduct'] = $this->url->link('extension/module/b24_apipro/UpdateProductToB24', 'token=' . $data['token'], 'SSL');
		
		// Синхронизация клиентов
		$data['button_synchronizationcontacts'] = $this->url->link('extension/module/b24_apipro/SyncCustomerToB24', 'token=' . $data['token'], 'SSL');
		$data['getContactForSync'] = count($this->model_extension_module_b24_customer->getCustomerForSync());
				
		// Отправка заказов в битрикс 24
        $data['SendOrderToBitrix'] = $this->url->link('extension/module/b24_apipro/SendOrderToBitrix', 'token=' . $data['token'], 'SSL');
		$data['getOrderForSync'] = count($this->model_extension_module_b24_order->getOrderForSend());

		$this->response->setOutput($this->load->view('extension/module/b24_apipro.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/b24_apipro')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	
	// Добавление товаров которых нет в Б24
	public function SyncProductToB24() {
		$this->load->model('extension/module/b24_category');
		$this->load->model('extension/module/b24_product');	
		$rootCat = $this->model_extension_module_b24_category->getById(0);	
		if(empty($rootCat)){
			$this->model_extension_module_b24_category->synchronizationcategories();
		}
		//$productForSync = $this->model_extension_module_b24_product->getProductForSync();
		$productSync = $this->model_extension_module_b24_product->addProductsBatch();
		$this->session->data['success'] = 'Добавлено '.$productSync['add'].' товаров в Битрикс 24';
		$this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));	
	}
	//Обновление товаров по крону или кнопке 
	public function UpdateProductToB24(){
		$this->load->model('extension/module/b24_product');
		$pushUpdate = $this->model_extension_module_b24_product->updateProductsBatch();
		$this->session->data['success'] = 'Добавлено '.$pushUpdate['add'].', обновлено '.$pushUpdate['update'].', удалено '.$pushUpdate['delete'].' товаров в Битрикс 24';
		$this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));
	}
	// синхронизация клиентов
	public function SyncCustomerToB24(){
		$this->load->model('extension/module/b24_customer');
		$getCustomerForSync = $this->model_extension_module_b24_customer->getCustomerForSync();
		$this->model_extension_module_b24_customer->AddCustomerToB24($getCustomerForSync);
		$this->session->data['success'] = ''.count($getCustomerForSync).' клиентов перенесено в Битрикс 24';
		$this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));
	}
	//Отправка заказов в битрикс 24
	public function SendOrderToBitrix(){
		$this->load->model('extension/module/b24_order');
		$orderForSend = $this->model_extension_module_b24_order->getOrderForSend();
        $this->model_extension_module_b24_order->SendOrderToBitrix($orderForSend);
		$this->session->data['success'] = 'Отправлено '.count($orderForSend).' заказов в Битрикс 24';
		$this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));
	}
	
	public function editCustomer($route = '', $data = [], $categoryId = 0){
		$this->load->model('extension/module/b24_customer');
		if(isset($data[0]) && $data[0] != 0){
			$this->model_extension_module_b24_customer->editCustomer($data[0]);
		} else {
			return false;
		}
	}
	
	// Получение статуса по фильтру
	public function getStatusList($ENTITY_ID) {		
		$params = [
			'type' => 'crm.status.list',
			'params' => [
				'filter' => ["ENTITY_ID" => "$ENTITY_ID"]
			]
		];
		$result = $this->b24->callHook($params);
		
		if (!empty($result['error'])) {
			$this->b24->writeLog('Ошибка получения списка статусов лидов Битрикс24');
			return false;
        } else {
			return $result['result'];
		}
	}

	// Проверка вебхуков
	public function getScopeList()
	{
		$params = [
			'type' => 'scope',
			'params' => []
		];

		$result = $this->b24->callHook($params);
		if(in_array('crm', $result['result']) && in_array('im', $result['result']) && in_array('user', $result['result'])){
			return true;
		} else {
			return false;
		} 		
	}
	
	// Получение пользователей Битрикс 24
	public function GetUser(){
		$params = [
			'type' => 'user.get',
			'params' => [
				'filter' => [
					'ACTIVE' => true,
				],
				'select' => [
				'ID', 'NAME', 'LAST_NAME'
				]
			],
			
		];
		$result = $this->b24->callHook($params);
		return $result['result'];
	}

    public function add($route = '', $data = [], $categoryId = 0){
		if (!isset($data[0])) {
            return;
        }
		$this->load->model('catalog/category');
        $this->load->model('extension/module/b24_category');
		
		$category_description = $data[0]['category_description'][$this->config->get('config_language_id')];
		$categoryName = $category_description['name'];
		$parent_id = $this->model_extension_module_b24_category->getById($data[0]['parent_id']);

        //Создаем
        $params = [
            'type' => 'crm.productsection.add',
            'params' => [
                'fields' => [
                    'CATALOG_ID' => $categoryId,
                    'NAME'=> $categoryName,
                    'SECTION_ID'=> $parent_id['b24_category_id'],
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
        $b24Id = $result['result'];

        $data = ['oc_category_id' => $categoryId, 'b24_category_id' => $b24Id];
        $this->model_extension_module_b24_category->addToDB($data);

        return $b24Id;
    }

    public function edit($route = '', $data = [], $param3 = 0){		
		if (!isset($data[0])) {
            return;
        }
		
        $this->load->model('extension/module/b24_category');

        $categoryId = !empty($data[0]) ? $data[0] : 0;
		$category_description = $data[1]['category_description'][$this->config->get('config_language_id')];
		$categoryName = $category_description['name'];
		$b24_cat_id = $this->model_extension_module_b24_category->getById($categoryId)['b24_category_id'];
        $parentId = $this->model_extension_module_b24_category->getById($data[1]['parent_id'])['b24_category_id'];
		
		$params = [
                'type' => 'crm.productsection.update',
                'params' => [
                    'id' => $b24_cat_id,
                    'fields' => [
                        'NAME'=> $categoryName,
                        'SECTION_ID'=> $parentId,
                    ]
                ]
            ];

		$result = $this->b24->callHook($params);

        if (!empty($result['error_description'])) {
		   $this->log->write(print_r($result['error_description'],true));
        }
    }

    public function delete($route = '', $data = [], $param3 = 0) {		
        if (!isset($data[0])) {
            return;
        }
		$this->load->model('extension/module/b24_category');

        $categoryId = !empty($data[0]) ? $data[0] : '';
        
        $categoryRow = $this->model_extension_module_b24_category->getById($categoryId);
        $b24_category_id = isset($categoryRow['b24_category_id']) ? $categoryRow['b24_category_id'] : '';

        if (!empty($b24_category_id)) {
            $params = [
                'type' => 'crm.productsection.delete',
                'params' => [
                    'id' => $b24_category_id,
                ]
            ];
            $result = $this->b24->callHook($params);
			$this->model_extension_module_b24_category->dellToDB($categoryId);
        }
    }

    // Product ?
    /*
	public function synchronizationproductsbatch()
    {
        $this->load->model('extension/module/b24_product');
        $this->model_extension_module_b24_product->addProductsBatch();

        $this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));
    }
*/
	
    // Contacts ?
    /*
	public function synchronizationcontacts(){
        $this->load->model('extension/module/b24_customer');
		
        $this->model_extension_module_b24_customer->cynchContacts($this->model_extension_module_b24_customer->getContacts());

        $this->response->redirect($this->url->link('extension/module/b24_apipro', 'token=' . $this->session->data['token'], 'SSL'));
    }
*/

    public function addproduct($route = '', $data = [], $productId = 0) {
		if (!isset($data[0])) {
            return;
        }
        $this->load->model('extension/module/b24_product');
        $dataToAdd = $this->model_extension_module_b24_product->prepareDataToB24($productId);
        $this->model_extension_module_b24_product->addProductBatch($productId, $dataToAdd);
    }

    public function editproduct($route = '', $data = [], $param3 = 0) {
        if (!isset($data[0])) {
            return;
        }

        $productId = !empty($data[0]) ? $data[0] : 0;
        $this->load->model('extension/module/b24_product');
        $dataToUpdate = $this->model_extension_module_b24_product->prepareDataToB24($productId);
        $this->model_extension_module_b24_product->editProductBatch($productId, $dataToUpdate);
    }

    public function deleteproduct($route = '', $data = [], $param3 = 0) {
        if (!isset($data[0])) {
            return;
        }

        $productId = !empty($data[0]) ? $data[0] : 0;
        $this->load->model('extension/module/b24_product');
        $this->model_extension_module_b24_product->deleteProduct($productId);
    }

    public function getFileToImport() {
        $this->load->model('catalog/product');
        $this->load->model('module/b24_product');
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/b24_api/product_import.csv';
        $file = fopen($filename, 'w+');

        $productList = $this->model_catalog_product->getProducts(['sort' => 'product_id']);
        //$index = 1;
        $columnNames = [];
        foreach ($productList as $key => $product) {
            $dataToCSV = $this->model_extension_module_b24_product->prepareDataToB24($product['product_id']);

            foreach ($dataToCSV as $sizeName => $newData) {
                unset($newData['DESCRIPTION']);

                if (empty($columnNames)) {
                    $columnNames = array_keys($newData);
                    fputcsv($file, $columnNames, ';', '"');
                }

                $newData['MEASURE'] = 'шт';
                //$newData['ID'] = $index;
                fputcsv($file, $newData, ';', '"');
                //$index++;
            }

            //if($key == 10){
            //	break;
            //}
        }

        if (file_exists($filename)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($filename).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));
            readfile($filename);
            exit;
        }
    }

    public function exportToB24() {
        $this->load->model('catalog/product');
        $this->load->model('module/b24_product');

        //$filename = $_SERVER['DOCUMENT_ROOT'] . '/b24_api/product_import.csv';
        //$file = fopen($filename, 'r');
        //$columnNames = fgetcsv($file, false, ';');

        //$columnNames = array_flip($columnNames);
        //$PROPERTY_OC_ID = $this->model_extension_module_b24_product->PROPERTY_OC_ID;
        $index = 0;
        //$productList = $this->model_catalog_product->getProducts(['limit' => 1]);
        $productList = $this->model_catalog_product->getProducts();

        foreach ($productList as $product) {
            $dataToB24 = $this->model_extension_module_b24_product->prepareDataToB24($product['product_id']);

            foreach ($dataToB24 as $sizeName => $newData) {
                //$dataFromCsv = array_combine($columnNames, $dataFromCsv);

                $PROPERTY_OC_ID = $this->model_extension_module_b24_product->PROPERTY_OC_ID;
                $PROPERTY_SIZE = $this->model_extension_module_b24_product->PROPERTY_SIZE;
                $name = $newData[$PROPERTY_OC_ID] . ';' . $newData[$PROPERTY_SIZE];

                $b24Row = $this->model_extension_module_b24_product->getList([
                    'oc_product_id' => $newData[$PROPERTY_OC_ID],
                    'size' => $newData[$PROPERTY_SIZE],
                ]);

                $isAlreadyAdded = !empty($b24Row['b24_product_id'][0]);
                if ($isAlreadyAdded) {
                    $dataToUpdate[$name] = "crm.product.update?id={$b24Row['b24_product_id']}&" . http_build_query(['fields' => $newData]);
                } else {
                    $dataToAdd[$name] = 'crm.product.add?' . http_build_query(['fields' => $newData]);
                }

                $index++;

                if (isset($dataToUpdate) && count($dataToUpdate) > 45) {
                    $result = $this->model_extension_module_b24_product->sendBatchQuery($dataToUpdate);
                    $this->model_extension_module_b24_product->updateToDB(1, $result['result']);
                    $this->printMessage('update', $dataToUpdate);
                    $dataToUpdate = [];
                    usleep(50000);
                }

                if (isset($dataToAdd) && count($dataToAdd) > 45) {
                    $result = $this->model_extension_module_b24_product->sendBatchQuery($dataToAdd);
                    $this->model_extension_module_b24_product->addToDB(1, $result['result']);
                    $this->printMessage('add', $dataToAdd);
                    $dataToAdd = [];
                    usleep(50000);
                }
            }

            if ($index >= 10) break;
        }

        //while ( $dataFromCsv = fgetcsv($file, false, ';') )
        //{
        //
        //}

        // остаточные товары
        if (!empty($dataToUpdate)) {
            $result = $this->model_extension_module_b24_product->sendBatchQuery($dataToUpdate);
            $this->model_extension_module_b24_product->updateToDB(1, $result['result']);
            $this->printMessage('update', $dataToUpdate);
        }

        if (!empty($dataToAdd)) {
            $result = $this->model_extension_module_b24_product->sendBatchQuery($dataToAdd);
            $this->model_extension_module_b24_product->addToDB(1, $result['result']);
            $this->printMessage('add', $dataToAdd);
        }

        $index = 0;
    }

    public function printMessage($type, array $data) {
        foreach ( $data as $sizeName => $item )
        {
            list( $productId, $size) = explode(';', $sizeName);
            $msg = $type == 'add'
                ? "<br> Добавлен товар с ИД $productId размер $size"
                : "<br> Обновлен товар с ИД $productId размер $size"
            ;

            echo $msg;
            flush();
        }

    }

    public function crmProductList() {		
        $this->load->model('extension/module/b24_product');
        $propertyProductId = $this->model_extension_module_b24_product->PROPERTY_OC_ID;
        $propertySize = $this->model_extension_module_b24_product->PROPERTY_SIZE;

        $params = [
            'type' => 'crm.product.list',
            'params' => [
                'start' => 0,
                'order' => ['ID' => "ASC"],
                'select' => ['ID', 'PROPERTY_*']
            ],
        ];

        do {
            $result = $this->b24->callHook($params);
            foreach ($result['result'] as $product) {
                $fields = [ 'b24_product_id' => $product['ID'],
                    'oc_product_id' => $product[$propertyProductId]['value'],
                    'size' => $product[$propertySize]['value']
                ];

                $this->model_extension_module_b24_product->insertToDB('b24_product', $fields);
            }

            $params['params']['start'] = $result['next'];
        } while(!empty($result['next']));

        return $result;
    }
	public function parseHookURL($url) {
		if(empty($url)){
			return;
		}
		$output = explode("/", $url);
		$auth = [
			'b24_key_domain' => $output[2],
			'b24_key_id' => $output[4],
			'b24_key_api' => $output[5],
		];
		return $auth;
	}
	
	// Получаем своства товаров
	public function getPropertyList() {		
		$params = [
			'type' => 'crm.product.property.list',
			'params' => [
				'order' => ["SORT" => "ASC"],
				'filter' => ["MANDATORY"=> "N"]
			]
		];
		$result = $this->b24->callHook($params);
		return $result['result'];
	}
	
	// Получаем направления сделок
	public function GetDealCategoryList() {		
		$params = [
			'type' => 'crm.dealcategory.list',
			'params' => [
				'filter' => ["IS_LOCKED" => "N"]
			]
		];
		$result = $this->b24->callHook($params);
		return $result['result'];
	}
	
	// Получаем статусы сделок
	public function GetDealCategoryStageList() {		
		$catlist = $this->GetDealCategoryList();
		
		if(!empty($catlist)){
			foreach ($catlist as $key => $stage) {
				$stagelist['cmd'][$key] = 'crm.dealcategory.stage.list?'. http_build_query(['id' => $stage['ID']]);
			}
			
			$params = [
						'type' => 'batch',
						'params' => $stagelist
					];
			
			$result = $this->b24->callHook($params);
			return $result['result']['result'];
		}
		
	}	
}