<?php
class ControllerExtensionModuleSuperDruidPremiumDBIndexes extends Controller {

public function install() {
	$this->load->model('extension/superdruid/premium_db_indexes');
	$this->model_extension_superdruid_premium_db_indexes->install();
}

public function uninstall() {
	$this->load->model('extension/superdruid/premium_db_indexes');
	$this->model_extension_superdruid_premium_db_indexes->uninstall();
}

	
	public function index() {
		$this->load->language('extension/module/superdruid_premium_db_indexes');
		$this->load->model('extension/superdruid/premium_db_indexes');
		

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/superdruid_premium_db_indexes', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);

		/*
		$this->template = 'module/superdruid_premium_db_indexes.tpl';
		$this->children = array(
			'common/header',
			'common/footer',
		);
		*/
		
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$data['error_warning'] = '';
		/*
		if(property_exists($this, 'data'))
			{
			$tmp = $this->data;
			$merged = array_merge($tmp, $data);
			$this->data = $merged;
			}
		
		if(method_exists($this, 'render'))
			$this->response->setOutput($this->render());
		*/
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/superdruid_premium_db_indexes.tpl', $data));
		
	}
	


}
?>
