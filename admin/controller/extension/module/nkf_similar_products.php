<?php
/*
@author  nikifalex
@skype   logoffice1
@email    nikifalex@yandex.ru
*/

class ControllerExtensionModuleNkfSimilarProducts extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/nkf_similar_products');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('extension/module');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (!isset($this->request->get['module_id'])) {
				$this->model_extension_module->addModule('nkf_similar_products', $this->request->post);
			} else {
				$this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_select_all'] = $this->language->get('text_select_all');
        $data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_limit'] = $this->language->get('entry_limit');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_height'] = $this->language->get('entry_height');
		$data['entry_status'] = $this->language->get('entry_status');
        $data['entry_use_category'] = $this->language->get('entry_use_category');
        $data['entry_use_manufacturer'] = $this->language->get('entry_use_manufacturer');
        $data['entry_add_diff_attributes'] = $this->language->get('entry_add_diff_attributes');
        $data['entry_excluded_attributes'] = $this->language->get('entry_excluded_attributes');
        $data['entry_cnt_diff'] = $this->language->get('entry_cnt_diff');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['width'])) {
			$data['error_width'] = $this->error['width'];
		} else {
			$data['error_width'] = '';
		}

		if (isset($this->error['height'])) {
			$data['error_height'] = $this->error['height'];
		} else {
			$data['error_height'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL')
		);

		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/nkf_similar_products', 'token=' . $this->session->data['token'], 'SSL')
			);
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/nkf_similar_products', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL')
			);
		}

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/nkf_similar_products', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$data['action'] = $this->url->link('extension/module/nkf_similar_products', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], 'SSL');
		}

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
		}

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} elseif (!empty($module_info) && isset($module_info['name'])) {
			$data['name'] = $module_info['name'];
		} else {
			$data['name'] = '';
		}

		if (isset($this->request->post['limit'])) {
			$data['limit'] = $this->request->post['limit'];
		} elseif (!empty($module_info) && isset($module_info['limit'])) {
			$data['limit'] = $module_info['limit'];
		} else {
			$data['limit'] = 5;
		}

        if (isset($this->request->post['use_category'])) {
            $data['use_category'] = $this->request->post['use_category'];
        } elseif (!empty($module_info) && isset($module_info['use_category'])) {
            $data['use_category'] = $module_info['use_category'];
        } else {
            $data['use_category'] = 0;
        }
        if (isset($this->request->post['use_manufacturer'])) {
            $data['use_manufacturer'] = $this->request->post['use_manufacturer'];
        } elseif (!empty($module_info) && isset($module_info['use_manufacturer'])) {
            $data['use_manufacturer'] = $module_info['use_manufacturer'];
        } else {
            $data['use_manufacturer'] = 0;
        }
        if (isset($this->request->post['add_diff_attributes'])) {
            $data['add_diff_attributes'] = $this->request->post['add_diff_attributes'];
        } elseif (!empty($module_info) && isset($module_info['add_diff_attributes'])) {
            $data['add_diff_attributes'] = $module_info['add_diff_attributes'];
        } else {
            $data['add_diff_attributes'] = 1;
        }
        if (isset($this->request->post['cnt_diff'])) {
            $data['cnt_diff'] = $this->request->post['cnt_diff'];
        } elseif (!empty($module_info) && isset($module_info['cnt_diff'])) {
            $data['cnt_diff'] = $module_info['cnt_diff'];
        } else {
            $data['cnt_diff'] = 3;
        }

        if (isset($this->request->post['excluded_attributes'])) {
            $data['excluded_attributes'] = $this->request->post['excluded_attributes'];
        } elseif (!empty($module_info) && isset($module_info['excluded_attributes'])) {
            $data['excluded_attributes'] = $module_info['excluded_attributes'];
        } else {
            $data['excluded_attributes'] = array();
        }


        $this->load->model('catalog/attribute');

        $data['attributes'] = $this->model_catalog_attribute->getAttributes();


		if (isset($this->request->post['width'])) {
			$data['width'] = $this->request->post['width'];
		} elseif (!empty($module_info) && isset($module_info['width'])) {
			$data['width'] = $module_info['width'];
		} else {
			$data['width'] = 200;
		}

		if (isset($this->request->post['height'])) {
			$data['height'] = $this->request->post['height'];
		} elseif (!empty($module_info) && isset($module_info['height'])) {
			$data['height'] = $module_info['height'];
		} else {
			$data['height'] = 200;
		}

		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($module_info) && isset($module_info['status'])) {
			$data['status'] = $module_info['status'];
		} else {
			$data['status'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/nkf_similar_products', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/nkf_similar_products')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!$this->request->post['width']) {
			$this->error['width'] = $this->language->get('error_width');
		}

		if (!$this->request->post['height']) {
			$this->error['height'] = $this->language->get('error_height');
		}

		return !$this->error;
	}
}