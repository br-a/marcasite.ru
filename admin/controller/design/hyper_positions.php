<?php
class ControllerDesignHyperPositions extends Controller {

    private $_text_string = array(

        'hp_name_titles',
        'hp_name',
        'width_title',
        'text_pos_left_center',
        'text_pos_center',
        'text_pos_right_center',
        // TOP HEADER
        'text_header_pos',
        // HEADER
        'text_top_menus',
        'text_headers',
        'text_menus',
        // TOP
        'text_pos_sliders',
        'text_top_pos',
        'text_top1_pos',
        'text_top2_pos',
        'text_banner_pos',
        'text_top3_pos',
        'text_top4_pos',
        // CONTENT
        'text_content_pos',
        'text_banner2_pos',
        'text_content2_pos',
        // BOTTOM
        'text_banner3_pos',
        'text_bottom_pos',
        'text_bottom1_pos',
        'text_bottom2_pos',
        'text_bottom3_pos',
        'text_banner4_pos',
        'text_bottom4_pos',
        // FOOTER
        'text_footer_pos',
        'text_map_pos',
        'text_footer2_pos',
        'text_footer3_pos',
        'text_footers',
        'text_footer4_pos',
        'text_footer5_pos',
        'text_copyright_pos',
        'text_copyright2_pos',

    );


    public function index() {

        $this->document->addStyle('view/javascript/hyper-extension/css/hyper-extension.css');
        $this->document->addStyle('view/javascript/hyper-extension/css/bootstrap-switch.min.css');

        $this->document->addScript('view/javascript/hyper-extension/js/bootstrap-switch.min.js');

        $this->load->language('design/hyper_positions');

        foreach ($this->_text_string as $text) {
            $data[$text] = $this->language->get($text);
        }

        return $data;

    }

    public function validate($full_width = array() ){

        $full_width['width'] = (int)$full_width['width'];

        return $full_width;
    }

}
