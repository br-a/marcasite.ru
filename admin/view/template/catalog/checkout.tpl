<?=$header?>
<?=$column_left?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="hidden">
                <button type="submit" form="form-attribute" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1>Заказ</h1>

        </div>
    </div>



    <div class="container-fluid">
    <form action="/admin/index.php?route=catalog/checkout/order&token=<?=$_GET['token']?>" method="POST">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Информация о покупателе</h3>
            </div>
            <div class="panel-body">

        <div class="col-lg-2">
        <label for="">

            Покупатель
            <select required class="form-control" name="customer_id" id="customer">
                <option value="0">Выбирите покупателя</option>
                <?php foreach($customers as $customer) { ?>
                <option value="<?=$customer['customer_id']?>" <?php if($customer_id==$customer['customer_id']){ ?> selected <?php } ?> ><?=$customer['name']?></option>
                <?php } ?>

            </select>

        </label>
        </div>

                <div class="choose col-lg-3">
                    <label>Выбирите адрес из списка( после выбора покупателя)
                    <select class="form-control ch"></select>
                    </label>
                </div>

                <div class="address col-lg-3">
                    <label for="">
                        Город
                    <input type="text" class="form-control" class="city" name="city">
                    </label>
                    <label for="">
                        Адрес
                    <input type="text" class="form-control" class="address_1" name="address_1">
                    </label>



                </div>

                <div class="type col-lg-3">
                    <label style="margin-right: 5px;" for="">Предложить клиенту
                        <input name="action" type="radio" class="form-control" value="2" >
                    </label>
                    <label for="">Оформить заказ
                        <input name="action" type="radio" class="form-control" value="1" checked >
                    </label>


                </div>
        <div class="col-lg-3">
            <input type="text" name="coupon" placeholder="Купон" value="" id="input-coupon" class="form-control">
        </div>
                <div class="pull-right" style="padding: 10px">
                    <input type="submit" class="btn btn-primary" value="Отправить">
                    <a href="<?=$clear?>" class="btn btn-danger">Очистить корзину</a>
                </div>

</div>


    </form>
        </div>



<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Информация о заказе</h3>
    </div>
<div class="panel-body">
   <table class="table table-responsive table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th style="width:200px;">Картинка</th>
        <th>Товар</th>
        <th>Модель</th>
        <th>Цена за ед.</th>
        <th>Количество</th>
<th>Цена</th>
        <th>Действие</th>
    </tr>

    </thead>
       <tbody>
<?php if($products) { ?>
       <?php foreach($products as $product){ ?>
       <tr>
           <td><input type="checkbox" name="product_id[]" value="<?=$product['product_id']?>"></td>
           <td><img src="<?=$product['thumb']?>" alt="">  </td>
           <td>
               <?=$product['name']?>
               <?php foreach ($product['option'] as $option) { ?>
               <br />
               <?php if ($option['type'] != 'file') { ?>
               &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
               <?php } else { ?>
               &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
               <?php } ?>
               <?php } ?>


           </td>

           <td>
               <?=$product['model']?>
           </td>
<td>    <?php if($product['special']) { ?>

    <s><?=$product['price']?></s>
    <?=$product['special']?>
    <?php } else { ?>
    <?=$product['price']?>
    <?php } ?></td>
           <td>
               <input name="quantity" data-product-id="<?=$product['cart_id']?>"  class="quantity form-control" type="number" min="1" max="<?=$product['max']?>" value="<?=$product['quantity']?>">

           </td>
           <td  data-price="<?php if($product['special']) { ?> <?=$product['special']?> <?php } else { ?> <?=$product['price']?><?php } ?>"><?=$product['total']?></td>
           <td><a href="<?=$product['delete']?>"><button class="btn btn-danger" value="<?=$product['cart_id']?>"><i class="fa fa-trash"></i></button></a> </td>


       </tr>


       <?php } ?>
       </tbody>
<tfooter id="totals">
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

        <td ><span>Скидка</span></td>



        <td ><?=$skidkaa?></td>

        <td></td>
    </tr>    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

        <td ><span>Дополнительная скидка</span></td>



        <td ><input type="text" value="<?=$skidka?>" name="skidka"> </td>

        <td></td>
    </tr>


    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>

    <td id="quant"><span><?=$total_quantity?></span></td>



    <td id="total"><?=$total?></td>

    <td></td>
    </tr>

</tfooter>
<?php } else { ?>
Добавьте товар в корзину
       <?php } ?>



   </table>
</div>

</div>



    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Информация о предложениях</h3>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                    <td>Покупатель</td>
                    <td>Товар</td>
                    <td>Дата создания</td>
                    <td>Действие</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($preorders as $order){ ?>
                    <tr>
<td><?=($order['name'])?>
<br>
    <?=$order['city']?> <?=$order['address_1']?>

</td>
                        <td>
                            <ul class="list-group">
                                <?php foreach($order['products'] as $product) { ?>

                                <li class="list-group-item"><img src="<?=$product['thumb']?>">  <?=$product['name']?></li>
                                <?php } ?>

                            </ul>
                        </td>

                        <td class="text-right"><?=$order['date_added']?></td>
                        <td><a href="<?=$order['link']?>"><button class="btn btn-default"><i class="fa fa-cart">ОФормить в заказ</i></button></a>
                            <a href="<?=$order['link2']?>"><button class="btn btn-default"><i class="fa fa-cart">Открыть</i></button></a>
                            <a href="<?=$order['link3']?>"><button class="btn btn-danger"><i class="fa fa-cart">Удалить</i></button></a>

                        </td>
                    </tr>
                    <?php } ?>

                    </tbody>
                </table>
            </div>




        </div>
    </div>



</div>

</div>


<script>
    $("#customer").change(function(){

        $.ajax({
            url:"/admin/index.php?route=catalog/checkout/getcustomerinfo&customer_id="+$(this).val()+"&token=<?=$token?>",
            dataType:"json",
            success:function(json){
                html="";

                var last_key;
                var i=0;
                $.each(json, function( key, value ) {
                    if(i==0)
                        last_key=key;
                    i++;
                    html+= "<option value='"+key+"' >"+value.city+" "+value.address_1+"  </option>";
                });


                $("[name=city]").val(json[last_key]['city']);
                $("[name=address_1]").val(json[last_key]['address_1']);

                $(".choose").find('select').html(html);
            }

        });

    });


    $(".ch").change(function(){
        var obj=$(this);
        $.ajax({
            url:"/admin/index.php?route=catalog/checkout/getcustomerinfo&customer_id="+$("#customer").val()+"&token=<?=$token?>",
            dataType:"json",
            success:function(json){
            console.log(obj.val());
                $("[name=city]").val(json[obj.val()]['city']);
                $("[name=address_1]").val(json[obj.val()]['address_1']);



            }

        });

    });



</script>

<script>

    $("[name=quantity]").change(function(){
        var quantity=$(this).val();
        var product_id=$(this).attr('data-product-id');
        var input=$(this);
        $.ajax({
            url:"/admin/index.php?route=catalog/checkout/update&product_id="+product_id+"&quantity="+quantity+"&token=<?=$token?>",
            success:function(){
                var price=input.parent().next().data('price');
                input.parent().next().text(price*quantity);
                $("#quant").load(location.href+" #quant");
                $("#total").load(location.href+" #total");
            }
        });



    });

</script>
<?php if($customer_id){ ?>
<script>

    $("#customer").change();
</script>

<?php } ?>
<script>
    $(document).ready(function() {
        $("form").eq(0).submit(function (e) {
            console.log($("#customer").val());
            if ($("#customer").val() == 0) {
                alert("Выберите покупателя");
                e.preventDefault();

                return false;
            }

        });
    });
</script>
<script>
    $("[name=skidka]").change(function(){
        var skidka=$(this).val();
        $.ajax({
            url:"https://marcasite.ru/admin/index.php?route=catalog/checkout/skidka&token=<?=$_GET['token']?>",
            type:"POST",
            data:"skidka="+skidka,
            success:function(){
                $("[name=quantity]").change();
            }
        });

    });
</script>
<?=$footer?>