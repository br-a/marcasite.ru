<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-layout" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid  h-editor">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-layout" class="form-horizontal">
          <fieldset>
            <legend><?php echo $text_route; ?></legend>
            <div class="form-group required">
              <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
              <div class="col-sm-10">
                <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                <?php if ($error_name) { ?>
                <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>
              </div>
            </div>
            <table id="route" class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left"><?php echo $entry_store; ?></td>
                  <td class="text-left"><?php echo $entry_route; ?></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                <?php $route_row = 0; ?>
                <?php foreach ($layout_routes as $layout_route) { ?>
                <tr id="route-row<?php echo $route_row; ?>">
                  <td class="text-left"><select name="layout_route[<?php echo $route_row; ?>][store_id]" class="form-control">
                      <option value="0"><?php echo $text_default; ?></option>
                      <?php foreach ($stores as $store) { ?>
                      <?php if ($store['store_id'] == $layout_route['store_id']) { ?>
                      <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select></td>
                  <td class="text-left"><input type="text" name="layout_route[<?php echo $route_row; ?>][route]" value="<?php echo $layout_route['route']; ?>" placeholder="<?php echo $entry_route; ?>" class="form-control" /></td>
                  <td class="text-center"><button type="button" onclick="$('#route-row<?php echo $route_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                </tr>
                <?php $route_row++; ?>
                <?php } ?>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"></td>
                  <td class="text-center"><button type="button" onclick="addRoute();" data-toggle="tooltip" title="<?php echo $button_route_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                </tr>
              </tfoot>
            </table>
          </fieldset>
          <fieldset>
            <legend><?php echo $text_module; ?>&emsp;
				<a target="_blank" data-toggle="tooltip" title="" data-original-title="<?php echo $hp_name_titles; ?>" class="pull-right btn btn-danger" href="https://opencartforum.com/user/28051-awaro/" role="button">
					<i class="fa fa-cogs" aria-hidden="true"></i>&nbsp; <?php echo $hp_name; ?></a>
			</legend>
            <?php $module_row = 0; ?>
            <div class="row hyper-positions">
              <div class="col-sm-12 top-header">
                <div class="row">
                <div class="text-center col-sm-12">
                    <div class="col-sm-12 text-center positions-name"><?php echo $text_header_pos; ?>
                        <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                            <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                   name="hyper_positions_width[top_header]" data-size="small" data-on-color="success"
                                <?php if(isset($hyper_positions_width['top_header']) && $hyper_positions_width['top_header'] == 1){ echo 'checked';}?> >
                        </div>
                    </div>
                </div>  
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <table id="module-header-left" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-center"><?php echo $text_column_left; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($layout_modules as $layout_module) { ?>
                                <?php if ($layout_module['position'] == 'header_left') { ?>
                                    <tr id="module-row<?php echo $module_row; ?>">
                                        <td class="text-left"><div class="input-group">
                                                <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                    <?php foreach ($extensions as $extension) { ?>
                                                        <optgroup label="<?php echo $extension['name']; ?>">
                                                            <?php if (!$extension['module']) { ?>
                                                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                    <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <?php foreach ($extension['module'] as $module) { ?>
                                                                    <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                        <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </optgroup>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                                <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                                </div>
                                            </div></td>
                                    </tr>
                                    <?php $module_row++; ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="text-left"><div class="input-group">
                                        <select class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" onclick="addModule('header-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <table id="module-header-cl" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($layout_modules as $layout_module) { ?>
                                <?php if ($layout_module['position'] == 'header_cl') { ?>
                                    <tr id="module-row<?php echo $module_row; ?>">
                                        <td class="text-left"><div class="input-group">
                                                <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                    <?php foreach ($extensions as $extension) { ?>
                                                        <optgroup label="<?php echo $extension['name']; ?>">
                                                            <?php if (!$extension['module']) { ?>
                                                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                    <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <?php foreach ($extension['module'] as $module) { ?>
                                                                    <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                        <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </optgroup>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                                <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                                </div>
                                            </div></td>
                                    </tr>
                                    <?php $module_row++; ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="text-left"><div class="input-group">
                                        <select class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" onclick="addModule('header-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <table id="module-header-cr" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($layout_modules as $layout_module) { ?>
                                <?php if ($layout_module['position'] == 'header_cr') { ?>
                                    <tr id="module-row<?php echo $module_row; ?>">
                                        <td class="text-left"><div class="input-group">
                                                <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                    <?php foreach ($extensions as $extension) { ?>
                                                        <optgroup label="<?php echo $extension['name']; ?>">
                                                            <?php if (!$extension['module']) { ?>
                                                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                    <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <?php foreach ($extension['module'] as $module) { ?>
                                                                    <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                        <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </optgroup>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                                <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                                </div>
                                            </div></td>
                                    </tr>
                                    <?php $module_row++; ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="text-left"><div class="input-group">
                                        <select class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" onclick="addModule('header-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12">
                        <table id="module-header-right" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-center"><?php echo $text_column_right; ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($layout_modules as $layout_module) { ?>
                                <?php if ($layout_module['position'] == 'header_right') { ?>
                                    <tr id="module-row<?php echo $module_row; ?>">
                                        <td class="text-left"><div class="input-group">
                                                <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                    <?php foreach ($extensions as $extension) { ?>
                                                        <optgroup label="<?php echo $extension['name']; ?>">
                                                            <?php if (!$extension['module']) { ?>
                                                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                    <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                                <?php } else { ?>
                                                                    <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                                <?php } ?>
                                                            <?php } else { ?>
                                                                <?php foreach ($extension['module'] as $module) { ?>
                                                                    <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                        <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                    <?php } else { ?>
                                                                        <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </optgroup>
                                                    <?php } ?>
                                                </select>
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                                <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                                <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                                </div>
                                            </div></td>
                                    </tr>
                                    <?php $module_row++; ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td class="text-left"><div class="input-group">
                                        <select class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <div class="input-group-btn">
                                            <button type="button" onclick="addModule('header-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
              </div>
              <div class="col-sm-12 pos-header">
                  <div class="col-sm-12 text-center header-menu positions-name"><?php echo $text_top_menus; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[header_menu]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['header_menu']) && $hyper_positions_width['header_menu'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
                  <div class="col-sm-12 text-center content-header positions-name"><?php echo $text_headers; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[content_header]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['content_header']) && $hyper_positions_width['content_header'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
                  <div class="col-sm-12 text-center general-menu positions-name"><?php echo $text_menus; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[general_menu]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['general_menu']) && $hyper_positions_width['general_menu'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
              </div>
              <div class="col-sm-12 pos-sliders">
                <div class="col-sm-12 text-center positions-name"><?php echo $text_pos_sliders; ?>
                    <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                        <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                               name="hyper_positions_width[pos_sliders]" data-size="small" data-on-color="success"
                            <?php if(isset($hyper_positions_width['pos_sliders']) && $hyper_positions_width['pos_sliders'] == 1){ echo 'checked';}?> >
                    </div>
                </div>
                <table id="module-pos-sliders" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <td class="text-center"></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'pos_sliders') { ?>
                            <tr id="module-row<?php echo $module_row; ?>">
                                <td class="text-left"><div class="input-group">
                                        <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                            <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                        <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                        <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            <?php $module_row++; ?>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td class="text-left"><div class="input-group">
                                <select class="form-control input-sm">
                                    <?php foreach ($extensions as $extension) { ?>
                                        <optgroup label="<?php echo $extension['name']; ?>">
                                            <?php if (!$extension['module']) { ?>
                                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                            <?php } else { ?>
                                                <?php foreach ($extension['module'] as $module) { ?>
                                                    <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </optgroup>
                                    <?php } ?>
                                </select>
                                <div class="input-group-btn">
                                    <button type="button" onclick="addModule('pos-sliders');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                </div>
                            </div></td>
                    </tr>
                    </tfoot>
                </table>
              </div>
              <div class="col-sm-12 pos-top">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_top_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_top]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_top']) && $hyper_positions_width['pos_top'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <table id="module-top-left" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"><?php echo $text_column_left; ?></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'top_left') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('top-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <table id="module-top-center" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_pos_center; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'top_center') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('top-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                    <table id="module-top-right" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_column_right; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'top_right') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('top-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-top1">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_top1_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_top1]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_top1']) && $hyper_positions_width['pos_top1'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top1-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top1_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top1-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top1-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top1_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top1-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top1-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top1_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top1-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top1-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top1_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top1-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-top2">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_top2_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_top2]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_top2']) && $hyper_positions_width['pos_top2'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-top2-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top2_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top2-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-top2-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top2_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top2-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-top2-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top2_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top2-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-banner">
                <div class="col-sm-12 text-center positions-name"><?php echo $text_banner_pos; ?>
                    <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                        <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                               name="hyper_positions_width[pos_banner]" data-size="small" data-on-color="success"
                            <?php if(isset($hyper_positions_width['pos_banner']) && $hyper_positions_width['pos_banner'] == 1){ echo 'checked';}?> >
                    </div>
                </div>
                <table id="module-pos-banner" class="table table-striped table-bordered table-hover">
                    <thead>
                      <tr>
                        <td class="text-center"></td>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'pos_banner') { ?>
                            <tr id="module-row<?php echo $module_row; ?>">
                                <td class="text-left"><div class="input-group">
                                        <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                            <?php foreach ($extensions as $extension) { ?>
                                                <optgroup label="<?php echo $extension['name']; ?>">
                                                    <?php if (!$extension['module']) { ?>
                                                        <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                            <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } else { ?>
                                                        <?php foreach ($extension['module'] as $module) { ?>
                                                            <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                        <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                        <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                        <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                            <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                        </div>
                                    </div></td>
                            </tr>
                            <?php $module_row++; ?>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td class="text-left"><div class="input-group">
                                <select class="form-control input-sm">
                                    <?php foreach ($extensions as $extension) { ?>
                                        <optgroup label="<?php echo $extension['name']; ?>">
                                            <?php if (!$extension['module']) { ?>
                                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                            <?php } else { ?>
                                                <?php foreach ($extension['module'] as $module) { ?>
                                                    <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </optgroup>
                                    <?php } ?>
                                </select>
                                <div class="input-group-btn">
                                    <button type="button" onclick="addModule('pos-banner');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                </div>
                            </div></td>
                      </tr>
                    </tfoot>
                </table>
              </div>
              <div class="col-sm-12 pos-top3">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_top3_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_top3]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_top3']) && $hyper_positions_width['pos_top3'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top3-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top3_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top3-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top3-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top3_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top3-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top3-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top3_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top3-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-top3-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'top3_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('top3-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-content">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_content_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_content]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_content']) && $hyper_positions_width['pos_content'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                    <table id="module-column-left" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_column_left; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'column_left') { ?>
                        <tr id="module-row<?php echo $module_row; ?>">
                          <td class="text-left"><div class="input-group">
                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } ?>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <?php if ($module['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                        <?php $module_row++; ?>
                        <?php } ?>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                              <select class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <div class="input-group-btn">
                                <button type="button" onclick="addModule('column-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                    <table id="module-content-top" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_content_top; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'content_top') { ?>
                        <tr id="module-row<?php echo $module_row; ?>">
                          <td class="text-left"><div class="input-group">
                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } ?>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <?php if ($module['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                        <?php $module_row++; ?>
                        <?php } ?>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                              <select class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <div class="input-group-btn">
                                <button type="button" onclick="addModule('content-top');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                      </tfoot>
                    </table>
                    <table id="module-content-bottom" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_content_bottom; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'content_bottom') { ?>
                        <tr id="module-row<?php echo $module_row; ?>">
                          <td class="text-left"><div class="input-group">
                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } ?>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <?php if ($module['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                        <?php $module_row++; ?>
                        <?php } ?>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                              <select class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <div class="input-group-btn">
                                <button type="button" onclick="addModule('content-bottom');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                    <table id="module-column-right" class="table table-striped table-bordered table-hover">
                      <thead>
                        <tr>
                          <td class="text-center"><?php echo $text_column_right; ?></td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($layout_modules as $layout_module) { ?>
                        <?php if ($layout_module['position'] == 'column_right') { ?>
                        <tr id="module-row<?php echo $module_row; ?>">
                          <td class="text-left"><div class="input-group">
                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <?php if ($extension['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } ?>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <?php if ($module['code'] == $layout_module['code']) { ?>
                                <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                        <?php $module_row++; ?>
                        <?php } ?>
                        <?php } ?>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td class="text-left"><div class="input-group">
                              <select class="form-control input-sm">
                                <?php foreach ($extensions as $extension) { ?>
                                <optgroup label="<?php echo $extension['name']; ?>">
                                <?php if (!$extension['module']) { ?>
                                <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                <?php } else { ?>
                                <?php foreach ($extension['module'] as $module) { ?>
                                <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                </optgroup>
                                <?php } ?>
                              </select>
                              <div class="input-group-btn">
                                <button type="button" onclick="addModule('column-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                              </div>
                            </div></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-banner2">
                  <div class="col-sm-12 text-center positions-name"><?php echo $text_banner2_pos; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[pos_banner2]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['pos_banner2']) && $hyper_positions_width['pos_banner2'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
                  <table id="module-pos-banner2" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'pos_banner2') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('pos-banner2');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
              </div>
              <div class="col-sm-12 pos-content2">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_content2_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_content2]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_content2']) && $hyper_positions_width['pos_content2'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-column2-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'column2_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('column2-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                      <table id="module-column2-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'column2_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('column2-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-column2-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'column2_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('column2-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-bottom">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_bottom_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_bottom]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_bottom']) && $hyper_positions_width['pos_bottom'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-banner3">
                  <div class="col-sm-12 text-center positions-name"><?php echo $text_banner3_pos; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[pos_banner3]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['pos_banner3']) && $hyper_positions_width['pos_banner3'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
                  <table id="module-pos-banner3" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'pos_banner3') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('pos-banner3');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
              </div>
              <div class="col-sm-12 pos-bottom1">
               <div class="row">
               <div class="text-center col-sm-12"><div class="col-sm-12 text-center positions-name"><?php echo $text_bottom1_pos; ?>
                       <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                           <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                  name="hyper_positions_width[pos_bottom1]" data-size="small" data-on-color="success"
                               <?php if(isset($hyper_positions_width['pos_bottom1']) && $hyper_positions_width['pos_bottom1'] == 1){ echo 'checked';}?> >
                       </div>
                   </div></div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                  <table id="module-bottom1-left" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"><?php echo $text_column_left; ?></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'bottom1_left') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('bottom1-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                  <table id="module-bottom1-center" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"><?php echo $text_pos_center; ?></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'bottom1_center') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('bottom1-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                  <table id="module-bottom1-right" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"><?php echo $text_column_right; ?></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'bottom1_right') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('bottom1-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
              </div>
               </div>
              </div>
              <div class="col-sm-12 pos-bottom2">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_bottom2_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_bottom2]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_bottom2']) && $hyper_positions_width['pos_bottom2'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom2-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom2_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom2-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom2-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom2_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom2-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom2-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom2_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom2-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-bottom2-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom2_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom2-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-bottom3">
                <div class="row">
                <div class="text-center col-sm-12"><div class="col-sm-12 text-center positions-name"><?php echo $text_bottom3_pos; ?>
                        <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                            <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                   name="hyper_positions_width[pos_bottom3]" data-size="small" data-on-color="success"
                                <?php if(isset($hyper_positions_width['pos_bottom3']) && $hyper_positions_width['pos_bottom3'] == 1){ echo 'checked';}?> >
                        </div>
                    </div></div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom3-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom3_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom3-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom3-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom3_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom3-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom3-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom3_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom3-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-banner4">
                  <div class="col-sm-12 text-center positions-name"><?php echo $text_banner4_pos; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[pos_banner4]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['pos_banner4']) && $hyper_positions_width['pos_banner4'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
                  <table id="module-pos-banner4" class="table table-striped table-bordered table-hover">
                      <thead>
                      <tr>
                          <td class="text-center"></td>
                      </tr>
                      </thead>
                      <tbody>
                      <?php foreach ($layout_modules as $layout_module) { ?>
                          <?php if ($layout_module['position'] == 'pos_banner4') { ?>
                              <tr id="module-row<?php echo $module_row; ?>">
                                  <td class="text-left"><div class="input-group">
                                          <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                              <?php foreach ($extensions as $extension) { ?>
                                                  <optgroup label="<?php echo $extension['name']; ?>">
                                                      <?php if (!$extension['module']) { ?>
                                                          <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                              <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                          <?php } else { ?>
                                                              <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                          <?php } ?>
                                                      <?php } else { ?>
                                                          <?php foreach ($extension['module'] as $module) { ?>
                                                              <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      <?php } ?>
                                                  </optgroup>
                                              <?php } ?>
                                          </select>
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                          <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                          <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                              <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                          </div>
                                      </div></td>
                              </tr>
                              <?php $module_row++; ?>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                      <tfoot>
                      <tr>
                          <td class="text-left"><div class="input-group">
                                  <select class="form-control input-sm">
                                      <?php foreach ($extensions as $extension) { ?>
                                          <optgroup label="<?php echo $extension['name']; ?>">
                                              <?php if (!$extension['module']) { ?>
                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                              <?php } else { ?>
                                                  <?php foreach ($extension['module'] as $module) { ?>
                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                  <?php } ?>
                                              <?php } ?>
                                          </optgroup>
                                      <?php } ?>
                                  </select>
                                  <div class="input-group-btn">
                                      <button type="button" onclick="addModule('pos-banner4');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                  </div>
                              </div></td>
                      </tr>
                      </tfoot>
                  </table>
              </div>
              <div class="col-sm-12 pos-bottom4">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 text-center positions-name"><?php echo $text_bottom4_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_bottom4]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_bottom4']) && $hyper_positions_width['pos_bottom4'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom4-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom4_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom4-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom4-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom4_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom4-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-bottom4-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'bottom4_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('bottom4-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-footer1">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_footer_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_footer1]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_footer1']) && $hyper_positions_width['pos_footer1'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-map">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_map_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_map]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_map']) && $hyper_positions_width['pos_map'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-map-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'map_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('map-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                      <table id="module-map-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'map_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('map-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-map-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'map_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('map-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-footer2">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_footer2_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_footer2]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_footer2']) && $hyper_positions_width['pos_footer2'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer2-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer2_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer2-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer2-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer2_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer2-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer2-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer2_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer2-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer2-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer2_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer2-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-footer3">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_footer3_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_footer3]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_footer3']) && $hyper_positions_width['pos_footer3'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-footer3-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer3_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer3-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                      <table id="module-footer3-cent" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer3_cent') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer3-cent');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-footer3-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer3_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer3-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-contents pos-footer">
                  <div class="col-sm-12 text-center positions-name"><?php echo $text_footers; ?>
                      <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                          <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                 name="hyper_positions_width[pos_footer]" data-size="small" data-on-color="success"
                              <?php if(isset($hyper_positions_width['pos_footer']) && $hyper_positions_width['pos_footer'] == 1){ echo 'checked';}?> >
                      </div>
                  </div>
              </div>
              <div class="col-sm-12 pos-footer4">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_footer4_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_footer4]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_footer4']) && $hyper_positions_width['pos_footer4'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer4-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer4_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer4-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer4-cl" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_left_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer4_cl') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer4-cl');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer4-cr" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_right_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer4_cr') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer4-cr');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                      <table id="module-footer4-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer4_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer4-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-footer5">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_footer5_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_footer5]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_footer5']) && $hyper_positions_width['pos_footer5'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-footer5-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer5_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer5-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                      <table id="module-footer5-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer5_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer5-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-footer5-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'footer5_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('footer5-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-copyright">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_copyright_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_copyright]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_copyright']) && $hyper_positions_width['pos_copyright'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-copy-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-copy-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12">
                      <table id="module-copy-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-12 pos-copyright2">
                <div class="row">
                  <div class="text-center col-sm-12"><div class="col-sm-12 positions-name"><?php echo $text_copyright2_pos; ?>
                          <div class="input-group pull-right" data-toggle="tooltip" title="" data-original-title="<?php echo $width_title; ?>">
                              <input class="bootstrap-switcher form-control" type="checkbox" value="1"
                                     name="hyper_positions_width[pos_copyright2]" data-size="small" data-on-color="success"
                                  <?php if(isset($hyper_positions_width['pos_copyright2']) && $hyper_positions_width['pos_copyright2'] == 1){ echo 'checked';}?> >
                          </div>
                      </div></div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-copy2-left" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_left; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy2_left') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy2-left');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-6 col-md-4 col-sm-12">
                      <table id="module-copy2-center" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_pos_center; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy2_center') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"> <a href="<?php echo $layout_module['edit']; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy2-center');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                  <div class="col-lg-3 col-md-4 col-sm-12">
                      <table id="module-copy2-right" class="table table-striped table-bordered table-hover">
                          <thead>
                          <tr>
                              <td class="text-center"><?php echo $text_column_right; ?></td>
                          </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($layout_modules as $layout_module) { ?>
                              <?php if ($layout_module['position'] == 'copy2_right') { ?>
                                  <tr id="module-row<?php echo $module_row; ?>">
                                      <td class="text-left"><div class="input-group">
                                              <select name="layout_module[<?php echo $module_row; ?>][code]" class="form-control input-sm">
                                                  <?php foreach ($extensions as $extension) { ?>
                                                      <optgroup label="<?php echo $extension['name']; ?>">
                                                          <?php if (!$extension['module']) { ?>
                                                              <?php if ($extension['code'] == $layout_module['code']) { ?>
                                                                  <option value="<?php echo $extension['code']; ?>" selected="selected"><?php echo $extension['name']; ?></option>
                                                              <?php } else { ?>
                                                                  <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                              <?php } ?>
                                                          <?php } else { ?>
                                                              <?php foreach ($extension['module'] as $module) { ?>
                                                                  <?php if ($module['code'] == $layout_module['code']) { ?>
                                                                      <option value="<?php echo $module['code']; ?>" selected="selected"><?php echo $module['name']; ?></option>
                                                                  <?php } else { ?>
                                                                      <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                                  <?php } ?>
                                                              <?php } ?>
                                                          <?php } ?>
                                                      </optgroup>
                                                  <?php } ?>
                                              </select>
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][position]" value="<?php echo $layout_module['position']; ?>" />
                                              <input type="hidden" name="layout_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $layout_module['sort_order']; ?>" />
                                              <div class="input-group-btn"><a href="<?php echo $layout_module['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" target="_blank" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                                                  <button type="button" onclick="$('#module-row<?php echo $module_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button>
                                              </div>
                                          </div></td>
                                  </tr>
                                  <?php $module_row++; ?>
                              <?php } ?>
                          <?php } ?>
                          </tbody>
                          <tfoot>
                          <tr>
                              <td class="text-left"><div class="input-group">
                                      <select class="form-control input-sm">
                                          <?php foreach ($extensions as $extension) { ?>
                                              <optgroup label="<?php echo $extension['name']; ?>">
                                                  <?php if (!$extension['module']) { ?>
                                                      <option value="<?php echo $extension['code']; ?>"><?php echo $extension['name']; ?></option>
                                                  <?php } else { ?>
                                                      <?php foreach ($extension['module'] as $module) { ?>
                                                          <option value="<?php echo $module['code']; ?>"><?php echo $module['name']; ?></option>
                                                      <?php } ?>
                                                  <?php } ?>
                                              </optgroup>
                                          <?php } ?>
                                      </select>
                                      <div class="input-group-btn">
                                          <button type="button" onclick="addModule('copy2-right');" data-toggle="tooltip" title="<?php echo $button_module_add; ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus-circle"></i></button>
                                      </div>
                                  </div></td>
                          </tr>
                          </tfoot>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--
var route_row = <?php echo $route_row; ?>;

function addRoute() {
	html  = '<tr id="route-row' + route_row + '">';
	html += '  <td class="text-left"><select name="layout_route[' + route_row + '][store_id]" class="form-control">';
	html += '  <option value="0"><?php echo $text_default; ?></option>';
	<?php foreach ($stores as $store) { ?>
	html += '<option value="<?php echo $store['store_id']; ?>"><?php echo addslashes($store['name']); ?></option>';
	<?php } ?>   
	html += '  </select></td>';
	html += '  <td class="text-left"><input type="text" name="layout_route[' + route_row + '][route]" value="" placeholder="<?php echo $entry_route; ?>" class="form-control" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#route-row' + route_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#route tbody').append(html);
	
	route_row++;
}

var module_row = <?php echo $module_row; ?>;

function addModule(type) {
	html  = '<tr id="module-row' + module_row + '">';
    html += '  <td class="text-left"><div class="input-group"><select name="layout_module[' + module_row + '][code]" class="form-control input-sm">';
	<?php foreach ($extensions as $extension) { ?>
	html += '    <optgroup label="<?php echo addslashes($extension['name']); ?>">';
	<?php if (!$extension['module']) { ?>
	html += '      <option value="<?php echo $extension['code']; ?>"><?php echo addslashes($extension['name']); ?></option>';
	<?php } else { ?>
	<?php foreach ($extension['module'] as $module) { ?>
	html += '      <option value="<?php echo $module['code']; ?>"><?php echo addslashes($module['name']); ?></option>';
	<?php } ?>
	<?php } ?>
	html += '    </optgroup>';
	<?php } ?>
	html += '  </select>';
    html += '  <input type="hidden" name="layout_module[' + module_row + '][position]" value="' + type.replace('-', '_') + '" />';
    html += '  <input type="hidden" name="layout_module[' + module_row + '][sort_order]" value="" />';
	html += '  <div class="input-group-btn"><a href="" target="_blank" type="button" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a><button type="button" onclick="$(\'#module-row' + module_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-sm"><i class="fa fa fa-minus-circle"></i></button></div></div></td>';
	html += '</tr>';
	
	$('#module-' + type + ' tbody').append(html);
	
	$('#module-' + type + ' tbody select[name=\'layout_module[' + module_row + '][code]\']').val($('#module-' + type + ' tfoot select').val());
	
	$('#module-' + type + ' select[name*=\'code\']').trigger('change');
		
	$('#module-' + type + ' tbody input[name*=\'sort_order\']').each(function(i, element) {
		$(element).val(i);
	});
	
	module_row++;
}

$('#module-pos-header-left, #module-pos-header-cl, #module-pos-header-cr, #module-pos-header-right, #module-pos-sliders, #module-pos-top-left, #module-pos-top-center, #module-pos-top-right, #module-pos-top1-left, #module-pos-top1-cl, #module-pos-top1-cr, #module-pos-top1-right, #module-pos-top2-left, #module-pos-top2-center, #module-pos-top2-right, #module-pos-banner, #module-pos-top3-left, #module-pos-top3-cl, #module-pos-top3-cr, #module-pos-top3-right, #module-column-left, #module-column-right, #module-content-top, #module-content-bottom, #module-pos-banner2, #module-column2-left, #module-column2-right, #module-column2-center, #module-pos-bottom-left, #module-pos-bottom-cl, #module-pos-bottom-cr, #module-pos-bottom-right, #module-pos-banner3, #module-bottom1-left, #module-bottom1-center, #module-bottom1-right, #module-pos-bottom2-left, #module-pos-bottom2-cl, #module-pos-bottom2-cr, #module-pos-bottom2-right, #module-bottom3-left, #module-bottom3-center, #module-bottom3-right, #module-bottom4-left, #module-botton4-center, #module-bottom4-right, #module-pos-banner4, #module-pos-footer-left, #module-pos-footer-cl, #module-pos-footer-cr, #module-pos-footer-right, #module-map-left, #module-map-right, #module-map-center, #module-pos-footer2-left, #module-pos-footer2-cl, #module-pos-footer2-cr, #module-pos-footer2-right, #module-footer3-left, #module-footer3-right, #module-footer3-cent, #module-pos-footer4-left, #module-pos-footer4-cl, #module-pos-footer4-cr, #module-pos-footer4-right, #module-footer5-left, #module-footer5-right, #module-footer5-center, #module-copy-left, #module-copy-right, #module-copy-center, #module-copy2-left, #module-copy2-right, #module-copy2-center').delegate('select[name*=\'code\']', 'change', function() {
	var part = this.value.split('.');
	
	if (!part[1]) {
		$(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&token=<?php echo $token; ?>');
	} else {
		$(this).parent().find('a').attr('href', 'index.php?route=extension/module/' + part[0] + '&token=<?php echo $token; ?>&module_id=' + part[1]);
	}
});

$('#module-pos-header-left, #module-pos-header-cl, #module-pos-header-cr, #module-pos-header-right, #module-pos-sliders, #module-pos-top-left, #module-pos-top-center, #module-pos-top-right, #module-pos-top1-left, #module-pos-top1-cl, #module-pos-top1-cr, #module-pos-top1-right, #module-pos-top2-left, #module-pos-top2-center, #module-pos-top2-right, #module-pos-banner, #module-pos-top3-left, #module-pos-top3-cl, #module-pos-top3-cr, #module-pos-top3-right, #module-column-left, #module-column-right, #module-content-top, #module-content-bottom, #module-pos-banner2, #module-column2-left, #module-column2-right, #module-column2-center, #module-pos-bottom-left, #module-pos-bottom-cl, #module-pos-bottom-cr, #module-pos-bottom-right, #module-pos-banner3, #module-bottom1-left, #module-bottom1-center, #module-bottom1-right, #module-pos-bottom2-left, #module-pos-bottom2-cl, #module-pos-bottom2-cr, #module-pos-bottom2-right, #module-bottom3-left, #module-bottom3-center, #module-bottom3-right, #module-bottom4-left, #module-bottom4-center, #module-bottom4-right, #module-pos-banner4, #module-pos-footer-left, #module-pos-footer-cl, #module-pos-footer-cr, #module-pos-footer-right, #module-map-left, #module-map-right, #module-map-center, #module-pos-footer2-left, #module-pos-footer2-cl, #module-pos-footer2-cr, #module-pos-footer2-right, #module-footer3-left, #module-footer3-right, #module-footer3-cent, #module-pos-footer4-left, #module-pos-footer4-cl, #module-pos-footer4-cr, #module-pos-footer4-right, #module-footer5-left, #module-footer5-right, #module-footer5-center, #module-copy-left, #module-copy-right, #module-copy-center, #module-copy2-left, #module-copy2-right, #module-copy2-center').trigger('change');
//--></script>
<script type="text/javascript"><!--

    function initSwitcher(){
        $('.bootstrap-switcher').bootstrapSwitch({
            onInit: function(event, state) {
                switcher(event, state, this);
            },
            onSwitchChange: function(event, state) {
                switcher(event, state, this);
            }
        });
    }


    function switcher(event, state, $this) {
        if(state == true){
            $($this).parent().parent().parent().parent().find('a').attr("disabled", "true");
            $($this).parent().parent().parent().parent().find('a').attr("onclick", "return false");
        }else{
            $($this).parent().parent().parent().parent().find('a').removeAttr("disabled");
            $($this).parent().parent().parent().parent().find('a').removeAttr("onclick");
        }
    }

  $( document ).ready(function() {
      $('body').addClass('hyper-position hd-switch');

      initSwitcher();

  });

//--></script>
</div>
<style type="text/css">
    .hyper-positions > div {
        background-color: ghostwhite;
        border: 2px solid rgba(1, 125, 255, 0.55);
        margin-bottom: 10px;
        padding-top: 10px;
    }
    .hyper-positions .form-control {
        padding: 0;
    }
    .hyper-positions > div:nth-child(2n+1){
        background-color: rgba(1, 125, 255, 0.1);
    }
    legend .btn-danger {
        color: #f8f8f8;
        font-size: 14px;
        font-weight: 600;
    }
    .positions-name {
        background-color: #f8f8f8;
        box-shadow: 0 0 3px #bdbdbd;
        color: #0068a6;
        font-size: 16px;
        font-weight: 600;
        line-height: 1.7;
        margin-bottom: 10px;
        padding: 10px;
    }
    .pos-content {
        border: 2px solid rgba(0, 0, 0, 0.45) !important;
    }
    .pos-contents {
        border: 2px solid rgba(0, 0, 0, 0.25) !important;
    }
    .pos-content .positions-name {
        color: teal;
    }
    .pos-contents .positions-name, .pos-header .positions-name {
        color: #c2c2c2;
    }
    .pos-header .positions-name {
        line-height: 1.6;
        padding: 5px 10px;
        margin-bottom: 0;
        box-shadow: 0 0 1px #bdbdbd;
    }
    .pos-header .positions-name + .positions-name {
        line-height: 4;
        margin-bottom: 0;
    }.pos-header .positions-name + .positions-name + .positions-name {
         line-height: 1.6;
         margin-bottom: 10px;
         padding: 10px;
    }
    .pos-footer3 + .pos-contents .positions-name {
        line-height: 4;
    }
</style>
<?php echo $footer; ?>