<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<?php if ($connectB24) { ?>
					<a data-toggle="tooltip" title="Проверка соединения с Битрикс 24" class="btn btn-success disabled"><i class="fa fa-check-circle"></i> Соединение установлено</a>
				<?php } else { ?>
					<a data-toggle="tooltip" title="Проверка соединения с Битрикс 24" class="btn btn-danger disabled"><i class="fa fa-ban"></i> Соединение не установлено</a>
				<?php } ?>
				
				<button type="submit" name='save-config' value='save-config' form="form-html" data-toggle="tooltip" title="Сохранить данные" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Сохранить</button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul> 
		</div>
	</div>
	<div class="container-fluid">
		<?php if (!empty($error_warning)) { ?>
			<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<?php if (!empty($success)) { ?>
			<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
				<button type="button" class="close" data-dismiss="alert">&times;</button>
			</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-html" class="form-horizontal">
					<div class="tab-pane">
						<ul class="nav nav-tabs" id="language">
							<li><a href="#b24_auth_tab" data-toggle="tab">Авторизация</a></li>
							<?php if(!empty($connectB24)) {?>
							<li><a href="#b24_order_tab" data-toggle="tab">Заказы</a></li> 
							<li><a href="#b24_customer_tab" data-toggle="tab">Клиенты</a></li>  
							<!-- <li><a href="#b24_product_tab" data-toggle="tab">Товары</a></li> -->
							<li><a href="#b24_status_tab" data-toggle="tab">Настройка статусов</a></li>
							<?php }?>
							<li><a href="#support-tab" data-toggle="tab">Поддержка</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane" id="b24_auth_tab">
								<legend>Входящий вебхук</legend>
								<div class="form-group">
									<label for="b24_key_domain" class="col-sm-2 control-label">Входящий вебхук</label>
									<div class="col-sm-5">
										<input type="text" name="b24_in_hook" class="form-control" value="<?php echo $b24_in_hook; ?>" />
									</div>
									<div class="col-sm-5 ">
										<div class="bg-info message">Вставьте ссылку полученную на странице создания входящего вебхука</div>
									</div>
								</div>
								<?php if($connectB24) {?>
									<legend>Исходящие вебхуки</legend>							
									<?php foreach($out_hooks as $hooks_name => $hooks_desc){ ?>
										<div class="form-group">
											<label for="b24_out_hooks[<?php echo $hooks_name;?>]" class="col-sm-2 control-label"><?php echo $hooks_desc;?></label>
											<div class="col-sm-5">
												<input type="text" name="b24_out_hooks[<?php echo $hooks_name;?>]" class="form-control" value="<?php echo isset($b24_out_hooks[$hooks_name]) ? $b24_out_hooks[$hooks_name] : ''; ?>" />
											</div>
										</div>
									<?php }?>
								<?php }?>
								
							</div>
							<div class="tab-pane" id="b24_order_tab">
								<div class="form-group">
										<label class="col-sm-3 control-label">Отправка заказов в Битрикс 24</label>
										<div class="col-sm-3 ">
											<p><a class="btn btn-primary btn-block <?= $getOrderForSync != 0 && isset($connectB24) ? '' : 'disabled'; ?>" href="<?php echo $SendOrderToBitrix; ?>"><i class="fa fa-refresh"></i> Перенести заказы в Битрикс</a></p>
										</div>
										<div class="col-sm-6 ">
											<div class="bg-warning message">В Битрикс 24 будут добавлены заказы из Opencart. Заказы от гостей в Лиды, заказы от клиентов в Сделки</div>
										</div>
								</div>
								<div class="form-group">
									<label for="manager" class="col-sm-3 control-label">Создатель заказов в Битрикс24</label>
									<div class="col-sm-3">
										<select name="b24_manager[created]" id="manager" class="form-control">
											<?php
												foreach($user_list as $manager)
												{
													$name = $manager['LAST_NAME'] .' '. $manager['NAME'];
													$managerId = $manager['ID'];
													$selected = $managerId == $created_id ? 'selected' : '';
													echo "<option value='$managerId' $selected>$name</option>";
												}
											?>
										</select>
									</div>
									<div class="col-sm-6">
										<p class="bg-warning message">Выбранный менеджер будет создателем заказов, клиентов.</p>
									</div>
								</div>
								<div class="form-group">
											<label for="b24_place_comment" class="col-sm-3 col-lg-3 control-label">Заказы видны всем менеджерам</label>
											<div class="col-sm-3 col-lg-3">
												<select name="b24_manager[order_open]" class="form-control">
													<option value='Y' <?= (isset($order_open) && $order_open == 'Y') ? 'selected' : ''; ?>>Да</option>
													<option value='N' <?= (isset($order_open) && $order_open == 'N') ? 'selected' : ''; ?>>Нет</option>
												</select>
											</div>
											<div class="col-sm-6 col-lg-6">
											<p class="bg-info message">Настройки видимости каждого заказа могут быть изменены в процессе его редактирования.</p>
											</div>
								</div>
								<div class="form-group">
											<label class="col-sm-3 col-lg-3 control-label">Отправить письмо клиенту при смене статуса заказа</label>
											<div class="col-sm-3 col-lg-3">
												<select name="b24_order[order_notify]" class="form-control">
													<option value='1' <?= (isset($b24_order['order_notify']) && $b24_order['order_notify'] == 1) ? 'selected' : ''; ?>>Да</option>
													<option value='0' <?= (isset($b24_order['order_notify']) && $b24_order['order_notify'] == 0) ? 'selected' : ''; ?>>Нет</option>
												</select>
											</div>
											<div class="col-sm-6 col-lg-6">
											<p class="bg-info message">Настройки видимости каждого заказа могут быть изменены в процессе его редактирования.</p>
											</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Источник заказа</label>
									<div class="col-sm-3">
										<select name="b24_order[source]" id="source" class="form-control">
											<?php
												foreach($b24_source as $source)
												{
													$sourceName = $source['NAME'];
													$sourceId = $source['STATUS_ID'];
													$selected = $sourceId == $source_id ? 'selected' : '';
													echo "<option value='$sourceId' $selected>$sourceName</option>";
												}
											?>
										</select>
									</div>
									<div class="col-sm-6">
										<p class="bg-info message">Выбранный источник будет назначен заказам.</p>
									</div>
								</div>
								<div class="form-group">
										<label class="col-sm-3 col-lg-3 control-label">Направление сделок по умолчанию</label>
										<div class="col-sm-3 col-lg-3">
										<select name="b24_order[dealcategory]" id="dealcategory" class="form-control">
												<option value='0' <?= (isset($b24_order_data['dealcategory']) && $b24_order_data['dealcategory'] == 0) ? 'selected' : ''; ?>>Не выбран</option>
												<?php
														foreach($b24_dealcategory as $dealcat){ ?>
														<option value='<?= $dealcat['ID']; ?>' <?= (isset($b24_order_data['dealcategory']) && $b24_order_data['dealcategory'] == $dealcat['ID']) ? 'selected' : ''; ?>><?= $dealcat['NAME']; ?></option>
													<?php } ?>
										</select>
										</div>
								</div>
								<div class="form-group">
										<label class="col-sm-3 col-lg-3 control-label">Тип сделки по умолчанию</label>
										<div class="col-sm-3 col-lg-3">
										<select name="b24_order[dealtype]" id="dealtype" class="form-control">
												<option value='0' <?= (isset($b24_order_data['dealtype']) && $b24_order_data['dealtype'] == 0) ? 'selected' : ''; ?>>Не выбран</option>
												<?php
														foreach($b24_deal_type as $dealtype){ ?>
														<option value='<?= $dealtype['STATUS_ID']; ?>' <?= (isset($b24_order_data['dealtype']) && $b24_order_data['dealtype'] == $dealtype['STATUS_ID']) ? 'selected' : ''; ?>><?= $dealtype['NAME']; ?></option>
													<?php } ?>
										</select>
										</div>
									</div>
								<legend>Менеджеры</legend>
								<?php foreach ($opencart_manager as $key => $oc_manager) { ?>
										<div class="form-group">
											<label class="col-sm-3 col-lg-3 control-label"><?php echo $oc_manager['name']; ?></label>
											<div class="col-sm-3 col-lg-3">
												<select name="b24_manager[manager][<?= $oc_manager['customer_group_id']; ?>]" id="m-group_id-<?= $oc_manager['customer_group_id']; ?>" class="form-control">
												<option value='0' <?= (isset($manager_group_id[$oc_manager['customer_group_id']]) && $manager_group_id[$oc_manager['customer_group_id']] == 0) ? 'selected' : ''; ?>>Не выбран</option>
													<?php
														foreach($user_list as $manager){ ?>
														<option value='<?= $manager['ID']; ?>' <?= (isset($manager['ID']) && $manager['ID'] == $manager_group_id[$oc_manager['customer_group_id']]) ? 'selected' : ''; ?>><?= $manager['LAST_NAME'] .' '. $manager['NAME']; ?></option>
													<?php } ?>
												</select>
											</div>
										</div>
								<?php } ?>
							</div>
							<div class="tab-pane" id="b24_customer_tab">
								<legend>Синхронизация клиентов</legend>
								<div class="form-group">
										<div class="col-sm-3 ">
											<a class="btn btn-primary btn-block <?= $getContactForSync != 0 && isset($connectB24) ? '' : 'disabled'; ?>" href="<?php echo $button_synchronizationcontacts; ?>"><i class="fa fa-refresh"></i> Синхронизировать (<?php echo $getContactForSync;?> клиентов)</a>
										</div>
										<div class="col-sm-9 ">
											<div class="bg-info message">Функция синхронизирует всех клиентов из Opencart и Битрикс 24. Это необходимо сделать один раз в процессе настройки модуля.</div>
										</div>
								</div>	
								<legend>Настройки клиентов</legend>		
								<div class="form-group">
									<label class="col-sm-3 control-label">Источник клиента</label>
									<div class="col-sm-3">
										<select name="b24_customer[settings][SOURCE]" id="source" class="form-control">
											<?php
												foreach($b24_source as $source)
												{
													$sourceName = $source['NAME'];
													$sourceId = $source['STATUS_ID'];
													$selected = $sourceId == $source_id ? 'selected' : '';
													echo "<option value='$sourceId' $selected>$sourceName</option>";
												}
											?>
										</select>
									</div>
									<div class="col-sm-6">
										<p class="bg-info message">Выбранный источник будет назначен клиентам.</p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Тип клиента</label>
									<div class="col-sm-3">
										<select name="b24_customer[settings][RETAIL]" id="source" class="form-control">
											<?php
												foreach($b24_contact_type as $type)
												{
													$sourceName = $type['NAME'];
													$sourceId = $type['STATUS_ID'];
													$selected = $sourceId == $retail_id ? 'selected' : '';
													echo "<option value='$sourceId' $selected>$sourceName</option>";
												}
											?>
										</select>
									</div>
									<div class="col-sm-6">
										<p class="bg-info message">Выбранный источник будет назначен заказам и клиентам.</p>
									</div>
								</div>
								<div class="form-group">
											<label for="b24_place_comment" class="col-sm-3 col-lg-3 control-label">Клиенты видны всем менеджерам</label>
											<div class="col-sm-3 col-lg-3">
												<select name="b24_customer[settings][customer_open]" class="form-control">
													<option value='Y' <?= (isset($customer_open) && $customer_open == 'Y') ? 'selected' : ''; ?>>Да</option>
													<option value='N' <?= (isset($customer_open) && $customer_open == 'N') ? 'selected' : ''; ?>>Нет</option>
												</select>
											</div>
											<div class="col-sm-6 col-lg-6">
											<p class="bg-info message">Настройки видимости каждого заказа могут быть изменены в процессе его редактирования.</p>
											</div>
								</div>
								<div class="form-group">
											<label for="b24_place_comment" class="col-sm-3 col-lg-3 control-label">Проверять клиента при регистрации</label>
											<div class="col-sm-3 col-lg-3">
												<select name="b24_customer[settings][customer_search]" class="form-control">
													<option value='1' <?= (isset($b24_customer['customer_search']) && $b24_customer['customer_search'] == 1) ? 'selected' : ''; ?>>Телефон</option>
													<option value='2' <?= (isset($b24_customer['customer_search']) && $b24_customer['customer_search'] == 2) ? 'selected' : ''; ?>>Email</option>
												</select>
											</div>
											<div class="col-sm-6 col-lg-6">
											<p class="bg-info message">При регистрации модуль будет искать клиента по email или телефону </p>
											</div>
								</div>
							</div>
							<div class="tab-pane" id="b24_status_tab">
								<?php if (!empty($connectB24)) { ?>
									<legend>Отправка заказа в Битрикс 24</legend>
									<div class="form-group">
												<label for="b24_out_hooks[b24_out_hooks_ONCRMACTIVITYDELETE]" class="col-sm-2 col-lg-2 control-label">Статус при котором происходит отправка заказа в Битрикс 24</label>
											<div class="col-sm-3 col-lg-3">
												<div class="well well-sm" style="height: 150px; overflow: auto;">
												  <?php foreach ($oc_statuses as $order_status) { ?>
												  <div class="checkbox">
													<label>
													  <?php if (in_array($order_status['order_status_id'], $b24_order_status)) { ?>
													  <input type="checkbox" name="b24_order[status][]" value="<?php echo $order_status['order_status_id']; ?>" checked="checked" />
													  <?php echo $order_status['name']; ?>
													  <?php } else { ?>
													  <input type="checkbox" name="b24_order[status][]" value="<?php echo $order_status['order_status_id']; ?>" />
													  <?php echo $order_status['name']; ?>
													  <?php } ?>
													</label>
												  </div>
												  <?php } ?>
												</div>
											</div>
											<div class="col-sm-7 col-lg-7">
											</div>
										</div>
									
									<a href="/admin/index.php?route=localisation/order_status&token=<?php echo $token;?>" target="_blank"><i class="fa fa-plus-circle" aria-hidden="true"></i> Перейти на страницу добавления статусов в Opencart</a>
									<?php foreach ($b24_dealcategory as $cats => $value) { ?>
									<?php if ($value['ID'] == 4) {?>
										<legend><? echo $value['NAME']?></legend>
										<input type="hidden" name="b24_dealcategory[<? echo $value['ID']?>]" value="<? echo $value['NAME']?>" />
										<?php foreach ($b24_deal_stage_lists[$cats] as $stage) { ?>
											<div class="form-group">
												<label class="col-sm-3 col-lg-3 control-label"><?php echo $stage['NAME']; ?>  (B24)</label>
												<div class="col-sm-6 col-lg-6">
													<select name="b24_status[deal][<? echo $stage['STATUS_ID']; ?>]" id="stage-<?= $stage['STATUS_ID']; ?>" class="form-control">
													<option value='0' <?= (isset($statuses['deal'][$b24_stage['STATUS_ID']]) && $statuses['deal'][$b24_stage['STATUS_ID']] == 0) ? 'selected' : ''; ?>>Не выбран</option>
													<?php
															foreach($oc_statuses as $oc_status){ ?>
															<option value='<?= $oc_status['order_status_id']; ?>' <?= (isset($statuses['deal'][$stage['STATUS_ID']]) && $statuses['deal'][$stage['STATUS_ID']] == $oc_status['order_status_id']) ? 'selected' : ''; ?>><?= $oc_status['name']; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>	
										<?php } ?>
										<?php } ?>

								<?php } ?>
								<?php } ?>
							</div>
							<!-- <div class="tab-pane" id="b24_product_tab">
								<legend>Синхронизация каталога товаров</legend>
								<div class="form-group">
										<div class="col-sm-3 ">
											<p><a class="btn btn-primary btn-block <?= $getProductForSync != 0 && isset($connectB24) ? '' : 'disabled'; ?>" href="<?php echo $button_synchronizationproducts; ?>"><i class="fa fa-refresh"></i> Синхронизировать (<?php echo $getProductForSync;?> товаров)</a></p>
											<p><a class="btn btn-warning btn-block" href="<?php echo $button_updateproduct; ?>"><i class="fa fa-trash-o"></i> Обновить товары в Б24</a></p>
										</div>
										<div class="col-sm-9 ">
											<div class="bg-info message"><p>Кнопка <b>"Добавить в Б24"</b> добавляет товары в Битрикс 24 которых ещё нет в каталоге Битрикса.</p><p>При этом используется информация из временной таблицы Opencart. </p><p>Товары добавляются со скоростью 50 шт в секунду. В случае превышения времени сервера возможна дозагрузка товаров повторным нажатием на кнопку не ранее чем через 5 минут после появления ошибки.</p>
											<p>Кнопка <b>"Обновить товары"</b> обновляет информацию в товарах которые добавлены в Битрикс 24. Товары обноляются со скоростью 50 шт в секунду.</p></div>
										</div>
										</div>
										<legend>Свойства товаров</legend>
										<?php foreach ($oc_propertys as $name => $oc_property) { ?>
										<div class="form-group">
											<label for="" class="col-sm-3 control-label"><?= $oc_property;?></label>
											<div class="col-sm-3">
												<select name="b24_productprops[<?= $name;?>]" id="<?= $name; ?>" class="form-control">
													<option value='0'>Не выбрано</option>
													<?php foreach ($b24_propertys as $key => $b24_property) { ?>
														<option value='<?= $b24_property['ID']; ?>'<?= (isset($b24_productprops[$name]) && $b24_productprops[$name] == $b24_property['ID']) ? 'selected' : ''; ?>>Битрикс 24 - <?= $b24_property['NAME']; ?></option>
													<?php } ?>
												</select>
											</div>
											<div class="col-sm-3">Выберите свойство соответствующее "<?= $oc_property;?>"</div>
										</div>
										<?php } ?>
							</div> -->
							<div class="tab-pane" id="support-tab">
								<div class="col-sm-2 col-lg-2">
									<p><strong>Автор модуля</strong></p>
								</div>
								<div class="col-sm-10 col-lg-10">
									<p> @apipro</p>
								</div>
								<div class="col-sm-2 col-lg-2">
									<p><strong>Сайт</strong></p>
								</div>
								<div class="col-sm-10 col-lg-10">
									<p><a href="http://api-pro.ru" target="_blank">api-pro.ru</a></p>
								</div>
								<div class="col-sm-2 col-lg-2">
									<p><strong>Телеграмм</strong></p>
								</div>
								<div class="col-sm-10 col-lg-10">
									<p><a href="https://t.me/api4pro" target="_blank">https://t.me/api4pro</a></p>
								</div>
								<div class="col-sm-2 col-lg-2">
									<p><strong>Email</strong></p>
								</div>
								<div class="col-sm-10 col-lg-10">
									<p><a href="mailto:api2pro@yandex.ru" target="_blank">api2pro@yandex.ru</a></p>
								</div>
							</div>
							<?php //} ?>
						</div>
					</div>
				</form>
				<style>
					.message{
					padding: 1em;
					border-radius: 3px;
					}
				</style>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$('#language a:first').tab('show');
		function selectedTab() {
			var tabId = localStorage.getItem('tab_b24_api_pro');
			if(!tabId)
			{
				tabId = $('#language a:first').attr('href');
			}
			return tabId;
		}
		$('#language a').click(function () {
			$this = $(this);
			var tabId = $this.attr('href');
			localStorage.setItem('tab_b24_api_pro', tabId);
		});
		$('#language a[href=' + selectedTab() +' ]').click();
	</script></div><?php echo $footer; ?>