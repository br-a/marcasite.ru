<?=$header?>


<link rel="stylesheet" href="/admin/view/lib/overhang.css">
<script src="/admin/view/lib/overhang.js"></script>
<?=$column_left?>
<div id="content">
    <form action="<?=$action?>" method="post">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" onclick='$("body form").eq(0).submit()' form="form-attribute" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?=$new?>" class="btn" >Добавить новый товар</a>
            </div>
            <h1><?=$heading_title?></h1>

        </div>
    </div>

        <form action="/admin/index.php?route=catalog/checkout/order&token=<?=$_GET['token']?>" method="POST">
        <input type="hidden" name="order_id" value="<?=$order_id?>">
    <div class="container-fluid">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Изменить информацию о заказе</h3>
                </div>
                <div class="panel-body">


                    <div class="address col-lg-3">
                        <label for="">
                            Город
                            <input type="text" class="form-control" class="city" value="<?=$city?>" name="city">
                        </label>
                        <label for="">
                            Адрес
                            <input type="text" class="form-control" class="address_1" value="<?=$address_1?>" name="address_1">
                        </label>



                    </div>
                    <div class="col-lg-3">
                        <label for="">
                            Купон
                            <input type="text" class="form-control"  value="<?=$coupon?>" name="coupon">
                        </label>

                    </div>
                    <div class="shipping col-lg-3">
                        Выберите способ доставки
                        <select class="form-control" name="shipping_method" id="">
                            <option value="<?=$shipping_code?>" selected></option>
                            
                        </select>
                        
                        
                        
                    </div>


                </div>


        </form>
    </div>



    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Информация о заказе</h3>
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered" id="products">
                <thead>
                <tr>
                    <th>#</th>
                    <th style="width:200px;">Картинка</th>
                    <th>Товар</th>
                    <th>Модель</th>
                    <th>Опции</th>

                    <th>Количество</th>
                    <th>Цена</th>
                    <th>Отмететь для удаления</th>
                </tr>

                </thead>
                <tbody >
                <?php if($products) { ?>
                <?php foreach($products as $product){ ?>
                <tr>
                    <td><input type="checkbox" name="product_id[]" value="<?=$product['product_id']?>"></td>
                    <td><img src="<?=$product['thumb']?>" alt="">  </td>

                    <td>


                        <?=$product['name']?>
                        <?php foreach ($product['option'] as $option) { ?>
                        <br />
                        <?php if ($option['type'] != 'file') { ?>
                        &nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                        <?php } else { ?>
                        &nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
                        <?php } ?>
                        <?php } ?>


                    </td>

                    <td>
                        <?=$product['model']?>
                    </td>
      <td>
          <?php foreach($product['opts'] as $option){ ?>

          <?php if($option['option_id']==23){  ?>

          <?php foreach($option['product_option_value'] as $val){ ?>

          <div class="optss">


              <input type="radio" product="<?=$product['product_id']?>" order="<?=$order_id?>" class="opts" <?php if($val['quantity']<=0){ ?> disabled <?php } ?>  <?php if($val['product_option_value_id']==$product['size']) { ?> checked <?php } ?> name="option[<?php echo $option['product_option_id']; ?>]" value="<?=$val['product_option_value_id']?>">
              <span class="sizes">
								<?php echo $val['name']?></span>
              <span class="sizes"><?php echo $val['price']?> p.</span>
              <span class="sizes"><?php echo round($val['quantity'])?>шт.</span>

              </span>

          </div>
          <?php } ?>




          <?php } ?>


          <?php } ?>

                    </td>

                    <td>
                        <input name="quantity[<?=$product['product_id']?>]" data-product-id="<?=$product['cart_id']?>"  class="quantity form-control" type="number" min="1" max="<?=$product['max']?>" value="<?=$product['quantity']?>">

                    </td>
                    <td data-price="<?=$product['price']?>">

                    <?php if($product['special']) { ?>

                      <s><?=$product['total']?></s><br>
                        <?=$product['special']?>
                    <?php } else { ?>
                        <?=$product['total']?>
                        <?php } ?>

                    </td>
                    <td><input type="checkbox" name="product_delete[]" value="<?=$product['product_id']?>"


                         </td>


                </tr>


                <?php } ?>

                </tbody>
                <tfooter>
                    <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>


                    <td>Скидка</td>



                    <td><?=$skidkaa?></td>

                    <td></td>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>


                        <td>Дополнительная скидка</td>



                        <td><input data-total="<?=$total_old?>" type="text" name="skidka" value="<?=$skidka?>"></td>

                        <td></td>
                    </tr>


                    </tr> <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>


                    <td>Итого</td>



                    <td id="total"><?=$total?></td>

                    <td></td>
                    </tr>

                </tfooter>
                <?php } else { ?>
                Добавьте товар в корзину
                <?php } ?>




            </table>
        </div>

        </form>

    </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-comment-o"></i> <?php echo $text_history; ?></h3>
            </div>
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-history" data-toggle="tab"><?php echo $tab_history; ?></a></li>
                    <li><a href="#tab-additional" data-toggle="tab"><?php echo $tab_additional; ?></a></li>
                    <?php foreach ($tabs as $tab) { ?>
                    <li><a href="#tab-<?php echo $tab['code']; ?>" data-toggle="tab"><?php echo $tab['title']; ?></a></li>
                    <?php } ?>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-history">
                        <div id="history"></div>
                        <br />
                        <fieldset>
                            <legend><?php echo $text_history_add; ?></legend>
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-order-status"><?php echo $entry_order_status; ?></label>
                                    <div class="col-sm-10">
                                        <select name="order_status_id" id="input-order-status" class="form-control">
                                            <?php foreach ($order_statuses as $order_statuses) { ?>
                                            <?php if ($order_statuses['order_status_id'] == $order_status_id) { ?>
                                            <option value="<?php echo $order_statuses['order_status_id']; ?>" selected="selected"><?php echo $order_statuses['name']; ?></option>
                                            <?php } else { ?>
                                            <option value="<?php echo $order_statuses['order_status_id']; ?>"><?php echo $order_statuses['name']; ?></option>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-override"><span data-toggle="tooltip" title="<?php echo $help_override; ?>"><?php echo $entry_override; ?></span></label>
                                    <div class="col-sm-10">
                                        <input type="checkbox" name="override" value="1" id="input-override" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-notify"><?php echo $entry_notify; ?></label>
                                    <div class="col-sm-10">
                                        <input type="checkbox" name="notify" value="1" id="input-notify" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>
                                    <div class="col-sm-10">
                                        <textarea name="comment" rows="8" id="input-comment" class="form-control"></textarea>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                        <div class="text-right">
                            <button id="button-history" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> <?php echo $button_history_add; ?></button>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-additional">
                        <?php if ($account_custom_fields) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td colspan="2"><?php echo $text_account_custom_field; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($account_custom_fields as $custom_field) { ?>
                                <tr>
                                    <td><?php echo $custom_field['name']; ?></td>
                                    <td><?php echo $custom_field['value']; ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                        <?php if ($payment_custom_fields) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td colspan="2"><?php echo $text_payment_custom_field; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($payment_custom_fields as $custom_field) { ?>
                                <tr>
                                    <td><?php echo $custom_field['name']; ?></td>
                                    <td><?php echo $custom_field['value']; ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                        <?php if ($shipping_method && $shipping_custom_fields) { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td colspan="2"><?php echo $text_shipping_custom_field; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($shipping_custom_fields as $custom_field) { ?>
                                <tr>
                                    <td><?php echo $custom_field['name']; ?></td>
                                    <td><?php echo $custom_field['value']; ?></td>
                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <td colspan="2"><?php echo $text_browser; ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><?php echo $text_ip; ?></td>
                                    <td><?php echo $ip; ?></td>
                                </tr>
                                <?php if ($forwarded_ip) { ?>
                                <tr>
                                    <td><?php echo $text_forwarded_ip; ?></td>
                                    <td><?php echo $forwarded_ip; ?></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td><?php echo $text_user_agent; ?></td>
                                    <td><?php echo $user_agent; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo $text_accept_language; ?></td>
                                    <td><?php echo $accept_language; ?></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php foreach ($tabs as $tab) { ?>
                    <div class="tab-pane" id="tab-<?php echo $tab['code']; ?>"><?php echo $tab['content']; ?></div>
                    <?php } ?>
                </div>
            </div>
        </div>
</div>
</div>






</div>


<script>
    $(".opts").change(function(){
        var option=$(this).val();
        var name=$(this).attr('name');
        var product=$(this).attr('product');
        var order=$(this).attr('order');

        $.ajax({
            url:'/admin/index.php?route=sale/checkout/options&order_id=807&token=<?=$_GET['token']?>',
            data:'product_id='+product+'&order_id='+order+'&'+name+'='+option,
            type:"post",
            success:function(){
                location.reload()
            }

        });

    });
</script>


<script>
    $('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

    $('#button-history').on('click', function() {
        /*
        if (typeof verifyStatusChange == 'function'){
            if (verifyStatusChange() == false){
                return false;
            } else{
                addOrderInfo();
            }
        } else{
            addOrderInfo();
        }*/

        $.ajax({
            url: '<?php echo $catalog; ?>index.php?route=api/order/history&token=' + token + '&store_id=<?php echo $store_id; ?>&order_id=<?php echo $order_id; ?>',
            type: 'post',
            dataType: 'json',
            data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + ($('input[name=\'notify\']').prop('checked') ? 1 : 0) + '&override=' + ($('input[name=\'override\']').prop('checked') ? 1 : 0) + '&append=' + ($('input[name=\'append\']').prop('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
            beforeSend: function() {
                $('#button-history').button('loading');
            },
            complete: function() {
                $('#button-history').button('reset');
            },
            success: function(json) {
                $('.alert').remove();

                if (json['error']) {
                    $('#history').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                if (json['success']) {
                    $('#history').load('index.php?route=sale/order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

                    $('#history').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('textarea[name=\'comment\']').val('');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });

    function changeStatus(){
        var status_id = $('select[name="order_status_id"]').val();

        $('#openbay-info').remove();

        $.ajax({
            url: 'index.php?route=extension/openbay/getorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
            dataType: 'html',
            success: function(html) {
                $('#history').after(html);
            }
        });
    }

    function addOrderInfo(){
        var status_id = $('select[name="order_status_id"]').val();

        $.ajax({
            url: 'index.php?route=extension/openbay/addorderinfo&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>&status_id=' + status_id,
            type: 'post',
            dataType: 'html',
            data: $(".openbay-data").serialize()
        });
    }

</script>
<script>
    var token = '';

    // Login to the API
    $.ajax({
        url: '<?php echo $catalog; ?>index.php?route=api/login',
        type: 'post',
        data: 'key=<?php echo $api_key; ?>',
        dataType: 'json',
        async:false,
        crossDomain: true,
        success: function(json) {
            $('.alert').remove();

            if (json['error']) {
                if (json['error']['key']) {
                    $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['key'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }

                if (json['error']['ip']) {
                    $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['ip'] + ' <button type="button" id="button-ip-add" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-danger btn-xs pull-right"><i class="fa fa-plus"></i> <?php echo $button_ip_add; ?></button></div>');
                }
            }

            if (json['token']) {
                token = json['token'];

                console.log(token);

                $('select[name=\'currency\']').trigger('change');
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
    console.log(token);
    var request = $.ajax({
        url: '<?php echo $catalog; ?>index.php?route=api/shipping/methods&token=' + token + '&store_id=0',
        dataType: 'json',
        beforeSend: function() {
            $('#button-shipping-address').button('loading');
        },
        complete: function() {
            $('#button-shipping-address').button('reset');
        },
        success: function(json) {
            if (json['error']) {
                $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
            } else {
                // Shipping Methods
                html = '<option value="">Выберите способ доставки</option>';

                if (json['shipping_methods']) {
                    for (i in json['shipping_methods']) {
                        html += '<optgroup label="' + json['shipping_methods'][i]['title'] + '">';

                        if (!json['shipping_methods'][i]['error']) {
                            for (j in json['shipping_methods'][i]['quote']) {
                                if (json['shipping_methods'][i]['quote'][j]['code'] == $('select[name=\'shipping_method\'] option:selected').val()) {
                                    html += '<option value="' + json['shipping_methods'][i]['quote'][j]['code'] +"_"+json['shipping_methods'][i]['quote'][j]['title']+ '" selected="selected">' + json['shipping_methods'][i]['quote'][j]['title'] + ' - ' + json['shipping_methods'][i]['quote'][j]['text'] + '</option>';
                                } else {
                                    html += '<option value="' + json['shipping_methods'][i]['quote'][j]['code'] +"_"+json['shipping_methods'][i]['quote'][j]['title']+ '">' + json['shipping_methods'][i]['quote'][j]['title'] + ' - ' + json['shipping_methods'][i]['quote'][j]['text'] + '</option>';
                                }
                            }
                        } else {
                            html += '<option value="" style="color: #F00;" disabled="disabled">' + json['shipping_method'][i]['error'] + '</option>';
                        }

                        html += '</optgroup>';
                    }
                }

                $('select[name=shipping_method]').html(html);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    })
</script>
<script>
    $('input[name=product]').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?=$_GET['token']?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id'],
                            model: item['model'],
                            option: item['option'],
                            price: item['price']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            order_id=$("[name=order_id]").val();
            $.ajax({
                url:"/admin/index.php?route=sale/checkout/add&product_id="+item.value+"&order_id="+order_id+"&token=<?=$_GET['token']?>",
                dataType:"json",
                success:function(json) {

                    if(json['success']){
                        location.reload();
                    }
                    if(json['quaerror']){
                       alert(json['quaerror']);
                    }
                    if(json['repeat']){
                        alert("Этот товар уже есть в корзине")
                    }





                  //  location.reload();
                }
            });
        }
    });


</script>

<style>

    .sizes {
        border: 1px solid #ccc;
        display: block;
        width: 70px;
        padding: 0;
        margin: 0;
        border-radius: 5px;
    }
    .rows{
        min-height: 190px;
    }
    .optss {
        display: -webkit-inline-box;
    }
    .pr_list {
        list-style:none;
        text-align:left;
        padding-left:10px;
    }
    .product {

        padding: 10px;
        width: 344px;
        display: inline-block;

        background: #fff;
        border: 1px solid #ebebeb;
        margin: 4px;
        text-align: center;
    }

    .product_back{
        background:#f8f8f8;
    }
    .list_title{
        color: #757575;
        display: inline-block;
        height: 45px;
        line-height:1.6em;
    }

</style>
<?=$footer?>