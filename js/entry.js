/* global require */

require('inputmask/dist/jquery.inputmask.js');
require('picturefill/dist/picturefill.js');
require('../blocks/form-validation/form-validation.js');
require('../blocks/object-fit-polyfill/object-fit-polyfill.js');
require('../blocks/sprite-svg/sprite-svg.js');
require('../blocks/account-nav/account-nav.js');
require('../blocks/card/card.js');
require('../blocks/card-gallery/card-gallery.js');
require('../blocks/cart/cart.js');
require('../blocks/categories-nav/categories-nav.js');
require('../blocks/categories-tabs/categories-tabs.js');
require('../blocks/confirm-order/confirm-order.js');
require('../blocks/field-select/field-select.js');
require('../blocks/field-text/field-text.js');
require('../blocks/filters/filters.js');
require('../blocks/footer/footer.js');
require('../blocks/header/header.js');
require('../blocks/main-slider/main-slider.js');
require('../blocks/mobile-nav/mobile-nav.js');
require('../blocks/products/products.js');
require('../blocks/tabs/tabs.js');
require('../blocks/video/video.js');
require('./script.js');
