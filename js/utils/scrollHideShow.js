/* global document window */

// fix scroll
export function hideScroll() {
    var _this = document;
    _this.body.classList.add('no-scroll');

    _this.scrollTop = window.pageYOffset;
    _this.body.style.position = 'fixed';
    if (hasScrollbar()) {
        _this.body.style.width = 'calc(100% - '+getScrollbarSize()+'px)';
    } else {
        _this.body.style.width = '100%';
    }
    _this.body.style.top = -_this.scrollTop + 'px';

    if (_this.scrollTop > 0) {
        document.querySelector('.header').classList.add('fixed');
    }
}
function getScrollbarSize() {
    var outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.width = '100px';
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

    document.body.appendChild(outer);

    var widthNoScroll = outer.offsetWidth;
    // force scrollbars
    outer.style.overflow = 'scroll';

    // add innerdiv
    var inner = document.createElement('div');
    inner.style.width = '100%';
    outer.appendChild(inner);

    var widthWithScroll = inner.offsetWidth;

    // remove divs
    outer.parentNode.removeChild(outer);
    return widthNoScroll - widthWithScroll;
}
function hasScrollbar() {
    return document.body.scrollHeight > document.body.clientHeight;
}
export function showScroll() {
    var _this = document;
    _this.body.classList.remove('no-scroll');
    _this.body.style.position = '';
    _this.body.style.width = '';
    _this.body.style.top = '';
    window.scroll(0, _this.scrollTop);
}
