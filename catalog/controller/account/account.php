<?php
class ControllerAccountAccount extends Controller {
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);
		
		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get($code . '_status') && $this->config->get($code . '_card')) {
				$this->load->language('extension/credit_card/' . $code);

				$data['credit_cards'][] = array(
					'name' => $this->language->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}
		
		$data['wishlist'] = $this->url->link('account/wishlist');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);
		
		$data['column_left'] = $this->load->controller('common/column_left');

		$data = array_merge($data,$this->load->language('account/order'));
		$data = array_merge($data,$this->load->language('account/edit'));
		$this->load->model('account/order');
		$this->load->model('account/customer');
		$this->load->model('account/download');
		$this->load->model('account/wishlist');
		$data['download_total'] = $this->model_account_download->getTotalDownloads();
		$results = $this->model_account_wishlist->getWishlist();
		$data['total_wishlist'] = count($results);
		$dat = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer c
			LEFT JOIN " . DB_PREFIX . "address a ON (a.address_id=c.address_id)
			WHERE c.customer_id = '" . (int)$this->customer->getId() . "' AND c.status = '1'");
		$data['customer_info'] = $dat->row;
		$data['order_total'] = $this->model_account_order->getTotalOrders();
		$results = $this->model_account_order->getOrders(0, 5);
		$data['orders_info'] = array();
		$orderprice =$pro = 0;
		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);
			$pro += $product_total + $voucher_total;
			$orderprice += $this->currency->format($result['total'], $result['currency_code'], $result['currency_value'],false); 
                      $this->load->model('sale/order');
	      $pred=0;
          $totall=0; 
          $totall_old=0;
          $products = $this->model_sale_order->getOrderProducts($result['order_id']);
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_sale_order->getOrderOptions($result['order_id'], $product['order_product_id']);
$old_option_price=0;
				foreach ($options as $option) {
					if ($option['type'] != 'file') {
					     $o=$this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE product_option_id=".$option['product_option_id'])->row;
				    $old_option_price=$o['price'];
                   if($o['special']){
                    // $special_option=$o['special'];
                   }
                       
                       
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $option['value'],
							'type'  => $option['type']
						);
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $upload_info['name'],
								'type'  => $option['type'],
								'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true)
							);
						}
					}
				}
 
 
	 
              $product['old_price']=$this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id=".$product['product_id'])->row['price'];
        if($product['special']){
            $product['old_price']=$product['special'];
        }
        if($old_option_price && $old_option_price>0){
            $product['old_price']=$old_option_price;
        }
        if($special_option>0){
           // $product['price']=$special_option;
        }
        //echo $product['price']."<br />";
         //echo $product['old_price']."<hr />";
	         $this->load->model('catalog/colorkit');
            $colors = $this->model_catalog_colorkit->getColors($product['product_id']);
            $totall_old+=$product['price']*$product['quantity'];
            $oold=$product['old_price'];
            $t=0;
            if(!empty($colors)){
                //$product['price']=$product['price']+$product['price']/100*$this->config->get('color_kit')['markup'];
          
             $product['old_price']=$product['old_price']+$product['old_price']*($this->config->get('color_kit')['markup'])/(100);
             $pred+=$product['old_price'];
            foreach($colors as $color){
                if($color['product_id']!=$product['product_id']){
                     $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$result['order_id'] . "' AND product_id=".$color['product_id']);
              
                        if($query->num_rows){
                            $t=1;
                        }
                        else{
                            $t=0;
                        }
                }
                
            }
            if($t){ 
                
               
                $product['price']=$product['old_price']-$product['old_price']/100*($this->config->get('color_kit')['markup']);
                }else{
                    $product['price']=$product['old_price'];
                }
                   

           
            }else{
                $pred+=$product['old_price']*$product['quantity'];
            }
            
           ////echo $cold."<br />";
             if($product['price']!=$oold){
              //  //echo $product['name']." -".$product['price']."<br />";
                 $totall+=$product['price']*$product['quantity'];
                 
                
                }else{
                    $totall+=$product['price']*$product['quantity'];
                     $product['price']=$product['old_price'];
                }
                
                
                if(!$t){
                    $product['price']=$oold;
                }
                $product['price']=$product['old_price'];
                
				$data['products'][] = array(
 
	      
					'order_product_id' => $product['order_product_id'],
					'product_id'       => $product['product_id'],
					'name'    	 	   => $product['name'],
					'model'    		   => $product['model'],
					'option'   		   => $option_data,
					'quantity'		   => $product['quantity'],
					'price'    		   => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    		   => $this->currency->format($product['price']*$product['quantity']+ ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'href'     		   => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], true)
				);
			}
//echo "PRED".$pred;
		

$totals = $this->model_account_order->getOrderTotals($result['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                    'value'=>$total['value']
				);
                }

$totall+=$data['totals'][1]['value'];
			$data['orders_info'][] = array(
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($totall, $result['currency_code'], $result['currency_value']),
				'href'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], 'SSL'),
			);
		}
		$data['orderprice'] = $this->currency->format($orderprice, $this->config->get('config_currency'));
		$data['orderproduct'] = $pro;

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		
			$this->response->setOutput($this->load->view('account/account_dashboard_page.tpl', $data));
		
		return ;
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/account', $data));
	}

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
