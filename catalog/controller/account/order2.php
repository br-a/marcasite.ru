<?php


include_once(DIR_SYSTEM . 'library/simple/simple_controller.php');
class ControllerAccountOrder extends Controller {
    
         public function formatCurrency($value) {
        $value=Round($value);

            return $value." руб.";
      
    }
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('account/order');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/order', $url, true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_product'] = $this->language->get('column_product');
		$data['column_total'] = $this->language->get('column_total');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');

		$data['button_view'] = $this->language->get('button_view');
    $data['button_ocstore_payeer_onpay'] = $this->language->get('button_ocstore_payeer_onpay');
    $data['button_ocstore_yk_onpay'] = $this->language->get('button_ocstore_yk_onpay');
		$data['button_continue'] = $this->language->get('button_continue');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['orders'] = array();

		$this->load->model('extension/payment/ocstore_payeer');
		$this->load->model('extension/payment/ocstore_yk');
		$this->load->model('account/order');

		$order_total = $this->model_account_order->getTotalOrders();

		$results = $this->model_account_order->getOrders(($page - 1) * 10, 10);

		foreach ($results as $result) {
			$product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
			$voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);

			$ocstore_yk_onpay_info  = $this->model_extension_payment_ocstore_yk->checkLaterpay($result['order_id']);


 
	        $this->load->model('tool/image');
                $this->load->model('sale/order');
	      $pred=0;
          $totall=0; 
          $totall_old=0;
          $products = $this->model_sale_order->getOrderProducts($result['order_id']);
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_sale_order->getOrderOptions($result['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
					if ($option['type'] != 'file') {
					     $o=$this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE product_option_id=".$option['product_option_id'])->row;
				    $old_option_price=$o['price'];
                   if($o['special']){
                    // $special_option=$o['special'];
                   }
                       
                       
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => $option['value'],
							'type'  => $option['type']
						);
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$option_data[] = array(
								'name'  => $option['name'],
								'value' => $upload_info['name'],
								'type'  => $option['type'],
								'href'  => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true)
							);
						}
					}
				}
 
 
	        if ($product['image'] && file_exists(DIR_IMAGE . $product['image'])) {
		     $image = $this->model_tool_image->resize($product['image'], 100, 100);
		      } else {
		      $image = $this->model_tool_image->resize('no_image.png', 100, 100);
		   }
              $product['old_price']=$this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id=".$product['product_id'])->row['price'];
        if($product['special']){
            $product['old_price']=$product['special'];
        }
        if($old_option_price && $old_option_price>0){
            $product['old_price']=$old_option_price;
        }
        if($special_option>0){
           // $product['price']=$special_option;
        }
        //echo $product['price']."<br />";
         //echo $product['old_price']."<hr />";
	         $this->load->model('catalog/colorkit');
            $colors = $this->model_catalog_colorkit->getColors($product['product_id']);
            $totall_old+=$product['price']*$product['quantity'];
            $oold=$product['old_price'];
            $t=0;
            if(!empty($colors)){
                //$product['price']=$product['price']+$product['price']/100*$this->config->get('color_kit')['markup'];
          
             $product['old_price']=$product['old_price']+$product['old_price']*($this->config->get('color_kit')['markup'])/(100);
             $pred+=$product['old_price'];
            foreach($colors as $color){
                if($color['product_id']!=$product['product_id']){
                     $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$result['order_id'] . "' AND product_id=".$color['product_id']);
              
                        if($query->num_rows){
                            $t=1;
                        }
                        else{
                            $t=0;
                        }
                }
                
            }
            if($t){ 
                
               
                $product['price']=$product['old_price']-$product['old_price']/100*($this->config->get('color_kit')['markup']);
                }else{
                    $product['price']=$product['old_price'];
                }
                   

           
            }else{
                $pred+=$product['old_price']*$product['quantity'];
            }
            
           ////echo $cold."<br />";
             if($product['price']!=$oold){
              //  //echo $product['name']." -".$product['price']."<br />";
                 $totall+=$product['price']*$product['quantity'];
                 
                
                }else{
                    $totall+=$product['price']*$product['quantity'];
                     $product['price']=$product['old_price'];
                }
                
                
                if(!$t){
                    $product['price']=$oold;
                }
                $product['price']=$product['old_price'];
                
				$data['products'][] = array(
 
	       'thumb'               => $image,
	      
					'order_product_id' => $product['order_product_id'],
					'product_id'       => $product['product_id'],
					'name'    	 	   => $product['name'],
					'model'    		   => $product['model'],
					'option'   		   => $option_data,
					'quantity'		   => $product['quantity'],
					'price'    		   => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    		   => $this->currency->format($product['price']*$product['quantity']+ ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'href'     		   => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], true)
				);
			}
//echo "PRED".$pred;
		





			$data['orders'][] = array(
				'order_id'   => $result['order_id'],
				'name'       => $result['firstname'] . ' ' . $result['lastname'],
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'products'   => ($product_total + $voucher_total),
				'total'      => $this->currency->format($totall, $result['currency_code'], $result['currency_value']),
				'ocstore_payeer_onpay'  => $this->model_extension_payment_ocstore_payeer->checkLaterpay($result['order_id']) ? $this->url->link('extension/payment/ocstore_payeer/laterpay', sprintf('order_id=%s&order_tt=%s', $result['order_id'], $result['total'], 'SSL')) : '',
				'ocstore_yk_onpay'      => $ocstore_yk_onpay_info['onpay'] ? $this->url->link('extension/payment/ocstore_yk/laterpay', sprintf('order_id=%s&order_ttl=%s&paymentType=%s', $result['order_id'], $result['total'], $ocstore_yk_onpay_info['payment_code']), 'SSL') : '',
				'view'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
			);
		}

		$pagination = new Pagination();
		$pagination->total = $order_total;
		$pagination->page = $page;
		$pagination->limit = 10;
		$pagination->url = $this->url->link('account/order', 'page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

		$data['continue'] = $this->url->link('account/account', '', true);

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/order_list', $data));
	}

	public function info() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order/info', 'order_id=' . $order_id, true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			$this->document->setTitle($this->language->get('text_order'));

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', $url, true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $this->request->get['order_id'] . $url, true)
			);

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_order_detail'] = $this->language->get('text_order_detail');
			$data['text_invoice_no'] = $this->language->get('text_invoice_no');
			$data['text_order_id'] = $this->language->get('text_order_id');
			$data['text_date_added'] = $this->language->get('text_date_added');
			$data['text_shipping_method'] = $this->language->get('text_shipping_method');
			$data['text_shipping_address'] = $this->language->get('text_shipping_address');
			$data['text_payment_method'] = $this->language->get('text_payment_method');
			$data['text_payment_address'] = $this->language->get('text_payment_address');
			$data['text_history'] = $this->language->get('text_history');
			$data['text_comment'] = $this->language->get('text_comment');
			$data['text_no_results'] = $this->language->get('text_no_results');

			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');
			$data['column_action'] = $this->language->get('column_action');
			$data['column_date_added'] = $this->language->get('column_date_added');
			$data['column_status'] = $this->language->get('column_status');
			$data['column_comment'] = $this->language->get('column_comment');

			$data['button_reorder'] = $this->language->get('button_reorder');
			$data['button_return'] = $this->language->get('button_return');
			$data['button_continue'] = $this->language->get('button_continue');

			if (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}


			if ($order_info['track_no']) {
				$data['track_no'] = $order_info['track_no'];
			} else {
				$data['track_no'] = '';
			}
				
			if ($order_info['invoice_no']) {
				$data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
			} else {
				$data['invoice_no'] = '';
			}

			$data['order_id'] = $this->request->get['order_id'];
			$data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

			if ($order_info['payment_address_format']) {
				$format = $order_info['payment_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['payment_firstname'],
				'lastname'  => $order_info['payment_lastname'],
				'company'   => $order_info['payment_company'],
				'address_1' => $order_info['payment_address_1'],
				'address_2' => $order_info['payment_address_2'],
				'city'      => $order_info['payment_city'],
				'postcode'  => $order_info['payment_postcode'],
				'zone'      => $order_info['payment_zone'],
				'zone_code' => $order_info['payment_zone_code'],
				'country'   => $order_info['payment_country']
			);

			$data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['payment_method'] = $order_info['payment_method'];

			if ($order_info['shipping_address_format']) {
				$format = $order_info['shipping_address_format'];
			} else {
				$format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
			}

			$find = array(
				'{firstname}',
				'{lastname}',
				'{company}',
				'{address_1}',
				'{address_2}',
				'{city}',
				'{postcode}',
				'{zone}',
				'{zone_code}',
				'{country}'
			);

			$replace = array(
				'firstname' => $order_info['shipping_firstname'],
				'lastname'  => $order_info['shipping_lastname'],
				'company'   => $order_info['shipping_company'],
				'address_1' => $order_info['shipping_address_1'],
				'address_2' => $order_info['shipping_address_2'],
				'city'      => $order_info['shipping_city'],
				'postcode'  => $order_info['shipping_postcode'],
				'zone'      => $order_info['shipping_zone'],
				'zone_code' => $order_info['shipping_zone_code'],
				'country'   => $order_info['shipping_country']
			);

			$data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

			$data['shipping_method'] = $order_info['shipping_method'];

			$this->load->model('catalog/product');
			$this->load->model('tool/upload');

			// Products
			$data['products'] = array();

			$products = $this->model_account_order->getOrderProducts($this->request->get['order_id']);
                $pred=0;
                $totall=0;
                $totall_old=0;
			foreach ($products as $product) {
				$option_data = array();

				$options = $this->model_account_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

				foreach ($options as $option) {
				   
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);
                        
						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}
                    $o=$this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE product_option_id=".$option['product_option_id'])->row;
				    $old_option_price=$o['price'];
                    
                   if($o['special']){
                  //  $old_option_price=$o['special'];
                   }
                    
                    $option_data[] = array(
						'name'  => $option['name'],
						'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
					);
				}

				$product_info = $this->model_catalog_product->getProduct($product['product_id']);

				if ($product_info) {
					$reorder = $this->url->link('account/order/reorder', 'order_id=' . $order_id . '&order_product_id=' . $product['order_product_id'], true);
				} else {
					$reorder = ''; 
				}
              
        $product['old_price']=$this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id=".$product['product_id'])->row['price'];
  
        if($old_option_price){
           $product['old_price']=$old_option_price;
        
        }
        
   $this->load->model('catalog/colorkit');
            $colors = $this->model_catalog_colorkit->getColors($product['product_id']);
            $totall_old+=$product['price']*$product['quantity'];
            $oold=$product['old_price'];
            $t=0;
            if(!empty($colors)){
                //$product['price']=$product['price']+$product['price']/100*$this->config->get('color_kit')['markup'];
          
             $product['old_price']=$product['old_price']+$product['old_price']*($this->config->get('color_kit')['markup'])/(100);
             //$pred+=$product['old_price'];
            foreach($colors as $color){
                if($color['product_id']!=$product['product_id']){
                     $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "' AND product_id=".$color['product_id']);
              
                        if($query->num_rows){
                            $t=1;
                        }
                        else{
                            $t=0;
                        }
                }
                
            }
            if($t){ 
                
               
                $product['price']=$product['old_price']-$product['old_price']/100*($this->config->get('color_kit')['markup']);
                }else{
                    $product['price']=$product['old_price'];
                }
                   

           
            }else{
                //$pred+=$product['old_price']*$product['quantity'];
            }
            
           //echo $cold."<br />";
             if($product['price']!=$oold){
              //  echo $product['name']." -".$product['price']."<br />";
                 $totall+=$product['price']*$product['quantity'];
                 
                
                }else{
                    $totall+=$product['price']*$product['quantity'];
                     $product['price']=$product['old_price'];
                }
                
                
                if(!$t){
                    $product['price']=$oold;
                }
                $product['price']=$product['old_price'];
				$pred+=$product['old_price']*$product['quantity'];
                $data['products'][] = array(
					'name'     => $product['name'],
					'model'    => $product['model'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'price'    => $this->currency->format($product['old_price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
					'total'    => $this->currency->format($product['old_price']*$product['quantity'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
					'reorder'  => $reorder,
					'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
				);
			}

          
			// Voucher
			$data['vouchers'] = array();

			$vouchers = $this->model_account_order->getOrderVouchers($this->request->get['order_id']);

			foreach ($vouchers as $voucher) {
				$data['vouchers'][] = array(
					'description' => $voucher['description'],
					'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
				);
			}

			// Totals
			$data['totals'] = array();

			$totals = $this->model_account_order->getOrderTotals($this->request->get['order_id']);

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                    'value'=>$total['value']
				);
			}
            
            
            $t=0;  
        foreach($this->cart->getProducts() as $product){
     
            if($this->db->query("SELECT * FROM oc_color_kit_group WHERE product_id=".$product['product_id'])->num_rows){
                $color_kits_id=$this->db->query("SELECT * FROM oc_color_kit_group WHERE product_id=".$product['product_id'])->row['color_kit_id'];
              
                $complects=$this->db->query("SELECT * FROM oc_color_kit_group WHERE color_kit_id=".$color_kits_id)->rows;
                if($complects)
                foreach($complects as $complect){
                    	if($complect['product_id']==$product['product_id']){
                             
                            continue;
                            }
                        $query = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "cart WHERE product_id = '" .$complect['product_id']. "' AND api_id = '" . (isset($this->session->data['api_id']) ? (int)$this->session->data['api_id'] : 0) . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND session_id = '" . $this->db->escape($this->session->getId()) . "'");
                        if($query->num_rows){
                            $t=1;
                        }

		
                    
                    
                }
            }
     
             
        }
  $data['totals'][0]['text']=$this->formatCurrency($pred);
        if(($pred-$totall)>0){
    
        $data['totals'][2]['title']="Скидка";
        $data['totals'][2]['text']=$this->formatCurrency($pred-$totall);//$this->formatCurrency(Round($totall*$this->config->get('color_kit')['markup']/100+$totall));

        
        
        }
   
        
        if($t){
        //$data['totals'][count($data['totals'])]=$data['totals'][count($data['totals'])-1];
 
        $data['totals'][2]['title']="Скидка";
        $data['totals'][2]['text']=$this->formatCurrency($pred-$totall);//$this->formatCurrency(Round($totall*$this->config->get('color_kit')['markup']/100+$totall));
        
        }else{
        if(isset($data['totals'][2]) && $data['totals'][2]['title']!="Скидка")
        $data['totals'][2]['text']=$this->formatCurrency($totall+$data['totals'][1]['value']); 

          //  $data['totals'][count($data['totals'])-1]['text']=$this->formatCurrency($totall_old);//$this->formatCurrency($totall*$this->config->get('color_kit')['markup']/100+$totall);
        $data['totals'][0]['text']=$this->formatCurrency($pred);
        }
        
                $pp=0;
        foreach($data['totals'] as $key=>$total){
        if(($total['title'])=="Итого"){
            unset($data['totals'][$key]);
            
        }    
        }
        
      
        $data['totals'][3]['title']="Итого";
        $data['totals'][3]['text']=$this->formatCurrency($totall+$data['totals'][1]['value']);//$this->formatCurrency(Round($totall*$this->config->get('color_kit')['markup']/100+$totall));
        
        if($t){
        $pp=0;
        foreach($data['totals'] as $key=>$total){
        if(($total['title'])=="Итого"){
            unset($data['totals'][$key]);
            
        }    
        }
        
      
        $data['totals'][3]['title']="Итого";
        $data['totals'][3]['text']=$this->formatCurrency($totall+$data['totals'][1]['value']);//$this->formatCurrency(Round($totall*$this->config->get('color_kit')['markup']/100+$totall));
       
        }
        //  unset($data['totals'][0]);
        
       
            

			$data['comment'] = nl2br($order_info['comment']);

			// History
			$data['histories'] = array();

			$results = $this->model_account_order->getOrderHistories($this->request->get['order_id']);

			foreach ($results as $result) {
				$data['histories'][] = array(
					'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
					'status'     => $result['status'],
					'comment'    => $result['notify'] ? nl2br($result['comment']) : ''
				);
			}

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/order_info', $data));
		} else {
			$this->document->setTitle($this->language->get('text_order'));

			$data['heading_title'] = $this->language->get('text_order');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['breadcrumbs'] = array();

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/home')
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_account'),
				'href' => $this->url->link('account/account', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('account/order', '', true)
			);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_order'),
				'href' => $this->url->link('account/order/info', 'order_id=' . $order_id, true)
			);

			$data['continue'] = $this->url->link('account/order', '', true);

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function reorder() {
		$this->load->language('account/order');

		if (isset($this->request->get['order_id'])) {
			$order_id = $this->request->get['order_id'];
		} else {
			$order_id = 0;
		}

		$this->load->model('account/order');

		$order_info = $this->model_account_order->getOrder($order_id);

		if ($order_info) {
			if (isset($this->request->get['order_product_id'])) {
				$order_product_id = $this->request->get['order_product_id'];
			} else {
				$order_product_id = 0;
			}

			$order_product_info = $this->model_account_order->getOrderProduct($order_id, $order_product_id);

			if ($order_product_info) {
				$this->load->model('catalog/product');

				$product_info = $this->model_catalog_product->getProduct($order_product_info['product_id']);

				if ($product_info) {
					$option_data = array();

					$order_options = $this->model_account_order->getOrderOptions($order_product_info['order_id'], $order_product_id);

					foreach ($order_options as $order_option) {
						if ($order_option['type'] == 'select' || $order_option['type'] == 'radio' || $order_option['type'] == 'image') {
							$option_data[$order_option['product_option_id']] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'checkbox') {
							$option_data[$order_option['product_option_id']][] = $order_option['product_option_value_id'];
						} elseif ($order_option['type'] == 'text' || $order_option['type'] == 'textarea' || $order_option['type'] == 'date' || $order_option['type'] == 'datetime' || $order_option['type'] == 'time') {
							$option_data[$order_option['product_option_id']] = $order_option['value'];
						} elseif ($order_option['type'] == 'file') {
							$option_data[$order_option['product_option_id']] = $this->encryption->encrypt($order_option['value']);
						}
					}

					$this->cart->add($order_product_info['product_id'], $order_product_info['quantity'], $option_data);

					$this->session->data['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $product_info['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

					unset($this->session->data['shipping_method']);
					unset($this->session->data['shipping_methods']);
					unset($this->session->data['payment_method']);
					unset($this->session->data['payment_methods']);
				} else {
					$this->session->data['error'] = sprintf($this->language->get('error_reorder'), $order_product_info['name']);
				}
			}
		}

		$this->response->redirect($this->url->link('account/order/info', 'order_id=' . $order_id));
	}
}