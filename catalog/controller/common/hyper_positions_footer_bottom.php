<?php
class ControllerCommonHyperPositionsFooterBottom extends Controller {

    public function index() {
        $this->load->model('design/layout');

        if (isset($this->request->get['route'])) {
            $route = (string)$this->request->get['route'];
        } else {
            $route = 'common/home';
        }

        $layout_id = 0;

        if ($route == 'product/category' && isset($this->request->get['path'])) {
            $this->load->model('catalog/category');

            $path = explode('_', (string)$this->request->get['path']);

            $layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
        }

        if ($route == 'product/product' && isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
        }

        if ($route == 'information/information' && isset($this->request->get['information_id'])) {
            $this->load->model('catalog/information');

            $layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
        }

        if (!$layout_id) {
            $layout_id = $this->model_design_layout->getLayout($route);
        }

        if (!$layout_id) {
            $layout_id = $this->config->get('config_layout_id');
        }


        $this->load->model('design/hyper_positions');

        $this->load->model('extension/module');

        $positions = $this->model_design_hyper_positions->getPositions($layout_id);

        $data['pos_footer4'] = isset($positions['pos_footer4']) ? '-fluid': false;
        $data['pos_footer5'] = isset($positions['pos_footer5']) ? '-fluid': false;
        $data['pos_copyright'] = isset($positions['pos_copyright']) ? '-fluid': false;
        $data['pos_copyright2'] = isset($positions['pos_copyright2']) ? '-fluid': false;

        for($i = 59; $i < 72; $i++){

            $data['modules'.$i] = array();

            $modules = array();

            switch ($i) {
                case 59:// FOOTER 4 - 4 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer4_left');
                    break;
                case 60:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer4_cl');
                    break;
                case 61:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer4_cr');
                    break;
                case 62:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer4_right');
                    break;
                case 63:// FOOTER 5 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer5_left');
                    break;
                case 64:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer5_center');
                    break;
                case 65:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'footer5_left');
                    break;
                case 66:// COPYRIGHT 1 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy_left');
                    break;
                case 67:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy_center');
                    break;
                case 68:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy_right');
                    break;
                case 69:// COPYRIGHT 2 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy2_left');
                    break;
                case 70:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy2_center');
                    break;
                case 71:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'copy2_right');
                    break;
            }

            foreach ($modules as $module) {
                $part = explode('.', $module['code']);

                if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
                    $module_data = $this->load->controller('extension/module/' . $part[0]);

                    if ($module_data) {
                        $data['modules'.$i][] = $module_data;
                    }
                }

                if (isset($part[1])) {
                    $setting_info = $this->model_extension_module->getModule($part[1]);

                    if ($setting_info && $setting_info['status']) {
                        $output = $this->load->controller('extension/module/' . $part[0], $setting_info);

                        if ($output) {
                            $data['modules'.$i][] = $output;
                        }
                    }
                }
            }



        }

        return $this->load->view('common/hyper_positions_footer_bottom', $data);
    }
}