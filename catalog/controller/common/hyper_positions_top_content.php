<?php
class ControllerCommonHyperPositionsTopContent extends Controller {

    public function index() {
        $this->load->model('design/layout');

        if (isset($this->request->get['route'])) {
            $route = (string)$this->request->get['route'];
        } else {
            $route = 'common/home';
        }

        $layout_id = 0;

        if ($route == 'product/category' && isset($this->request->get['path'])) {
            $this->load->model('catalog/category');

            $path = explode('_', (string)$this->request->get['path']);

            $layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
        }

        if ($route == 'product/product' && isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
        }

        if ($route == 'information/information' && isset($this->request->get['information_id'])) {
            $this->load->model('catalog/information');

            $layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
        }

        if (!$layout_id) {
            $layout_id = $this->model_design_layout->getLayout($route);
        }

        if (!$layout_id) {
            $layout_id = $this->config->get('config_layout_id');
        }


        $this->load->model('design/hyper_positions');

        $this->load->model('extension/module');

        $positions = $this->model_design_hyper_positions->getPositions($layout_id);
//print_r($positions);die;
        $data['top_header'] = isset($positions['top_header']) ? '-fluid': false;
        $data['header_menu'] = isset($positions['header_menu']) ? '-fluid': false;
        $data['content_header'] = isset($positions['content_header']) ? '-fluid': false;
        $data['general_menu'] = isset($positions['general_menu']) ? '-fluid': false;
        $data['pos_sliders'] = isset($positions['pos_sliders']) ? '-fluid': false;
        $data['pos_top'] = isset($positions['pos_top']) ? '-fluid': false;
        $data['pos_top1'] = isset($positions['pos_top1']) ? '-fluid': false;
        $data['pos_top2'] = isset($positions['pos_top2']) ? '-fluid': false;
        $data['pos_banner'] = isset($positions['pos_banner']) ? '-fluid': false;
        $data['pos_top3'] = isset($positions['pos_top3']) ? '-fluid': false;
        $data['pos_content'] = isset($positions['pos_content']) ? '-fluid': false;

        for($i = 5; $i < 21; $i++){

            $data['modules'.$i] = array();

            $modules = array();

            switch ($i) {
                case 5:// SLIDER - 1 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'pos_sliders');
                    break;
                case 6:// TOP - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top_left');
                    break;
                case 7:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top_center');
                    break;
                case 8:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top_right');
                    break;
                case 9:// TOP 1 - 4 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top1_left');
                    break;
                case 10:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top1_cl');
                    break;
                case 11:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top1_cr');
                    break;
                case 12:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top1_right');
                    break;
                case 13:// TOP 2 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top2_left');
                    break;
                case 14:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top2_center');
                    break;
                case 15:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top2_right');
                    break;
                case 16:// BANNER 1 - 1 block
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'pos_banner');
                    break;
                case 17:// TOP 3 - 4 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top3_left');
                    break;
                case 18:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top3_cl');
                    break;
                case 19:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top3_cr');
                    break;
                case 20:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'top3_right');
                    break;
            }

            foreach ($modules as $module) {
                $part = explode('.', $module['code']);

                if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
                    $module_data = $this->load->controller('extension/module/' . $part[0]);

                    if ($module_data) {
                        $data['modules'.$i][] = $module_data;
                    }
                }

                if (isset($part[1])) {
                    $setting_info = $this->model_extension_module->getModule($part[1]);

                    if ($setting_info && $setting_info['status']) {
                        $output = $this->load->controller('extension/module/' . $part[0], $setting_info);

                        if ($output) {
                            $data['modules'.$i][] = $output;
                        }
                    }
                }
            }



        }



        return $this->load->view('common/hyper_positions_top_content', $data);
    }
}

