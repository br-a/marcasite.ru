<?php
class ControllerCommonHyperPositionsBottomContent extends Controller {

    public function index() {
        $this->load->model('design/layout');

        if (isset($this->request->get['route'])) {
            $route = (string)$this->request->get['route'];
        } else {
            $route = 'common/home';
        }

        $layout_id = 0;

        if ($route == 'product/category' && isset($this->request->get['path'])) {
            $this->load->model('catalog/category');

            $path = explode('_', (string)$this->request->get['path']);

            $layout_id = $this->model_catalog_category->getCategoryLayoutId(end($path));
        }

        if ($route == 'product/product' && isset($this->request->get['product_id'])) {
            $this->load->model('catalog/product');

            $layout_id = $this->model_catalog_product->getProductLayoutId($this->request->get['product_id']);
        }

        if ($route == 'information/information' && isset($this->request->get['information_id'])) {
            $this->load->model('catalog/information');

            $layout_id = $this->model_catalog_information->getInformationLayoutId($this->request->get['information_id']);
        }

        if (!$layout_id) {
            $layout_id = $this->model_design_layout->getLayout($route);
        }

        if (!$layout_id) {
            $layout_id = $this->config->get('config_layout_id');
        }

        $this->load->model('design/hyper_positions');

        $this->load->model('extension/module');

        $positions = $this->model_design_hyper_positions->getPositions($layout_id);

        $data['pos_banner2'] = isset($positions['pos_banner2']) ? '-fluid': false;
        $data['pos_content2'] = isset($positions['pos_content2']) ? '-fluid': false;
        $data['pos_bottom'] = isset($positions['pos_bottom']) ? '-fluid': false;
        $data['pos_banner3'] = isset($positions['pos_banner3']) ? '-fluid': false;
        $data['pos_bottom1'] = isset($positions['pos_bottom1']) ? '-fluid': false;
        $data['pos_bottom2'] = isset($positions['pos_bottom2']) ? '-fluid': false;
        $data['pos_bottom3'] = isset($positions['pos_bottom3']) ? '-fluid': false;
        $data['pos_banner4'] = isset($positions['pos_banner4']) ? '-fluid': false;
        $data['pos_bottom4'] = isset($positions['pos_bottom4']) ? '-fluid': false;

        for($i = 21; $i < 45; $i++){

            $data['modules'.$i] = array();

            $modules = array();

            switch ($i) {
                case 21:// BANNER - 1 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'pos_banner2');
                    break;
                case 22:// CONTENT 2 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'column2_left');
                    break;
                case 23:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'column2_center');
                    break;
                case 24:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'column2_right');
                    break;
                case 25:// BOTTOM - 4 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom_left');
                    break;
                case 26:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom_cl');
                    break;
                case 27:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom_cr');
                    break;
                case 28:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom_right');
                    break;
                case 29:// BANNER 3 - 1 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'pos_banner3');
                    break;
                case 30:// BOTTOM 1 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom1_left');
                    break;
                case 31:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom1_center');
                    break;
                case 32:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom1_right');
                    break;
                case 33:// BOTTOM 2 - 4 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom2_left');
                    break;
                case 34:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom2_cl');
                    break;
                case 35:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom2_cr');
                    break;
                case 36:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom2_right');
                    break;
                case 37:// BOTTOM 3 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom3_left');
                    break;
                case 38:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom3_center');
                    break;
                case 39:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom3_right');
                    break;
                case 40:// BANNER 4 - 1 block
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'pos_banner4');
                    break;
                case 41:// BOTTOM 4 - 3 blocks
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom4_left');
                    break;
                case 42:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom4_center');
                    break;
                case 43:
                    $modules = $this->model_design_layout->getLayoutModules($layout_id, 'bottom4_right');
                    break;
            }

            foreach ($modules as $module) {
                $part = explode('.', $module['code']);

                if (isset($part[0]) && $this->config->get($part[0] . '_status')) {
                    $module_data = $this->load->controller('extension/module/' . $part[0]);

                    if ($module_data) {
                        $data['modules'.$i][] = $module_data;
                    }
                }

                if (isset($part[1])) {
                    $setting_info = $this->model_extension_module->getModule($part[1]);

                    if ($setting_info && $setting_info['status']) {
                        $output = $this->load->controller('extension/module/' . $part[0], $setting_info);

                        if ($output) {
                            $data['modules'.$i][] = $output;
                        }
                    }
                }
            }



        }



        return $this->load->view('common/hyper_positions_bottom_content', $data);
    }
}

