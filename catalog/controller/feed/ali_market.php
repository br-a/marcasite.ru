<?php
   // header("Content-Type:text/xml");
    header('Content-Type: text/html; charset=UTF-8');
    require 'config_marc.php';
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <!DOCTYPE yml_catalog SYSTEM \"shops.dtd\">";
    $host = $_SERVER['SERVER_NAME'];
    
    //для обрезки тегов из описания
    $search = array ("'<script[^>]*?>.*?</script>'si",  // Вырезает javaScript
                 "'<[\/\!]*?[^<>]*?>'si",           // Вырезает HTML-теги
                 "'([\r\n])[\s]+'",                 // Вырезает пробельные символы
                 "'&(quot|#34);'i",                 // Заменяет HTML-сущности
                 "'&(amp|#38);'i",
                 "'&(lt|#60);'i",
                 "'&(gt|#62);'i",
                 "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i",
                 "'&(cent|#162);'i",
                 "'&(pound|#163);'i",
                 "'&(copy|#169);'i",
                 "'&#(\d+);'e");                    // интерпретировать как php-код
 
    $replace = array ("",
                      "",
                      "\\1",
                      "\"",
                      "&",
                      "<",
                      ">",
                      " ",
                      chr(161),
                      chr(162),
                      chr(163),
                      chr(169),
                      "chr(\\1)");
	

//	БЫСТРЫЙ ВАРИАНТ ДЛЯ МЕРЧАНДА, ПРОПИШЕМ id товаров В ТАБЛИЦЕ РЕДИРЕКТИОН	
	$prod_id_seourl_arr = array();
	
	$q = "SELECT * FROM `oc_url_alias` WHERE `query` LIKE '%product_id=%' ORDER BY `url_alias_id` ASC";
	$res = mysql_query($q);
	echo mysql_error();
		if(mysql_num_rows($res)) {
			while($info = mysql_fetch_array($res)) {					 
				$id = $info["url_alias_id"];
				$new_url = $info["query"];
				$old_url = $info["keyword"];
				$product_id = search_isk($new_url, 'product_id=');
				
//				$q3 = "UPDATE bread_redirection SET `product_id` = '".$product_id ."' WHERE `id`='".$id ."'";
//				$res3 = mysql_query($q3);
				
				$prod_id_seourl_arr[$product_id] = $old_url;
														 
			  }
	  }

	function search_isk($new_url, $isk) { 
	$dlina_iskomogo = strlen($isk);
	$pos_category_id = stripos($new_url, $isk);
		if ($pos_category_id!==false) {		 
			$sled_segm = stripos($new_url, '&', $pos_category_id+1);
				if ($sled_segm===false) {				
					$iskomoe = substr($new_url,$pos_category_id+$dlina_iskomogo);				  
				}
				else {
					$dlina = $sled_segm - ($pos_category_id+$dlina_iskomogo);
					$iskomoe = substr($new_url,$pos_category_id+$dlina_iskomogo,$dlina);
				}
			
		}
		return $iskomoe;
	}  
//	БЫСТРЫЙ ВАРИАНТ ДЛЯ МЕРЧАНДА, ПРОПИШЕМ id товаров В ТАБЛИЦЕ РЕДИРЕКТИОН
	
// Заведем вспомогательный массив названий производителей

// Заведем вспомогательный массив названий производителей
    
    ?>
    <yml_catalog date="<?php echo date("Y-m-d H:i"); ?>">
        <shop>
            <name>Марказит Розничный</name>
            <company>Марказит Розничный</company>
            <url>https://marcasite.opencartdev.ru/</url>
            <phone>(495)730-51-14, (977) 819-73-57</phone>
            <platform>Opencart 2.3</platform>
            <version>2.3</version>
            <currencies>
                <currency id="RUR" rate="1"></currency>
            </currencies>
            <categories>
                <?php 
                $q = "SELECT ocd.name,  ocd.category_id, oc.category_parent_id
                        FROM `oc_category_description` ocd
                        LEFT JOIN `oc_category` oc ON ocd.category_id = c.category_id
                        WHERE oc.status = '1'
                        ORDER BY oc.parent_id";
                $res = mysql_query($q);
                echo mysql_error();
                while($category = mysql_fetch_array($res)) {
                    ?>
                    <category id="<?php echo $category["category_id"]; ?>" <?php if((int)$category["parent_id"]) { echo "parentId=\"".$category["parent_id"]."\""; } ?>><?php echo $category["name"]; ?></category>
                    <?php
                }
                ?>
            </categories>
            <?php /*?><local_delivery_cost>159</local_delivery_cost><?php */?>
            <offers>

                <?php
                $q = "SELECT p.product_id, p.quantity, opd.name, p.image, p.weight, optc.category_id, p.price, ps.price `s_price`  
                        FROM `oc_product` p
						LEFT JOIN `oc_product_description` opd ON p.product_id = opd.product_id
                        LEFT JOIN `oc_product_to_category` optc ON p.product_id = optc.product_id
                        LEFT JOIN `oc_product_special` ps ON p.product_id = ps.product_id
						WHERE p.status='1' AND p.quantity >= 3 AND p.image!=''
                        AND p.price > 1 AND p.weight > 0
                        GROUP BY p.product_id";
                $res = mysql_query($q);
                echo mysql_error();
                $product = mysql_fetch_array($res);


// Проверим есть ли SEOURL на нужный нам id товара в быстром массиве расчитанном в начале
	if (!isset($prod_id_seourl_arr[$product["product_id"]])) {
		//echo 'problem'.'<br>';
		$rozisk_prod_id = $product["product_id"];
		$q2 = "SELECT * FROM `oc_url_alias` WHERE `query` LIKE '%product_id=".$rozisk_prod_id."%' ORDER BY `url_alias_id` ASC";
        	$res2 = mysql_query($q2);
    		echo mysql_error();
    		if(mysql_num_rows($res2)) {        		
        		while($info2 = mysql_fetch_array($res2)) {
					$new_url = $info2['query'];
					$seo_url = $info2['keyword'];
					$product_id = search_isk($new_url, '&product_id=');
						if ($product_id == $rozisk_prod_id) {
							$prod_id_seourl_arr[$product["product_id"]] = $seo_url;							
						//	echo 'nashli'.' - '.$seo_url.'<br>';
							break;
						}					
			}
		}
		
	}	
// Проверим есть ли SEOURL на нужный нам id товара в быстром массиве расчитанном в начале


$p_add_images = '';
$p_id = $product["product_id"];
// "дополнительная ссылка на изображение" (additional image link)
$q1 = "SELECT * FROM `oc_product`";
$res1 = mysql_query($q1);
echo mysql_error();
if(mysql_num_rows($res1)) {        		
	while($info1 = mysql_fetch_array($res1)) {
		// Проверка и модификация ссылки картинки (чаcть почему то без домена часть без www
		$pic_dop = $info1['image'];
		if (substr($pic_dop,0,7)=='https://') {
			if (substr($pic_dop,7,3)!='www') {
				$pic_dop = str_replace('https://','https://www.',$pic_dop);
				$pic_dop = str_replace('marcasite.opencartdev.ru',$pic_dop);
			}						
		}
		if (substr($pic_dop,0,1)=='/') {
			$pic_dop = 'https://'.$host . $pic_dop;
		}

		// Маркет не любит названия файлов содержащие пробелы, поэтому отфильтруем
		if (stripos($info1['image'], ' ')===false) {
			$p_add_images.='<picture>'.$pic_dop.'</picture>'."\n";
		}		
	}
}
// "дополнительная ссылка на изображение" (additional image link)
$p_s_price= number_format($product["s_price"],2, '.', '');
$p_price = number_format($product["price"],2, '.', '');
//$isklyuch = iconv("UTF-8", "windows-1251", 'Хлебная смесь');
if (isset($prod_id_seourl_arr[$product["product_id"]]) && stripos($product["name"], 'Распродажа')===false) {
//	echo $product["product_name"].'<br>';
//	continue;	
?>
<offer id="<?php echo $product["product_id"]; ?>" available="true">						
<?php /*?><url>http://<?php echo $host.'/'.$prod_id_seourl_arr[$product["product_id"]]; ?><?php echo str_replace('&amp;utm_source=Yandex.Market','?utm_source=Yandex.Market',$product["utm"]); ?></url><?php */?>
<url>https://<?php echo $host.'/'.$prod_id_seourl_arr[$product["product_id"]]; ?><?php echo '?utm_source=Yandex.Market&amp;utm_medium=cpc&amp;utm_campaign='.$product["product_id"]; ?></url>
<?php if ($p_s_price!='') {?>
<price><?php echo $p_s_price; ?></price>
<oldprice><?php echo $p_price; ?></oldprice>
<?php } else { ?>
<price><?php echo $p_price; ?></price>
<?php } ?>
<currencyId>RUR</currencyId>
<categoryId><?php echo $product["category_id"]; ?></categoryId>
<picture>https://<?php echo $host; ?>/image/cache/<?php echo $product["image"]; ?></picture>
<?php if ($p_add_images!='') {
echo $p_add_images;
} ?>
<pickup>true</pickup>
<delivery>true</delivery>
<?php /*?><local_delivery_cost><?php echo $cost; ?></local_delivery_cost><?php */?>
<weight><?php echo $product["weight"]; ?></weight>
<name><?php echo $product["name"]; ?></name>
<vendor>Marcasite, Ltd.</vendor>
</offer>
<?php
echo "\n";
}
                ?>
            </offers>
        </shop>
    </yml_catalog>