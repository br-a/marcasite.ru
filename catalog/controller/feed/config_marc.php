<?php
// HTTP
define('HTTP_SERVER', 'https://marcasite.opencartdev.ru/');

// HTTPS
define('HTTPS_SERVER', 'https://marcasite.opencartdev.ru/');

// DIR
define('DIR_APPLICATION', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/catalog/');
define('DIR_EXCEL', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/excel/');
define('DIR_SYSTEM', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/');
define('DIR_IMAGE', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/image/');
define('DIR_LANGUAGE', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/catalog/language/');
define('DIR_TEMPLATE', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/catalog/view/theme/');
define('DIR_CONFIG', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/config/');
define('DIR_CACHE', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/storage/download/');
define('DIR_LOGS', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/storage/logs/');
define('DIR_MODIFICATION', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/storage/modification/');
define('DIR_UPLOAD', '/var/www/marcroz/data/www/marcasite.opencartdev.ru/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'marcrozbd');
define('DB_PASSWORD', '4C1r6Y9v');
define('DB_DATABASE', 'marcrozbd');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
