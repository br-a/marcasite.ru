<?php
class ControllerModuleB24Apipro extends Controller
{
	private $error = array();

	public function addOrder($route, $data, $output){
		$this->load->model('module/b24_order');
		$order_id = isset($data[0]) ? $data[0] : '';
		$order_status = isset($data[1]) ? $data[1] : '';
		$order_status_send = $this->config->get('b24_order')['status'];
		$get_b24_order = $this->model_module_b24_order->getById($order_id);
		if(isset($get_b24_order['oc_order_id'])){
			$this->editOrder($order_id);
			$this->log->write(print_r($order_id,true));
			return;
		}
		if (in_array($order_status, $order_status_send)){
			$this->model_module_b24_order->addOrder($order_id);
		} else {
			$this->model_module_b24_order->editOrder($order_id);
		}
	}

	public function editOrderStatus($route){
		//$this->load->model('module/b24_order');
		//$order_id = $this->request->get['order_id'];
		//$order_status_id = $this->request->post['order_status_id'];
		//$this->model_module_b24_order->editOrderStatus($order_id, $order_status_id);
	}
	
	public function editOrder($order_id){
		$this->load->model('module/b24_order');
		$this->model_module_b24_order->editOrder($order_id);
	}

	public function addCustomer($route, $data, $customerId){
		$this->load->model('module/b24_customer');
		$this->model_module_b24_customer->addCustomer($customerId);
	}

	public function editCustomer($route, $data){
		$this->load->model('module/b24_customer');
		$customerId = $this->customer->getId();
		$this->model_module_b24_customer->editCustomer($customerId);
	}

	public function addAddress($route, $data, $addressId){
		$this->load->model('module/b24_customer');
		if ($this->isMainAddress($addressId)) {
			$this->model_module_b24_customer->editCustomerAddress($addressId);
		}
	}

	public function editAddress($route, $addressId, $data){
		$this->load->model('module/b24_customer');
		if ($this->isMainAddress($addressId[0])) {
			$this->model_module_b24_customer->editCustomerAddress($addressId[0]);
		}
	}

	public function index(){
		$this->document->setTitle("Customer Module");
		$this->load->model('module/b24_order');
	}

	public function isMainAddress($addressId){
		$this->load->model('account/customer');

		$customer = $this->model_account_customer->getCustomer($this->customer->getId());

		$currentAddressId = intval($customer['address_id']);

		return $currentAddressId === intval($addressId);
	}
}