<!-- SLIDER - 1 blocks -->
<?php if ($modules5) { ?>
    <div id="pos-slider">
        <div class="container<?php echo $pos_sliders; ?>">
            <div class="row">
                <?php foreach ($modules5 as $module) { ?>
                    <?php echo $module; ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- TOP - 3 blocks -->
<?php if ($modules6 || $modules7 || $modules8) { ?>
    <div id="pos-top">
        <div class="container<?php echo $pos_top; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules6 && $modules7 && $modules8) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules6 && $modules8) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules6 && $modules7) || ($modules7 && $modules8)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules6) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules6 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules7) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules7 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules8) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules8 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- TOP 1 - 4 blocks -->
<?php if ($modules9 || $modules10 || $modules11 || $modules12) { ?>
    <div id="pos-top1">
        <div class="container<?php echo $pos_top1; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules9 && $modules10 && $modules11 && $modules12) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules9 && $modules10 && $modules11) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules10 && $modules11 && $modules12) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules10 && $modules11) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules9 && $modules10 && $modules12) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules9 && $modules11 && $modules12) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules9 && $modules11) || ($modules10 && $modules12)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules9) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules9 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules10) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules10 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules11) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules11 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules12) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules12 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- TOP 2 - 3 blocks -->
<?php if ($modules13 || $modules14 || $modules15) { ?>
    <div id="pos-top2">
        <div class="container<?php echo $pos_top2; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules13 && $modules14 && $modules15) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules13 && $modules15) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules13 && $modules14) || ($modules14 && $modules15)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules13) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules13 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules14) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules14 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules15) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules15 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BANNER 1 - 1 block -->
<?php if ($modules16) { ?>
    <div id="pos-banner">
        <div class="container<?php echo $pos_banner; ?>">
            <div class="row">
                <?php foreach ($modules16 as $module) { ?>
                    <?php echo $module; ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- TOP 3 - 4 blocks-->
<?php if ($modules17 || $modules18 || $modules19 || $modules20) { ?>
    <div id="pos-top3">
        <div class="container<?php echo $pos_top3; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules17 && $modules18 && $modules19 && $modules20) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules17 && $modules18 && $modules19) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules18 && $modules19 && $modules20) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules18 && $modules19) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules17 && $modules18 && $modules20) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules17 && $modules19 && $modules20) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules17 && $modules19) || ($modules18 && $modules20)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules17) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules17 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules18) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules18 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules19) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules19 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules20) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules20 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
<?php } ?>
