<!-- FOOTER 4 - 4 blocks -->
<?php if ($modules59 || $modules60 || $modules61 || $modules62) { ?>
    <div id="pos-footer4">
        <div class="container<?php echo $pos_footer4; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules59 && $modules60 && $modules61 && $modules62) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules59 && $modules60 && $modules61) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules60 && $modules61 && $modules62) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules60 && $modules61) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules59 && $modules60 && $modules62) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules59 && $modules61 && $modules62) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules59 && $modules61) || ($modules60 && $modules62)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules59) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules59 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules60) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules60 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules61) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules61 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules62) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules62 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- FOOTER 5 - 3 blocks -->
<?php if ($modules63 || $modules64 || $modules65) { ?>
    <div id="pos-footer5">
        <div class="container<?php echo $pos_footer5; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules63 && $modules64 && $modules65) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules63 && $modules65) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules63 && $modules64) || ($modules64 && $modules65)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_c = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules63) { ?>
                    <div id="column-left" class="<?php echo $class_l; ?>">
                        <?php foreach ($modules63 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules64) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules64 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules65) { ?>
                    <div id="column-right" class="<?php echo $class_r; ?>">
                        <?php foreach ($modules65 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div><hr>
        </div>
    </div>
<?php } ?>
<!-- COPYRIGHT 1 - 3 blocks -->
<?php if ($modules66 || $modules67 || $modules68) { ?>
    <div id="pos-copyright">
        <div class="container<?php echo $pos_copyright; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules66 && $modules67 && $modules68) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules66 && $modules68) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules66 && $modules67) || ($modules67 && $modules68)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules66) { ?>
                    <div id="column-left" class="<?php echo $class_l; ?>">
                        <?php foreach ($modules66 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules67) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules67 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules68) { ?>
                    <div id="column-right" class="<?php echo $class_r; ?>">
                        <?php foreach ($modules68 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- COPYRIGHT 2 - 3 blocks -->
<?php if ($modules69 || $modules70 || $modules71) { ?>
    <div id="pos-copyright2">
        <div class="container<?php echo $pos_copyright2; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules69 && $modules70 && $modules71) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules69 && $modules71) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules69 && $modules70) || ($modules70 && $modules71)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_c = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules69) { ?>
                    <div id="column-left" class="<?php echo $class_l; ?>">
                        <?php foreach ($modules69 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules70) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules70 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules71) { ?>
                    <div id="column-right" class="<?php echo $class_r; ?>">
                        <?php foreach ($modules71 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    $( document ).ready(function() {
        var posFooter4 = $('#pos-footer4');
        $("#pos-footer4").remove();
        $(posFooter4).appendTo('FOOTER');

        var posFooter5 = $('#pos-footer5');
        $("#pos-footer5").remove();
        $(posFooter5).appendTo('FOOTER');
    });
</script>