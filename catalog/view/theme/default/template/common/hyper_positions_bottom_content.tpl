<!-- BANNER 2 - 1 blocks -->
<?php if ($modules21) { ?>
    <div id="pos-banner2">
        <div class="container<?php echo $pos_banner2; ?>">
            <div class="row">
                <?php foreach ($modules21 as $module) { ?>
                    <?php echo $module; ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- CONTENT 2 - 3 blocks -->
<?php if ($modules22 || $modules23 || $modules24) { ?>
    <div id="pos-content2">
        <div class="container<?php echo $pos_content2; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules22 && $modules23 && $modules24) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules22 && $modules24) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules22 && $modules23) || ($modules23 && $modules24)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_c = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules22) { ?>
                    <aside id="column-left" class="<?php echo $class_l; ?> hidden-xs">
                        <?php foreach ($modules22 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </aside>
                <?php } ?>
                <?php if ($modules23) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules23 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules24) { ?>
                    <aside id="column-right" class="<?php echo $class_r; ?> hidden-xs">
                        <?php foreach ($modules24 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </aside>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BOTTOM - 4 blocks -->
<?php if ($modules25 || $modules26 || $modules27 || $modules28) { ?>
    <div id="pos-bottom">
        <div class="container<?php echo $pos_bottom; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules25 && $modules26 && $modules27 && $modules28) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules25 && $modules26 && $modules27) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules26 && $modules27 && $modules28) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules26 && $modules27) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules25 && $modules26 && $modules28) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules25 && $modules27 && $modules28) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules25 && $modules27) || ($modules26 && $modules28)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules25) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules25 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules26) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules26 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules27) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules27 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules28) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules28 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BANNER 3 - 1 blocks -->
<?php if ($modules29) { ?>
    <div id="pos-banner3">
        <div class="container<?php echo $pos_banner3; ?>">
            <div class="row">
                <?php foreach ($modules29 as $module) { ?>
                    <?php echo $module; ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BOTTOM 1 - 3 blocks -->
<?php if ($modules30 || $modules31 || $modules32) { ?>
    <div id="pos-bottom1">
        <div class="container<?php echo $pos_bottom1; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules30 && $modules31 && $modules32) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules30 && $modules32) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules30 && $modules31) || ($modules31 && $modules32)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules30) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules30 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules31) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules31 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules32) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules32 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BOTTOM 2 - 4 blocks -->
<?php if ($modules33 || $modules34 || $modules35 || $modules36) { ?>
    <div id="pos-bottom2">
        <div class="container<?php echo $pos_bottom2; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules33 && $modules34 && $modules35 && $modules36) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules33 && $modules34 && $modules35) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules34 && $modules35 && $modules36) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules34 && $modules35) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules33 && $modules34 && $modules36) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules33 && $modules35 && $modules36) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules33 && $modules35) || ($modules34 && $modules36)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules33) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules33 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules34) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules34 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules35) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules35 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules36) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules36 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
<?php } ?>
<!-- BOTTOM 3 - 3 blocks -->
<?php if ($modules37 || $modules38 || $modules39) { ?>
    <div id="pos-bottom3">
        <div class="container<?php echo $pos_bottom3; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules37 && $modules38 && $modules39) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules37 && $modules39) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules37 && $modules38) || ($modules38 && $modules39)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules37) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules37 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules38) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules38 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules39) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules39 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BANNER 4 - 1 block -->
<?php if ($modules40) { ?>
    <div id="pos-banner4">
        <div class="container<?php echo $pos_banner4; ?>">
            <div class="row">
                <?php foreach ($modules40 as $module) { ?>
                    <?php echo $module; ?>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- BOTTOM 4 - 3 blocks -->
<?php if ($modules41 || $modules42 || $modules43) { ?>
    <div id="pos-bottom4">
        <div class="container<?php echo $pos_bottom4; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules41 && $modules42 && $modules43) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 2 или 2 и 3 = 4,8 / 8,4-->
                <?php } elseif ($modules41 && $modules43) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules41 && $modules42) || ($modules42 && $modules43)) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-8'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules41) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules41 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules42) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules42 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules43) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules43 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
