<?php if ($modules1 || $modules2 || $modules3 || $modules4) { ?>
    <div id="top-header" class="collapse in">
        <div class="container<?php echo $top_header; ?>">
            <div class="row">
        <!-- Все 4ре по 3 работает-->
            <?php if ($modules1 && $modules2 && $modules3 && $modules4) { ?>
                <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
        <!-- 1,2 и 3 = 3,3,6 работает-->
            <?php } elseif ($modules1 && $modules2 && $modules3) { ?>
                <?php $class_l = 'col-sm-3'; ?>
                <?php $class_cl = 'col-sm-3'; ?>
                <?php $class_cr = 'col-sm-6'; ?>
        <!-- 2 и 3,4 = 6,3,3 работает-->
            <?php } elseif ($modules2 && $modules3 && $modules4) { ?>
                <?php $class_cl = 'col-sm-6'; ?>
                <?php $class_cr = 'col-sm-3'; ?>
                <?php $class_r = 'col-sm-3'; ?>
        <!-- 2 и 3 = 6,6 работает-->
            <?php } elseif ($modules2 && $modules3) { ?>
                <?php $class_cl = 'col-sm-6'; ?>
                <?php $class_cr = 'col-sm-6'; ?>
        <!-- 1,2 и 4 = 4,4,4 работает-->
            <?php } elseif ($modules1 && $modules2 && $modules4) { ?>
                <?php $class_l = 'col-sm-4'; ?>
                <?php $class_cl = 'col-sm-4'; ?>
                <?php $class_r = 'col-sm-4'; ?>
        <!-- 1 и 3,4 = 3,6,3 работает-->
            <?php } elseif ($modules1 && $modules3 && $modules4) { ?>
                <?php $class_l = 'col-sm-3'; ?>
                <?php $class_cr = 'col-sm-6'; ?>
                <?php $class_r = 'col-sm-3'; ?>
        <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
            <?php } elseif (($modules1 && $modules3) || ($modules2 && $modules4)) { ?>
                <?php $class_l = 'col-sm-3'; ?>
                <?php $class_cl = 'col-sm-9'; ?>
                <?php $class_cr = 'col-sm-9'; ?>
                <?php $class_r = 'col-sm-3'; ?>
        <!-- одна = 12 работает-->
            <?php } else { ?>
                <?php $class_l = 'col-sm-12'; ?>
                <?php $class_cl = 'col-sm-12'; ?>
                <?php $class_cr = 'col-sm-12'; ?>
                <?php $class_r = 'col-sm-12'; ?>
            <?php } ?>

                <?php if ($modules1) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules1 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules2) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules2 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules3) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules3 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules4) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules4 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
<?php } ?>