<!-- FOOTER 1 - 4 blocks -->
<?php if ($modules45 || $modules46 || $modules47 || $modules48) { ?>
    <div id="pos-footer1">
        <div class="container<?php echo $pos_footer1; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules45 && $modules46 && $modules47 && $modules48) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules45 && $modules46 && $modules47) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules46 && $modules47 && $modules48) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules46 && $modules47) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules45 && $modules46 && $modules48) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules45 && $modules47 && $modules48) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules45 && $modules47) || ($modules46 && $modules48)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules45) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules45 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules46) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules46 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules47) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules47 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules48) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules48 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- MAP - 3 blocks -->
<?php if ($modules49 || $modules50 || $modules51) { ?>
    <div id="pos-map">
        <div class="container<?php echo $pos_map; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules49 && $modules50 && $modules51) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3 = 6.6-->
                <?php } elseif ($modules49 && $modules51) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules49 && $modules50) || ($modules50 && $modules51)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_c = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules49) { ?>
                    <div id="column-left" class="<?php echo $class_l; ?>">
                        <?php foreach ($modules49 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules50) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules50 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules51) { ?>
                    <div id="column-right" class="<?php echo $class_r; ?>">
                        <?php foreach ($modules51 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- FOOTER 2 - 4 blocks -->
<?php if ($modules52 || $modules53 || $modules54 || $modules55) { ?>
    <div id="pos-footer2">
        <div class="container<?php echo $pos_footer2; ?>">
            <div class="row">
                <!-- Все 4ре по 3 работает-->
                <?php if ($modules52 && $modules53 && $modules54 && $modules55) { ?>
                    <?php $class_l = 'col-sm-3'; ?><?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-3'; ?><?php $class_r = 'col-sm-3'; ?>
                    <!-- 1,2 и 3 = 3,3,6 работает-->
                <?php } elseif ($modules52 && $modules53 && $modules54) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 2 и 3,4 = 6,3,3 работает-->
                <?php } elseif ($modules52 && $modules53 && $modules55) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-3'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 2 и 3 = 6,6 работает-->
                <?php } elseif ($modules53 && $modules54) { ?>
                    <?php $class_cl = 'col-sm-6'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <!-- 1,2 и 4 = 4,4,4 работает-->
                <?php } elseif ($modules52 && $modules53 && $modules55) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_cl = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3,4 = 3,6,3 работает-->
                <?php } elseif ($modules52 && $modules54 && $modules55) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cr = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules52 && $modules54) || ($modules53 && $modules55)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_cl = 'col-sm-9'; ?>
                    <?php $class_cr = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_cl = 'col-sm-12'; ?>
                    <?php $class_cr = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules52) { ?>
                    <div class="<?php echo $class_l; ?>">
                        <?php foreach ($modules52 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules53) { ?>
                    <div class="<?php echo $class_cl; ?>">
                        <?php foreach ($modules54 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules54) { ?>
                    <div class="<?php echo $class_cr; ?>">
                        <?php foreach ($modules54 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules55) { ?>
                    <div class="<?php echo $class_r; ?>">
                        <?php foreach ($modules55 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<!-- FOOTER 3 - 3 blocks -->
<?php if ($modules56 || $modules57 || $modules58) { ?>
    <div id="pos-footer3">
        <div class="container<?php echo $pos_footer3; ?>">
            <div class="row">
                <!-- Все 3ри по 4 -->
                <?php if ($modules56 && $modules57 && $modules58) { ?>
                    <?php $class_l = 'col-sm-4'; ?>
                    <?php $class_c = 'col-sm-4'; ?>
                    <?php $class_r = 'col-sm-4'; ?>
                    <!-- 1 и 3 = 6.6-->
                <?php } elseif ($modules56 && $modules58) { ?>
                    <?php $class_l = 'col-sm-6'; ?>
                    <?php $class_r = 'col-sm-6'; ?>
                    <!-- 1или3 или 2или4 = 3,9 или 9,3 работает-->
                <?php } elseif (($modules56 && $modules57) || ($modules57 && $modules58)) { ?>
                    <?php $class_l = 'col-sm-3'; ?>
                    <?php $class_c = 'col-sm-9'; ?>
                    <?php $class_r = 'col-sm-3'; ?>
                    <!-- одна = 12 работает-->
                <?php } else { ?>
                    <?php $class_l = 'col-sm-12'; ?>
                    <?php $class_c = 'col-sm-12'; ?>
                    <?php $class_r = 'col-sm-12'; ?>
                <?php } ?>

                <?php if ($modules56) { ?>
                    <div id="column-left" class="<?php echo $class_l; ?>">
                        <?php foreach ($modules56 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules57) { ?>
                    <div class="<?php echo $class_c; ?>">
                        <?php foreach ($modules57 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if ($modules58) { ?>
                    <div id="column-right" class="<?php echo $class_r; ?>">
                        <?php foreach ($modules58 as $module) { ?>
                            <?php echo $module; ?>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div><hr>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">
    $( document ).ready(function() {
        var posFooter3 = $('#pos-footer3');
        $("#pos-footer3").remove();
        $(posFooter3).prependTo('FOOTER');

        var posFooter2 = $('#pos-footer2');
        $("#pos-footer2").remove();
        $(posFooter2).prependTo('FOOTER');
    });
</script>
