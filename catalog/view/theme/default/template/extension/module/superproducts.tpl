<?php foreach ($products as $product) { ?>
<div class="item-product  item-product--border-wide">
<a class="item-product__body" href="<?php echo $product['href']; ?>">
                  <div class="item-product__img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></div>
                  <div class="item-product__info">
				  <?php if ($product['special']) { ?><span class="label item-product__label">-<?php echo $product['skidka']; ?>%</span><?php } ?>
                    <div class="item-product__name"><?php echo $product['name']; ?></div>
                    <div class="item-product__prices">
                      <?php if (!$product['special']) { ?>
                      <div class="item-product__price"><span><?php echo $product['price']; ?> </span>руб</div>
					  <?php } else { ?>
					  <div class="item-product__old-price"><?php echo $product['price']; ?> </span>руб</div>
                      <div class="item-product__price"><span><?php echo $product['special']; ?> </span>руб</div>
					  <?php } ?>
                    </div>
                  </div>
</a>
</div>
<?php } ?>