<h3><?php echo $heading_title; ?></h3>
<?php $class = 3; $id = rand(0, 5000)*rand(0, 5000); $all = 4; $row = 4; ?>
<div class="box box-with-products box-carousel">
	<!-- Carousel nav -->
	<a class="next" href="#myCarousel<?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_next"><span></span></a>
	<a class="prev" href="#myCarousel<?php echo $id; ?>" id="myCarousel<?php echo $id; ?>_prev"><span></span></a>

	<script type="text/javascript">
	  $(document).ready(function() {
		var owl<?php echo $id; ?> = $(".box #myCarousel<?php echo $id; ?> .carousel-inner");

		$("#myCarousel<?php echo $id; ?>_next").click(function(){
			owl<?php echo $id; ?>.trigger('owl.next');
			return false;
		  })
		$("#myCarousel<?php echo $id; ?>_prev").click(function(){
			owl<?php echo $id; ?>.trigger('owl.prev');
			return false;
		});

		owl<?php echo $id; ?>.owlCarousel({
			slideSpeed : 500,
			items: 4,
			itemsCustom : [
				[0, 2],
				[450, 2],
				[550, 2],
				[768, 3],
				[1200, 4]
			]
		 });
	  });
	</script>

	  <div class="box-content products">
		<div id="myCarousel<?php echo $id; ?>" class="carousel slide">
			<div class="carousel-inner">
				<?php $i = 0; foreach($products as $product) { ?>
				<div class="item<?php if($i == 0) { echo ' active'; } ?>">
					<div class="product-grid">
						<div class="col-sm-12">
							<?php include('catalog/view/theme/marcjewelry/template/new_elements/product_new.tpl'); ?>
						</div>
					</div>
				</div>
				<?php $i++; } ?>
			</div>
		</div>
	  </div>
</div>
