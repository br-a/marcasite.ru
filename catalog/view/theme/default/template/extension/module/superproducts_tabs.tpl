<section class="products  products--link-more">
        <div class="container products__container">
          <div class="products__top">
            <h2 class="page-title page-title--line products__title"><?php echo $heading_title; ?></h2>
            <div class="categories-tabs products__tabs"><a class="categories-tabs__select" href="javascript:void(0);"></a>
              <ul class="categories-tabs__list">
                <?php
                $mas_name_ic=array("Золото"=>"gold","Кольца" => "rings", "Серьги" => "earrings", "Браслеты" => "bracelets", "Броши" => "brooches", "Колье" => "necklace", "Подвески" => "necklace",);
                $n =0; foreach($tabs as $tab) { ?>
				<li class="categories-tabs__item"><a class="categories-tabs__link" href="#<?php echo $tab['id']; ?>"><svg class="icon icon-rings " width="18px" height="18px">
                            <?php
                            $name_ic_linc="img/sprite.svg#".$mas_name_ic[$tab['head']];
                            if ($tab['head'] != 'Золото'){
                            $name = $tab['head'];
                            }
                            ?>
                            <use xlink:href="<?php echo $name_ic_linc; ?>"></use>
                    </svg><?php echo $name; ?></a></li>
                <?php

                 $n++; }

                ?>
              </ul>
                <?php
                if ($heading_title == 'Золото'){
                $url_tag_filt = '/jewelry/gold/';
                }else{
                $url_tag_filt = '/jewelry/mfp/stock_status,7/';
                }
                ?>
            </div><a class="link-more products__link-more" href="<?php echo $url_tag_filt; ?>">Смотреть все<svg class="icon icon-arrow " width="8px" height="14px">
                <use xlink:href="img/sprite.svg#arrow"></use>
              </svg></a>
          </div>
		  <?php $n =0; foreach($tabs as $tab) { ?>
          <div class="categories-tabs__content categories-tabs__content<?php if ($n==0) { ?>--active<?php } ?>" id="<?php echo $tab['id']; ?>">
            <div class="products__row js-products-slider">
              <?php echo $tab['body']; ?>
            </div>
          </div>
         <?php $n++; } ?>

        </div>
</section>