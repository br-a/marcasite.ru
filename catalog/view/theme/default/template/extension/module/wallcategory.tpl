<section class="categories">
  <div class="container categories__container">
    <h2 class="page-title page-title--line categories__title">Категории</h2>
    <div class="categories__row">
      <?php foreach ($categories as $category) { ?>
      <div class="item-category">
        <a class="item-category__body" href="<?php echo urldecode($category['href']); ?>">
          <div class="item-category__img">
            <img
              src="image/<?php echo $category['image']; ?>"
              alt="<?php echo $category['name']; ?>"
            />
          </div>
          <h4 class="item-category__name"><?php echo $category['name']; ?></h4>
        </a>
      </div>
      <?php } ?>
    </div>
  </div>
</section>
