<?php

	$mfp_options = array(
		'display_list_of_items' => array(
			'type' => empty( $settings['display_list_of_items']['type'] ) ? 'scroll' : $settings['display_list_of_items']['type'],
			'limit_of_items' => empty( $settings['display_list_of_items']['limit_of_items'] ) ? 4 : (int) $settings['display_list_of_items']['limit_of_items'],
			'max_height' => empty( $settings['display_list_of_items']['max_height'] ) ? 155 : (int) $settings['display_list_of_items']['max_height'],
			'standard_scroll' => empty( $settings['display_list_of_items']['standard_scroll'] ) ? false : $settings['display_list_of_items']['standard_scroll'],
		)
	);

?>


<?php if( ! empty( $settings['javascript'] ) ) { ?>
	<script type="text/javascript">
		<?php echo htmlspecialchars_decode( $settings['javascript'] ); ?>
	</script>
<?php } ?>

<?php

	$button_template = '<div class="mfilter-button mfilter-button-%s">%s</div>';
	$button_temp = '<a href="#" class="%s" %s>%s</a>';
	$buttons = array( 'top' => array(), 'bottom' => array() );

	if( ! empty( $settings['show_reset_button'] ) ) {
		$buttons['bottom'][] = sprintf( $button_temp, 'filters__reset js-reset mfilter-button-reset', '', '' . $text_reset_all );
	}

	if( ! empty( $settings['show_top_reset_button'] ) ) {
		$buttons['top'][] = sprintf( $button_temp, 'filters__reset js-reset mfilter-button-reset', '', '' . $text_reset_all );
	}

	if( ! empty( $settings['refresh_results'] ) && $settings['refresh_results'] == 'using_button' && ! empty( $settings['place_button'] ) ) {
		$place_button = explode( '_', $settings['place_button'] );

		if( in_array( 'top', $place_button ) ) {
			$buttons['top'][] = sprintf( $button_temp, 'btn btn-primary btn-xs', '', $text_button_apply );
		}

		/*if( in_array( 'bottom', $place_button ) ) {
			//$buttons['bottom'][] = sprintf( $button_temp, 'btn btn-primary btn-xs', '', $text_button_apply );
		}*/
	}
$buttons['bottom'][] = sprintf( $button_temp, 'btn btn-primary btn-xs', 'onclick="open_filter(this);"', $text_button_apply );
	foreach( $buttons as $bKey => $bVal ) {
		$buttons[$bKey] = $bVal ? sprintf( $button_template, $bKey, implode( '', $bVal ) ) : '';
	}


?>

<?php if( ! empty( $hide_container ) ) { ?>
	<div style="display: none;">
<?php } ?>

<div class="filters catalog__filters box mfilter-box left mfilter-box-<?php echo (int) $_idx; ?> mfilter-<?php echo $_position; ?><?php echo ! empty( $hide_container ) ? ' mfilter-hide-container' : '' ?><?php echo ! empty( $_displayOptionsAs ) && $_displayOptionsAs == 'modern_horizontal' ? ' mfilter-hide' : ''; ?> mfilter-direction-<?php echo $direction; ?>" id="mfilter-box-<?php echo (int) $_idx; ?>">

   <div class="filters__header"><a class="logo filters__logo" href="javascript:void(0);"><img src="img/logo-white.svg" width="74px" height="40px" alt="Марказит"></a><button class="close filters__close" type="button"><span></span></button></div>
                <div class="filters__top">
                  <div class="filters__main-title">Фильтр</div>
				 <?php echo $buttons['top']; ?>
                </div>
<div class="mfilter-content">
		<ul style="list-style: none; padding-left: 0px;">
			<?php foreach( $filters as $kfilter => $filter ) { ?>
				<?php

					$base_type = empty( $filter['base_type'] ) ? $filter['type'] : $filter['base_type'];
					$base_id = empty( $filter['id'] ) ? '' : $filter['id'];
					$display_list_of_items = empty( $filter['display_list_of_items'] ) ? $mfp_options['display_list_of_items']['type'] : $filter['display_list_of_items'];

					if( in_array( $filter['type'], array( 'vehicles', 'levels', 'price', 'slider', 'select', 'search', 'search_oc', 'text', 'related' ) ) ) {
						$display_list_of_items = '-1';
					}

				?>
				<li
					data-type="<?php echo $filter['type']; ?>"
					data-base-type="<?php echo $base_type; ?>"
					data-id="<?php echo $base_id; ?>"
					data-seo-name="<?php echo $filter['seo_name']; ?>"
					data-inline-horizontal="<?php echo $_horizontalInline ? '1' : '0'; ?>"
					<?php if( isset( $filter['auto_levels'] ) ) { ?>
						data-auto-levels="<?php echo $filter['auto_levels']; ?>"
					<?php } ?>
					data-display-live-filter="<?php
						$display_live_filter = ! empty( $settings['display_live_filter']['enabled'] ) ? '1' : '-1';

						if( ! empty( $filter['display_live_filter'] ) ) {
							$display_live_filter = $filter['display_live_filter'];
						}

						if( $display_live_filter == '1' && ! empty( $settings['display_live_filter']['items'] ) ) {
							echo $settings['display_live_filter']['items'];
						} else {
							echo 0;
						}
					?>"
					<?php if( $filter['type'] == 'levels' ) { ?>
						data-labels="<?php echo implode( '||', $filter['labels'] ); ?>"
					<?php } ?>
					data-display-list-of-items="<?php echo $display_list_of_items; ?>"
					class="mfilter-filter-item mfilter-<?php echo $filter['type']; ?> mfilter-<?php echo $base_type; ?><?php echo $base_type == 'attribute' ? ' mfilter-attributes' : ( $base_type == 'option' ? ' mfilter-options' : ( $base_type == 'filter' ? ' mfilter-filters' : '' ) ); ?>"
					>
	
					

								<div class="mfilter-options">
							
							   
									<?php if( $base_type == 'categories' ) { ?>
			
									<?php } else if( $filter['type'] == 'search' || $filter['type'] == 'search_oc' ) { ?>
										
									<?php } else if( $filter['type'] == 'price' ) { ?>
									
				<div class="filters__group">
                  <div class="filters__title">Цена, руб</div>
				  
                  <div class="extra-controls filters__extra-controls">
				  <input
													id="mfilter-opts-price-min"
													type="text"
													class="js-input-from"
													value="<?php echo isset( $params['price'][0] ) ? $params['price'][0] : ''; ?>"
													/>
				  <input
                                                        id="mfilter-opts-price-max"
                                                        type="text"
                                                        class="js-input-to"
                                                        value="<?php echo isset( $params['price'][1] ) ? $params['price'][1] : ''; ?>"
                                                        />
				  </div>
				  
                  <div class="range-slider filters__range-slider" style="margin-top: 20px;">
				 <div id="mfilter-price-slider"></div>
				  </div>
				  
                </div>
				
									<?php } else if( $filter['type'] == 'slider' ) { ?>
										
									<?php } else if( $filter['type'] == 'text' ) { ?>
										
									<?php } else if( $filter['type'] == 'rating' ) { ?>
										
									<?php } else if( in_array( $filter['type'], array( 'stock_status', 'manufacturers', 'checkbox', 'radio', 'image_list_radio', 'image_list_checkbox' ) ) ) { ?>
										<?php

											$_tmp_type = $filter['type'];

											if( in_array( $filter['type'], array( 'stock_status', 'manufacturers' ) ) ) {
												$_tmp_type = 'checkbox';
											}

										?>
								
										<div class="filters__group">
                                            <?php if ($filter['name'] == "ВидНоменклатуры"){ //условиена переименование фильтра
                                                $filter['name'] = "Тип изделия";}
                                            if ($filter['name'] == "скидка"){
                                                $filter['name'] = "Скидка";
                                            }?>
								<a class="filters__title filters__title--dropdown" href="javascript:void(0);"><?php echo $filter['name']; ?></a>
                                            <div class="filters__dropdown">
                                                <?php if ($filter['name'] == "Вставка") {
                                                $filter_fo_vstavka = "filter_vstavka";
                                                ?>
                                                <input class="search-input" type="text" placeholder="Введите текст для поиска" style="width: 100%; height: 31px; margin-bottom: 10px; font-family: 'Gilroy', -apple-system, BlinkMacSystemFont, 'Roboto', 'Ubuntu', 'Droid Sans', 'Helvetica Neue', 'Arial',
                                       sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; font-size: 16px;"/>
                                                <?php } ?>
							   <div class="filters__dropdown">
                               <div class="filters__items">
							   
											<?php $options_tmp = array(); ?>
											<?php foreach( $filter['options'] as $option_id => $option ) { if( $option['name'] === '' || isset( $options_tmp[$option['key']] ) ) continue; $options_tmp[$option['key']] = true; ?>
												<?php echo $_position == 'content_top' ? '<div class="mfilter-tb">' : ''; ?>
												
						<div class="field-checkbox mfilter-option mfilter-tb-as-tr">
						<label class="field-checkbox__name <?php echo $filter_fo_vstavka ?>">
						<input
                                                            id="mfilter-opts-attribs-<?php echo (int) $_idx; ?>-<?php echo $base_id; ?>-<?php echo $option['key']; ?>"
                                                            class="field-checkbox__input" name="<?php echo $filter['seo_name']; ?>"
                                                            type="<?php echo $_tmp_type == 'image_list_checkbox' ? 'checkbox' : ( $_tmp_type == 'image_list_radio' ? 'radio' : $_tmp_type ); ?>"
                                                        <?php echo ! empty( $params[$filter['seo_name']] ) && ( in_array( $option['value'], $params[$filter['seo_name']] ) || in_array( $option['key'], $params[$filter['seo_name']] ) ) ? ' checked="checked"' : ''; ?>
                                                            value="<?php echo str_replace( '"', '&quot;', $option['value'] ); ?>" />
                          <div class="field-checkbox__name-text"><span class="filter-title"><?php echo $option['name']; ?></span></div>
						  <span class="mfilter-counter"></span>
                        </label>
						</div>
												
												
												<?php echo $_position == 'content_top' ? '</div>' : ''; ?>
											<?php } ?>
											
									
										</div>
								</div>
								</div>
								
									<?php } else if( $filter['type'] == 'vehicles' ) { ?>
									
									<?php } else if( $filter['type'] == 'levels' ) { ?>
										
									<?php } else if( $filter['type'] == 'select' ) { ?>
										
									<?php } else if( in_array( $filter['type'], array( 'image', 'image_radio' ) ) ) { ?>
										
									<?php } ?>
							
								</div>
							

				

					
				</li>
			<?php } ?>
		</ul>

</div>

</div>

        <?php if( ! empty( $hide_container ) ) { ?>
	</div>
<?php } ?>
<script type="text/javascript"> //новый скрипт фильтра вставка
    jQuery.fn.jcOnPageFilter = function(settings) {
        settings = jQuery.extend({
            focusOnLoad: true,
            highlightColor: '#BFE2FF',
            textColorForHighlights: '#000000',
            caseSensitive: false,
            hideNegatives: true,
            parentSectionClass: 'filter-table',
            parentLookupClass: 'filter_vstavka',
            childBlockClass: 'filter-title',
            noFoundClass: 'no-found'
        }, settings);
        jQuery.expr[':'].icontains = function(obj, index, meta) {
            return jQuery(obj).text().toUpperCase().indexOf(meta[3].toUpperCase()) >= 0;
        };
        if(settings.focusOnLoad) {
            jQuery(this).focus();
        }
        jQuery('.'+settings.noFoundClass).css("display", "none");
        var rex = /(<span.+?>)(.+?)(<\/span>)/g;
        var rexAtt = "g";
        if(!settings.caseSensitive) {
            rex = /(<span.+?>)(.+?)(<\/span>)/gi;
            rexAtt = "gi";
        }
        return this.each(function() {
            jQuery(this).keyup(function(e) {
                jQuery('.'+settings.parentSectionClass).show();
                jQuery('.'+settings.noFoundClass).hide();
                if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
                    return false;
                } else {
                    var textToFilter = jQuery(this).val();
                    if (textToFilter.length > 0) {
                        if(settings.hideNegatives) {
                            jQuery('.'+settings.parentLookupClass).stop(true, true).hide();
                        }
                        var _cs = "icontains";
                        if(settings.caseSensitive) {
                            _cs = "contains";
                        }
                        jQuery.each(jQuery('.'+settings.childBlockClass),function(i,obj) {
                            jQuery(obj).html(jQuery(obj).html().replace(new RegExp(rex), "$2"));
                        });
                        jQuery.each(jQuery('.'+settings.childBlockClass+":"+_cs+"(" + textToFilter + ")"),function(i,obj) {
                            if(settings.hideNegatives) {
                                jQuery(obj).closest('.'+settings.parentLookupClass).stop(true, true).show();
                            }
                            var newhtml = jQuery(obj).text();
                            jQuery(obj).html(newhtml.replace(
                                new RegExp(textToFilter, rexAtt),
                                function(match) {
                                    return ["<span style='background:"+settings.highlightColor+";color:"+settings.textColorForHighlights+"'>", match, "</span>"].join("");
                                }
                            ));
                        });

                    } else {
                        jQuery.each(jQuery('.'+settings.childBlockClass),function(i,obj) {
                            var html = jQuery(obj).html().replace(new RegExp(rex), "$2");
                            jQuery(obj).html(html);
                        });
                        if(settings.hideNegatives) {
                            jQuery('.'+settings.parentLookupClass).stop(true, true).show();
                        }
                    }
                }
                if (!jQuery('.'+settings.parentLookupClass+':visible').length) {
                    jQuery('.'+settings.parentSectionClass).hide();
                    jQuery('.'+settings.noFoundClass).show();
                }
            });
        });
    };
</script>
<script>
    $('.search-input').jcOnPageFilter();
</script>
<script type="text/javascript">
	MegaFilterLang.text_display = '<?php echo $text_display; ?>';
	MegaFilterLang.text_list	= '<?php echo $text_list; ?>';
	MegaFilterLang.text_grid	= '<?php echo $text_grid; ?>';
	MegaFilterLang.text_select	= '<?php echo $text_select; ?>';

	jQuery().ready(function(){
		jQuery('#mfilter-box-<?php echo (int) $_idx; ?>').each(function(){
			var _t = jQuery(this).addClass('init'),
				_p = { };

			for( var i = 0; i < MegaFilterINSTANCES.length; i++ ) {
				if( _t.attr('id') == MegaFilterINSTANCES[i]._box.attr('id') ) {
					return;
				}
			}

			<?php foreach( $requestGet as $k => $v ) { if( is_array( $v ) || ! in_array( $k, array( 'mfp_path_aliases', 'mfp_org_path_aliases', 'path_aliases', 'mfp_org_path', 'mfp_path', 'path', 'category_id', 'manufacturer_id', 'filter', 'search', 'sub_category', 'description', 'filter_tag' ) ) ) continue; ?>
				_p['<?php echo $k; ?>'] = '<?php echo addslashes( $v ); ?>';
			<?php } ?>

			MegaFilterINSTANCES.push((new MegaFilter()).init( _t, {
				'idx'					: '<?php echo (int) $_idx; ?>',
				'route'					: '<?php echo $_route; ?>',
				'routeProduct'			: '<?php echo $_routeProduct; ?>',
				'routeHome'				: '<?php echo $_routeHome; ?>',
				'routeCategory'			: '<?php echo $_routeCategory; ?>',
				'routeInformation'		: '<?php echo $_routeInformation; ?>',
				'contentSelector'		: '<?php echo empty( $settings['content_selector'] ) ? '#mfilter-content-container' : addslashes( htmlspecialchars_decode( $settings['content_selector'] ) ); ?>',
				'refreshResults'		: '<?php echo empty( $settings["refresh_results"] ) ? "immediately" : $settings["refresh_results"]; ?>',
				'refreshDelay'			: <?php echo empty( $settings["refresh_delay"] ) ? 1000 : (int) $settings["refresh_delay"]; ?>,
				'autoScroll'			: <?php echo empty( $settings["auto_scroll_to_results"] ) ? 'false' : 'true'; ?>,
				'ajaxGetInfoUrl'		: '<?php echo $ajaxGetInfoUrl; ?>',
				'ajaxResultsUrl'		: '<?php echo $ajaxResultsUrl; ?>',
				'ajaxGetCategoryUrl'	: '<?php echo $ajaxGetCategoryUrl; ?>',
				'priceMin'				: <?php echo (string) $price['min']; ?>,
				'priceMax'				: <?php echo (string) $price['max']; ?>,
				'mijoshop'				: <?php echo $mijo_shop ? 'true' : 'false'; ?>,
				'joo_cart'				: <?php echo $joo_cart ? json_encode( $joo_cart ) : 'false'; ?>,
				'showNumberOfProducts'	: <?php echo empty( $settings['show_number_of_products'] ) ? 'false' : 'true'; ?>,
				'calculateNumberOfProducts' : <?php echo empty( $settings['calculate_number_of_products'] ) ? 'false' : 'true'; ?>,
				'addPixelsFromTop'		: <?php echo empty( $settings['add_pixels_from_top'] ) ? 0 : (int) $settings['add_pixels_from_top']; ?>,
				'displayListOfItems'	: {
					'type'				: '<?php echo $mfp_options['display_list_of_items']['type']; ?>',
					'limit_of_items'	: <?php echo $mfp_options['display_list_of_items']['limit_of_items']; ?>,
					'maxHeight'			: <?php echo $mfp_options['display_list_of_items']['max_height']; ?>,
					'textMore'			: '<?php echo $text_show_more; ?>',
					'textLess'			: '<?php echo $text_show_less; ?>',
					'standardScroll'	: <?php echo $mfp_options['display_list_of_items']['standard_scroll'] ? 'true' : 'false'; ?>
				},
				'smp'					: {
					'isInstalled'			: <?php echo empty( $smp['isInstalled'] ) ? 'false' : 'true'; ?>,
					'disableConvertUrls'	: <?php echo empty( $smp['disableConvertUrls'] ) ? 'false' : 'true'; ?>
				},
				'params'					: _p,
				'inStockDefaultSelected'	: <?php echo empty( $settings['in_stock_default_selected'] ) ? 'false' : 'true'; ?>,
				'inStockStatus'				: '<?php echo empty( $settings['in_stock_status'] ) ? 7 : $settings['in_stock_status']; ?>',
				'showLoaderOverResults'		: <?php echo empty( $settings['show_loader_over_results'] ) ? 'false' : 'true'; ?>,
				'showLoaderOverFilter'		: <?php echo empty( $settings['show_loader_over_filter'] ) ? 'false' : 'true'; ?>,
				'hideInactiveValues'		: <?php echo empty( $settings['hide_inactive_values'] ) ? 'false' : 'true'; ?>,
				'manualInit'				: <?php echo empty( $settings['manual_init'] ) ? 'false' : 'true'; ?>,
				'homePageAJAX'				: <?php echo empty( $settings['home_page_ajax'] ) ? 'false' : 'true'; ?>,
				'homePageContentSelector'	: '<?php echo empty( $settings['home_page_content_selector'] ) ? '#content' : addslashes( htmlspecialchars_decode( $settings['home_page_content_selector'] ) ); ?>',
				'ajaxPagination'			: <?php echo empty( $settings['ajax_pagination'] ) ? 'false' : 'true'; ?>,
				'text'						: {
					'loading'		: '<?php echo $text_loading; ?>',
					'go_to_top'		: '<?php echo $text_go_to_top; ?>',
					'init_filter'	: '<?php echo $text_init_filter; ?>',
					'initializing'	: '<?php echo $text_initializing; ?>'
				},
				'color' : {
					'loader_over_results' : '<?php echo empty( $settings['color_of_loader_over_results'] ) ? '#ffffff' : $settings['color_of_loader_over_results']; ?>',
					'loader_over_filter' : '<?php echo empty( $settings['color_of_loader_over_filter'] ) ? '#ffffff' : $settings['color_of_loader_over_filter']; ?>'
				},
				'direction'				: '<?php echo $direction; ?>',
				'seo' : {
					'enabled'	: <?php echo empty( $seo['enabled'] ) ? 'false' : 'true'; ?>,
					'alias'		: '<?php echo addslashes( $seo_alias ); ?>'
				},
				'displayAlwaysAsWidget'		: <?php echo empty( $displayAlwaysAsWidget ) ? 'false' : 'true'; ?>,
				'displaySelectedFilters'	: <?php echo empty( $displaySelectedFilters ) ? 'false' : "'" . $displaySelectedFilters . "'"; ?>,
				'isMobile' : <?php echo empty( $is_mobile ) ? 'false' : 'true'; ?>,
				'widgetWithSwipe' : <?php echo empty( $widgetWithSwipe ) ? 'false' : 'true'; ?>,
				'data' : {
					'category_id' : <?php echo empty( $_data['filter_category_id'] ) ? 'null' : (int) $_data['filter_category_id']; ?>
				}
			}));
		});
	});

</script>
