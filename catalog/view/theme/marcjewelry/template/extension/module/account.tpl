 <div class="account-nav__title">Меню</div>
 
<ul class="account-nav__list">
                <?php if (!$logged) { ?>
				<li class="account-nav__item"><a class="account-nav__link" href="<?php echo $login; ?>">Вход</a></li>
				<li class="account-nav__item"><a class="account-nav__link" href="<?php echo $register; ?>">Регистрация</a></li>
				<?php } ?>
				<?php if ($logged) { ?>
				<li class="account-nav__item"><a class="account-nav__link" href="<?php echo $account; ?>">Личный кабинет</a></li>
				<?php } ?>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $password; ?>">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $address; ?>">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $wishlist; ?>">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $order; ?>">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $download; ?>">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $reward; ?>">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $transaction; ?>">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $newsletter; ?>">Подписка на новости</a></li>
               <?php if ($logged) { ?>
			   <li class="account-nav__item"><a class="account-nav__link" href="<?php echo $logout; ?>">Выйти </a></li>
			   <?php } ?>
</ul>