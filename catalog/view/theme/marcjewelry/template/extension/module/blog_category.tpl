     <div class="categories-tabs categories-tabs--btns page__tabs"><a class="categories-tabs__select" href="javascript:void(0);"></a>
            <ul class="categories-tabs__list">
               <?php
            $i = 0;
            foreach ($categories as $category) {
                $i++;
                ?>
			  
			  <?php if ($category['category_id'] == $category_id) { ?>
			 <li class="categories-tabs__item categories-tabs__item--active"><a class="categories-tabs__link" style="cursor: pointer;" onclick="location.href='<?php echo $category['href']; ?>'" ><?php echo $category['name']; ?></a></li>

			  <?php } else { ?>
			  			  <li class="categories-tabs__item"><a class="categories-tabs__link" style="cursor: pointer;" onclick="location.href='<?php echo $category['href']; ?>'"><?php echo $category['name']; ?></a></li>
              <?php } ?>
			  
			   <?php } ?>
            </ul>
          </div>