<div class="sidebar__group sidebar__group--mobile-hidden">
              <div class="sidebar__title">Поиск по блогу</div>
              <div class="search sidebar__search search-form">
			  
			                      <input type="text" name="search" value="" id="input-search" placeholder="Введите ключевые слова" class="search__field">

			          <input type="hidden" name="route" value="<?php echo urldecode('blog/blog') ?>"/>

			  <button class="blog-button-search search__btn"><svg class="icon icon-search " width="20px" height="20px">
                    <use xlink:href="img/sprite.svg#search"></use>
                  </svg></button></div>
 </div>
			

<script>
    $(function(){
        /* Search */
        $('.blog-button-search').bind('click', function() {
            url = $('base').attr('href') + 'index.php?route=blog/blog';

            var search = $('.search-form input[name=\'search\']').val();

            if (search) {
                url += '&search=' + encodeURIComponent(search);
            }
            location = url;
        });
        
        $('#input-search').bind("enterKey",function(e){
            $('.blog-button-search').trigger('click');
         });
         $('#input-search').keyup(function(e){
             if(e.keyCode == 13)
             {
                 $(this).trigger("enterKey");
             }
         });
    })
    
</script>