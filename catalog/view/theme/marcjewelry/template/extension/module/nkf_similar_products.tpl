  <section class="products">
          <h2 class="page-title page-title--line products__title">Похожие товары</h2>
          <div class="products__row js-products-slider">
            <?php foreach ($products as $product) { ?>
			<div class="item-product"><a class="item-product__body" href="<?php echo $product['href']; ?>">
                <div class="item-product__img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></div>
                <div class="item-product__info">
                  <div class="item-product__name"><?php echo $product['name']; ?></div>
                  <div class="item-product__prices">
                    <div class="item-product__price"><span><?php if (!$product['special']) { ?><?php echo $product['price']; ?><?php } else { ?><?php echo $product['special']; ?><?php } ?> </span>руб</div>
                  </div>
                </div>
              </a></div>
			 <?php } ?>
    
          </div>
        </section>