<section class="main-slider">
    <!--<div class="slider-nav main-slider__nav "><a class="slider-nav__prev js-main-slider-prev" href="javascript:void(0);">
            <svg class="icon icon-arrow " width="11px" height="18px">
                <use xlink:href="img/sprite.svg#arrow"></use>
            </svg>
        </a><a class="slider-nav__next js-main-slider-next" href="javascript:void(0);">
            <svg class="icon icon-arrow " width="11px" height="18px">
                <use xlink:href="img/sprite.svg#arrow"></use>
            </svg>
        </a></div> -->
    <div class="main-slider__items js-main-slider mobile-banner">
        <?php foreach ($banners as $banner) { ?>
        <?php if ($banner['title']=='mobile') { ?>
        <a class="desktop" href="<?php echo $banner['link']; ?>">
            <div class="main-slider__item" style="background-image: url(<?php echo $banner['image']; ?>);">
                <div class="container main-slider__container">
                    <div class="main-slider__wrapper">
                        <div class="main-slider__img"></div>
                    </div>
                </div>
            </div>
        </a>
        <?php }} ?>
    </div>

    <div class="main-slider__items js-main-slider desktop-banner">
        <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['title']=='desktop') { ?>
    <a class="desktop" href="<?php echo $banner['link']; ?>">
            <div class="main-slider__item" style="background-image: url(<?php echo $banner['image']; ?>);">
                <div class="container main-slider__container">
                    <div class="main-slider__wrapper">
                        <div class="main-slider__img"></div>
                    </div>
                </div>
            </div>
        </a>
    <?php }} ?>
    </div>
</section>
