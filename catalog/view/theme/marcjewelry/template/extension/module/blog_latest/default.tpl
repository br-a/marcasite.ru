<section class="blog  blog--preview">
        <div class="container blog__container">
          <h2 class="page-title page-title--line blog__title">Блог</h2>
          <div class="blog__row">
            <?php foreach($articles as $article):?>
			<div class="item-blog"><a class="item-blog__body" href="<?php echo $article['href']; ?>">
                <div class="item-blog__img"><img src="<?php echo $article['thumb'] ?>" /></div>
                <div class="item-blog__info">
                  <div class="item-blog__date"><?php echo date('d.m.Y', strtotime($article['date_published'])) ?></div>
                  <h4 class="item-blog__title"><?php echo $article['title'] ?></h4>
                  <div class="item-blog__text"><?php echo $article['description']?></div>
                </div>
              </a></div>
           <?php endforeach; ?>
          </div>
        </div>
</section>