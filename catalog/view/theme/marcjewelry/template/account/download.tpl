<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>Файлы для скачивания</span></li>
        </ul>
        <h1 class="page-title page-title--line">Файлы для скачивания</h1>
        <main class="page__main">
          <div class="account">
             
			<?php if ($downloads) { ?>
			<div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Название файла</th>
                    <th>За что начислено</th>
                    <th class="table-responsive__text-right"> </th>
                  </tr>
                </thead>
                <tbody>
				   
				  <?php foreach ($downloads as $download) { ?>
                  <tr>
                    <td data-label="Название файла"><?php echo $download['name']; ?></td>
                    <td data-label="За что начислено">Заказ #<?php echo $download['order_id']; ?></td>
                    <td class="table-responsive__text-right"> <a class="btn  btn--transparent" href="<?php echo $download['href']; ?>">Скачать</a></td>
                  </tr>
				  <?php } ?>

                </tbody>
              </table>
            </div>
			<?php } else { ?>
<div class="account__main">
              <div class="account__text">У вас не было заказов с файлами для скачивания!</div>
            </div>
<?php } ?>
			
			
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>