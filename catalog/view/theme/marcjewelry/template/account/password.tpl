<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="jv">Главная</a></li>
          <li class="breadcrumbs__item"><span>Смена пароля</span></li>
        </ul>
        <h1 class="page-title page-title--line">Смена пароля</h1>
        <main class="page__main">
          <div class="account">
            <div class="account__main">
			  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form account__form">
			  <label class="field-text">
			  <span class="field-text__input-wrap">
			       <input type="password" name="password" value="<?php echo $password; ?>" id="input-password" class="field-text__input" />
        <?php if ($error_password) { ?>
        <div class="text-danger"><?php echo $error_password; ?></div>
        <?php } ?>
			  <span class="field-text__help-text">Новый пароль</span>
			  </span>
			  </label>
			  <label class="field-text">
			  <span class="field-text__input-wrap">
			       <input type="password" name="confirm" value="<?php echo $confirm; ?>" id="input-confirm" class="field-text__input" />
        <?php if ($error_confirm) { ?>
        <div class="text-danger"><?php echo $error_confirm; ?></div>
        <?php } ?>
			  <span class="field-text__help-text">Подтвердите новый пароль</span></span>
			  </label>
			  <button type="submit" class="btn form__btn">Сохранить</button>
			  </form>
            </div>
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>