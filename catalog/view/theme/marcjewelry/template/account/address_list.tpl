<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>Адреса доставки</span></li>
        </ul>
        <h1 class="page-title page-title--line">Адреса доставки</h1>
		
		  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
        <main class="page__main">
          <div class="account">
                       <div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Адрес</th>
                    <th> </th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  
				  <?php if ($addresses) { ?>
				  <?php foreach ($addresses as $result) { ?>
				  <tr>
                    <td data-label="Адрес"><?php echo $result['address']; ?></td>
                    <td class="table-responsive__text-right"> <a class="btn  btn--black" href="<?php echo $result['update']; ?>">Редактировать</a></td>
                    <td class="table-responsive__text-right"> <a class="btn  btn--transparent" href="<?php echo $result['delete']; ?>">Удалить</a></td>
                  </tr>
				  <?php } ?>
				  <?php } else { ?>
				  <p><?php echo $text_empty; ?></p>
				  <?php } ?>

				  
                </tbody>
              </table><a class="btn" href="<?php echo $add; ?>">Новый адрес</a>
            </div>
			
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>