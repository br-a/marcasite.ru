<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>История заказов</span></li>
        </ul>
        <h1 class="page-title page-title--line">История заказов</h1>
        <main class="page__main">
          <div class="account">

<div class="account__main">
              <?php if ($orders) { ?>
			  <table class="table-responsive">
                <thead>
                  <tr>
                    <th>№ заказа</th>
                    <th>Статус заказа</th>
                    <th>Добавлено</th>
                    <th>Кол-во</th>
                    <th>Получатель</th>
                    <th class="table-responsive__text-right">Сумма</th>
                  </tr>
                </thead>
                <tbody>
				  <?php foreach ($orders as $order) { ?>
				  <tr>
                    <td data-label="№ заказа"><?php echo $order['order_id']; ?></td>
                    <td data-label="Статус заказа"><?php echo $order['status']; ?></td>
                    <td data-label="Добавлено"><?php echo $order['date_added']; ?></td>
                    <td data-label="Кол-во"><?php echo $order['products']; ?></td>
                    <td data-label="Получатель"><?php echo $order['name']; ?></td>
                    <td class="table-responsive__text-right" data-label="Сумма"><?php echo $order['total']; ?></td>
                  </tr>
				  <?php } ?>
				  
                </tbody>
              </table>
			  
			  <?php } else { ?>
			          <table class="table-responsive">
                <thead>
                  <tr>
                    <th>№ заказа</th>
                    <th>Статус заказа</th>
                    <th>Добавлено</th>
                    <th>Кол-во</th>
                    <th>Получатель</th>
                    <th class="table-responsive__text-right">Сумма</th>
                  </tr>
                </thead>
              </table>
			  
			  			 <div class="account__text account__text--center">История заказов пуста!</div>
      <?php } ?>
			 
</div>

            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>