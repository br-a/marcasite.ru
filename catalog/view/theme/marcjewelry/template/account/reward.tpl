<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="jv">Главная</a></li>
          <li class="breadcrumbs__item"><span>Смена пароля</span></li>
        </ul>
        <h1 class="page-title page-title--line">Смена пароля</h1>
        <main class="page__main">
          <div class="account">
		  
            <?php if ($rewards) { ?>
			<div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Дата начисления</th>
                    <th>Начислены за</th>
                    <th class="table-responsive__text-right">Всего баллов</th>
                  </tr>
                </thead>
                <tbody>
                 
				  <?php foreach ($rewards  as $reward) { ?>
				  <tr>
                    <td data-label="Дата начисления"><?php echo $reward['date_added']; ?></td>
                    <td data-label="Начислены за"><?php echo $reward['description']; ?></td>
                    <td class="table-responsive__text-right" data-label="Всего баллов"><?php echo $reward['points']; ?></td>
                  </tr>
				  <?php } ?>
				  
                </tbody>
              </table> 
            </div>
			<?php } else { ?>
          <div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Дата начисления</th>
                    <th>Начислены за</th>
                    <th class="table-responsive__text-right">Всего баллов</th>
                  </tr>
                </thead>
              </table>
              <div class="account__text account__text--center">У вас нет бонусных баллов!</div>
            </div>
      <?php } ?>
			
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>