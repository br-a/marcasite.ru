<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="jv">Главная</a></li>
          <li class="breadcrumbs__item"><span>Новый адрес доставки</span></li>
        </ul>
        <h1 class="page-title page-title--line">Новый адрес доставки</h1>
        <main class="page__main">
          <div class="account">
            <div class="account__main">
			  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form account__form">
			  
			  <label class="field-text">
			  <span class="field-text__input-wrap">
		<input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="field-text__input" />
              <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
			  <span class="field-text__help-text">Имя получателя</span>
			  </span>
			  </label>
			  
			  
			  <label class="field-text">
			  
			  <span class="field-text__input-wrap">
			        <input type="text" name="lastname" value="<?php echo $lastname; ?>" id="input-lastname" class="field-text__input" />
              <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
			  <span class="field-text__help-text">Фамилия получателя</span></span>
			  </label>
			  
			    <label class="field-text">
			  
			  <span class="field-text__input-wrap">
		       <input type="text" name="address_1" value="<?php echo $address_1; ?>" id="input-address-1" class="field-text__input" />
              <?php if ($error_address_1) { ?>
              <div class="text-danger"><?php echo $error_address_1; ?></div>
              <?php } ?>
			  <span class="field-text__help-text">Адрес доставки</span></span>
			  </label>
			  
			    <label class="field-text">
			  <span class="field-text__input-wrap">
			    <input type="text" name="city" value="<?php echo $city; ?>" id="input-city" class="field-text__input" />
              <?php if ($error_city) { ?>
              <div class="text-danger"><?php echo $error_city; ?></div>
              <?php } ?>
			  <span class="field-text__help-text">Город</span></span>
			  </label>
			  
			      <label class="field-text">
			  <span class="field-text__input-wrap">
	 <input type="text" name="postcode" value="<?php echo $postcode; ?>" id="input-postcode" class="field-text__input" />
              <?php if ($error_postcode) { ?>
              <div class="text-danger"><?php echo $error_postcode; ?></div>
              <?php } ?>
			  <span class="field-text__help-text">Индекс</span></span>
			  </label>
			  
			  <button type="submit" class="btn form__btn">Сохранить</button>
			  </form>
            </div>
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>