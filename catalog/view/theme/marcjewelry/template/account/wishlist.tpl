<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>Избранное</span></li>
        </ul>
        <h1 class="page-title page-title--line">Избранное</h1>
        <main class="page__main">
          <div class="account">
        
		    <?php if ($products) { ?>
		    <div class="account__main">
              <div class="products">
                <div class="products__row products__row--3">
                  
			<?php foreach ($products as $product) { ?>
				 <div class="item-product"><a class="item-product__body" href="<?php echo $product['href']; ?>">
                      <div class="item-product__img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></div>
                      <div class="item-product__info">
                        <div class="item-product__name"><?php echo $product['name']; ?></div>
                        <?php if (!$product['special']) { ?>
						<div class="item-product__prices">
                          <div class="item-product__price"><span><?php echo $product['price']; ?> </span>руб</div>
                        </div>
						<?php } else { ?>
                  	<div class="item-product__prices">
					      <div class="item-product__old-price"><?php echo $product['price']; ?> </span>руб</div>
                          <div class="item-product__price"><span><?php echo $product['special']; ?> </span>руб</div>
                        </div>
                  <?php } ?>
						
                      </div>
                    </a><a class="item-product__cart" onclick="cart.add('<?php echo $product['product_id']; ?>');" style="cursor: pointer;"><svg class="icon icon-cart-1 " width="16px" height="16px">
                        <use xlink:href="img/sprite.svg#cart-1"></use>
                      </svg></a><a class="item-product__remove" href="<?php echo $product['remove']; ?>"><svg class="icon icon-trash " width="19px" height="19px">
                        <use xlink:href="img/sprite.svg#trash"></use>
                      </svg></a></div>
			<?php } ?>
         
                </div>
              </div>
            </div>
			 <?php } else { ?>
     <div class="account__main">
              <div class="account__text">Ваш список желаний пуст</div>
            </div>
      <?php } ?>
			
			
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>