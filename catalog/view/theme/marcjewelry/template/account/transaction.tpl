<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>История платежей</span></li>
        </ul>
        <h1 class="page-title page-title--line">История платежей</h1>
        <main class="page__main">
          <div class="account">
           
		    <?php if ($transactions) { ?>
		    <div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Добавлено</th>
                    <th>Описание</th>
                    <th class="table-responsive__text-right">Сумма (Руб)</th>
                  </tr>
                </thead>
                <tbody>
                  
				  <?php foreach ($transactions  as $transaction) { ?>
				  <tr>
                    <td data-label="Добавлено"><?php echo $transaction['date_added']; ?></td>
                    <td data-label="Описание"><?php echo $transaction['description']; ?></td>
                    <td class="table-responsive__text-right" data-label="Сумма (Руб)"><?php echo $transaction['amount']; ?></td>
                  </tr>
				  <?php } ?>
				  
                </tbody>
              </table>
            </div>
			<?php } else { ?>
         <div class="account__main">
              <table class="table-responsive">
                <thead>
                  <tr>
                    <th>Добавлено</th>
                    <th>Описание</th>
                    <th class="table-responsive__text-right">Сумма (Руб)</th>
                  </tr>
                </thead>
              </table>
              <div class="account__text account__text--center">История платежей пуста!</div>
            </div>
      <?php } ?>
			
			
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>