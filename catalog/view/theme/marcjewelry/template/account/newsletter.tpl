<?php echo $header; ?>
  <div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>Подписка на новости</span></li>
        </ul>
        <h1 class="page-title page-title--line">Подписка на новости</h1>
        <main class="page__main">
          <div class="account">
            <div class="account__main">

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
    <div class="form-group">
      <label class="col-sm-2 control-label"><?php echo $entry_newsletter; ?></label>
      <div class="col-sm-10">
        <?php if ($newsletter) { ?>
        <label class="radio-inline">
          <input type="radio" name="newsletter" value="1" checked="checked" />
          <?php echo $text_yes; ?> </label>
        <label class="radio-inline">
          <input type="radio" name="newsletter" value="0" />
          <?php echo $text_no; ?></label>
        <?php } else { ?>
        <label class="radio-inline">
          <input type="radio" name="newsletter" value="1" />
          <?php echo $text_yes; ?> </label>
        <label class="radio-inline">
          <input type="radio" name="newsletter" value="0" checked="checked" />
          <?php echo $text_no; ?></label>
        <?php } ?>
      </div>
    </div>
<br>
  <div class="buttons clearfix">
    <div class="pull-right">
      <input type="submit" value="Сохранить" class="btn btn-primary"/>
    </div>
  </div>
</form>
	  
            </div>
            <nav class="account-nav account__sidebar">
              <div class="account-nav__title">Меню</div><a class="account-nav__select" href="javascript:void(0);">Смена пароля</a>
              <ul class="account-nav__list">
<li class="account-nav__item account-nav__item"><a class="account-nav__link" href="/index.php?route=account/account">Личный кабинет</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/password">Смена пароля</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/address">Адреса доставки</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/wishlist">Избранное</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/order">История заказов</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/download">Файлы для скачивания</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/reward">Бонусные баллы</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/transaction">История платежей</a></li>
                <li class="account-nav__item--active"><a class="account-nav__link" href="/index.php?route=account/newsletter">Подписка на новости</a></li>
                <li class="account-nav__item"><a class="account-nav__link" href="/index.php?route=account/logout">Выйти </a></li>
              </ul>
            </nav>
          </div>
        </main>
      </div>
<?php echo $footer; ?>