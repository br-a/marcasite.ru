<?php echo $header; ?>
      <div class="container page__container">
    <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <h1 class="page-title page-title--line">Вход</h1>
		
		  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
        <main class="page__main">
          <div class="account">
            <div class="account__main">
			  
			  <form action="<?php echo $action; ?>" method="post" class="form account__form" enctype="multipart/form-data">
			  
			  <label class="field-text"><span class="field-text__input-wrap">
			  <input type="text" name="email" value="<?php echo $email; ?>"  id="input-email" class="field-text__input" />
			  <span class="field-text__help-text">Email</span></span></label>
			  
			  <label class="field-text"><span class="field-text__input-wrap">
			  <input type="password" name="password" value="<?php echo $password; ?>"  id="input-password" class="field-text__input"  />
			  <span class="field-text__help-text">Пароль</span></span></label>
			  
			  <button type="submit" class="btn form__btn">Войти</button>
			  
			  </form>
			  
            </div>
			
            <nav class="account-nav account__sidebar">
			<?php echo $column_right; ?>
            </nav>
			
          </div>
        </main>
      </div>
<?php echo $footer; ?>