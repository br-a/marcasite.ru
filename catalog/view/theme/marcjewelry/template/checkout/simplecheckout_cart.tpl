<fieldset class="form__fieldset simplecheckout-block"  id="simplecheckout_cart" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $has_error ? 'data-error="true"' : '' ?>>
<?php if ($attention) { ?>
    <div class="alert alert-danger simplecheckout-warning-block"><?php echo $attention; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
    <div class="alert alert-danger simplecheckout-warning-block"><?php echo $error_warning; ?></div>
<?php } ?>

                    <legend class="form__legend">Промокод</legend>
                    <div class="form__promo js-promo"><label class="field-text"><span class="field-text__input-wrap">
					<input class="field-text__input" type="text" data-onchange="reloadAll" name="coupon" value="<?php echo $coupon; ?>" />
					<span class="field-text__help-text">Введите промокод</span></span></label><a class="btn  btn--disabled form__btn js-promo-btn"data-onchange="reloadAll" style="cursor: pointer;">Применить</a></div>
                    
					<div class="form__text"> <a href="/login">Авторизуйтесь</a>, чтобы использовать свои промокоды.</div>

<input type="hidden" name="remove" value="" id="simplecheckout_remove">
<div style="display:none;" id="simplecheckout_cart_total"><?php echo $cart_total ?></div>
<?php if ($display_weight) { ?>
    <div style="display:none;" id="simplecheckout_cart_weight"><?php echo $weight ?></div>
<?php } ?>
<?php if (!$display_model) { ?>
    <style>
    .simplecheckout-cart col.model,
    .simplecheckout-cart th.model,
    .simplecheckout-cart td.model {
        display: none;
    }
    </style>
<?php } ?>

</fieldset>
	

<div class="form__footer-order">
<span id="movehere"></span>	
                    <div class="form__text form__text--light">Нажимая на кнопку, вы соглашаетесь<br>с <a  target="_blank" href="/uslovia-dogovora">условиями договора</a>, и <a target="_blank" href="/politika-konfidentsialnosti.html">политикой конфиденциальности</a>.</div>
</div>
				  
</div>