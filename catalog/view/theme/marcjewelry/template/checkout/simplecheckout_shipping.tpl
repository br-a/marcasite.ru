        <div class="order__form" data-check-form="true">
                  <div class="tabs tabs--btns order__tabs">
				  
                    <ul class="tabs__links simplecheckout-block" id="simplecheckout_shipping" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
                      
					<?php foreach ($shipping_methods as $shipping_method) { ?>
                    <?php foreach ($shipping_method['quote'] as $quote) { ?>
					   <label for="<?php echo $quote['code']; ?>" style="margin-right: 1.5em;cursor: pointer;">
					  <li class="tabs__link-wrap tabs__link-wrap<?php if ($quote['code'] == $code) { ?>--active<?php } ?>" role="presentation"><a class="tabs__link"><svg class="icon icon-location " width="23px" height="23px">
                            <use xlink:href="img/sprite.svg#location"></use>
                          </svg>
						  
						   <input type="radio" style="display: none;" data-onchange="reloadAll" name="shipping_method" <?php echo !empty($quote['dummy']) ? 'disabled="disabled"' : '' ?> <?php echo !empty($quote['dummy']) ? 'data-dummy="true"' : '' ?> value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" <?php if ($quote['code'] == $code) { ?>checked="checked"<?php } ?> />
						   
						  <?php echo !empty($quote['title']) ? $quote['title'] : ''; ?></a></li>
						                                  </label>

				<?php } ?>
				<?php } ?>
						  
				
				 <input type="hidden" name="shipping_method_current" value="<?php echo $code ?>" />
            <input type="hidden" name="shipping_method_checked" value="<?php echo $checked_code ?>" />
    
        <?php if (empty($shipping_methods) && $address_empty && $display_address_empty) { ?>
            <div class="simplecheckout-warning-text"><?php echo $text_shipping_address; ?></div>
        <?php } ?>
        <?php if (empty($shipping_methods) && !$address_empty) { ?>
            <div class="simplecheckout-warning-text"><?php echo $error_no_shipping; ?></div>
        <?php } ?>
		
                    </ul>

                      <script>
                          if($('.tabs__links label').length == 3) {
                              $('.tabs__links label:nth-child(2)').hide();
                          }
                      </script>
