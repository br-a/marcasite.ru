<div class="page-title order__title">Ваш заказ</div>
                
<div class="cart order__cart">
                  <div class="cart__items">
                    <?php foreach ($products as $product) { ?>
					<div class="item-cart cart__item"> <a class="item-cart__img" href="<?php echo $product['href']; ?>"> <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
                      <div class="item-cart__info">
                        <div class="item-cart__col">
                          <div class="item-cart__name"><?php echo $product['name']; ?></div>
                        </div>
                        <div class="item-cart__col">
						<?php foreach ($product['option'] as $option) { ?>
						<span class="item-cart__info-title"><?php echo $option['name']; ?>: </span><span class="item-cart__info-text"><?php echo $option['value']; ?></span>
						<?php } ?>
						</div>
						
                        <div class="item-cart__col"><span class="item-cart__info-title">Вес: </span><span class="item-cart__info-text"><?php echo $product['weight']; ?></span></div>
                        <div class="item-cart__col"><span class="item-cart__price"> <span><?php echo $product['total']; ?> </span>руб</span></div>
                      </div>
                    </div>
					<?php } ?>

                  </div>
                  <div class="footer-cart cart__footer">
                    <?php foreach ($totals as $total) { ?>
					<div class="footer-cart__item">
                      <div class="footer-cart__title"><?php echo $total['title']; ?>:</div>
                      <div class="footer-cart__sale"> <span><?php echo $total['text']; ?> </span>руб</div>
                    </div>
					<?php } ?>
                
                  </div>
</div>