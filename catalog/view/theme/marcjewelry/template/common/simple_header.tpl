<?php echo $header; ?>

	  
<div class="container page__container">
        <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <li class="breadcrumbs__item"><a href="/">Главная</a></li>
          <li class="breadcrumbs__item"><span>Оформление заказа</span></li>
        </ul>
        <main class="page__main">
          <div class="order">
            <div class="order__steps">
              <div class="order__step order__step--active">
                <div class="order__step-number">1</div>
                <div class="order__step-title">Оформление</div>
              </div>
              <div class="order__step">
                <div class="order__step-number">2</div>
                <div class="order__step-title">Подтверждение</div>
              </div>
            </div>
            <div class="order__promotions">БЕСПЛАТНАЯ ДОСТАВКА <br>при покупке от&nbsp;10&nbsp;000&nbsp;рублей! </div>
			
			  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>