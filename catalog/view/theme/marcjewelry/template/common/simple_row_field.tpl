
      <?php if ($type == 'select' || $type == 'select2') { ?>
        <select class="form-control" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $bootstrap ? 'data-theme="bootstrap"' : '' ?> <?php echo $type == 'select2' ? 'data-type="select2"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
          <?php foreach ($values as $info) { ?>
            <option value="<?php echo $info['id'] ?>" <?php echo $value == $info['id'] ? 'selected="selected"' : '' ?>><?php echo $info['text'] ?></option>
          <?php } ?>
        </select>
      <?php } elseif ($type == 'radio') { ?>
        <div>
        <?php foreach ($values as $info_id => $info) { ?>
          <div class="radio">
            <label><input type="radio" name="<?php echo $name ?>" id="<?php echo $id ?>_<?php echo $info_id ?>" value="<?php echo $info['id'] ?>" <?php echo $value == $info['id'] ? 'checked="checked"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $info['text'] ?></label>
          </div>
        <?php } ?>
        </div>
      <?php } elseif ($type == 'checkbox') { ?>
        <div>
        <?php foreach ($values as $info_id => $info) { ?>
          <div class="checkbox">
            <input type="hidden" name="<?php echo $name ?>[<?php echo $info['id'] ?>]" value="0">
            <label><input type="checkbox" name="<?php echo $name ?>[<?php echo $info['id'] ?>]" id="<?php echo $id ?>_<?php echo $info_id ?>" value="1" <?php echo !empty($value[$info['id']]) ? 'checked="checked"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $info['text'] ?></label>
          </div>
        <?php } ?>
        </div>
      <?php } elseif ($type == 'switcher') { ?>
        <div>
          <div class="checkbox">
            <input type="hidden" name="<?php echo $name ?>" value="0">
            <label><input type="checkbox" name="<?php echo $name ?>" id="<?php echo $id ?>" value="1" <?php echo $value == '1' ? 'checked="checked"' : '' ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>><?php echo $placeholder ?></label>
          </div>
        </div>
      <?php } elseif ($type == 'textarea') { ?>
			   <div class="tabs__content-item--active" >
                        <fieldset class="form__fieldset">
                          <legend class="form__legend"><?php echo $label ?></legend><label class="field-text"><span class="field-text__input-wrap">
						   <input  class="field-text__input" type="<?php echo $type ?>" <?php echo $type == 'password' ? 'data-validate-on="keyup"' : '' ?> name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
						  <span class="field-text__help-text">Добавить адрес доставки</span></span></label>
                        </fieldset>
                      </div>
					  
      <?php } elseif ($type == 'captcha') { ?>
        <?php if ($site_key) { ?>
          <script src="https://www.google.com/recaptcha/api.js?hl=<?php echo $lang; ?>&onload=recaptchaInit&render=explicit" type="text/javascript" async defer></script>
          <input type="hidden" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>">
          <script type="text/javascript">
            function recaptchaCallback(value) {
              $('#<?php echo $id ?>').val(value).trigger('change');
            }
            function recaptchaInit(){
              grecaptcha.render('simple-recaptcha');
            }
          </script>
          <div id="simple-recaptcha" data-sitekey="<?php echo $site_key; ?>" data-callback="recaptchaCallback"></div>
        <?php } else { ?>
          <input type="text" class="form-control" name="<?php echo $name ?>" id="<?php echo $id ?>" value="" placeholder="<?php echo $placeholder ?>" <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
          <div class="simple-captcha-container"><img src="index.php?<?php echo $additional_path ?>route=common/simple_connector/captcha&t=<?php echo $time ?>" alt="" id="captcha" /></div>
        <?php } ?>
      <?php } elseif ($type == 'file') { ?>

  <div class="tabs__content-item   tabs__content-item--active" >
                        <div class="order__info">
                          <p>Москва, 127474<br>Дмитровское шоссе, 60А, Бизнес-центр Лихоборский. Офис 218<br>Можно забрать <b>сегодня</b></p>
                        </div>
                      </div>
					  
      <?php } elseif ($type == 'date') { ?>
        <div class="input-group date">
          <input class="form-control" type="text" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
          <span class="input-group-btn">
            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
          </span>
        </div>
      <?php } elseif ($type == 'time') { ?>
      <div class="input-group time">
        <input class="form-control" type="text" name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
        <span class="input-group-btn">
          <button type="button" class="btn btn-default"><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
      <?php } else { ?>
	  
	  <label class="field-text"><span class="field-text__input-wrap">
	   <input style='123'  class="field-text__input" type="<?php echo $type ?>" <?php echo $type == 'password' ? 'data-validate-on="keyup"' : '' ?> name="<?php echo $name ?>" id="<?php echo $id ?>" value="<?php echo $value ?>" placeholder="<?php echo $placeholder ?>" <?php echo $attrs ?> <?php echo $reload ? 'data-onchange="reloadAll"' : 'data-reload-payment-form="true"'?>>
	  <span class="field-text__help-text"><?php echo $label ?></span></span>
	  </label>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.6/jquery.inputmask.min.js" integrity="sha512-6Jym48dWwVjfmvB0Hu3/4jn4TODd6uvkxdi9GNbBHwZ4nGcRxJUCaTkL3pVY6XUQABqFo3T58EMXFQztbjvAFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
      $(document).ready(function(){
        $('.field-text__input[name="customer[telephone]"]').inputmask({"mask": "+7 (999) 999-9999"}); 
      });
      
    </script>	  
    <style>
      .field-text__input:not(:placeholder-shown) + .field-text__help-text {
        top: 0;
        -webkit-transform: translateY(0);
        transform: translateY(0);
        font-size: 12px;
      }
      .field-text__input:not(:empty) + span {
        top: 0;
      }
    </style>
       
      <?php } ?>
      <?php if (!empty($rules)) { ?>
        <div class="simplecheckout-rule-group" data-for="<?php echo $id ?>">
          <?php foreach ($rules as $rule) { ?>
            <div <?php echo $rule['display'] && !$rule['passed'] ? '' : 'style="display:none;"' ?> data-for="<?php echo $id ?>" data-rule="<?php echo $rule['id'] ?>" class="simplecheckout-error-text simplecheckout-rule" <?php echo $rule['attrs'] ?>><?php echo $rule['text'] ?></div>
          <?php } ?>
        </div>
      <?php } ?>
      <?php if ($description) { ?>
        <div class="simplecheckout-tooltip" data-for="<?php echo $id ?>"><?php echo $description ?></div>
      <?php } ?>
