<?php
$theme_options = $registry->get('theme_options');
$config = $registry->get('config');
$page_direction = $theme_options->get( 'page_direction' );

require_once( DIR_TEMPLATE.$config->get($config->get('config_theme') . '_directory')."/lib/module.php" );
$modules_old_opencart = new Modules($registry);
?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" class="page  no-js" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="format-detection" content="telephone=no">
  <meta name="format-detection" content="date=no">
  <meta name="format-detection" content="address=no">
  <meta name="format-detection" content="email=no">
  <meta content="notranslate" name="google">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title;  ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
<script src="js/fix-script.js?6"></script>
<script src="js/bundle.js?6"></script>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>

<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/style.css?6">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js";, "ym");

   ym(84075019, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/84075019"; style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PWCL8WS');</script>
<!-- End Google Tag Manager -->

<script src="https://yastatic.net/share2/share.js"></script>
	<script src="/catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
	<script src="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
	<link href="/catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet">
	<link href="/catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen">
	<link href="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen">
	<link href="/catalog/view/theme/default/stylesheet/quickview.css" rel="stylesheet">
	<script type="text/javascript">
	var color_schem = '1';
	var loading_masked_img = '<img src="catalog/view/theme/default/image/ring-alt-'+ color_schem +'.svg" />';
	function loading_masked(action) {
		if (action) {
			$('.loading_masked').html(loading_masked_img);
			$('.loading_masked').show();
		} else {
			$('.loading_masked').html('');
			$('.loading_masked').hide();
		}
	}
	function creatOverlayLoadPage(action) {
		if (action) {
			$('#messageLoadPage').html(loading_masked_img);
			$('#messageLoadPage').show();
		} else {
			$('#messageLoadPage').html('');
			$('#messageLoadPage').hide();
		}
	}
	function quickview_open(id) {
	$('body').prepend('<div id="messageLoadPage"></div><div class="mfp-bg-quickview"></div>');
		$.ajax({
			type:'post',
			data:'quickviewpost=1',
			url:'index.php?route=product/product&product_id='+id,	
			beforeSend: function() {
				creatOverlayLoadPage(true); 
			},
			complete: function() {
				$('.mfp-bg-quickview').hide();
				$('#messageLoadPage').hide();
				creatOverlayLoadPage(false); 
			},	
			success:function (data) {
				$('.mfp-bg-quickview').hide();
				$data = $(data);
				var new_data = $data.find('#quickview-container').html();							
				$.magnificPopup.open({
					tLoading: loading_masked_img,
					items: {
						src: new_data,
					},
					type: 'inline'
				});
			}
	});							
	}



	</script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PWCL8WS";
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="page__wrapper">
    <div class="page__content">
      <header class="header" role="banner">
        <div class="header__top">
          <div class="container header__container">
            <div class="header__wrapper">
              <nav class="main-nav header__main-nav">
                <ul class="main-nav__list">
                  <li class="main-nav__item"><a class="main-nav__link" href="/onas">О нас</a></li>
                  <li class="main-nav__item"><a class="main-nav__link" href="/dostavka">Доставка</a></li>
                  <li class="main-nav__item"><a class="main-nav__link" href="/contact-us/">Контакты</a></li>
                  <li class="main-nav__item main-nav__item--selected"><a class="main-nav__link" href="https://marcasite.su">Стать партнером</a></li>
                </ul>
              </nav>
              <div class="header__links"><a class="header__phone" href="tel:<?php echo $telephone; ?>"> <svg class="icon icon-phone " width="20px" height="20px">
                    <use xlink:href="img/sprite.svg#phone"></use>
                  </svg>7 (495) 730-51-14</a><a class="header__user" href="<?php echo $account; ?>"><svg class="icon icon-user " width="15px" height="17px">
                    <use xlink:href="img/sprite.svg#user"></use>
                  </svg>Кабинет</a></div>
            </div>
          </div>
        </div>
        <div class="header__main">
          <div class="container header__container">
            <div class="header__wrapper">
              <nav class="mobile-nav header__mobile-nav"><button class="burger mobile-nav__burger" aria-label="Toggle block undefined"><span>Toggle block undefined</span></button>
                <div class="mobile-nav__hidden">
                  <div class="mobile-nav__header"><a class="logo mobile-nav__logo" href="/"><img src="img/logo-white.svg" width="74px" height="40px" alt="Марказит" /></a><button class="close mobile-nav__close" type="button"><span></span></button></div>
                  <div class="mobile-nav__body">
                    <ul class="mobile-nav__list">
                      <li class="mobile-nav__item mobile-nav__item--has-child"><a class="mobile-nav__link" href="javascript:void(0);">Каталог товаров</a>
                        <ul class="mobile-nav__list mobile-nav__list--lvl-2">
                          <?php foreach ($categories as $category) { ?>
						  <li class="mobile-nav__item mobile-nav__item--lvl-2"><a class="mobile-nav__link mobile-nav__link--lvl-2" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                          <?php } ?>
                          <li class="mobile-nav__item mobile-nav__item--lvl-2 mobile-nav__item--selected"><a class="mobile-nav__link mobile-nav__link--lvl-2" href="/jewelry/mfp/discounts,30/stock_status,7/">Акции</a></li>
                        </ul>
                      </li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/onas">О нас</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/dostavka">Доставка</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="/contact-us/">Контакты</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="https://marcasite.su">Стать партнером</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="<?php echo $account; ?>">Кабинет</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="<?php echo $wishlist; ?>">Избранное</a></li>
                      <li class="mobile-nav__item"><a class="mobile-nav__link" href="tel:<?php echo $telephone; ?>">Тел. <?php echo $telephone; ?></a></li>
                    </ul>
                  </div>
                </div>
              </nav><a class="logo header__logo" href="/"><img src="img/logo.svg" width="130px" height="69px" alt="Марказит" /></a>
			  
			  <?php echo $search; ?>
			
              <div class="header__links"><a class="header__favorite" href="<?php echo $wishlist; ?>">
                  <div class="header__favorite-icon"><svg class="icon icon-heart " width="25px" height="20px">
                      <use xlink:href="img/sprite.svg#heart"></use>
                    </svg><span class="header__favorite-number" id="wishlist-total"><span><?php echo $text_wishlist; ?></span></span></div>Избранное
                </a>
				
			
				<a class="header__cart" href="/cart">
                  <div class="header__cart-icon"><svg class="icon icon-cart " width="23px" height="19px">
                      <use xlink:href="img/sprite.svg#cart"></use>
                    </svg><?php echo $cart; ?></div>Корзина
                </a>

                </div>

				</div>

              <nav class="categories-nav header__categories"><a class="categories-nav__title" href="javascript:void(0);"><svg class="icon icon-burger " width="20px" height="14px">
                    <use xlink:href="img/sprite.svg#burger"></use>
                  </svg>Каталог</a>
                <div class="categories-nav__dropdown">
                  <ul class="categories-nav__list">
                    <?php foreach ($categories as $category) { ?>
					<li class="categories-nav__item"><a class="categories-nav__link" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
					<?php } ?>
					<li class="categories-nav__item"><a class="categories-nav__link" href="/jewelry/mfp/6f-komplekty,комплекты">Комплекты</a></li>
					
					
                    <li class="categories-nav__item categories-nav__item--selected"><a class="categories-nav__link" href="/jewelry/mfp/discounts,30/stock_status,7/">Акции</a></li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
        </div>
			<?php $slideshow = $modules_old_opencart->getModules('slideshow'); ?>
	<?php  if(count($slideshow)) { ?>
	<!-- Slider -->
	<div id="slider" style="margin-bottom:50px;" class="<?php if($theme_options->get( 'slideshow_layout' ) == 1) { echo 'full-width'; } elseif($theme_options->get( 'slideshow_layout' ) == 4) { echo 'fixed3 fixed2'; } elseif($theme_options->get( 'slideshow_layout' ) == 3) { echo 'fixed2'; } else { echo 'fixed'; } ?>">
		<div class="background-slider"></div>
		<div class="background">
			<div class="shadow"></div>
			<div class="pattern">
				<?php foreach($slideshow as $module) { ?>
				<?php echo $module; ?>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>

      </header>