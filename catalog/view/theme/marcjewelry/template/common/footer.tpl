</div>
<div class="page__footer-wrapper">
      <footer class="footer" role="contentinfo">
        <div class="container footer__container">
          <div class="footer__wrapper">
            <nav class="footer-nav footer-nav--wide footer__group">
              <div class="footer-title footer__nav-title">Меню</div>
              <ul class="footer-nav__list footer-nav__list--col-3">
                <li class="footer-nav__item"><a class="footer-nav__link" href="/onas">О нас</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/blog">Блог</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/vystavki">Выставки</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/rekvizity">Реквизиты</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/contact-us">Наши контакты</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/dostavka">Доставка</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/publichnaya-oferta">Публичная оферта</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/sitemap">Карта сайта</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/vakansiy">Вакансии</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/specials">Акции</a></li>
                <li class="footer-nav__item"><a class="footer-nav__link" href="/partnerskaya-programma">Партнерская программа</a></li>
              </ul>
            </nav>
            <nav class="footer-nav footer-nav--narrow footer__group">
              <div class="footer-title footer__nav-title">Каталог</div>
              <ul class="footer-nav__list footer-nav__list--col-2">
			    <?php foreach ($categories as $category) { ?>
                <li class="footer-nav__item"><a class="footer-nav__link" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
				<?php } ?>

              </ul>
            </nav>
            <div class="footer__info"><a class="logo footer__logo" href="javascript:void(0);"><img src="img/logo-white.svg" width="161px" height="86px" alt="Марказит" /></a>
              <div class="footer__info-group">
                <div class="footer-title footer__info-title">Всегда с вами</div>
                <div class="footer__social">
                  <div class="footer__social-text">Добро пожаловать в наши<br/>группы в соцсетях:</div>
                  <ul class="social footer__social-list">
                    <li class="social__item" title="instagram"><a class="social__link" href="https://www.instagram.com"><svg class="icon icon-instagram " width="31px" height="32px">
                          <use xlink:href="img/sprite.svg#instagram"></use>
                        </svg></a></li>
                    <li class="social__item" title="facebook"><a class="social__link" href="https://www.facebook.com"><svg class="icon icon-facebook " width="34px" height="35px">
                          <use xlink:href="img/sprite.svg#facebook"></use>
                        </svg></a></li>
                    <li class="social__item" title="twitter"><a class="social__link" href="https://www.twitter.com"><svg class="icon icon-twitter " width="35px" height="30px">
                          <use xlink:href="img/sprite.svg#twitter"></use>
                        </svg></a></li>
                    <li class="social__item" title="youtube"><a class="social__link" href="https://www.youtube.com"><svg class="icon icon-youtube " width="36px" height="27px">
                          <use xlink:href="img/sprite.svg#youtube"></use>
                        </svg></a></li>
                  </ul>
                </div><a class="footer__write" href="/contact-us/">Написать руководству</a>
              </div>
            </div>
            <div class="footer__bottom">
              <div class="footer__copyright">2010-2020 Markazit. <br/>Все права защищены.</div>
              <div class="footer__bottom-group"><a class="footer__privacy-policy" href="/politika-konfidentsialnosti">Политика конфиденциальности</a><a class="footer__develop"
                </a></div>
            </div>
          </div>
        </div>
      </footer>
    </div>
  </div>
<!-- Yandex.Metrika counter -->


<script  type="text/javascript">
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

   ym(47357799, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        ecommerce:"dataLayer"
   });
</script>

<noscript><div><img src="https://mc.yandex.ru/watch/47357799" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
 

</body>

</html>