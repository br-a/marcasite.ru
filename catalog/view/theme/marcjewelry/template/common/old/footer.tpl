<?php 
if($registry->has('theme_options') == true) { 
	$theme_options = $registry->get('theme_options');
	$config = $registry->get('config');
	
	require_once( DIR_TEMPLATE.$config->get($config->get('config_theme') . '_directory')."/lib/module.php" );
	$modules_old_opencart = new Modules($registry);
	
	// Pobranie zmiennych
	$language_id = $config->get( 'config_language_id' );
	$customfooter = $theme_options->get( 'customfooter' );
	if(!isset($customfooter[$language_id])) {
		$customfooter[$language_id] = array(
			'aboutus_status' => 0,
			'twitter_status' => 0,
			'facebook_status' => 0,
			'contact_status' => 0,
			'customblock_status' => 0
		);
	}
	
	if(!isset($customfooter[$language_id]['customblock_status'])) $customfooter[$language_id]['customblock_status'] = 0;
	
	$customfooter_top = $modules_old_opencart->getModules('customfooter_top');
	$customfooter_bottom = $modules_old_opencart->getModules('customfooter_bottom');
	$customfooter_center = $modules_old_opencart->getModules('customfooter');
	$footer_center = $modules_old_opencart->getModules('footer');
	
	// Sprawdzanie czy panele są włączone
	if(isset($customfooter[$language_id]) || count($customfooter_top) || count($customfooter_bottom) || count($customfooter_center)) { 
		if($customfooter[$language_id]['twitter_status'] == '1' || $customfooter[$language_id]['contact_status'] == '1' || $customfooter[$language_id]['aboutus_status'] == '1' || $customfooter[$language_id]['facebook_status'] == '1' || count($customfooter_top) || count($customfooter_bottom) || count($customfooter_center)) { 
			
			// Ustalanie szerokości paneli
			$grids = 12; $test = 0;  
			if($customfooter[$language_id]['aboutus_status'] == '1') { $test++; } 
			if($customfooter[$language_id]['twitter_status'] == '1') { $test++; } 
			if($customfooter[$language_id]['facebook_status'] == '1') { $test++; } 
			if($customfooter[$language_id]['contact_status'] == '1') { $test++; } 
			if($customfooter[$language_id]['customblock_status'] == '1') { $test++; } 
			if($test == 0) { $test = 1; }
			$grids = 12/$test; 
			if($test == 5) $grids = 25;
	
	?>
     
	<!-- CUSTOM FOOTER
		================================================== -->
	<div class="custom-footer <?php if($theme_options->get( 'custom_footer_layout' ) == 1) { echo 'full-width'; } elseif($theme_options->get( 'custom_footer_layout' ) == 4) { echo 'fixed3 fixed2'; } elseif($theme_options->get( 'custom_footer_layout' ) == 3) { echo 'fixed2'; } else { echo 'fixed'; } ?>">
		<div class="background-custom-footer"></div>
		<div class="background">
			<div class="shadow"></div>
			<div class="pattern">
				<div class="container">
					<?php 
					if( count($customfooter_top) ) { 
						foreach ($customfooter_top as $module) {
							echo $module;
						}
					} ?>
					
					<?php 
					if( count($customfooter_center) ) { 
						foreach ($customfooter_center as $module) {
							echo $module;
						}
					} else { ?>
     					<div class="row">
     						<?php if($customfooter[$language_id]['aboutus_status'] == '1') { ?>
     						<!-- About us -->
     						<div class="col-sm-<?php echo $grids; ?>">
     							<?php if($customfooter[$language_id]['aboutus_title'] != '') { ?>
     							<h4><?php echo $customfooter[$language_id]['aboutus_title']; ?></h4>
     							<?php } ?>
     							<div class="custom-footer-text"><?php echo html_entity_decode($customfooter[$language_id]['aboutus_text']); ?></div>
     						</div>
     						<?php } ?>
     						
     						<?php if($customfooter[$language_id]['contact_status'] == '1') { ?>
     						<!-- Contact -->
     						<div class="col-sm-<?php echo $grids; ?>">
     							<?php if($customfooter[$language_id]['contact_title'] != '') { ?>
     							<h4><?php echo $customfooter[$language_id]['contact_title']; ?></h4>
     							<?php } ?>
     							<ul class="contact-us clearfix">
     								<?php if($customfooter[$language_id]['contact_phone'] != '' || $customfooter[$language_id]['contact_phone2'] != '') { ?>
     								<!-- Phone -->
     								<li>
     									<i class="fa fa-mobile-phone"></i>
     									<p>
     										<?php if($customfooter[$language_id]['contact_phone'] != '') { ?>
     											<?php echo $customfooter[$language_id]['contact_phone']; ?><br>
     										<?php } ?>
     										<?php if($customfooter[$language_id]['contact_phone2'] != '') { ?>
     											<?php echo $customfooter[$language_id]['contact_phone2']; ?>
     										<?php } ?>
     									</p>
     								</li>
     								<?php } ?>
     								<?php if($customfooter[$language_id]['contact_email'] != '' || $customfooter[$language_id]['contact_email2'] != '') { ?>
     								<!-- Email -->
     								<li>
     									<i class="fa fa-envelope"></i>
     									<p>
     										<?php if($customfooter[$language_id]['contact_email'] != '') { ?>
     											<span><?php echo $customfooter[$language_id]['contact_email']; ?></span><br>
     										<?php } ?>
     										<?php if($customfooter[$language_id]['contact_email2'] != '') { ?>
     											<span><?php echo $customfooter[$language_id]['contact_email2']; ?></span>
     										<?php } ?>
     									</p>
     								</li>
     								<?php } ?>
     								<?php if($customfooter[$language_id]['contact_skype'] != '' || $customfooter[$language_id]['contact_skype2'] != '') { ?>
     								<!-- Phone -->
     								<li>
     									<i class="fa fa-skype"></i>
     									<p>
     										<?php if($customfooter[$language_id]['contact_skype'] != '') { ?>
     											<?php echo $customfooter[$language_id]['contact_skype']; ?><br>
     										<?php } ?>
     										<?php if($customfooter[$language_id]['contact_skype2'] != '') { ?>
     											<?php echo $customfooter[$language_id]['contact_skype2']; ?>
     										<?php } ?>
     									</p>
     								</li>
     								<?php } ?>
     							</ul>
     						</div>
     						<?php } ?>
     						
     						<?php if($customfooter[$language_id]['twitter_status'] == '1') { ?>
     						<!-- Twitter -->
     						<div class="col-sm-<?php echo $grids; ?>">
     							<?php if($customfooter[$language_id]['twitter_title'] != '') { ?>
     							<h4><?php echo $customfooter[$language_id]['twitter_title']; ?></h4>
     							<?php } ?>
     							
     							<div class="twitter-feed">
     							    <div class="twitter-wrapper"><div class="tweets clearfix" id="twitterFeed"><small>Please wait whilst our latest tweets load.</small></div></div>
     							    <script type="text/javascript">
     							        $(window).load(function(){
     							            twitterFetcher.fetch('<?php echo $customfooter[$language_id]['twitter_widget_id'] ; ?>', 'twitterFeed', 2, true, false);
     							        });
     							    </script>
     							</div>  
     						</div>
     						<?php } ?>
     						
     						<?php if($customfooter[$language_id]['facebook_status'] == '1') { ?>
     						<!-- Facebook -->
     						<div class="col-sm-<?php echo $grids; ?>">
     							<?php if($customfooter[$language_id]['facebook_title'] != '') { ?>
     							<h4><?php echo $customfooter[$language_id]['facebook_title']; ?></h4>
     							<?php } ?>
     							
     							<div id="fb-root"></div>
     							<script>(function(d, s, id) {
     							  var js, fjs = d.getElementsByTagName(s)[0];
     							  if (d.getElementById(id)) return;
     							  js = d.createElement(s); js.id = id;
     							  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
     							  fjs.parentNode.insertBefore(js, fjs);
     							}(document, 'script', 'facebook-jssdk'));</script>
     														
     							<div id="facebook">
     								<?php if(!isset($customfooter[$language_id]['color_scheme'])) { $customfooter[$language_id]['color_scheme'] = false; } ?>
     								<div class="fb-like-box fb_iframe_widget" profile_id="<?php echo $customfooter[$language_id]['facebook_id']; ?>" data-show-border="false"
     								 data-width="260" data-height="<?php if($customfooter[$language_id]['facebook_height'] > 0) { echo $customfooter[$language_id]['facebook_height']; } else { echo '210'; } ?>" <?php if($customfooter[$language_id]['show_faces'] != '1') { ?>data-connections="<?php if($customfooter[$language_id]['facebook_faces'] > 0) { echo $customfooter[$language_id]['facebook_faces']; } else { echo '8'; } ?>"<?php } ?> data-colorscheme="<?php if($customfooter[$language_id]['color_scheme'] != '1') { echo 'light'; } else { echo 'dark'; } ?>" data-stream="false" data-header="false" data-show-faces="<?php if($customfooter[$language_id]['show_faces'] == '1') { echo 'false'; } else { echo 'true'; } ?>" fb-xfbml-state="rendered"></div>
     							</div>
     						</div>
     						<?php } ?>
     						
     						<?php if($customfooter[$language_id]['customblock_status'] == '1') { ?>
     						<!-- Custom block -->
     						<div class="col-sm-<?php echo $grids; ?>">
     							<?php if($customfooter[$language_id]['customblock_title'] != '') { ?>
     							<h4><?php echo $customfooter[$language_id]['customblock_title']; ?></h4>
     							<?php } ?>
     							<div class="custom-footer-text"><?php echo html_entity_decode($customfooter[$language_id]['customblock_text']); ?></div>
     						</div>
     						<?php } ?>
     					</div>
					<?php } ?>
					
					<?php 
					if( count($customfooter_bottom) ) { 
						foreach ($customfooter_bottom as $module) {
							echo $module;
						}
					} ?>
				</div>
			</div>
		</div>
	</div>
	<?php } } ?>
	
	<!-- FOOTER
		================================================== -->
	<div class="footer <?php if($theme_options->get( 'footer_layout' ) == 2) { echo 'fixed'; } elseif($theme_options->get( 'footer_layout' ) == 4) { echo 'fixed3 fixed2'; } elseif($theme_options->get( 'footer_layout' ) == 3) { echo 'fixed2'; } else { echo 'full-width'; } ?>">
		<div class="background-footer"></div>
		<div class="background">
			<div class="shadow"></div>
			<div class="pattern">
				<div class="container">					
					<?php 
					if( count($footer_center) ) { 
						foreach ($footer_center as $module) {
							echo $module;
						}
					} else { ?>
					     <?php 
					     $footer_top = $modules_old_opencart->getModules('footer_top');
					     if( count($footer_top) ) { 
					     	foreach ($footer_top as $module) {
					     		echo $module;
					     	}
					     } ?>
					     
     					<div class="row footer-main-box">
     						<?php 
     						$footer_left = $modules_old_opencart->getModules('footer_left');
     						$footer_right = $modules_old_opencart->getModules('footer_right');
     						
     						$span = 3;
     						if( count($footer_left) && count($footer_right) ) {
     							$span = 2;
     						} elseif( count($footer_left) || count($footer_right) ) {
     							$span = 25;
     						} ?>
     						
     						<?php if( count($footer_left) ) { ?>
     						<div class="col-sm-<?php echo $span; ?>">
     						<?php foreach ($footer_left as $module) {
     								echo $module;
     							} ?>
     						</div>
     						<?php } ?>
     						
     						<!-- Information -->
     						<div class="col-sm-<?php echo $span; ?>">
     							<h4><?php echo $text_information; ?></h4>
     							<div class="strip-line"></div>
     							<ul>
     								<?php foreach ($informations as $information) { ?>
     								<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
     								<?php } ?>
     							</ul>
     						</div>
     						
     						<!-- Customer Service -->
     						<div class="col-sm-<?php echo $span; ?>">
     							<h4><?php echo $text_service; ?></h4>
     							<div class="strip-line"></div>
     							<ul>
     								<li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
     								<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
     								<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
     							</ul> 
     						</div>
     						
     						<!-- Extras -->
     						<div class="col-sm-<?php echo $span; ?>">
     							<h4><?php echo $text_extra; ?></h4>
     							<div class="strip-line"></div>
     							<ul>
     								<li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
     								<li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
     								<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
     								<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?> </a></li>
     							</ul>
     						</div>
     						
     						<!-- My Account -->
     						<div class="col-sm-<?php echo $span; ?>">
     							<h4><?php echo $text_account; ?></h4>
     							<div class="strip-line"></div>
     							<ul>
     								<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
     								<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
     								<li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
     								<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
     							</ul>
     						</div>
     						
     						<?php if( count($footer_right) ) { ?>
     						<div class="col-sm-<?php echo $span; ?>">
     						<?php foreach ($footer_right as $module) {
     								echo $module;
     							} ?>
     						</div>
     						<?php } ?>
     					</div>
     					
     					<?php 
     					$footer_bottom = $modules_old_opencart->getModules('footer_bottom');
     					if( count($footer_bottom) ) { 
     						foreach ($footer_bottom as $module) {
     							echo $module;
     						}
     					} ?>
     					
     					<div class="row copyright">
     					     <div class="col-sm-12">
     					          <?php if(is_array($theme_options->get( 'payment' ))) { if($theme_options->get( 'payment_status' ) != '0') { ?>
     					          <ul>
     					          	<?php foreach($theme_options->get( 'payment' ) as $payment) { 
     					          		echo '<li>';
     					          		if($payment['link'] != '') {
     					          			$new_tab = false;
     					          			if($payment['new_tab'] == 1) {
     					          				$new_tab = ' target="_blank"';
     					          			}
     					          			echo '<a href="' .$payment['link']. '"'.$new_tab.'>';
     					          		}
     					          		echo '<img src="image/' .$payment['img']. '" alt="' .$payment['name']. '">';
     					          		if($payment['link'] != '') {
     					          			echo '</a>';
     					          		}
     					          		echo '</li>'; 
     					          	} ?>
     					          </ul>
     					          <?php } } ?>
     					          
     					          <?php 
     					          $bottom = $modules_old_opencart->getModules('bottom');
     					          if( count($bottom) ) { 
     					          	foreach ($bottom as $module) {
     					          		echo $module;
     					          	}
     					          } ?>
     					     </div>
     					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="catalog/view/theme/<?php echo $config->get($config->get('config_theme') . '_directory'); ?>/js/megamenu.js"></script>
</div>
</div>
<?php } ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >

    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47357799 = new Ya.Metrika({
                    id:47357799,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
	
	function sendYandexEcommerce(array, action) {
		if(typeof dataLayer == 'undefined')
			return false;

		if(action=="remove") {
			var product = [{
				"id": array['metrika_product_id'],
				"name": array['metrika_product_name'],
				"price": array['metrika_product_price']
			}];
		} else {
			var product = [{
				"id": array['metrika_product_id'],
				"name": array['metrika_product_name'],
				"price": array['metrika_product_price'],
				"quantity": array['metrika_product_quantity']
			}];
		}

		if(action=="remove")
			dataLayer.push({"ecommerce": {"remove": {"products": product}}});
		else
			dataLayer.push({"ecommerce": {"add": {"products": product}}});
	}
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47357799" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div class="botttom-pannel-menu">
	<ul class="clearfix">
		<li>
			<?php if ($is_logged) { ?>
				<a href="#" data-toggle="modal" data-target="#myAccountCab">
					<i class="fa fa-user"></i>
					Кабинет
				</a>
			<?php } else { ?>
				<a href="#" data-toggle="modal" data-target="#myAccount">
					<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAoEAQAAABsW0wrAAAB3UlEQVR42u3YM4BcURhA4RPbZhnbti11sdnFtm3b6OK0sW1bWhv/+o51R2fsb56EP39e2r+/EB3t+Ev/AdiSfPjMafj0Ue5HRcKVyzBxEjRvAc2aWX/58hkmTLAZJF+SLj3UqweLFsLTp/L8x4+wfj20agWZMmNxb17bD1KqUAGmToMb1+U9v37C3r3Qowdkz+5SkFrp0jB2HFy6CJGREBwMO7ZrAanlzw9LFsvn9IMS6tbNclDx4pA7txuBThyH+/ehUGE3AZUsCS+ew+PHcl87iATUs6cCE5RWkFSosIw6QbkYtHYttGyFkoJyNkjNLEo/SEU5GFSiJFy/DnduW3d5/Vq+4/lzB4Ny5ITJU2DWTOsuF87Ld+zbi/5R1qIFBAXC/v2QIaPTQTLU0qfHYG3aKBjng+7dhaHDUOrSBcJCScBoWTCqmC2bFYyrQCpm40bZMgWNoE6dFIxe0Lq1sHq1gtEA0rnF6Af5QTNmQN681oN27TR8CfhvB+joUQgNhfBwOHcWRo6SXRdLQKdOGr80aoTt5coNffvBoYPw92/qAw5lyhgFuaYsWaF9B1nnfP0qP/zwIcybCzVrKSDXlj69DPrly+HlS0G8e5u0caW/KlVg5izZBP3yGW8vBqu/tgJE5mDqAAAAAElFTkSuQmCC" />
					Войти
				</a>
			<?php } ?>
		</li>
		<li>
			<a href="#" data-toggle="modal" data-target="#mySearch">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkEAQAAAAbmYxQAAACfUlEQVR42u2XA4wdURiFv9q2bSOqbdu2w+5unNruc1Tbtm3bts0T1sP6S174Zubcn+fyn//8XPLlh379YP06uHUL3r1Dv6dP4MAB8PugVi2IFh13KV5CIvTxc+fA64Fu3aBOHSSgZSsYOAC2boG3b+DKFejVC6JGxVlixoTx45CQNWugbDmIFJnvkj49jB4FL17A3j2QIyfOkCAhbN4EDx5A02aYJnt22LULPV+qlP3ISMzlS5AnL5aJERPmzoFHj6BoUazj8ehkEmOX6NFh5Qq4eAESJsQ8pcugmmnSBMdImhRu3FAzmEadsmoVjtOuHbx+BVmzYpxChVF0SpbCaTQCzp+DYcMwjGbJ2TNqbVcYMECijKLOUp7dQZFXBjJkxBh37mgCu4RmmwRVroIR9GetAzd59gxatPiNBD1+BK3bYIyHD6B9B1wjdmx06Oo1MMaePTByBK5RtKgEac8ZYswY2L8P1+gbJh9lfKyULacTyIi5gExcIIBxIkeGo0dg0iQcp2o1Hdb81m/cBD1YooSzRu/4MViwAGssWYLsQrLkOEIoJDujCW3ZLpw+JQtqR5SKd/AgePMGatfBDrIJWrRnzkCRopgmfnyYMQOlf/cuGTX7pEwpcy8fM3YMpE5txCHK++jmcfMG9O6D0rVwgTOi1HkdO6EPvHoFS5d8/IicpaKYMSPqnOYtwO/TnNH/PB6lXhQrhu5u06dBlKg4gU4nWztnDtzWJfHT34sXyG2GR3w9kuXKIVFTpkiU06jYdQkoWFBRMnQprFQJXr4An08F/3tQqxYS5fH8RqIaNECjYOgQfh+at5AozanfBI0HNUREBL8PXbtJlKwJvAcLavLoQlNz1QAAAABJRU5ErkJggg==" />
				Поиск
			</a>
		</li>
		<li>
			<a href="index.php?route=common/footer/get_wishlists" data-title="Избранное" data-toggle="modal" data-target="#myModal" data-remote="false">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAACnUlEQVRYhe2Xz0sVURzFFcvsIWgq/UAaxLa+wE0tCsWtFm0KAmvhshaCUFjk2kUirlwa2SKF6NffUC8pISKQoB+LfqhBiwoiKCs7h87wvozz3pu5M2+EauADw/fec77nDXfu3FdT8//6Gy6vY+8WcBLcAMvgB1gD78AcOAZqy+hrNWdemjV5LMuT3lujhjkMnoN1QZMCeABWTP0h6AzR79OYP29F2oLC+fUX7FUpzKB+zS9wDeRD5nSDWzL9aOdo7JPGOKc7RN8FZtWDvQZLhenRY/0CjkZ4ksNq/AbsAC3grWrDEfQD4DP4CXqDg/XglVIfqWRmdJcVYFrwfiKGfkA92bveDpyS2WxUM+kawGvwXfC+IabHVfU+bYu3VdywZiIYnjML9byDPi/tHVtc5dsQ10xazwTqcPTgm7xqC1xYCy5m0j8mCfQLzGALfP2WEhhuj7t2AvolZrCFRb3yba6mCcK0qfcjW7ygNTCyCYFG1HvUFvdoQ3wPGjMM06ie7L0zODiupFMZBppSz/GwwZzZrfsyCNOnXvzI5kpN6tEWwMfYXsUw7erBxXyo0uRRPcbFksmThcnJO9rO7v05XF33t3NQl2KYOq94dGGPkoe8oJAfzYKEVyILy3vyh87I8z7YFtegCTyVwWQKgSbkRc8mV5Pd4KWMLiUIc1Ee9Nrl6uObdXrFs/BZB/0Zaemx4QzuGorn4A/aEk7E0B2XhtquVMIY84PgK/jmBc/A4fN7NZeaA6mGMU36tZnxcL6/zLy85nBuf1XCmGZDZk14IeOeWXNDVQ1jmo6p4TPQauotqnFsLJMwprn/t6egz0HObKbTmYZRIH4GbirAXbGuWmqfm7ih+Im55xX/dfDe+WydVqhm8EQ0b2qYf+L6DdFwHteeuyirAAAAAElFTkSuQmCC" />
				Избранное
			</a>
		</li>
		<li>
			<a href="index.php?route=common/footer/get_cartItem" data-title="Корзина" data-toggle="modal" data-target="#myModal" data-remote="false">
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAASCAYAAABfJS4tAAABAUlEQVQ4jcXUvw7BYBQF8OtPDIYmTV/AIh6Awdalq50Iq5HEKmFhNJN4CFErk0mEAaMX8BTOxyEf+ZBqEzf5LefenqEpIn+cDBwgTQdmoSYFExhoWZ9ZKkyxB3uwtcxm5v1aqgqWUDHsKtzZht3XacEckoadynzeBJocbKHw4SbPm9y7g5HBGk5vdroTb0072UEVypoG1F4ykxpv9azKTjmD9e3dBBiLnbKAUoTFJXZKE8aQiKA0wS7VKVk4yo/f5MvY7Mregw24ERS77HpMB4YQC1EaY0dHD4uwAidEscOOoh7GYQZTaMvtpxpEm8/67Hoa9V+rPvgu9AJSz9TZcZ0LhI5DbFnbS+QAAAAASUVORK5CYII=" />
				<span class="cq hidden" style="
    position: absolute;
    margin-left: -20px;
    margin-top: -10px;
    font-size: 15px;
    font-weight: 800;
    color: white;
    border: 1px solid #0a72a5;
    background: #0a72a5;
    border-radius: 165px;
    /* padding: 3px; */
    padding-left: 5px;
    padding-right: 5px;
"></span>
               <span class="ct"> Корзина  </span>
			</a>
            <style>
            @media(max-width:700px){
                .cq{
                        margin-left: 46px!important;
    margin-top: -26px!important;
                }
            }
            
            </style>
                    <script>
        var refreshCart= function(){
$.ajax({
url:"/index.php?route=common/header/getCart",
dataType:"json",
success:function(json){
if(json['quantity']){
$(".cq").text(json['quantity']);
$(".cq").removeClass('hidden');
}
else{
    $(".cq").addClass('hidden');
}
if(json['price'])
$(".ct").text(json['price']);

}


})



}
refreshCart();        
        
        </script>
		</li>
		<li>
			<a href="#" data-toggle="modal" data-target="#myContact">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkEAQAAAAbmYxQAAACw0lEQVR4Ae2YA4wkeRhH39juuzh3a9v2hmvbDNa2bdu2bdu2bVu/ZCq1GnXV8o3Zr7//pyr+8hvRsQM8egTv3ln3dv4c5M5NxKRODQ8fQJasEBxszZvDAS1aSipCKlaEjRuwHIdDkYqQypVh/TosRpGKUMjFFerW/UmESpeGu3eMpHv9CmbOAHd3vsDbG5o1h82bYPcu6NlD33OikGSqVIX//9dbkqRw9gwUK44JH1/Ytg3274OqVaFkSThyBNq2dbLQrVtQu7YhlDw5XLgAhQuDgSJz6BD4+hJOwYJw5oyO3GlChQrDpUvGkT15AqNHgZs7JpYvgyZNMeHlDffuQfr0ThCKapWtW6ejNaN807HZLDRlCrRrD2ZUnStX2C6kKEybhglXVxg8SH9ns5AS+MQJwilfHm7eUFHky4/9Qv/8q38U5gCQTK3a4OHp1LJXGdepC/PnfRx6Gn7JkvNVjh1Tf/L0/PhPJQlOFlq6BPbugfHj9DZrlko/bly+oE8f/Q7A5UuQI6fThTQqEibExMYN6sZmJHDnjvrP9GnQrZtFEdq/78sIxYuPMFWVxkrZsmqoV69AQOAPzCHQz3fskNzHITtkCMLeKjPw94cbN5Tc8eJrODdtCoGB0KMHnDihXNPosUEIFNFz5ySXJasS/M0b2LIFKlXWFjBggG1COq7t29SlQdGIHZtwEiRUHqphWi4kkiaFp0/Uwb9G/fpaTXx8bRES1atr/YgTFzNo4zx8CNq0jZKQUb7ah/Ssx40zkjIiRoxQIjscX6/I1auivjFSs6axMaZIqfIvUhQiRuKLF6kVGKNEM+/4MWjePIY7daLEX9+piaCXzZ2jJ9erF/TvBzduSFTDNwpCpUrD7Vvmq47p05QDkUd7dfHiOsKhQxRh7do2JnXE/E5CP921ferUuhWTLbu1dz/atFUV/8T3h94DgLe+4/eGVqYAAAAASUVORK5CYII=" />
				Контакты
			</a>
		</li>
	</ul>
</div>
<script>
$(document).ready(function(){
	$('#lang_show_mobile').click(function(){
		var show_container = $(this).next().find('ul');
		$(show_container).css({
			display: "block",
			top: "-101px"
		});
		
		return false;
	});

	$("#myModal").on("show.bs.modal", function(e) {
		var link = $(e.relatedTarget);
		var title = link.attr("data-title");
		$(this).find(".modal-body").load(link.attr("href"));
		$(this).find(".modal-title").html(title);
	});
});
</script>
<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				...
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<?php if ($is_logged) { ?>
	<div class="modal fade" id="myAccountCab" tabindex="-1" role="dialog" aria-labelledby="myAccountCabLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myAccountCabLabel">Личный кабинет</h4>
				</div>
				<div class="modal-body">
					<?php echo $account;?>
				</div>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="modal fade" id="myAccount" tabindex="-1" role="dialog" aria-labelledby="myAccountLabel">
		<div class="modal-dialog" role="document">
			<form action="https://marcasite.opencartdev.ru/login/" method="post" enctype="multipart/form-data">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myAccountLabel">Личный кабинет</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label class="control-label" for="input-email">E-Mail:</label>
							<input name="email" value="" placeholder="E-Mail:" id="input-email" class="form-control" type="text">
						</div>
						<div class="form-group" style="padding-bottom: 10px">
							<label class="control-label" for="input-password">Пароль:</label>
							<input name="password" value="" placeholder="Пароль:" id="input-password" class="form-control" type="password">
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default">Войти</button>
						<a href="index.php?route=account/register" class="btn btn-primary">Регистрация</a>
					</div>
				</div>
			</form>
		</div>
	</div>
<?php } ?>
<div class="modal fade" id="mySearch" tabindex="-1" role="dialog" aria-labelledby="mySearchLabel">
	<div class="modal-dialog" role="document">
		<form action="https://marcasite.opencartdev.ru/search/" method="get" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="mySearchtLabel">Поиск по каталогу</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label" for="input-email">Поиск</label>
						<input name="search" value="" placeholder="Поиск по каталогу" id="input-search" class="form-control" type="text">
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default">Найти</button>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="myContact" tabindex="-1" role="dialog" aria-labelledby="myContactLabel">
	<div class="modal-dialog" role="document">
		<form action="https://marcasite.opencartdev.ru/search/" method="get" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myContactLabel">Контакты</h4>
				</div>
				<div class="modal-body">
					<div class="text-center">
						<?php if ($telephone) { ?>
							<h4>Телефон</h4> 
							<p>
								
								<i class="fa fa-phone"></i> <?php echo $telephone[0]; ?>
								<br> 
								<?php echo isset($telephone[1]) ? '<i class="fa fa-phone"></i> ' . $telephone[1] : ''; ?>
							</p> 
						<?php } ?>
						<?php if ($fax) { ?>
							<h4>Факс</h4>
							<p><i class="fa fa-phone"></i> <?php echo $fax; ?></p>
						<?php } ?>
					</div>
					<p class="text-center">
						<a href="index.php?route=information/contact" class="btn btn-default">Подробнее</a>
					</p>
				</div>
			</div>
		</form>
	</div>
</div>
     <!-- Begin Talk-Me {literal} -->
<script type='text/javascript'>
     (function(d, w, m) {
          window.supportAPIMethod = m;
          var s = d.createElement('script');
          s.type ='text/javascript'; s.id = 'supportScript'; s.charset = 'utf-8';
          s.async = true;
          var id = '9657d6bc8d00b73b17912910f1e27182';
          s.src = '//lcab.talk-me.ru/support/support.js?h='+id;
          var sc = d.getElementsByTagName('script')[0];
          w[m] = w[m] || function() { (w[m].q = w[m].q || []).push(arguments); };
          if (sc) sc.parentNode.insertBefore(s, sc); 
          else d.documentElement.firstChild.appendChild(s);
     })(document, window, 'TalkMe');
</script>
<!-- {/literal} End Talk-Me -->
<script type="text/javascript">
          $(".name_prod").each(function(){
               console.log($(this).parent().find('.color_list').text().length);
               if($(this).parent().find('.color_list').text().length<10)
               {
                    $(this).css('position', 'relative');
                    $(this).css('top', '-26px');
               }
          })
     </script>
<style>
	.mobile-contact{
		display: none;
	}
@media screen and (max-width: 991px){
	.mobile-contact{
		display: block;
	}
	.desct-hide {
		display: none;
	}
}


</style>
<script src="catalog/view/javascript/myjs.js"></script>
</body>
</html>