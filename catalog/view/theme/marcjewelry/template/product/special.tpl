<?php echo $header; ?>
  <div class="container page__container">
    <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <h1 class="page-title"><?php echo $heading_title; ?></h1>

        <main class="page__main">
          <div class="catalog">
            <div class="catalog__wrapper">
            
			<?php echo $column_left; ?>
			
              <section class="products catalog__products">
			  
			        <?php if ($products) { ?>

                <div class="products__options"><a class="products__filters-link" href="javascript:void(0);"><svg class="icon icon-filters " width="22px" height="22px">
                      <use xlink:href="img/sprite.svg#filters"></use>
                    </svg>Фильтр</a>
                  <div class="products__sort">
                    <div class="field-select">
                      <div class="field-select__name">Сортировать:</div>
                      <div class="field-select__select-wrap">

						
						 <select id="input-sort" class="field-select__select" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
			
						</div>
                    </div>
                  </div>
                </div>
                
				
				<div class="products__row products__row--4">
                  <?php foreach ($products as $product) { ?>
				  <div class="item-product"><a class="item-product__body" href="<?php echo $product['href']; ?>">
                      <div class="item-product__img"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></div>
                      <div class="item-product__info">
                        <div class="item-product__name"><?php echo $product['name']; ?></div>
                        <div class="item-product__prices">
                          <div class="item-product__price"><span><?php echo $product['price']; ?> </span>руб</div>
                        </div>
                      </div>
                    </a></div>
					<?php } ?>

                
                </div>
				
                <div class="products__footer">
				<?php echo $pagination; ?>

					  
					  <span class="products__pagination-text"><?php echo $results; ?></span>
                </div>
				
		<?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
	  
              </section>
            </div>
          </div>
        </main>
		

      </div>
	  
<?php echo $footer; ?>