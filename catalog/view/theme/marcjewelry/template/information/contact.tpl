<?php echo $header; ?>
      <div class="container page__container">
           <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
        <h1 class="page-title page-title--line">Контакты</h1>
        <main class="page__main">
          <section class="contacts">
            <div class="contacts__row">
              <div class="feedback contacts__feedback">
                <div class="feedback__body">
                  <h3 class="feedback__subtitle">Форма обратной связи</h3>
				  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form feedback__form" data-check-form="true">
                    <div class="form__row">
					<label class="field-text"><span class="field-text__input-wrap">
					 <input type="text" name="name" class="field-text__input" value="<?php echo $name; ?>"  data-check-pattern="^[a-zа-яё]+.+" id="input-name"  />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
					<span class="field-text__help-text">Имя</span></span></label>
					<label class="field-text"><span class="field-text__input-wrap">
					<input type="text" name="email" value="<?php echo $email; ?>" id="input-email" class="field-text__input" data-check-pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?"/>
              <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
					<span class="field-text__help-text">E-Mail</span></span></label></div>
					<label class="field-text"><span class="field-text__input-wrap">
					    <textarea name="enquiry" id="input-enquiry" class="field-text__input"></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><?php echo $error_enquiry; ?></div>
              <?php } ?>
					<span class="field-text__help-text">Текст сообщения</span></span></label>
					<button type="submit" class="btn form__btn">Отправить</button>
                  </form>
                </div>
              </div>
			  
              <div class="contacts__info">
                <div class="contacts__info-body">
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Наш адрес</h3>
                    <div class="contacts__text">
                      <p><?php echo $address; ?></p>
                    </div>
                  </div>
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Время работы</h3>
                    <div class="contacts__text">
                      <p><?php echo $open; ?></p>
                    </div>
                  </div>
                  <div class="contacts__item">
                    <h3 class="contacts__subtitle">Телефон</h3>
                    <div class="contacts__text"><?php echo $telephone; ?></div>
                  </div>
                </div>
              </div>
			  
              <div class="contacts__map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1888.2397271052707!2d37.537376924345935!3d55.749674999036024!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x55d34404a345a73a!2z0KTQldCU0JXQoNCQ0KbQmNCv!5e0!3m2!1sru!2s!4v1612548646984!5m2!1sru!2s" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
            </div>
          </section>
        </main>
      </div>
<?php echo $footer; ?>
