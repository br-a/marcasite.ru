<?php if(!empty($articles)):?> 
    <?php $index = 1; ?>
    <?php foreach($articles as $article):?>
     <div class="item-blog  item-blog--xl-img"><a class="item-blog__body" href="<?php echo $article['href'] ?>">
                  
					  <div class="item-blog__img"><?php echo $article['gallery'][0]['output'] ?></div>
					
					   
                      <div class="item-blog__info">
                        <div class="item-blog__date"><?php echo date('d.m.Y', strtotime($article['date_published'])); ?></div>
                        <h4 class="item-blog__title"><?php echo $article['title'] ?></h4>
                        <div class="item-blog__text"><?php echo $article['description']?></div>
                      </div>
                    </a></div>
    <?php $index++; ?>
    <?php endforeach; ?>

<?php endif; ?>