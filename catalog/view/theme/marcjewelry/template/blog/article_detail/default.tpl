<?php echo $header; ?>

<?php
$theme_options = $registry->get('theme_options');
$config = $registry->get('config'); 
$page_direction = $theme_options->get( 'page_direction' );
include('catalog/view/theme/' . $config->get($config->get('config_theme') . '_directory') . '/template/new_elements/wrapper_top.tpl'); ?>

<style>
.categories-tabs {display: none;}
</style>

      <div class="container page__container">
	  
      <ul class="breadcrumbs" aria-label="Breadcrumb" role="navigation">
          <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
	<?php if($i+1<count($breadcrumbs)) { ?>
	<li class="breadcrumbs__item"><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } else { ?>
	<li class="breadcrumbs__item"><span><?php echo $breadcrumb['text']; ?></span></li>
	<?php } ?>
	<?php } ?>
        </ul>
		
      
	  <h1 class="page-title page-title--line"><?php echo $heading_title; ?></h1>
        <div class="page__cols">
		
          <main class="page__main">
            <article class="article">
			<?php echo $article['gallery'][0]['output'] ?>
              <div class="article__date"> <?php echo date('d', strtotime($article['date_published'])); ?> <?php echo date('M', strtotime($article['date_published'])); ?> <?php echo date('Y', strtotime($article['date_published'])); ?></div>
             <?php echo $article['content']?>
            </article>
            <div class="feedback">
              <div class="feedback__body leave-reply" id="reply-block">
		  
                <h3 class="feedback__subtitle">Форма обратной связи</h3>
  
				 <form class="form feedback__form" method="post" data-check-form="true" id="form-comment">

                  <div class="form__row">
				  <label class="field-text"><span class="field-text__input-wrap">
				 <input type="text" name="name" value="" id="input-name" data-check-pattern="^[a-zа-яё]+.+" class="field-text__input">

				  <span class="field-text__help-text">Имя</span></span></label>
				  
				  <label class="field-text"><span class="field-text__input-wrap">
				 <input type="text" name="email" value="" data-check-pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" id="input-email" class="field-text__input">

				  <span class="field-text__help-text">E-Mail</span></span></label></div>
				  
				  <label class="field-text"><span class="field-text__input-wrap">

				  <textarea rows="10" id="input-content" name="content" class="field-text__input"></textarea>

				  <span class="field-text__help-text">Текст сообщения</span></span></label><button id="button-comment" type="submit" class="btn form__btn">Отправить</button>
                </form>
			
              </div>
			  
            </div>
          </main>
		  
          <sidebar class="sidebar page__sidebar">
		<?php include('catalog/view/theme/' . $config->get($config->get('config_theme') . '_directory') . '/template/new_elements/wrapper_bottom.tpl'); ?>
          </sidebar>
        </div>
      </div>
	  
	  <script>
    $(function(){
        $('.media-slider').owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true,
            pagination: true,
            autoHeight : true,
            lazyLoad: true,
            navigationText: false,
            <?php if($page_direction[$config->get( 'config_language_id' )] == 'RTL'): ?>
            direction: 'rtl'
            <?php endif; ?>
        })
        
        $('#button-comment').on('click', function(e) {
            e.preventDefault();
            $.ajax({
                url: 'index.php?route=blog/article/write&article_id=<?php echo $article_id; ?>',
                type: 'post',
                dataType: 'json',
                data: $("#form-comment").serialize(),
                beforeSend: function() {
                    $('#button-comment').button('loading');
                },
                complete: function() {
                    $('#button-comment').button('reset');
                },
                success: function(json) {
                    $('.alert-success, .alert-danger').remove();

                    if (json['error']) {
                        $('.leave-reply .box-content > *:first-child').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    }

                    if (json['success']) {
                        $('.leave-reply .box-content > *:first-child').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                        $('input[name=\'name\']').val('');
                        $('input[name=\'email\']').val('');
                        $('textarea[name=\'content\']').val('');
                    }
                }
            });
        });
    });
</script>
	  
<?php echo $footer; ?>