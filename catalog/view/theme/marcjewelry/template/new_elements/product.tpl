<?php
$newprice = '';
$custom_thumb = '';
$product['newprice'] = '';

$theme_options = $registry->get('theme_options');
$config = $registry->get('config');


if (!isset($product['icon1'])) $product['icon1'] = '0';
if (!isset($product['icon2'])) $product['icon2'] = '0';

if (!isset($product['position1'])) $product['position1'] = '';
if (!isset($product['position2'])) $product['position2'] = '';

if (!isset($product['custom_thumb'])) $product['custom_thumb'] = '';
$v = date("nd");
?><!-- Product -->
<div class="newproduct" style="background:#fff;border:1px solid #ebebeb;margin:4px;text-align:center;">
	<div class="left">
	<?php if (isset($product['icon1']) || $product['icon1']!=0) { ?>
	<div class="iconprod <?php echo $product['position1'];?>"><i class="fa <?php echo $product['icon1'];?>" aria-hidden="true"></i></div>
	<?php } ?>
	<?php if (isset($product['icon2']) || $product['icon2']!=0) { ?>
	<div class="iconprod <?php echo $product['position2'];?>"><i class="fa <?php echo $product['icon2'];?>" aria-hidden="true"></i></div>
	<?php } ?>
		<?php if ($product['thumb'] || $product['custom_thumb']) { ?>
			<?php if($product['newprice'] > 0 && $theme_options->get( 'display_text_sale' ) != '0') { ?>
				<?php $text_sale = 'Sale';
				if($theme_options->get( 'sale_text', $config->get( 'config_language_id' ) ) != '') {
					$text_sale = $theme_options->get( 'sale_text', $config->get( 'config_language_id' ) );
				} ?>
				<?php if($theme_options->get( 'type_sale' ) == '1') { ?>
					<?php
					$roznica_ceny = $product['price']-$product['newprice'];
					$procent = ($roznica_ceny*100)/$product['price']; ?>
					<div class="sale">-<?php echo round($procent); ?>%</div>
				<?php } else { ?>
					<div class="sale"><?php echo $text_sale; ?></div>
				<?php } ?>
			 <?php } elseif($theme_options->get( 'display_text_new' ) != '0' && $theme_options->isLatestProduct( $product['product_id'] )) { ?>
				 <div class="new"><?php if($theme_options->get( 'new_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'new_text', $config->get( 'config_language_id' ) ); } else { echo 'New'; } ?></div>
			 <?php } ?>

			<div class="image <?php if($theme_options->get( 'product_image_effect' ) == '1') { echo 'image-swap-effect'; } ?>">
				<a href="<?php echo $product['href']; ?>">

                <?php if($product['special']>0){ ?>
                      <?php $str= str_replace(' ','',$product['price']);

                                   $product_old= $str;

                                   $product_new=str_replace(' ','',$product['special']);

                             ?>
                            <span style="font-size: 16px;background: #c31b1b;color: #ffffff;padding: 6px;border-radius: 50%;position: absolute;margin: 2px;font-weight: 600;"><?php echo 100-Round(($product_new/$product_old)*100); ?>%</span>
                <?php } ?>
                 <?php if(!empty($product['complect_price_special'])) { ?>
         
                    <span style="font-size: 15px;background: #bd3010;color: #ffffff;padding: 6px;border-radius: 50%;position: absolute;margin: 2px;font-weight: 600;"><?php echo  $markup; ?> %</span>
             <?php   } ?>

                    <?php if($theme_options->get( 'product_image_effect' ) == '1') {
						$nthumb = str_replace(' ', "%20", ($product['custom_thumb'] ? $product['custom_thumb'] : $product['thumb']));
						$nthumb = str_replace(HTTP_SERVER, "", $nthumb);
						$image_size = getimagesize($nthumb);
						$image_swap = $theme_options->productImageSwap($product['product_id'], $image_size[0], $image_size[1]);
						if($image_swap != '') echo '<img src="' . $image_swap . '" alt="' . $product['name'] . '" class="swap-image" />';
					} ?>
					<?php if($theme_options->get( 'lazy_loading_images' ) != '0') { ?>
					<img src="image/catalog/blank.gif" data-echo="<?php echo ($product['custom_thumb'] ? $product['custom_thumb'] : $product['thumb']); ?>?d" alt="<?php echo $product['name']; ?>" class="<?php if($theme_options->get( 'product_image_effect' ) == '2') { echo 'zoom-image-effect'; } ?>" />
					<?php } else { ?>
					<img src="<?php echo ($product['custom_thumb'] ? $product['custom_thumb'] : $product['thumb'].'?v'.$v); ?>" alt="<?php echo $product['name']; ?>" class="<?php if($theme_options->get( 'product_image_effect' ) == '2') { echo 'zoom-image-effect'; } ?>" />
					<?php } ?>
				</a>
			</div>
		<?php } else { ?>
			<div class="image">
				<a href="<?php echo $product['href']; ?>"><img src="image/no_image.jpg" alt="<?php echo $product['name']; ?>" style="height: 200px;" <?php if($theme_options->get( 'product_image_effect' ) == '2') { echo 'class="zoom-image-effect"'; } ?> /></a>
			</div>
		<?php } ?>
	</div>
	<div class="right">

<!---Комплекты начало--->
	     <?php if(!empty($product['colors'])) { ?>
	     <div style="padding-left: 10px; padding-right: 10px;">
	     	<?php
}else{
	?>
	<div style="padding-left: 10px; padding-right: 10px;">
	<?php
}
	     	?>
	    <?php if(!empty($product['colors'])) { ?>
        <div class="color_list">
            <div class="color_heading" style="color: #757575; margin-bottom: 10px;">Комплект <i class="fa fa-chevron-circle-down"></i></div>
	  <div class="color_items">
          <?php foreach($product['colors'] as $color){ ?>
            <div class="color-item">
              <a href="<?php echo $color['href']?>" rel="<?php echo $color['thumb'] ?>" default-image="<?php echo ($product['custom_thumb'] ? $product['custom_thumb'] : $product['thumb']); ?>">
                <?php if($color['tpl'] == 'color'){ ?>
                  <div class="color_block" style="background:<?php echo $color['color'] ?>;"></div>
                <?php } elseif($color['tpl'] == 'photos'){ ?>
                  <div class="image_block"><img style="padding: 2px; border: 1px solid #ebebeb;" src="<?php echo $color['ico_photo'].'?v'.$v; ?>" /></div>
		          <!---<div class="color_name"><?php echo $color['product_name'] ?></div><div class="color_price"><?php echo "от " . $color['product_price'] . " ₽"; ?></div>--->
                <?php } else { ?>
                  <div class="image_block"><img src="<?php echo $color['ico_color'] ?>" /></div>
                <?php } ?>
              </a>
            </div>
          <?php } ?>
	  </div>
        </div>
      <?php } else{ ?>

<div class="color_list"><div class="color_heading" style="color: #757575; margin-bottom: 10px;">&nbsp;</div><div class="color_items">
	&nbsp;
</div>
</div>
      <?php
  		}
  		?>
      <!---Комплекты конец--->

		<a href="<?php echo $product['href']; ?>" class="name_prod" style="color:#757575; display: inline-block; height: 45px;"><?php echo $product['name']; ?></a>


	</div>


		<?php if($theme_options->get( 'product_grid_type' ) == '7') { ?>
		<?php $product_detail = $theme_options->getDataProduct( $product['product_id'] ); ?>
		<div class="brand"><?php echo $product_detail['manufacturer']; ?></div>
		<?php } ?>


		<div style="background: #ecebeb; padding:5px 0px 4px;position:relative; top:5px;">
			<div class="mob_left" style="width: 45%; float: left;">

				<?php if($product['price']) { ?>
					<div class="price" style="color: #212121; position: relative; font-weight: 600;font-size: 15px;">
						<?php if ($product['special'] > 0 ) { ?>
                        <span style="display:block;    " class="price-new">от <?php echo $product['special'] . " руб."; ?></span>
						<?php echo "<span style='text-decoration: line-through;color:#c31b1b' class=\"price-old\">" ."от ". $product['price'] . " руб.</span>"; ?>

						<?php } else { ?>

                          <?php if(!empty($product['complect_price_special'])) { ?>
                           <span style="display: block;font-size: 15px;font-weight:750;color: #212121;">
						<?php echo "от " . $product['complect_price_special'] . " руб."; ?>
                        </span>

                        <span style="display: block;font-size: 14px;font-weight:600; color: red; text-decoration: line-through;" >

                        <?php echo "от " .$product['complect_price']. " руб."; ?>
                        </span>
                        <?php } else{ ?>
                        	<?php echo "от " . $product['price']  . " руб."; ?>

                        <?php } ?>
						<?php } ?>
					</div>
				<?php
				}
				?>
			</div>
			<div class="mob_right" style="width: 55%; float: right">
				<ul style="list-style: none; padding: 0px; margin: 0px; position: relative; top: 3px;">

					<?php if($theme_options->get( 'display_add_to_compare' ) != '0') { ?>
					<li style="display: inline-block;"><a style="cursor: pointer;" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-toggle="tooltip" data-original-title="<?php if($theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_compare_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to compare'; } ?>"><img src="/catalog/view/theme/marcjewelry/img/1215.png" /></a></li>
					<?php } ?>

					<?php if($theme_options->get( 'display_add_to_wishlist' ) != '0') { ?>
					<li style="display: inline-block; margin: 0px 5px;"><a style="cursor: pointer;" onclick="wishlist.add('<?php echo $product['product_id']; ?>');yaCounter47357799.reachGoal('zakladka'); return true;" data-toggle="tooltip" data-original-title="<?php if($theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ) != '') { echo $theme_options->get( 'add_to_wishlist_text', $config->get( 'config_language_id' ) ); } else { echo 'Add to wishlist'; } ?>"><img src="/catalog/view/theme/marcjewelry/img/1218.png" /></a></li>
					<?php } ?>



		          <?php if($theme_options->get( 'display_add_to_cart' ) != '0') { ?>
			               <?php $enquiry = false; if($config->get( 'product_blocks_module' ) != '') { $enquiry = $theme_options->productIsEnquiry($product['product_id']); }
			               if(is_array($enquiry)) { ?>
			               <li style="display: inline-block;"><a style="cursor: pointer;" href="javascript:openPopup('<?php echo $enquiry['popup_module']; ?>', '<?php echo $product['product_id']; ?>')" data-toggle="tooltip" data-original-title="<?php echo $enquiry['block_name']; ?>"><img src="/catalog/view/theme/marcjewelry/img/buy.png" /></a></li>
			               <?php } else { ?>
			               <li style="display: inline-block;"><a style="cursor: pointer;" onclick="cart.add('<?php echo $product['product_id']; ?>');yaCounter47357799.reachGoal('v-korzinu'); return true;" data-toggle="tooltip" data-original-title="<?php echo $button_cart; ?>"><img src="/catalog/view/theme/marcjewelry/img/1216.png" /></a></li>
			               <?php } ?>
			          <?php } ?>

			          <?php if($theme_options->get( 'quick_view' ) == 1) { ?>
			          <li class="quickview"  style="display: inline-block;"><a style="cursor: pointer;" href="index.php?route=product/quickview&amp;product_id=<?php echo $product['product_id']; ?>" data-toggle="tooltip" data-original-title="<?php if($theme_options->get( 'quickview_text', $config->get( 'config_language_id' ) ) != '') { echo html_entity_decode($theme_options->get( 'quickview_text', $config->get( 'config_language_id' ) )); } else { echo 'Quickview'; } ?>"><i class="fa fa-search"></i></a></li>
			          <?php } ?>


				</ul>
			</div>
			<div style="clear: both;"></div>
		</div>

	</div>
</div>
