function open_filter(elem) {
    if ($('#mfilter-box-1').hasClass('is-open')) {
        $('#mfilter-box-1').removeClass('is-open');
        $("#button-filter-open").removeClass('filter_change_pos');
        $("#button-filter-open").html('<i class="fa fa-filter"></i> Фильтры');
    } else {
        $('#mfilter-box-1').addClass('is-open');
        $("#button-filter-open").addClass('filter_change_pos');
        $("#button-filter-open").html('Показать результаты');
    }
}
document.addEventListener("DOMContentLoaded", function(){
	jQuery(function(f){
		f(window).scroll(function(){
			if (f(this).scrollTop() > 100) {
				$('#button-filter-open').addClass('filter_change_pos');
			} else {
				$('#button-filter-open').removeClass('filter_change_pos');
			}
		});
	});
});
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('#arrowup').fadeIn(300); //скорость исчезновения кнопки
        } else {
    $('#arrowup').fadeOut(200); //скорость появления кнопки
        }
    });
    $('#arrowup').click(function() {
        $('body,html').animate({
            scrollTop: 0
        }, 200); //скорость прокрутки
        return false;
    });});
