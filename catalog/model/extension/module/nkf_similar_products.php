<?php
/*
@author  nikifalex
@skype   logoffice1
@email    nikifalex@yandex.ru
@link https://opencartforum.com/files/file/4617-pohozhie-tovary/
*/

class ModelExtensionModuleNkfSimilarProducts extends Model {

    private function getProductAttributes($product_id,$ea) {
        $attributes=array();
        $sql1="
          SELECT *
          FROM " . DB_PREFIX . "product_attribute pa
          JOIN " . DB_PREFIX . "product p on p.product_id=pa.product_id
          WHERE p.product_id = '" . (int)$product_id . "'
          AND p.status = '1'
          AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "' 
          AND p.date_available <= NOW()
          ".$ea."
        ";
        $a=$this->db->query($sql1);
        foreach ($a->rows as $b) {
            $attributes[$b['attribute_id']]=$b['text'];
        }

        return $attributes;
    }

    private function getAttributeName($attribute_id) {
        $sql1="
          SELECT *
          FROM " . DB_PREFIX . "attribute_description ad
          WHERE ad.attribute_id = '" . (int)$attribute_id . "'
          AND ad.language_id = '" . (int)$this->config->get('config_language_id') . "' 
        ";
        $a=$this->db->query($sql1);
        if ($a->num_rows>0)
            return $a->row['name'];
        else
            return false;
    }


    public function getProductSimilar($data) {
        $this->load->model('catalog/product');

        $ea='';
        if (isset($data['excluded_attributes']) && count($data['excluded_attributes'])>0) {
            $ea=" AND pa.attribute_id not in (".implode(',',$data['excluded_attributes']).")";
        }


        $product_data = array();
        $sql2=array();
        $product_attributes=$this->getProductAttributes((int)$data['product_id'],$ea);
        foreach ($product_attributes as $i => $b) {
            $sql2[]=" (pa.attribute_id='".$i."' AND pa.text='".$this->db->escape($b)."') ";
        }

        $categories = array();
        if ($data['use_category']) {
            $sql1 = "
                  SELECT *
                  FROM " . DB_PREFIX . "product_to_category p2c
                  JOIN " . DB_PREFIX . "product p on p.product_id=p2c.product_id
                  WHERE p.product_id = '" . (int)$data['product_id'] . "'
                  AND p.status = '1'
                  AND p.date_available <= NOW()
            ";
            $a = $this->db->query($sql1);
            foreach ($a->rows as $b) {
                $categories[]=$b['category_id'];
            }
        }
        $manufacturer = '';
        if ($data['use_manufacturer']) {
            $sql1 = "
                  SELECT *
                  FROM " . DB_PREFIX . "product p
                  WHERE p.product_id = '" . (int)$data['product_id'] . "'
                  AND p.status = '1'
                  AND p.date_available <= NOW()
            ";
            $a = $this->db->query($sql1);
            if ($a->num_rows>0 && $a->row['manufacturer_id']>0)
                $manufacturer=$a->row['manufacturer_id'];
        }

        $j1='';
        $w1='';
        if (count($categories)>0) {
            $j1="JOIN " . DB_PREFIX . "product_to_category p2c on p.product_id=p2c.product_id";
            $w1="AND p2c.category_id in (".implode(',',$categories).")";
        }

        $w2='';
        if ($manufacturer!='') {
            $w2="AND p.manufacturer_id = ".(int)$manufacturer."";
        }


        if (count($sql2)>0) {
            $sql2=implode(' OR ',$sql2);
            $sql3="
                  SELECT p.product_id
                  FROM " . DB_PREFIX . "product_attribute pa
                  JOIN " . DB_PREFIX . "product p on p.product_id=pa.product_id
                  ".$j1."
                  WHERE  p.product_id <> '" . (int)$data['product_id'] . "'
                  AND p.status = '1'
                  AND p.date_available <= NOW()
                  AND p.quantity>0
                  AND p.price>0
                  AND pa.language_id = '" . (int)$this->config->get('config_language_id') . "'
                  AND (".$sql2.")
                  ".$w1."
                  ".$w2."
                  ".$ea."
                  group by product_id
                  order by count(*) DESC, p.price, p.product_id
                ";
            $query = $this->db->query($sql3);
            $cnt_products=0;
            foreach ($query->rows as $result) {
                $diff['attributes']=array();
                $product_attrubutes_p=$this->getProductAttributes($result['product_id'],$ea);
                $cnt_diff=0;
                foreach ($product_attrubutes_p as $i => $p) {
                    if (isset($product_attributes[$i]) && ($product_attributes[$i]!=$p)) {
                        $diff['attributes'][]= $this->getAttributeName($i).': '. $p;
                        $cnt_diff++;
                    }
                }
                $cnt_products++;
                if ($cnt_diff>$data['cnt_diff'] || $cnt_products>$data['limit'])
                    break;
                $product_data[$result['product_id']] = $this->model_catalog_product->getProduct($result['product_id']);
                $product_data[$result['product_id']]['diff']=$diff;

            }
        }
        return $product_data;
    }
}
