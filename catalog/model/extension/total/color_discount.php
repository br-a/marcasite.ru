<?php
class ModelExtensionTotalColorDiscount extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/color_discount');

		$discount_total = 0;

		$products = $this->cart->getProducts();

		$sub_total = 0;

		$color_kits = array();

		$color_kits_list = array();

		foreach($products as $product){
			$colors_query = $this->db->query("SELECT ck.tpl, ckg.color_kit_id as color_kit_id, ck.name FROM `" . DB_PREFIX . "color_kit_group` ckg LEFT JOIN " . DB_PREFIX . "color_kits ck ON (ck.color_kit_id = ckg.color_kit_id) WHERE ckg.product_id = '" . (int)$product['product_id'] . "' AND ck.status <> 0");
			if($colors_query->num_rows){
				if(!in_array($colors_query->row['color_kit_id'], $color_kits_list)){		
					
						$colors_group_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "color_kit_group` ckg LEFT JOIN " . DB_PREFIX . "color_options co ON (co.option_id = ckg.option_id) LEFT JOIN " . DB_PREFIX . "color_options_description cod ON (ckg.option_id = cod.option_id AND cod.language_id = '" .(int)$this->config->get('config_language_id'). "') WHERE ckg.color_kit_id = '" . (int)$colors_query->row['color_kit_id'] . "' ORDER BY co.sort");

						foreach($colors_group_query->rows as $colors_group){
							if(!isset($color_kits[$colors_group['color_kit_id']])){
								$color_kits[$colors_group['color_kit_id']] = array();
							}

							if(!($color_kit = array_filter($color_kits[$colors_group['color_kit_id']], function($color_kit) use($colors_group){ return $color_kit['product_id'] == $colors_group['product_id']; }))) {
								$color_kits[$colors_group['color_kit_id']][] = array(
									'product_id' => $colors_group['product_id'],
									'name'=> $colors_query->row['name'],
									'count' => 1
								);
							}else{
								$color_kits[$colors_group['color_kit_id']][key($color_kit)]['count']++;
							}

							/*
							if(!isset($color_kits[$colors_group['color_kit_id']])){
								$color_kits[$colors_group['color_kit_id']] = array();
							}

							if(!($color_kit = array_filter($color_kits[$colors_group['color_kit_id']], function($color_kit) use($colors_group){ return $color_kit['product_id'] == $colors_group['product_id']; }))) {

								if($colors_group['product_id'] == $product['product_id']){
									$color_kits[$colors_group['color_kit_id']][] = array(
										'status' => true,
										'product_id' => $colors_group['product_id'],
										'use' => $product['quantity']
									);
								}else{
									$color_kits[$colors_group['color_kit_id']][] = array(
										'status' => false,
										'product_id' => $colors_group['product_id'],
										'use' => 0
									);
								}
							}else{
								if($colors_group['product_id'] == $product['product_id']){
									$color_kits[$colors_group['color_kit_id']][key($color_kit)]['status'] = true;

									$color_kits[$colors_group['color_kit_id']][key($color_kit)]['use'];
								}
								
							}
							*/

						}
					

					$color_kits_list[] = $colors_query->row['color_kit_id'];
				}
			}
		}

		/*
		$color_kits = array_filter($color_kits, function($color_kit){ 
			foreach($color_kit as $item){
				if(!$item['status']){
					return false;
				}
			} 

			return true;
		});
		*/
		
		do {
			$kit_items = array();
			$pattern_keys = array();

			$status = false;

			foreach($color_kits as $color_kit){
				$kit_items = array();
				$pattern_keys = array();

				foreach($color_kit as $item){
					$kit_items[] = $item['product_id'];

					$product_ids = array();

					foreach($products as $key => $product){
						if($product['product_id'] == $item['product_id'] && $product['quantity'] >= $item['count']){
							if(!in_array($item['product_id'], $product_ids)){
								$pattern_keys[] = array(
									'key' => $key,
									'count' => $item['count'],
									'name' => $item['name']
								);

								$product_ids[] = $item['product_id'];
							}	
						}
					}
				}

				if(count($kit_items) == count($pattern_keys)){
					$status = true;

					break;
				}
			}
			if($status){
				foreach($pattern_keys as $pattern_key){
					$discount_total += ($products[$pattern_key['key']]['price'] * $pattern_key['count']) * ($this->config->get('color_discount_markup') / 100); 

					if($products[$pattern_key['key']]['quantity'] == $pattern_key['count']){
						unset($products[$pattern_key['key']]);
					}else{
						$products[$pattern_key['key']]['quantity'] -= $pattern_key['count'];
					}
				}
			}
		} while ($status);

		if($discount_total){
			$total['totals'][] = array(
				'code'       => 'color_discount',
				'title'      => $this->language->get('text_color_discount'),
				'value'      => -$discount_total,
				'sort_order' => $this->config->get('color_discount_sort_order')
			);

			$total['total'] -= $discount_total;
		}

		foreach($color_kits as $color_kit){
			

			/*
			if($pattern_keys){
				foreach($pattern_keys as $pattern_key){
					$discount_total += ($products[$pattern_key['key']]['price'] * $pattern_key['count']) * ($this->config->get('color_discount_markup') / 100); 

					if($pattern_key['count'] == $products[$pattern_key['key']]['quantity']){
						unset($products[$pattern_key['key']]);
					}else{
						$products[$pattern_key['key']]['quantity'] -= $pattern_key['count'];
					}
				}
			}
			*/
		}

		

		/*
		foreach($products as $product){
			if(array_filter($color_kits, function($color_kit) use ($product) { 
				foreach($color_kit as $item){
					if($item['product_id'] == $product['product_id']) {
						return true;
					}
				} 

				return false;
			})) {
				$discount_total += $product['total'] * ($this->config->get('color_discount_markup') / 100);
			}
		}
		*/


		
		
	}
}