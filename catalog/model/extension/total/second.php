<?php
class ModelExtensionTotalSecond extends Model {
	public function getTotal($total) {
			$this->load->language('extension/total/second');

			$discount_total = 0;

			$products = $this->cart->getProducts();

			usort($products, function($a, $b){
				return $a['total'] > $b['total'];
			});

			$products = array_slice($products, 0, intdiv(count($products), 2));

			foreach ($products as $product) {
				$discount_total += $product['total'];
			}

			if($discount_total){


				

				$total['totals'][] = array(
					'code'       => 'second',
					'title'      => sprintf($this->language->get('text_second')),
					'value'      => -$discount_total,
					'sort_order' => $this->config->get('second_sort_order')
				);

				$total['total'] -= $discount_total;

			}
				

	}
}
