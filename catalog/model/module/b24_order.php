<?php
class ModelModuleB24Order extends Model{
	const TABLE_NAME = 'b24_order';
	const MANAGER = 10;
	const CONFIG_CUSTOMER_GROUP_COEF =  'customer_group_coef';
	const RETAIL_CUSTOMER_GROUP = 1;
	const MEASURE_PIECE = 796;
	const WITHOUT_SIZE = 0;

	public $UF_DEAL_PAYMENT_METHOD = 'UF_CRM_1527844108';
	public $UF_LEAD_PAYMENT_METHOD = 'UF_CRM_1527844072';
	
	public $UF_DEAL_DISCOUNT_TYPE = 'UF_CRM_1527844360';
	public $UF_LEAD_DISCOUNT_TYPE = 'UF_CRM_1524036962';
	
	
	
	public function __construct( $registry ){ 
		parent::__construct($registry);
		
        $this->load->model('setting/setting');

        $b24_setting = $this->model_setting_setting->getSetting('b24');
		$this->b24->setFields($b24_setting);
	}

	protected function initEditOrder($order_id){	
        $this->load->model('module/b24_customer');
        $this->load->model('checkout/order');
		
        $order = $this->model_checkout_order->getOrder($order_id);
        $customerName = $order['firstname'];

        $b24Contact = $this->getContactFromDB($order['customer_id']);

        if (empty($b24Contact)) {
            $b24Contact = $this->getContactFromB24($order['email']);
        }

        $managerId = isset($b24Contact['ASSIGNED_BY_ID']) ? $b24Contact['ASSIGNED_BY_ID'] : $this->config->get('b24_manager')['manager'];
        $b24ContactId = !empty($b24Contact['ID']) ? $b24Contact['ID'] : 0;
		
        return [
            'fields' => [
                'CONTACT_ID' => $b24ContactId,
                'ASSIGNED_BY_ID' => $managerId,
                'NAME' => $customerName
            ]
        ];
	}

	public function addToDB( $order_id, $b24Id, $type, array $fields = []){
		if( empty($b24Id) || (int) $order_id <= 0 ){
			trigger_error('Empty $b24Id or $order_id '
				.". Order ID : ". print_r($order_id, 1) .". ID B24: ".  print_r($b24Id, 1),
				E_USER_WARNING);
		}
		$fieldsToAdd = ['oc_order_id' => $order_id, 'b24_order_id' => $b24Id, 'type' => $type];
		$this->insertToDB($fieldsToAdd);
	}

	public function addOrder($order_id) {
		$this->load->model('account/order');
        $this->load->model('module/b24_customer');
		
		$order = $this->getOrder($order_id);
		$siteName = html_entity_decode($this->config->get('config_name'),ENT_QUOTES, 'UTF-8');

		$dataToAdd = $this->prepareDataToB24($order_id);

		$dataToAdd = array_merge($dataToAdd, ['params' => ['REGISTER_SONET_EVENT' => 'Y']]);

		$productToAdd = $this->prepareProductToB24($order_id);

		$type = 2;
			$typeApi = 'deal';
            $typeApiRu = 'сделка';
            $typeApiRu2 = 'сделки';
            $typeApiUrl = '/crm/deal/details/';
            $managerId = $order['customer_group_id'] != 0 ?  $this->config->get('b24_manager')['manager'][$order['customer_group_id']] : 1;
            $text = 'На сайте ' . $siteName . ' полученa новая <a href="{typeApiUrl}{b24Id}/">{typeApiRu}</a> от {dataToAdd[fields][NAME]}' . '. Перейдите к просмотру новой {typeApiRu2} <a href="{typeApiUrl}{b24Id}/">{b24Id}</a>';
		
		$params = [
			'type' => 'batch',
			'params' => [
			    'cmd' => [
                    'order_add' => 'crm.' . $typeApi . '.add?' . http_build_query($dataToAdd),
                    //'product_add' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
			]
		];

		$result = $this->b24->callHook($params);
		$b24Id = $result['result']['result']['order_add'];

		if (!empty($result['result']['result_error'])) {
			$this->log->write(print_r($result['result_error'],true));
		}
		$this->addToDB($order_id, $b24Id, $type);

        $findSearch = ['{typeApiUrl}', '{b24Id}', '{typeApiRu}', '{dataToAdd[fields][NAME]}', '{typeApiRu2}'];
        $findReplace = [$typeApiUrl, $b24Id, $typeApiRu, $dataToAdd['fields']['NAME'], $typeApiRu2];

        $message = str_replace($findSearch, $findReplace, $text);

        $params2 = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'im_notify' => 'im.notify?' . http_build_query([
                        'to' => $managerId,
                        'message' => $message
                    ]),
                ],
            ],
        ];
        $this->b24->callHook($params2);
	}

	public function editOrderStatus($order_id, $order_status_id) {
		$dataToB24 = $this->initEditOrder($order_id);	 
		$typeApi = ($dataToB24['fields']['CONTACT_ID'] == 0) ? 'lead' : 'deal';
		
		$b24OrderStatusById = $this->getStatusById($order_status_id);
			
		if ($typeApi == 'lead') {	
			$b24OrderStatusId = !empty($b24OrderStatusById['b24_status_id']) ? $b24OrderStatusById['b24_status_id'] : 'NEW';
			$dataToB24['fields']['STATUS_ID'] =  $b24OrderStatusId;
		} elseif ($typeApi == 'deal') {	
			$b24OrderStatusId = !empty($b24OrderStatusById['b24_stage_id']) ? $b24OrderStatusById['b24_stage_id'] : 'NEW';
			$dataToB24['fields']['STAGE_ID'] =  $b24OrderStatusId;
		}
	 
		$b24OrderById = $this->getById($order_id);
		$b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;		

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id='. $b24OrderId . http_build_query($dataToB24),
                ]
            ]
        ];

        $result = $this->b24->callHook($params);
	}
	
	public function editOrder($order_id) {
        $this->load->model('module/b24_customer');
        $this->load->model('checkout/order'); 
		
        $b24OrderById = $this->getById($order_id);
        $b24OrderId = !empty($b24OrderById['b24_order_id']) ? $b24OrderById['b24_order_id'] : 0;
		
		$order = $this->model_checkout_order->getOrder($order_id); 

		$managerId = $order['customer_group_id'] != 0 ?  $this->config->get('b24_manager')['manager'][$order['customer_group_id']] : 1;
		$status_id = $this->getStatusById($order['order_status_id'],$order['customer_id']);
        $typeApi = 'deal';


        $dataToB24 = [
            'fields' => [
                'ASSIGNED_BY_ID' => $managerId,
				'STAGE_ID' => $status_id,
            ]
        ];
		$this->log->write(print_r($dataToB24,true)); 
		 

        $params = [
            'type' => 'batch',
            'params' => [
                'cmd' => [
                    'order_update' => 'crm.' . $typeApi . '.update?id='. $b24OrderId . http_build_query($dataToB24),
                    //'product_update' => 'crm.' . $typeApi . '.productrows.set?id=$result[order_add]&' . http_build_query($productToAdd)
                ]
            ]
        ];

        $result = $this->b24->callHook($params);


        if (!empty($result['result']['result_error'])) {
            trigger_error('Ошибка при обновлении заказа в Б24 ' . print_r($result['result_error'], 1), E_USER_WARNING);
        }
	}

	public function prepareProductToB24( $order_id ){
		$this->load->model('account/order');

		$productToAdd = [];
		$productRows = $this->model_account_order->getOrderProducts($order_id);
		foreach ( $productRows as $product )
		{
			$productId = $product['product_id'];

			$orderOption = $this->model_account_order->getOrderOptions($order_id, $product['order_product_id']);
			$productOptions = '';
			foreach ($orderOption as $option) {
				$productOptions .= ' | ' .  $option['name'] . ': ' . $option['value'];
			}

			$taxRate = ($product['tax']/$product['price']) * 100;
			$price = $product['price'] + $product['tax'];
			$productName = html_entity_decode(trim($product['name'] . $productOptions));
			$productToAdd['rows'][] = [
				//'PRODUCT_ID' => $b24Product['b24_product_id'],
				'PRODUCT_NAME' => $productName,
				'PRICE' => $price,
				//'DISCOUNT_RATE' => $newProduct['discount_rate'],
				//'DISCOUNT_SUM' => $newProduct['discount_sum'],
				//'DISCOUNT_TYPE_ID' => 2,
				'TAX_RATE' => $taxRate,
				'TAX_INCLUDED' => 'N',
				'QUANTITY' => $product['quantity'],
				'MEASURE_CODE' => self::MEASURE_PIECE, // piece
			];
		}

		$productToAdd = $this->addDeliveryCost($order_id, $productToAdd);

		return $productToAdd;
	}

	public function addDeliveryCost($order_id, array $productToAdd){
		$orderTotalList = $this->model_account_order->getOrderTotals($order_id);

		foreach ($orderTotalList as $orderTotal) {
			if ($orderTotal['code'] == 'shipping') {
				$productToAdd['rows'][] = [
					'PRODUCT_ID' => 0,
					'PRICE' => $orderTotal['value'],
					'PRODUCT_NAME' => $orderTotal['title'],
					'QUANTITY' => 1,
					'MEASURE_CODE' => self::MEASURE_PIECE, // piece
				];
			}
		}

		return $productToAdd;
	}
	
	public function prepareDataToB24($order_id){
		$this->load->model('account/order');
		$this->load->model('checkout/order');
		$this->load->model('module/b24_customer');
		$this->load->model('setting/setting');

		$order = $this->getOrder($order_id);
        $orderName = html_entity_decode($order['store_name'],ENT_QUOTES, 'UTF-8') . '. Заказ № ' . $order['order_id'];
		$orderComment = $order['comment'];
		$customerLastname = $order['lastname'];
		$customerName = $order['firstname'];
		$customerEmail = $order['email'];
		$customerPhone = preg_replace("/[^0-9]/", '', $order['telephone']);
		$customerId = $order['customer_id'];
		
		$roistat = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : 0;

		if ($this->customer->isLogged()) {
			$b24Contact = $this->getContactFromDB($order['customer_id']);
            $b24ContactId = $b24Contact['ID'];
		} else {
			$b24ContactByPhone = $this->getContactFromDBByPhone($customerPhone);
			$b24ContactId = isset($b24ContactByPhone['ID']) ? $b24ContactByPhone['ID'] : 0;
			
			if (!$b24ContactId){
				$b24Contact = $this->getContactFromB24($order['email']);
				$b24ContactId = 0;				
			}
		}
		$managerId = $order['customer_group_id'] != 0 ?  $this->config->get('b24_manager')['manager'][$order['customer_group_id']] : 1;
		
		$status_id = $this->getStatusById($order['order_status_id'],$order['customer_id']);

		$dataToB24 = [];
		$dataToB24 = [
		    'fields' => [
                'TITLE' => $orderName,
				'STAGE_ID' => $status_id,
                'CURRENCY_ID' => $this->config->get('config_currency'),
                'SOURCE_ID' => !empty($this->config->get('b24_order')['source']) ? $this->config->get('b24_order')['source'] : 'WEB',  
				'OPPORTUNITY' => $order['total'],				
				'OPENED' => isset($this->config->get('b24_manager')['order_open']) ? $this->config->get('b24_manager')['order_open'] : 'N',
				'TYPE_ID' => isset($this->config->get('b24_order')['dealtype']) ? $this->config->get('b24_order')['dealtype'] : 'SALE',
				'CATEGORY_ID' => isset($this->config->get('b24_order')['dealcategory']) && $this->config->get('b24_order')['dealcategory'] != 0 ? $this->config->get('b24_order')['dealcategory'] : '0',
                'ASSIGNED_BY_ID' => $managerId,
                'CONTACT_ID' => $b24ContactId,
                'COMMENTS' => $orderComment .'<br><hr>Имя клиента: ' .$customerName .'<br>Фамилия клиента: ' .$customerLastname 
				.'<br><hr>Адрес клиента: ' .$order['payment_country'].','. $order['payment_zone'].','. $order['payment_postcode'] .','. $order['payment_city'] .',<br>Телефон: '. $customerPhone .'<br>Email: '.$customerEmail .'<br><hr>Способ оплаты: '.$order['payment_method'].'<br><hr><a href="https://marcasite.opencartdev.ru/admin/index.php?route=sale/order/info&token=1t6oCFzJm7qeGFUp5wgIO1A75ET76Vxl&order_id='.$order_id.'" target="_blank">Посмотреть заказ на сайте</a>',
                'UTM_SOURCE' => isset($_COOKIE['utm_source']) ? $_COOKIE['utm_source'] : '',
				'UTM_MEDIUM' => isset($_COOKIE['utm_medium']) ? $_COOKIE['utm_medium'] : '',
				'UTM_CAMPAIGN' => isset($_COOKIE['utm_campaign']) ? $_COOKIE['utm_campaign'] : '',
				'UTM_CONTENT' => isset($_COOKIE['utm_content']) ? $_COOKIE['utm_content'] : '',
				'UTM_TERM' => isset($_COOKIE['utm_term']) ? $_COOKIE['utm_term'] : '',

				'STATUS_ID' => $status_id,
                'NAME' => $customerName,
                'LAST_NAME' => $customerLastname,
                'ADDRESS' => $order['payment_address_1'],
                'ADDRESS_COUNTRY' => $order['payment_country'],
                'ADDRESS_PROVINCE' => $order['payment_zone'],
                'ADDRESS_CITY' => $order['payment_city'],
                'ADDRESS_POSTAL_CODE' => $order['payment_postcode'],
                'PHONE' => [['VALUE' => $customerPhone, "VALUE_TYPE" => "WORK"]],
                'EMAIL' => [['VALUE' => $customerEmail, "VALUE_TYPE" => "WORK"]],
                'UF_CRM_1518879521' => $roistat,
            ]
			];
		return $dataToB24;
		
	} 

	public function getContactFromDB($customerId){
		if (abs($customerId) <= 0) {
		    return [];
		}

		$b24Row = $this->model_module_b24_customer->getById($customerId);
		$b24Contact = json_decode(isset($b24Row['b24_contact_field']) ? $b24Row['b24_contact_field'] : 0, 1);

		return $b24Contact;
	}
	
	public function getContactFromDBByPhone($phone){
		$result = array();
		
		$query = $this->db->query("SELECT * FROM b24_customer WHERE phone = '" . $phone . "'");
		
		if ($query->num_rows){
			$result = json_decode($query->row['b24_contact_field'], 1);
		}

		return $result;
	}

	public function getContactFromB24($contactEmail){
        $B24ContactList = $this->getB24ContactList(['EMAIL' => $contactEmail]);
        $b24Contact = isset($B24ContactList[0]) ? $B24ContactList[0] : [];

		return $b24Contact;
	}

	public function getDiscountCoef( $customerGroupId ){
		if(abs($customerGroupId) <= 0 ){ $customerGroupId = self::RETAIL_CUSTOMER_GROUP;}
	
		$groupCoef = $this->getCustomerGroupCoef();
		$discount = $groupCoef[$customerGroupId];
	
		return $discount;
	}

	public function getCustomerGroupName( $customerGroupId ){
		if(abs($customerGroupId) <= 0 ){ $customerGroupId = self::RETAIL_CUSTOMER_GROUP;}
		$this->load->model('account/customer_group');
	
		$groupName = $this->model_account_customer_group->getCustomerGroup($customerGroupId)['name'];
	
		return $groupName;
	}

	public function getCustomerGroupCoef(){
		$sql = "SELECT * FROM b24_order_config WHERE name = '" .self::CONFIG_CUSTOMER_GROUP_COEF ."'";
		$query = $this->db->query($sql);
	
		return json_decode($query->row['value'], 1);
	}

	public function getB24ContactList($filter) {
		if (empty($filter)) {
		    trigger_error('Empty filter', E_USER_WARNING);
		}

		foreach ($filter as $value) {
			if (empty($value)) {
			    return false;
			}
		}

		$params = [
			'type' => 'crm.contact.list',
			'params' => [
				'filter' => $filter
			]
		];

		$result = $this->b24->callHook($params);

		return $result['result'];
	}

	public function insertToDB(array $fields){
		$db = $this->db;

		$sql = 'REPLACE INTO `b24_order` SET ' . $this->prepareFields($fields) . ';';
		$db->query($sql);

		$lastId = $this->db->getLastId();

		return $lastId;
	}

	public function prepareFields(array $fields){
		$sql = '';
		$index = 0;
		foreach ( $fields as $columnName => $value )
		{
			$glue = $index === 0 ? ' ' : ', ';
			$sql .= $glue . "`$columnName`" . ' = "' . $this->db->escape($value) . '"';
			$index++;
		}

		return $sql;
	}

	public function getStatusById($order_status_id, $customer_id) {		
		$status_map = $this->config->get('b24_status')['deal'];
		return array_search($order_status_id, $status_map); 
	}

	public function getById($order_id) {
		if (abs($order_id ) <= 0) {
		    trigger_error('ID must be integer', E_USER_WARNING);
		}

		$db = $this->db;
		$sql = 'SELECT * FROM `b24_order` WHERE oc_order_id = "' . $db->escape($order_id ) . '"';
		$query = $db->query($sql);

		return $query->row;
	}

	public function getList(array $filter){
		$db = $this->db;
		$where = ' WHERE ';
		$index = 0;
		foreach ($filter as $columnName => $value) {
			$glue = $index === 0 ? ' ' : ' AND ';
			$where .= $glue . $columnName. ' = "' . $db->escape($value) . '"';
			$index++;
		}

		$sql = 'SELECT * FROM ' . self::TABLE_NAME . $where . ';';
		$query = $db->query($sql);

		return $query->rows;
	}
	
	public function getOrder($order_id) {
		$order_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int)$order_id . "' AND customer_id = '" . (int)$this->customer->getId() . "' AND order_status_id > '0'");

		if ($order_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['payment_country_id'] . "'");

			if ($country_query->num_rows) {
				$payment_iso_code_2 = $country_query->row['iso_code_2'];
				$payment_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$payment_iso_code_2 = '';
				$payment_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['payment_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$payment_zone_code = $zone_query->row['code'];
			} else {
				$payment_zone_code = '';
			}

			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$order_query->row['shipping_country_id'] . "'");

			if ($country_query->num_rows) {
				$shipping_iso_code_2 = $country_query->row['iso_code_2'];
				$shipping_iso_code_3 = $country_query->row['iso_code_3'];
			} else {
				$shipping_iso_code_2 = '';
				$shipping_iso_code_3 = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$order_query->row['shipping_zone_id'] . "'");

			if ($zone_query->num_rows) {
				$shipping_zone_code = $zone_query->row['code'];
			} else {
				$shipping_zone_code = '';
			}

			return array(
				'order_id'                => $order_query->row['order_id'],
				'invoice_no'              => $order_query->row['invoice_no'],
				'invoice_prefix'          => $order_query->row['invoice_prefix'],
				'store_id'                => $order_query->row['store_id'],
				'store_name'              => $order_query->row['store_name'],
				'store_url'               => $order_query->row['store_url'],
				'customer_id'             => $order_query->row['customer_id'],
				'customer_group_id'       => $order_query->row['customer_group_id'],
				'firstname'               => $order_query->row['firstname'],
				'lastname'                => $order_query->row['lastname'],
				'email'                   => $order_query->row['email'],
				'telephone'               => $order_query->row['telephone'],
				'fax'                     => $order_query->row['fax'],
				'custom_field'            => $order_query->row['custom_field'],
				'payment_firstname'       => $order_query->row['payment_firstname'],
				'payment_lastname'        => $order_query->row['payment_lastname'],
				'payment_company'         => $order_query->row['payment_company'],
				'payment_address_1'       => $order_query->row['payment_address_1'],
				'payment_address_2'       => $order_query->row['payment_address_2'],
				'payment_city'            => $order_query->row['payment_city'],
				'payment_postcode'        => $order_query->row['payment_postcode'],
				'payment_country'         => $order_query->row['payment_country'],
				'payment_country_id'      => $order_query->row['payment_country_id'],
				'payment_zone'            => $order_query->row['payment_zone'],
				'payment_zone_id'         => $order_query->row['payment_zone_id'],
				'payment_address_format'  => $order_query->row['payment_address_format'],
				'payment_custom_field'    => $order_query->row['payment_custom_field'],
				'payment_method'          => $order_query->row['payment_method'],
				'payment_code'            => $order_query->row['payment_code'],
				'shipping_firstname'      => $order_query->row['shipping_firstname'],
				'shipping_lastname'       => $order_query->row['shipping_lastname'],
				'shipping_company'        => $order_query->row['shipping_company'],
				'shipping_address_1'      => $order_query->row['shipping_address_1'],
				'shipping_address_2'      => $order_query->row['shipping_address_2'],
				'shipping_city'           => $order_query->row['shipping_city'],
				'shipping_postcode'       => $order_query->row['shipping_postcode'],
				'shipping_country'        => $order_query->row['shipping_country'],
				'shipping_country_id'     => $order_query->row['shipping_country_id'],
				'shipping_zone'           => $order_query->row['shipping_zone'],
				'shipping_zone_id'        => $order_query->row['shipping_zone_id'],
				'shipping_address_format' => $order_query->row['shipping_address_format'],
				'shipping_custom_field'   => $order_query->row['shipping_custom_field'],
				'shipping_method'         => $order_query->row['shipping_method'],
				'shipping_code'           => $order_query->row['shipping_code'],
				'comment'                 => $order_query->row['comment'],
				'total'                   => $order_query->row['total'],
				'order_status_id'         => $order_query->row['order_status_id'],
				'affiliate_id'            => $order_query->row['affiliate_id'],
				'commission'              => $order_query->row['commission'],
				'marketing_id'            => $order_query->row['marketing_id'],
				'tracking'                => $order_query->row['tracking'],
				'language_id'             => $order_query->row['language_id'],
				'currency_id'             => $order_query->row['currency_id'],
				'currency_code'           => $order_query->row['currency_code'],
				'currency_value'          => $order_query->row['currency_value'],
				'ip'                      => $order_query->row['ip'],
				'forwarded_ip'            => $order_query->row['forwarded_ip'],
				'user_agent'              => $order_query->row['user_agent'],
				'accept_language'         => $order_query->row['accept_language'],
				'date_added'              => $order_query->row['date_added'],
				'date_modified'           => $order_query->row['date_modified'],
			);
		} else {
			return false;
		}
	}
}